/********************************************************************************
  Copyright (C) 2019 - 2023 by the lifex authors.

  This file is part of lifex.

  lifex is free software; you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  lifex is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with lifex.  If not, see <http://www.gnu.org/licenses/>.
********************************************************************************/

/**
 * @file
 *
 * @author Marco Fedele <marco.fedele@polimi.it>.
 * @author Ivan Fumagalli <ivan.fumagalli@polimi.it>.
 * @author Pasquale Claudio Africa <pasqualeclaudio.africa@polimi.it>.
 * @author Alberto Zingaro <alberto.zingaro@polimi.it>.
 * @author Michele Bucelli <michele.bucelli@polimi.it>.
 * @author Anna Peruso <anna.peruso@mail.polimi.it>.
 * @author Thomas Rimbot <thomas.rimbot@mail.polimi.it>.
 * @author Luca Crugnola <luca.crugnola@polimi.it>.
 */

#ifndef LIFEX_PHYSICS_FLUID_DYNAMICS_HPP_
#define LIFEX_PHYSICS_FLUID_DYNAMICS_HPP_

#include "core/source/core_model.hpp"

#include "core/source/geometry/control_volume.hpp"
#include "core/source/geometry/mesh_handler.hpp"

#include "core/source/io/csv_writer.hpp"
#include "core/source/io/vtk_function.hpp"

#include "core/source/numerics/bc_handler.hpp"
#include "core/source/numerics/linear_solver_handler.hpp"
#include "core/source/numerics/non_linear_solver_handler.hpp"
#include "core/source/numerics/preconditioner_handler.hpp"
#include "core/source/numerics/time_handler.hpp"

#include "source/helpers/ale_handler.hpp"
#include "source/helpers/fluid_dynamics_preconditioners.hpp"
#include "source/helpers/riis_handler.hpp"

#include <deal.II/base/symmetric_tensor.h>

#include <deal.II/fe/fe_values_extractors.h>

#include <deal.II/grid/grid_out.h>
#include <deal.II/grid/grid_tools.h>

#include <algorithm>
#include <deque>
#include <map>
#include <memory>
#include <set>
#include <string>
#include <tuple>
#include <utility>
#include <variant>
#include <vector>

namespace lifex
{
  /**
   * @brief Class to model the fluid-dynamics of a Newtonian fluid through the
   * Navier-Stokes equations.
   *
   * # Table of contents
   * - @ref model
   *   - @ref diffusive-term
   *   - @ref advection
   *   - @ref ale
   *   - @ref riis
   *   - @ref stabilization
   *   - @ref bcs
   *   - @ref backflowstabilization
   * - @ref algebraic
   *   - @ref preconditioning
   * - @ref postprocessing
   *   - @ref output-ale
   *   - @ref volumes-flows
   *   - @ref control-volumes
   *
   * @section model Fluid dynamics model
   *
   * The FluidDynamics class implements a solver for incompressible
   * Navier-Stokes equations. The user has the flexibility to choose among
   * different formulations, stabilization techniques and several other options
   * through the parameter file. The different available options are detailed in
   * this page.
   *
   * In the most general form, the discrete Galerkin formulation of
   * Navier-Stokes equations, as implemented by this class, is as follows:
   * @f[
   * \begin{alignedat}{2}
   * & \text{Given } \mathbf{u}^{n}, \text{ find }(\mathbf{u}^{n + 1}, p^{n +
   1})
   \in V_h \times Q_h \text{ such that } \forall (\mathbf{v}, q) \in V_h \times
   Q_h \\
   * & \\
   * & \left ( \rho \frac{\alpha_{\text{BDF}\sigma} \mathbf{u}^{n+1} -
   \mathbf{u}^n_{\text{BDF}\sigma}}{\Delta t} , \mathbf{v} \right )_{\Omega}
   * + {\color{red}{\mathcal{D}(\mathbf{u}^{n+1},\mathbf v;
   \mu)}}
   * + \left ( \rho
   {\color{orange}{{\color{orange}{{\color{orange}{\mathbf{u}^{n+1}_{*}}}}}}}
   \cdot \nabla \mathbf{u}^{n+1} , \mathbf{v} \right )_\Omega - (p^{n+1}, \nabla
   \cdot \mathbf{v})_\Omega
   * + ( \nabla \cdot \mathbf{u}^{n+1}, q)_\Omega
   * + {\color{green}{\mathcal{R}(\mathbf u^{n+1}, \mathbf v)}}
   * + {\color{blue}{\mathcal{S}(\mathbf u^{n+1}, {\color{orange}{\mathbf
   * u^{n+1}_*}}, p^{n+1}, p^{n+1}_{\text{EXT},\sigma}; \mathbf v, q)}}
   * = (\mathbf{f}^{n+1}, \mathbf{v})_\Omega
   * + (\mathbf{h}^{n+1}, \mathbf{v})_{\Gamma_N},
   * \quad  \\
   * \end{alignedat}
   * @f]
   * In the above equation:
   * - @f$\alpha_{\text{BDF}\sigma}@f$ and @f$\mathbf{u}^n_{\text{BDF}\sigma}@f$
   * are terms related to the time derivative approximation with a BDF scheme,
   * as implemented by @ref utils::BDFHandler;
   * - @f${\color{red}{\mathcal{D}(\mathbf{u}^{n+1},\mathbf v; \mu)}}@f$ is the diffusion term (see @ref diffusive-term);
   * - @f${\color{orange}{\mathbf u^{n+1}_*}}@f$ is the advection velocity (see @ref advection);
   * - @f${\color{green}{\mathcal{R}(\mathbf u^{n+1}, \mathbf v)}}@f$ are terms
   * related to the RIIS model used to deal with immersed surfaces (see @ref riis);
   * - @f${\color{blue}{\mathcal{S}(\mathbf u^{n+1}, {\color{orange}{\mathbf
   * u^{n+1}_*}}, p^{n+1}, p^{n+1}_{\text{EXT},\sigma}; \mathbf v, q)}}@f$ are the stabilization terms (see @ref stabilization);
   * - @f$\mathbf f^{n+1}@f$ and @f$\mathbf h^{n+1}@f$ are a forcing term and
   * Neumann boundary condition on @f$\Gamma_N@f$ respectively.
   *
   * Different options are available for most of the terms, and are detailed
   * below, together with the corresponding parameters.
   *
   * **General reference**: @refcite{Quarteroni2017, Quarteroni (2017)}.
   * <br>
   * (other, more specific references are provided in the next sections)
   *
   * @subsection diffusive-term Diffusion term
   * Three different options are available for the diffusion term, and are
   * accessed in the parameter file by setting the value of the parameter
   * <kbd>Physical constants and models / Diffusion term formulation</kbd>. They
   * are:
   * - gradient-gradient formulation (<kbd>Grad-Grad</kbd>):
   * @f${\color{red}{\mathcal{D}(\mathbf{u}^{n+1},\mathbf v;
   * \mu)}} = (\mu \nabla\mathbf
   * u^{n+1}, \nabla\mathbf v)@f$;
   * - symmetric gradient-gradient formulation (<kbd>SymGrad-Grad</kbd>):
   * @f${\color{red}{\mathcal{D}(\mathbf{u}^{n+1},\mathbf v;
   * \mu)}} = (2\mu \nabla^S\mathbf
   * u^{n+1}, \nabla\mathbf v)@f$, where @f$\nabla^S(\mathbf w) =
   * \frac{1}{2}(\nabla\mathbf w + \nabla\mathbf w^T)@f$;
   * - symmetric gradient-symmetric gradient formulation
   * (<kbd>SymGrad-SymGrad</kbd>):
   * @f${\color{red}{\mathcal{D}(\mathbf{u}^{n+1},\mathbf v;
   * \mu)}} = (2\mu\nabla^S\mathbf
   * u^{n+1}, \nabla^S\mathbf v)@f$.
   *
   * The default setting is <kbd>SymGrad-Grad</kbd>.
   *
   * Be aware that the gradient-gradient formulation differs from the other two
   * in the different physical meaning given to Neumann boundary conditions.
   *
   * @subsection advection Advection term
   * The advection term introduces a non-linearity in the equation, that can be
   * dealt with in two ways. The different options are exposed by the
   * parameter <kbd>Time solver / Non-linearity treatment</kbd>. In the case of
   * a fixed domain, they are:
   * - implicit formulation (<kbd>Implicit</kbd>): @f${\color{orange}{\mathbf
   * u^{n+1}_*}} = \mathbf u^{n+1}@f$; the formulation is non-linear, and the
   * problem is solved by means of Newton's method as implemented in @ref utils::NonLinearSolverHandler;
   * - semi-implicit formulation (<kbd>Semi-implicit</kbd>):
   * @f${\color{orange}{\mathbf u^{n+1}_*}} = \mathbf
   u^{n+1}_{\text{EXT},\sigma}@f$,
   * where @f$\mathbf u^{n+1}_{\text{EXT},\sigma}@f$ is the extrapolation provided by @ref utils::BDFHandler.
   * This formulation is linear. Beware that the problem is still solved with
   * Newton's method, which in this case needs only one iteration to converge.
   * To save the computational cost needed to assemble the system at second
   * iteration to check tolerances, you should set <kbd>Non-linear solver /
   * Linearized = true</kbd>.
   *
   * @subsection ale Arbitrary Lagrangian Eulerian (ALE) formulation
   * To deal with a moving domain, the ALE formulation is implemented. It can be
   * toggled through <kbd>Arbitrary Lagrangian Eulerian / Active</kbd>.
   *
   * With this formulation, at every timestep the domain geometry is updated by
   * lifting a given displacement from the boundary to the interior of the
   * domain, then displacing the mesh according to the computed displacement
   * @f$\mathbf d_\mathrm{ALE}^{n+1}@f$ and computing the fluid domain velocity
   * @f$\mathbf u_\mathrm{ALE}^{n+1} = (\mathbf d^{n+1} - \mathbf d^n)/\Delta
   * t \approx \partial\mathbf d_\mathrm{ALE}/\partial t@f$. The advective term
   * is then modified by subtracting the fluid domain velocity:
   * - implicit formulation: @f${\color{orange}{\mathbf u^{n+1}_*}} = \mathbf
   * u^{n+1} - \mathbf u_\mathrm{ALE}^{n+1}@f$;
   * - semi-implicit formulation: @f${\color{orange}{\mathbf u^{n+1}_*}} =
   * \mathbf u^{n+1}_{\text{EXT},\sigma} - \mathbf u_\mathrm{ALE}^{n+1}@f$.
   *
   * Boundary displacement can be given either in the form of an analytical
   * expression or through VTP files. Further details can be found in the
   * classes @ref ALEHandler and @ref Lifting.
   *
   * @note In the ALE case, the time-derivative term is modified as follows:
   * @f[
   * \left ( \rho \frac{\alpha_{\text{BDF}\sigma} \mathbf{u}^{n+1}}{\Delta t} ,
   * \mathbf{v} \right )_{\Omega^{n+1}} -
   * \left ( \rho \frac{\mathbf{u}^n_{\text{BDF}\sigma}}{\Delta t} ,
   * \mathbf{v} \right )_{\Omega^n},
   * @f]
   * i.e. the term at previous timestep is integrated on the mesh at previous
   * timestep, and the @f$L^2@f$ inner product in all the other terms is
   * actually @f$\left(\cdot,\cdot\right)_{\Omega^{n+1}}@f$, defined over the
   * current configuration @f$\Omega^{n+1}@f$ of the domain.
   *
   * @warning At the moment, this modification does not affect the assembly of
   * the stabilization terms, which are all assembled on the current mesh.
   *
   * @subsection riis Immersed surfaces: Resistive Immersed Implicit Surface
   * To model the presence of immersed surfaces (such as cardiac valves), the
   * Resistive Immersed Implict Surface (RIIS) model can be activated through
   * <kbd>Resistive Immersed Implicit Surface / Active</kbd>. This adds to the
   * equation the term
   * @f[{\color{green}{\mathcal{R}(\mathbf u^{n+1}, \mathbf v)}} = \left (
   * \frac{R_\Gamma}{\varepsilon} \delta_{\Gamma,\varepsilon} (\varphi_\Gamma)
   * (\mathbf{u}^{n+1} - \mathbf{u}_{\Gamma}^{n+1}), \mathbf {v} \right )_\Omega
   * @f]
   * For further details, refer to the documentation of the @ref RIISHandler class.
   *
   * **References**: @refcite{Fedele2017, Fedele et al. (2017)},
   * @refcite{Fumagalli2020, Fumagalli et al. (2020)}
   *
   * @subsection stabilization Stabilization and turbulence modeling
   * Without stabilization, inf-sup compatible finite element spaces must be
   * chosen for velocity and pressure in order for the discrete problem to be
   * well posed. Stabilization allows to use equal-order finite element spaces,
   * adding terms to the equations.
   *
   * Moreover, turbulence modeling can be used to account for the effect of
   * unresolved scales of the flow.
   *
   * Stabilization and turbulence models can be selected through the parameter
   * <kbd>Stabilization and turbulence models / Model</kbd>. Available options
   * are <kbd>None</kbd>, <kbd>SUPG</kbd>, <kbd>VMS-LES</kbd> and
   <kbd>Sigma-LES</kbd>.
   *
   * @subsubsection supgvmsles SUPG/PSPG and VMS-LES
   *
   * SUPG and VMS-LES add the following terms to the equation
   * (@f$K@f$ denotes a generic cell of the mesh @f$\mathcal T_h@f$):
   *
   * @f[ {\color{blue}{\mathcal{S}(\mathbf u^{n+1}, {\color{orange}{\mathbf
   * u^{n+1}_*}}, p^{n+1}, p^{n+1}_{\text{EXT},\sigma}; \mathbf v, q)}}
   * = C_\text{S} \sum_{K\in\mathcal
   * T_h}\left(\color{blue}{\underbrace{\color{blue}{ \left
   * (\tau_M({\color{orange}{\mathbf{u}^{n+1}_{*}}}) \mathbf{r}_M
   * (\mathbf{u}^{n+1},p^{n+1}), \rho {\color{orange}{\mathbf{u}^{n+1}_{*}}}
   * \cdot \nabla \mathbf{v} + \nabla q \right)_{K}}
   * + \left ( \tau_C ({\color{orange}{\mathbf{u}^{n+1}_{*}}}) r_C
   * (\mathbf{u}^{n+1}), \nabla \cdot \mathbf{v}\right)_{K}}_{\text{SUPG}}}
   * + \color{violet}{\underbrace{\left
   * (\tau_M({\color{orange}{\mathbf{u}^{n+1}_{*}}}) \mathbf{r}_M
   * (\mathbf{u}^{n+1},p^{n+1}), \rho {\color{orange}{\mathbf{u}^{n+1}_{*}}}
   * \cdot \nabla^T \mathbf{v} \right)_{K}}_{\text{VMS}}} \color{violet}{+
   * \text{LES}_K} \right) @f]
   *
   * We remark that the term denoted as SUPG corresponds more precisely to an
   * SUPG-PSPG stabilization. For more details, refer e.g. to
   * http://www.inf.ufes.br/~luciac/tei-femef/referencias/4-1-TEZDUYAR-V4N1.pdf
   *
   * The LES terms are defined differently for the implicit and semi-implicit
   * cases:
   * @f[
   * \begin{alignedat}{2}
   * & {\color{violet}{\text{LES}_K}} =
   * \begin{cases}
   * -\left(\tau_M({\color{orange}{\mathbf{u}^{n+1}_{*}}}) \mathbf{r}_M
   * (\mathbf{u}^{n+1},p^{n+1}) \otimes
   * \tau_M({\color{orange}{\mathbf{u}^{n+1}_{*}}}) \mathbf{r}_M
   * (\mathbf{u}^{n+1},p^{n+1}), \rho \nabla \mathbf{v} \right)_{K}  &
   * \text{Implicit}\\
   * -\left(\tau_M({\color{orange}{\mathbf{u}^{n+1}_{*}}}) \mathbf{r}_M
   * (\mathbf{u}_{\text{EXT}\sigma}^{n+1},p_{\text{EXT}\sigma}^{n+1}) \otimes
   * \tau_M({\color{orange}{\mathbf{u}^{n+1}_{*}}}) \mathbf{r}_M
   * (\mathbf{u}^{n+1},p^{n+1}), \rho \nabla \mathbf{v} \right)_{K}  &
   * \text{SemiImplicitSoft}\\
   * -\left ( \tau_M({\color{orange}{\mathbf{u}^{n+1}_{*}}}) \mathbf{r}_M
   * (\mathbf{u}_{\text{EXT}\sigma}^{n+1},p_{\text{EXT}\sigma}^{n+1}) \otimes
   * \tau_M({\color{orange}{\mathbf{u}^{n+1}_{*}}}) \mathbf{r}_M^\text{LHS}
   * (\mathbf{u}^{n+1},p^{n+1}),\rho \nabla \mathbf{v} \right)_{K}
   * + \left ( \tau_M({\color{orange}{\mathbf{u}^{n+1}_{*}}})
   * \mathbf{r}_M(\mathbf{u}^{n+1},p^{n+1}) \otimes
   * \tau_M({\color{orange}{\mathbf{u}^{n+1}_{*}}}) \mathbf{r}_M^\text{RHS},\rho
   * \nabla \mathbf{v}\right )_K & \text{SemiImplicitHard}
   * \end{cases} \\
   * \mathbf{r}_M (\mathbf{u}, p)
   * ~=~ & \mathbf{r}_M^\text{LHS}(\mathbf{u}, p) - \mathbf{r}_M^\text{RHS},\\
   * \mathbf{r}_M^\text{LHS}(\mathbf{u}, p)
   * ~=~ & \rho \left(\dfrac{\alpha_{\text{BDF}\sigma}\mathbf{u}}{\Delta t} +
   * {\color{orange}{\mathbf{u}^{n+1}_{*}}} \cdot \nabla \mathbf{u} \right) +
   * \nabla p - \mu\Delta \mathbf{u} +
   * {\color{green}{\frac{R_\Gamma}{\varepsilon}
   * \delta_{\Gamma,\varepsilon} (\varphi_\Gamma) \mathbf{u}}}  \\
   * \mathbf{r}_M^\text{RHS} ~=~&
   * \rho \dfrac{\mathbf{u}^n_{\text{BDF}\sigma}}{\Delta t} +
   * {\color{green}{\dfrac{R_\Gamma}{\varepsilon} \delta_{\Gamma,\varepsilon}
   * (\varphi_\Gamma) \mathbf{u}_{\Gamma}^{n+1}}} + \mathbf{f}^{n+1} \\
   * r_C (\mathbf{u}) ~=~ &   \nabla \cdot \mathbf{u} \\
   * \tau_M ({\color{orange}{\mathbf{u}^{n+1}_{*}}}) ~=~&
   * \left (
   * \dfrac{\rho^2\sigma^2_\text{BDF}}{\Delta t^2}
   * + \rho^2 {\color{orange}{\mathbf{u}^{n+1}_{*}}} \cdot \mathbf{G}
   * {\color{orange}{\mathbf{u}^{n+1}_{*}}}
   * + C_r \mu^2 \mathbf{G} : \mathbf{G}
   * + {\color{green}{\dfrac{R_\Gamma^2}{\varepsilon^2 }
   * \delta_{\Gamma,\varepsilon}^2 (\varphi_\Gamma)}} \right ) ^ {-\frac{1}{2}}
   * \\
   * \tau_C ({\color{orange}{\mathbf{u}^{n+1}_{*}}}) ~=~&
   * \left(\tau_M ( {\color{orange}{\mathbf{u}^{n+1}_{*}}} )
   * \mathbf{g} \cdot \mathbf{g}\right)^{-1}
   * \end{alignedat}
   * @f]
   * where @f$\mathbf G=J^{-T}J^{-1}@f$ and @f$\mathbf g=J^{-T}\mathbf 1@f$
   * denote the metric tensor and vector, respectively, depending on the
   * Jacobian @f$J@f$ of the map from the reference element to the current one.
   *
   * **References**:
   * - @refcite{Bazilevs2007, Bazilevs et al. (2007)}
   * - @refcite{Forti2015, Forti Dede' (2015)}
   *
   * @subsection bcs Boundary conditions
   * Different types of boundary conditions are supported. In the most general
   * case, the user can instantiate boundary conditions as @ref utils::FunctionDirichlet
   * or @ref utils::FunctionNeumann objects (or as custom classes deriving from those),
   * and forward those objects to the FluidDynamics instance through
   * @ref set_BCs_dirichlet and @ref set_BCs_neumann before calling
   * @ref FluidDynamics::run to start the simulation.
   *
   * For most common cases, predefined functions are available in the
   * @ref FluidDynamicsBCs namespace. Such functions allow configuration from
   * parameter file and are used by most tests and examples. Refer to the
   * documentation for the namespace @ref FluidDynamicsBCs for further details,
   * and to the fluid dynamics tests and examples (e.g. @ref tests::TestFluidDynamicsCylinder)
   * for concrete use examples.
   *
   * The case of periodic boundary conditions is managed separately and
   * it requires to create @ref FluidDynamics::PeriodicBC objects and to
   * forward them to the FluidDynamics instance through a @ref set_BCs_periodic
   * call before @ref FluidDynamics::run.
   *
   * @subsection backflowstabilization Backflow stabilization for Neumann boundaries
   * If inflow occurs through boundaries on which Neumann conditions are
   imposed,
   * instabilities can occur. It is possible to correct those instabilities by
   * adding the following term to the weak formulation:
   * @f[
   * \left(\beta\rho ({\color{orange}{\mathbf{u}^{n+1}_*}} \cdot \mathbf n)_-
   * \mathbf u, \mathbf v \right)_{\Gamma_N}\;,
   * @f]
   * where @f$(x)_- = \min\{x, 0\}@f$, and @f$\beta \in (0, 1]@f$. This can be
   * enabled by configuring the parameters within the section <kbd>Backflow
   * stabilization</kbd>.
   *
   * @section algebraic Algebraic formulation
   * The time-discrete weak formulation is discretized in space using finite
   * elements, after linearization with Newton's method (see also
   * @ref utils::NonLinearSolverHandler). The resulting system has the form
   * @f[
   * \mathcal{J}\delta\mathbf x = \mathbf b\;,
   * @f]
   * where
   * @f[
   * \mathcal{J} = \begin{bmatrix} F & B^T \\ -B & S \end{bmatrix} \qquad
   * \delta\mathbf x = \begin{bmatrix}\delta\mathbf U \\ \delta\mathbf
   * P\end{bmatrix} \qquad \mathbf b = \begin{bmatrix} \mathbf b_u \\ \mathbf
   * b_p \end{bmatrix}\;,
   * @f]
   * and the solution is updated as @f$[\mathbf U^{n+1}; \mathbf P^{n+1}] =
   * [\mathbf U^{n}; \mathbf P^{n}] - [\delta\mathbf U; \delta\mathbf P]@f$.
   * In the above equation, @f$\mathbf b_u@f$ and @f$\mathbf b_p@f$ are
   * the discrete residuals of the momentum and continuity equations,
   * respectively.
   * For more details on the definition of individual blocks
   * of the system refer e.g. to @refcite{Quarteroni2017, Quarteroni (2017)} or
   * @refcite{Deparis2014, Deparis et al. (2014)}.
   *
   * @subsection preconditioning Preconditioning
   * The algebraic system must be preconditioned in order to obtain a solution.
   * To this aim, @lifex offers the SIMPLE preconditioner, implemented in
   * @ref FluidDynamicsPreconditioners::SIMPLE. For a description of the
   * preconditioner, please refer to the documentation of its class.
   *
   * The default is the SIMPLE preconditioner. Where the inverse of some matrix
   * block is needed, such inverse is approximated with a suitable black-box
   * preconditioner for that matrix. Black-box preconditioners are made
   * available through the @ref utils::PreconditionerHandler class. Parameters
   * for such preconditioners can be selected in the <kbd>Preconditioner</kbd>
   * section of the parameters file.
   *
   * **References**: @refcite{Deparis2014, Deparis et al. (2014)}
   *
   * @section postprocessing Postprocessing and output
   * Two possible outputs are available:
   * - the 3D output of the solution, in HDF5/XDMF format;
   * - a CSV file containing aggregate information about the solution at every
   * timestep.
   *
   * Both can be toggled and configured in the <kbd>Output</kbd> section of the
   * parameters file.
   *
   * @subsection output-ale Output with ALE formulation
   * If ALE is enabled, it is possible to include in the output the quantities
   * related to the ALE formulation (fluid domain displacement and velocity). To
   * do so, configure the parameters in <kbd>Arbitrary Lagrangian Eulerian /
   * Output</kbd>. ALE output variables are saved in the same file as fluid
   * solution variables (velocity and pressure).
   *
   * If ALE is enabled, it is possible to export the mesh in the deformed
   * configuration at every timestep. To do so, set <kbd>Arbitrary Lagrangian
   * Eulerian / Export mesh at every output</kbd> to <kbd>true</kbd>. Beware
   * that this will result in larger solution files, since the mesh must be
   * repeated in every file instead of saved only in the first one.
   *
   * @subsection volumes-flows Computation of submesh volumes and boundary flowrates and pressures
   * The class allows to compute and export the volume of volume-tagged regions
   * of the mesh. To do so, in the parameter file set the parameters <kbd>Volume
   * tags / Labels of volume-tagged regions</kbd> and <kbd>Volume tags /
   * Material IDs of volume-tagged regions</kbd>. The volumes are updated at
   * each timestep. The volumes at current, previous and second previous
   * timesteps are stored. The volumes and their time derivatives can be
   * retrieved by calling @ref get_volume and @ref get_volume_derivative.
   *
   * Moreover, it is possible to compute flowrates and average pressures at
   * portions of the boundary. To do so, set the parameters <kbd>Compute
   * flowrates and pressures / Boundary tags</kbd> and <kbd>Compute flowrates
   * and pressures / Boundary labels</kbd>.
   *
   * All volumes, flowrates and pressures are exported in the CSV file.
   *
   * @subsection control-volumes Average quantities over control volumes
   * It is possible to obtain the pressure and velocity magnitude over time,
   * averaged within a control volume inside the domain. To do so, you can set
   * the parameters within the <kbd>Control volumes</kbd> section. Control
   * volumes can be either spheres (defined in terms of their center and radius)
   * or arbitrary closed surfaces loaded through a VTP file.
   *
   * Each control volume is given a label, and the corresponding average
   * quantities are exported in the CSV file.
   *
   * Notice that control volumes may be required by some settings related to
   * RIIS surfaces. For more info, refer to @ref RIISHandler and @ref RIISSurface.
   *
   * @note If the control volume is described by a VTP file, the normal vectors
   * on such surface must point outwards.
   */
  class FluidDynamics : public CoreModel
  {
  public:
    /**
     * Constructor.
     *
     * @param[in] subsection         parameter subsection path for
     *                                    this class;
     * @param[in] standalone_ If true, parameters for time discretization will be
     * declared; otherwise, they are expected to be set via @ref set_time_discretization.
     */
    FluidDynamics(const std::string &subsection,
                  const bool &       standalone_ = true);

    friend class Perfusion;

    /// @name Flags.
    /// @{

    /// Enumeration of the possible Navier-Stokes diffusive terms.
    enum class DiffusionTerm
    {
      /// @f$ 2\mu \int_\Omega \frac{1}{2}(\nabla \mathbf{u} + \nabla^T
      /// \mathbf{u})  : \nabla \mathbf{v} \, d \mathbf{x}@f$.
      SymGrad_Grad,
      /// @f$ 2\mu \int_\Omega \frac{1}{2}(\nabla \mathbf{u} + \nabla^T
      /// \mathbf{u}) : \frac{1}{2}(\nabla \mathbf{v} + \nabla^T \mathbf{v})\, d
      /// \mathbf{x}@f$.
      SymGrad_SymGrad,
      /// @f$ \mu \int_\Omega \nabla \mathbf{u} : \nabla \mathbf{v} \, d
      /// \mathbf{x}@f$.
      Grad_Grad
    };

    /// Enumeration of the implemented stabilization and turbulence models.
    enum class Stabilization
    {
      /// Using inf-sup stable elements without any stabilization.
      None,
      /// SUPG stabilization.
      SUPG,
      /// VMS-LES turbulence model.
      VMS_LES
    };

    /// Enumeration of the possible non-linear terms treatments.
    enum class Nonlinearity
    {
      SemiImplicit, ///< Semi-implicit treatment.
      Implicit      ///< Implicit treatment.
    };

    /// Enumeration used to tell interpolate_local_quantities what quantities to
    /// interpolate.
    enum class LocalInterpolationFlags
    {
      none                   = 1 << 0,  ///< Nothing.
      compute_u_loc          = 1 << 1,  ///< Local velocity.
      compute_grad_u_loc     = 1 << 2,  ///< Local velocity gradient.
      compute_symgrad_u_loc  = 1 << 3,  ///< Local symmetric gradient.
      compute_div_u_loc      = 1 << 4,  ///< Local velocity divergence.
      compute_lapl_u_loc     = 1 << 5,  ///< Local velocity laplacian.
      compute_p_loc          = 1 << 6,  ///< Local pressure.
      compute_grad_p_loc     = 1 << 7,  ///< Local pressure gradient.
      compute_u_star_loc     = 1 << 8,  ///< Local advection velocity.
      compute_u_bdf_loc      = 1 << 9,  ///< Local BDF velocity.
      compute_u_ext_loc      = 1 << 10, ///< Local extrapolated velocity.
      compute_grad_u_ext_loc = 1 << 11, ///< Local extrapolated gradient of u.
      compute_lapl_u_ext_loc = 1 << 12, ///< Local extrapolated laplacian of u.
      compute_grad_p_ext_loc = 1 << 13, ///< Local extrapolated gradient of p.
      compute_rhs_values     = 1 << 14, ///< Local forcing term.
      compute_riis_factor    = 1 << 15, ///< Local RIIS factor.
      compute_riis_velocity  = 1 << 16, ///< Local RIIS velocity.
      compute_u_ale_loc      = 1 << 18, ///< ALE velocity on quadrature nodes.

      /// Time derivative terms.
      compute_time_derivative = (1 << 21) | compute_u_loc | compute_u_bdf_loc,

      /// Advection terms.
      compute_advection = (1 << 22) | compute_grad_u_loc | compute_u_star_loc,

      /// Diffusion terms.
      compute_diffusion = (1 << 23),

      /// Resistive terms.
      compute_riis = (1 << 24) | compute_riis_factor,

      /// Stabilization terms (tau coefficients and residuals). This
      /// automatically implies local velocity, advection velocity, velocity
      /// gradient, velocity laplacian and pressure gradient.
      compute_stabilization_terms = (1 << 25) | compute_u_loc |
                                    compute_u_star_loc | compute_grad_u_loc |
                                    compute_lapl_u_loc | compute_grad_p_loc
    };

    /// Interpolation flags for system assembly (see @ref LocalInterpolationFlags
    /// and @ref assemble_callback_cell).
    LocalInterpolationFlags local_interpolation_flags_assemble_system =
      LocalInterpolationFlags::none;

    /// @}

    /// Override of @ref CoreModel::declare_parameters.
    virtual void
    declare_parameters(ParamHandler &params) const override;

    /// Override of @ref CoreModel::parse_parameters.
    virtual void
    parse_parameters(ParamHandler &params) override;

    /**
     * @brief Run model simulation.
     *
     * Boundary conditions must be set before this method is called. In
     * particular, if the method is called without previously calling at least
     * one of @ref set_BCs_dirichlet, @ref set_BCs_neumann or @ref set_BCs_periodic,
     * an exception is thrown.
     */
    virtual void
    run() override;

    /// @name Post-processing and output.
    /// @{

    /**
     * @brief Compute the outgoing flow and average pressure at multiple
     * portions of the boundary.
     *
     * The flow through a surface @f$\Gamma@f$ with outer normal vector
     * @f$\mathbf{n}@f$ is defined as @f$\int_\Gamma (\mathbf{u} -
     * \mathbf{u}_\mathrm{ALE})\cdot\mathbf{n}\,\mathrm{d}\gamma@f$,
     * and the average pressure is @f$\frac{1}{|\Gamma|}\int_\Gamma
     * p\,\mathrm{d}\gamma@f$.
     *
     * @param[in] tags_flowrate A vector of sets of boundary tags; the flow
     * will be computed for each set.
     * @param[in] tags_pressure A vector of sets of boundary tags; the pressure
     * will be computed for each set.
     * @param[in] normalize_flowrates If true, the flow is divided by
     * @f$\left|\Gamma\right|@f$.
     *
     * @return Flowrates and pressure for each set of boundary tags.
     */
    std::pair<std::vector<double>, std::vector<double>>
    compute_boundary_flow_and_pressure(
      const std::vector<std::set<types::boundary_id>> &tags_flowrate,
      const std::vector<std::set<types::boundary_id>> &tags_pressure,
      const bool &normalize_flowrates = false) const;

    /// Calls compute_boundary_flow_and_pressure for a vector of single boundary
    /// tags.
    std::pair<std::vector<double>, std::vector<double>>
    compute_boundary_flow_and_pressure(
      const std::vector<types::boundary_id> &tags_flowrate,
      const std::vector<types::boundary_id> &tags_pressure,
      const bool &                           normalize_flowrates = false) const;

    /// Calls compute_boundary_flow_and_pressure for a single set of boundary
    /// tags.
    std::pair<double, double>
    compute_boundary_flow_and_pressure(
      const std::set<types::boundary_id> &tags_flowrate,
      const std::set<types::boundary_id> &tags_pressure,
      const bool &                        normalize_flowrates = false) const;

    /// Calls compute_boundary_flow_and_pressure for single boundary tags.
    std::pair<double, double>
    compute_boundary_flow_and_pressure(
      const types::boundary_id &tag_flowrate,
      const types::boundary_id &tag_pressure,
      const bool &              normalize_flowrate = false) const;

    /**
     * @brief Compute the total force exerted by the fluid on a portion of
     * boundary with the given tag.
     *
     * The function performs the residual-based computation of the force.
     *
     * **References**: @refcite{Dede2007, Dede' (2007)}
     */
    Tensor<1, dim, double>
    compute_force(const types::boundary_id &tag) const;

    /// Save results to output.
    void
    output_results();

    /// Attach output data to a DataOut object.
    void
    attach_output(DataOut<dim> &data_out) const;

    /// Output solution, only on boundary faces.
    void
    output_boundary() const;

    /// Print information about the fluid dynamics solution.
    virtual void
    print_info() const;

    /// @}

    /// @name Getters.
    /// @{

    /// Get alpha.
    double
    get_alpha() const
    {
      return bdf_handler.get_alpha();
    }

    /// Get FE object.
    const std::unique_ptr<FESystem<dim>> &
    get_fe() const
    {
      return fe;
    }

    /// Get FEValues object.
    const std::unique_ptr<FEValues<dim>> &
    get_fe_values() const
    {
      return fe_values;
    }

    /// Get homogeneous constraints for Dirichlet DoFs.
    const AffineConstraints<double> &
    get_zero_constraints() const
    {
      return zero_constraints;
    }

    /// Get owned dofs.
    const std::vector<IndexSet> &
    get_owned_dofs() const
    {
      return owned_dofs;
    }

    /// Get relevant dofs.
    const std::vector<IndexSet> &
    get_relevant_dofs() const
    {
      return relevant_dofs;
    }

    /// Get jacobian.
    const LinAlg::MPI::BlockSparseMatrix &
    get_jac() const
    {
      return jac;
    }

    /// Get sparsity pattern of the jacobian.
    const BlockDynamicSparsityPattern &
    get_dsp_jac() const
    {
      return dsp_jac;
    }

    /// Get DoF handler.
    DoFHandler<dim> &
    get_dof_handler()
    {
      return dof_handler;
    }

    /// Get DoF handler, const overload.
    const DoFHandler<dim> &
    get_dof_handler() const
    {
      return dof_handler;
    }

    /// Get quadrature rule.
    const Quadrature<dim> &
    get_quadrature() const
    {
      return *quadrature_formula;
    }

    /// Get solution.
    const LinAlg::MPI::BlockVector &
    get_solution() const
    {
      return sol;
    }

    /// Get the preconditioner.
    FluidDynamicsPreconditioners::InexactBlockLU &
    get_preconditioner()
    {
      return *preconditioner;
    }

    /// Getter of the density.
    double
    get_density() const
    {
      return prm_density;
    }

    /// Getter of the viscosity.
    double
    get_viscosity() const
    {
      return prm_viscosity;
    }

    /// Get current time.
    double
    get_time() const
    {
      return time;
    }

    /// Get current timestep number.
    unsigned int
    get_timestep_number() const
    {
      return timestep_number;
    }

    /// Get initial time.
    double
    get_initial_time() const
    {
      return prm_time_init;
    }

    /// Get final time.
    double
    get_final_time() const
    {
      return prm_time_final;
    }

    /// Get period.
    double
    get_period() const
    {
      return prm_period;
    }

    /// Get timestep
    double
    get_time_step() const
    {
      return prm_time_step;
    }

    /**
     * @brief Retrieve the volume of a volume-tagged region.
     *
     * @param[in] label Label identifying the region, as declared in the
     * corresponding section of the parameter file.
     * @param[in] timestep_index Index identifying the timestep at which the
     * volume is computed. @ref n_stored_volumes - 1 stands for current (last
     * available) timestep, @ref n_stored_volumes - 2 stands for previous timestep,
     * etc. The number of available
     * timesteps is given by @ref n_stored_volumes.
     */
    double
    get_volume(const std::string & label,
               const unsigned int &timestep_index = n_stored_volumes - 1)
    {
      return volumes.at(label).at(timestep_index);
    }

    /**
     * @brief Retrieve time derivative of the volume of a volume-tagged region.
     *
     * For a given timestep @f$i@f$, the time derivative of the volume @f$V@f$
     * is approximated as @f$(V(t_i) - V(t_{i-1})) / \Delta t@f$.
     *
     * @param[in] label Label identifying the region, as declared in the
     * corresponding section of the parameter file.
     * @param[in] timestep_index Index identifying the timestep at which the
     * derivative is computed. 0 stands for previous time step and 1 stands for
     * current (last available) timestep. The number of available timesteps
     * is given by @ref n_stored_volumes - 1.
     */
    double
    get_volume_derivative(
      const std::string & label,
      const unsigned int &timestep_index = n_stored_volumes - 1) const
    {
      const std::deque<double> &volume_deque = volumes.at(label);

      return (volume_deque.at(timestep_index) -
              volume_deque.at(timestep_index - 1)) /
             prm_time_step;
    }

    /// Get pressure in the control volume with given label.
    double
    get_pressure_control_volume(const std::string &label) const
    {
      for (unsigned int k = 0; k < control_volumes.size(); ++k)
        if (control_volumes[k].get_label() == label)
          return pressure_control_volumes[k];

      AssertThrow(false,
                  ExcMessage("No control volume labelled " + label +
                             " is defined."));
      return 0.0;
    }

    /// Return the name of the file the CSV output is written to.
    std::string
    get_output_csv_filename() const
    {
      return prm_csv_filename;
    }

    /// Get whether ALE is enabled.
    bool
    ale_enabled() const
    {
      return prm_ale;
    }

    /// Get diffusion term.
    const DiffusionTerm &
    get_diffusion_flag() const
    {
      return prm_flag_diffusion_term;
    }

    /// @}

    /// @name Setters.
    /// @{

    /** Setter method for @ref end_timestep_callback.
     *
     * @param[in] end_timestep_callback_ A pointer to a void functional. If not null,
     * the functional is called at the end of each timestep.
     *
     **/
    void
    set_end_timestep_callback(
      const std::function<void()> &end_timestep_callback_ = {})
    {
      end_timestep_callback = end_timestep_callback_;
    }

    /**
     * @brief Set Dirichlet boundary conditions.
     *
     * @param[in] dirichlet_bcs_ vector of @ref utils::BC with the Dirichlet
     * boundary conditions;
     * @param[in] dirichlet_normal_flux_bcs_ vector of @ref utils::BC with the
     * Dirichlet normal-flux boundary conditions;
     * @param[in] dirichlet_tangential_flux_bcs_ vector of @ref utils::BC with
     * the Dirichlet tangential-flux boundary conditions.
     */
    virtual void
    set_BCs_dirichlet(
      const std::vector<utils::BC<utils::FunctionDirichlet>> &dirichlet_bcs_,
      const std::vector<utils::BC<utils::FunctionDirichlet>>
        &dirichlet_normal_flux_bcs_ = {},
      const std::vector<utils::BC<utils::FunctionDirichlet>>
        &dirichlet_tangential_flux_bcs_ = {})
    {
      dirichlet_bcs                 = dirichlet_bcs_;
      dirichlet_normal_flux_bcs     = dirichlet_normal_flux_bcs_;
      dirichlet_tangential_flux_bcs = dirichlet_tangential_flux_bcs_;

      // Boolean variable to check if there are any flux bcs.
      dirichlet_flux_bcs = !(dirichlet_tangential_flux_bcs.empty() &&
                             dirichlet_normal_flux_bcs.empty());

      bcs_are_set = true;
    }

    /**
     * @brief Set Neumann boundary conditions.
     *
     * @param[in] neumann_bcs_ vector of @ref utils::BC with the Neumann boundary
     * conditions.
     */
    virtual void
    set_BCs_neumann(
      const std::vector<utils::BC<utils::FunctionNeumann>> &neumann_bcs_)
    {
      neumann_bcs = neumann_bcs_;
      bcs_are_set = true;
    }

    /**
     * @brief Periodic boundary condition.
     *
     * Wraps a pair of tags and an axis, representing two faces and their normal
     * direction on which to impose periodic boundary conditions.
     */
    class PeriodicBC
    {
    public:
      /// Constructor.
      PeriodicBC(const std::array<types::boundary_id, 2> &tags_,
                 const unsigned int &                     axis_)
        : tags(tags_)
        , axis(axis_)
      {}

      /// Constructor.
      PeriodicBC(const types::boundary_id &tag_0,
                 const types::boundary_id &tag_1,
                 const unsigned int &      axis_)
        : tags({{tag_0, tag_1}})
        , axis(axis_)
      {}

      /// Tags between which periodic boundary conditions are imposed.
      std::array<types::boundary_id, 2> tags;

      /// Axis normal to the periodic boundary faces (0 = x, 1 = y, 2 = z).
      unsigned int axis;
    };

    /**
     * @brief Set periodic boundary conditions.
     */
    virtual void
    set_BCs_periodic(const std::vector<PeriodicBC> &periodic_bcs_)
    {
      periodic_bcs = periodic_bcs_;
      bcs_are_set  = true;
    }

    /**
     * @brief Set parameters related to ALE boundary conditions.
     *
     * @param[in] ale_tags_ set of tags were the no slip condition
     * @f$\mathbf{u}^{n+1} = \mathbf{u}_\text{ALE}^{n+1}@f$ has to be applied,
     * in case of ALE formulation;
     * @param[in] ale_displacement_function_ analytical function implementing
     * the ALE displacement, used only if the displacement is not read from
     * file;
     * @param[in] ale_component_mask_ if an analytical function for ALE
     * displacement is specified, it is applied only to the components of the
     * displacement matched by this mask.
     */
    virtual void
    set_BCs_ale(
      const std::set<types::boundary_id> &  ale_tags_,
      const std::shared_ptr<Function<dim>> &ale_displacement_function_ =
        std::make_shared<Functions::ZeroFunction<dim>>(dim),
      const ComponentMask &ale_component_mask_ = ComponentMask())
    {
      ale_displacement_function = ale_displacement_function_;
      ale_tags                  = ale_tags_;
      ale_component_mask        = ale_component_mask_;
    }

    /**
     * @brief Set forcing term.
     *
     * @param[in] forcing_term_ function implementing the Navier-Stokes forcing
     * term.
     */
    virtual void
    set_forcing_term(const std::shared_ptr<Function<dim>> &forcing_term_)
    {
      forcing_term = forcing_term_;
    }

    /**
     * @brief Reload boundary conditions.
     *
     * If boundary conditions are switched during the simulation (by calling
     * set_BCs_dirichlet, set_BCs_neumann and/or set_BCs_ale), this method
     * should be called right after so that changes on boundary conditions
     * become effective. Notice that this is not needed if the set_BCs methods
     * are called before calling run.
     */
    void
    reload_BCs()
    {
      bc_handler.initialize(dirichlet_bcs,
                            neumann_bcs,
                            *face_quadrature_formula,
                            dirichlet_normal_flux_bcs,
                            dirichlet_tangential_flux_bcs);

      reinit_constraints();

      // We rebuild the sparsity pattern of the jacobian (and reinit the
      // jacobian itself), since constrained DoFs might have changed due to
      // changes in Dirichlet boundary conditions.
      make_sparsity();

      // Preconditioner must be reinitialized if sparsity has changed.
      preconditioner->initialize();

      // Dirichlet BCs may need to be reinitialized due to boundary conditions
      // having changed.
      bcs_dirichlet_initialized = false;
    }

    /// Set non-null initial condition.
    void
    set_initial_condition(
      const std::shared_ptr<Function<dim>> &initial_condition_)
    {
      initial_condition = initial_condition_;
    }

    /// Set time discretization parameters.
    void
    set_time_discretization(const double &      time_initial,
                            const double &      time_final,
                            const double &      time_step,
                            const double &      time_,
                            const unsigned int &timestep_number_)
    {
      AssertThrow(!standalone, ExcNotStandalone());

      prm_time_init  = time_initial;
      prm_time_final = time_final;
      prm_time_step  = time_step;

      time            = time_;
      timestep_number = timestep_number_;
    }

    /// Set time and timestep number.
    void
    set_time(const double &time_, const unsigned int &timestep_number_)
    {
      AssertThrow(!standalone, ExcNotStandalone());

      time            = time_;
      timestep_number = timestep_number_;
    }

    /// Set restart-related parameters.
    void
    set_restart(const bool &        restart,
                const unsigned int &restart_timestep_init,
                const std::string & restart_basename)
    {
      AssertThrow(!standalone, ExcNotStandalone());

      prm_restart               = restart;
      prm_restart_timestep_init = restart_timestep_init;
      prm_restart_basename      = restart_basename;

      timestep_number_init = restart_timestep_init;
    }

    /// Set how often output must be saved.
    void
    set_output_every_n_timesteps(
      const unsigned int &output_every_n_timesteps,
      const unsigned int &serialize_every_n_timesteps)
    {
      prm_output_every_n_timesteps    = output_every_n_timesteps;
      prm_serialize_every_n_timesteps = serialize_every_n_timesteps;

      if (prm_ale)
        ale_handler->set_output_every_n_timesteps(output_every_n_timesteps);
    }

    /// @}

    /**
     * @brief Compute the norm of the difference between the solution and
     * an analytical function.
     *
     * Given a vector-valued function @f$\mathbf{f} = \begin{bmatrix}
     * \mathbf{f}_{\mathbf{u}} & \mathbf{f}_p \end{bmatrix}^T@f$, computes the
     * following norms:
     * - @f$\|\mathbf{u} - \mathbf{f}_\mathbf{u}\|_{L^2} =
     * \sqrt{\int_{\Omega}|\mathbf{u} - \mathbf{f}_\mathbf{u}|^2}@f$;
     * - @f$|\mathbf{u} - \mathbf{f}_\mathbf{u}|_{H^1} =
     * \sqrt{\int_{\Omega}|\nabla\mathbf{u} -
     * \nabla\mathbf{f}_\mathbf{u}|^2}@f$;
     * - @f$\|p - \mathbf{f}_p\|_{L^2} = \sqrt{\int_{\Omega}|p -
     * \mathbf{f}_p|^2}@f$.
     *
     * The three norms are stored in the resulting tuple in the above order.
     * This method is typically used to compute the error between the numerical
     * solution and an analytical one.
     *
     * @param[in] exact_solution The function @f$\mathbf{f}@f$.
     * @param[in] normalize If true, the computed norms are divided by the
     * corresponding norm of @f$\mathbf{f}@f$.
     */
    std::tuple<double, double, double>
    integrate_difference(Function<dim> &exact_solution,
                         const bool &   normalize = true);

  protected:
    /// Solve current time step.
    virtual void
    solve_time_step(const bool &assemble_jac);

    /// Create mesh.
    void
    create_mesh();

    /// Apply BCs.
    virtual void
    apply_BCs();

    /// Time advance
    virtual void
    time_advance();

    /// Setup system, @a i.e. allocate matrices and vectors.
    virtual void
    setup_system();

    /**
     * @brief Assemble @ref res_ale_old on the old mesh.
     * Used only if both @ref prm_ale and @ref prm_ale_old_mesh_terms
     * are @a true.
     *
     * To be used @b before calling @ref move_mesh().
     */
    void
    assemble_ale_old();

    /// Assemble linear system.
    /// @param[in] assemble_jac toggle assembling Jacobian,
    /// used in case of quasi-Newton methods.
    /// @note Compute also orifice areas (temporarily here to reduce computational effort).
    virtual void
    assemble_system(const bool &assemble_jac = true);

    /// Method to initialize the assembly. Reinitializes sol_ext and sol_bdf and
    /// quadrature evaluators for ALE and RIIS if needed.
    void
    initialize_assembly() const;

    /// Method called before assembling, right before the element loop.
    /// Calls initialize_assembly; can be overridden by derived classes in order
    /// to perform extra initialization tasks before assembly loop.
    virtual void
    assemble_callback_pre();

    /// Method called before assembling res_ale_old, right before the element
    /// loop. Initializes all the vectors and variables needed for the assembly.
    virtual void
    assemble_callback_pre_ale_old();

    /// Assemble the linear system contributions of a single cell.
    /// @param[in] cell Iterator to the cell being assembled.
    /// @param[in] c Index of the cell being assembled.
    /// @param[in] dof_indices Global indices of the DoFs of the cell,
    /// as returned by cell->get_dof_indices.
    /// @param[out] cell_matrix Local matrix to be filled; previous values will
    /// be discarded.
    /// @param[out] cell_res Local residual vector to be filled; previous values
    /// will be discarded.
    /// @param[in] assemble_jac Toggle assembling the jacobian.
    virtual void
    assemble_callback_cell(
      const DoFHandler<dim>::active_cell_iterator &cell,
      const unsigned int &                         c,
      const std::vector<types::global_dof_index> & dof_indices,
      FullMatrix<double> &                         cell_matrix,
      Vector<double> &                             cell_res,
      const bool &                                 assemble_jac = true);

    /// Assemble the contributions of a single cell to res_ale_old.
    virtual void
    assemble_callback_cell_ale_old(
      const DoFHandler<dim>::active_cell_iterator &cell,
      const std::vector<types::global_dof_index> & dof_indices,
      Vector<double> &                             cell_res);

    /// Method called at the end of system assembly, after the element loop,
    /// before the system matrix and right-hand-side are compressed. Does
    /// nothing by default.
    virtual void
    assemble_callback_post()
    {}

    /// Assemble preconditioner and solve linear system.
    void
    solve_system(const bool &assemble_prec, LinAlg::MPI::BlockVector &incr);

    /// Serialize solution to output. If filename_ale is not specified, the
    /// filename implied by parameters is used.
    void
    output_serialize() const;

    /// Restart from serialized solution.
    void
    restart();

    /// Reinitialize zero_constraints with boundary conditions.
    virtual void
    reinit_constraints();

    /// Make sparsity pattern and initialize the jacobian matrix accordingly.
    void
    make_sparsity();

    /// Declare entries for the CSV log: time, volume of chambers, and flowrates
    /// and pressures at boundaries. Optionally, the following outputs are also
    /// exported:
    /// - integral (if @ref prm_compute_fluid_energies and/or @ref prm_compute_space_averaged_stabilization_parameters are enabled);
    /// - control-volume-based velocity and pressure (if computed);
    /// - other RIIS variables (if @ref prm_riis): @ref RIISHandler::declare_entries_csv.
    virtual void
    declare_entries_csv(utils::CSVWriter &csv_writer) const;

    /// Set entries for the CSV log: write to a CSV file time, volume of
    /// chambers, and flowrates and pressures at boundaries. Optionally, the
    /// following outputs are also exported:
    /// - space-averaged fluid properties (if @ref prm_compute_fluid_energies is enabled);
    /// - RIIS-related stress (if @ref prm_compute_fluid_energies and/or @ref prm_compute_space_averaged_stabilization_parameters are enabled);
    /// - control-volume-based pressure jumps (if computed);
    /// - other RIIS variables (if @ref prm_riis): @ref RIISHandler::set_entries_csv.
    virtual void
    set_entries_csv(utils::CSVWriter &csv_writer) const;

    /**
     * @brief Compute integral quantities.
     *
     * Compute integral quantities as selected from the parameters `Export
     * integral fluid properties` and  `Export space-averaged
     * stabilization parameters`.
     * - If `Export space-averaged
     * fluid properties` is set to `true`, this method computes the total
     * kinetic energy
     * @f$E_k@f$, the total kinetic energy computed with fine-scale velocity
     * @f$E_{k,\mathrm{fs}}@f$ (if the VMS-LES model is active)
     * and the enstrophy @f$S@f$ defined as:
     * @f[
     * \begin{aligned}
     * E_k & = \frac{1}{2} \rho \int_{\Omega_t} |\mathbf u^h|^2 d \mathbf x, \\
     * E_{k,\mathrm{fs}} & = \frac{1}{2} \rho \int_{\Omega_t} |\mathbf u'|^2 d
     * \mathbf x,
     * \\ S & = \frac{1}{2} \rho \int_{\Omega_t} |\nabla \times \mathbf u^h|^2 d
     * \mathbf x.
     * \end{aligned}
     * @f]
     * In the equations above, @f$\textbf u^h @f$ is the coarse-scale velocity,
     * while @f$\textbf u'@f$ the fine-scale velocity. In the dynamic
     * stabilization, the latter is the solution of the fine-scale ODE, whereas
     * in the quasi-static approach it is simply defined as @f$\textbf u' = -
     * \tau_{\text M} (\textbf u^h) \textbf r_{\text M} (\textbf u^h)@f$, i.e.
     * following the quasi-static approximation.
     * - If `Export space-averaged stabilization parameters` is set to `true`,
     * this method computes the space-averaged stabilization parameters:
     * @f[
     * \begin{aligned}
     * \frac{1}{|\Omega|}\int_\Omega \tau_\mathrm{M} \, \mathrm d \mathbf x \\
     * \frac{1}{|\Omega|}\int_\Omega \tau_\mathrm{C} \, \mathrm d \mathbf x. \\
     * \end{aligned}
     * @f]
     */
    void
    compute_integral_quantities();

    /** Compute and update volumes.
     *
     * The method computes the value of volumes specified in the Volume tags
     * section of the parameter file. For each declared region, computes its
     * current volume. The volume is stored at second prevous, previous and
     * current timestep. The value of the volume can be retrieved through
     * the @ref get_volume method, while its time derivative can be retrieved
     * through @ref get_volume_derivative.
     */
    void
    compute_volumes();

    /// Update RIIS.
    virtual void
    update_riis();

    /// Get whether RIIS is enabled.
    bool
    riis_enabled() const
    {
      return prm_riis;
    }

    /**
     * @brief Evaluate RIIS factor at each quadrature node.
     */
    void
    cache_riis_factor();

    /**
     * @brief Interpolate local quantities.
     *
     * This method is used internally by assemble-like methods to interpolate
     * local quantities (velocity, pressure and their derivatives) on
     * quadrature nodes of a single cell. It assumes that:
     * - sol_bdf, sol_ext have been correctly initialized;
     * - quadrature_riis and quadrature_u_ale have been correctly
     * initialized and updated for current cell;
     * - if automatic differentiation is needed, sol_dof_AD has been correctly
     * initialized for current cell DoFs.
     *
     * After this function is called, the locally interpolated members (u_loc,
     * u_bdf_loc etc.) are filled, according to the flags (see @ref LocalInterpolationFlags).
     *
     * @param[in] flags An OR combination of @ref LocalInterpolationFlags
     * specifying which quantities need to be interpolated. Beware that there
     * are different flags for quantities with and without automatic
     * differentiation (e.g. compute_u_loc fills u_loc, a vector of doubles,
     * while compute_u_loc_AD fills u_loc_AD, a vector of double_ADs). If you
     * need automatic differentiation, you should pass the flags with the _AD
     * suffix.
     * @param[in] fe_values A FEValues object, already initialized for the current cell.
     * @param[in] dof_indices The vector of global DoF indices for the current
     * cell, as returned by <kbd>cell->get_dof_indices(dof_indices)</kbd>.
     * @param[in] c The index of current cell (needed for the quantities related to dynamic stabilization).
     * @param[in] override_ale_old_mesh_terms If set to true, overrides the setting of prm_ale_old_mesh_terms
     * for the computation of the right-hand-side in the time derivative term.
     * In that case, such terms are always computed in current configuration.
     *
     * @note This function interpolates quantities on the interior of a cell, not
     * on its faces. Interpolation on face quadrature nodes must be carried out
     * manually if needed.
     */
    template <class NumberType = double_AD>
    inline void
    interpolate_local_quantities(
      const LocalInterpolationFlags &             flags,
      const FEValues<dim> &                       fe_values,
      const std::vector<types::global_dof_index> &dof_indices,
      const unsigned int &                        c,
      const bool &override_ale_old_mesh_terms = false) const
    {
      if (flags == LocalInterpolationFlags::none)
        return;

      // Flags.
      // We redefine them locally as const to facilitate compiler optimization
      // (is it necessary?).
      const bool riis = prm_riis;
      const bool ale  = prm_ale;
      const bool semi_implicit =
        prm_flag_non_linear_terms == Nonlinearity::SemiImplicit;
      const bool semi_implicit_vms_les =
        prm_flag_stabilization == Stabilization::VMS_LES && semi_implicit;
      const bool stabilization_parameters_metric_tensors =
        prm_stabilization_parameters_metric_tensors;

      const double       alpha_bdf = bdf_handler.get_alpha();
      const unsigned int order_bdf = bdf_handler.get_order();

      // Current cell.
      const auto & cell          = fe_values.get_cell();
      const double cell_diameter = cell->diameter();

      // Sanity checks on flags.
      {
        // Extrapolated variables can be computed only if in semi-implicit
        // formulation (otherwise the sol_ext vector is not updated).
        Assert(semi_implicit ||
                 !(bitmask_contains(
                     flags, LocalInterpolationFlags::compute_u_ext_loc) ||
                   bitmask_contains(
                     flags, LocalInterpolationFlags::compute_lapl_u_ext_loc) ||
                   bitmask_contains(
                     flags, LocalInterpolationFlags::compute_grad_u_ext_loc) ||
                   bitmask_contains(
                     flags, LocalInterpolationFlags::compute_grad_p_ext_loc)),
               ExcMessage("Cannot interpolate extrapolated quantities if not "
                          "in semi-implicit formulation."));
      }

      // Reset local quantities.
      {
        if (bitmask_contains(flags, LocalInterpolationFlags::compute_u_loc))
          std::fill(u_loc.begin(), u_loc.end(), Tensor<1, dim, NumberType>());

        if (bitmask_contains(flags, LocalInterpolationFlags::compute_u_bdf_loc))
          std::fill(u_bdf_loc.begin(),
                    u_bdf_loc.end(),
                    Tensor<1, dim, double>());

        if (bitmask_contains(flags,
                             LocalInterpolationFlags::compute_time_derivative))
          std::fill(time_derivative_loc.begin(),
                    time_derivative_loc.end(),
                    Tensor<1, dim, NumberType>());

        if (bitmask_contains(flags, LocalInterpolationFlags::compute_advection))
          std::fill(advection_loc.begin(),
                    advection_loc.end(),
                    Tensor<1, dim, NumberType>());

        if (bitmask_contains(flags, LocalInterpolationFlags::compute_diffusion))
          std::fill(diffusion_loc.begin(),
                    diffusion_loc.end(),
                    Tensor<2, dim, NumberType>());

        if (bitmask_contains(flags,
                             LocalInterpolationFlags::compute_grad_u_loc))
          std::fill(grad_u_loc.begin(),
                    grad_u_loc.end(),
                    Tensor<2, dim, NumberType>());

        if (bitmask_contains(flags,
                             LocalInterpolationFlags::compute_symgrad_u_loc))
          std::fill(symgrad_u_loc.begin(),
                    symgrad_u_loc.end(),
                    Tensor<2, dim, NumberType>());

        if (bitmask_contains(flags, LocalInterpolationFlags::compute_div_u_loc))
          std::fill(div_u_loc.begin(), div_u_loc.end(), NumberType(0.0));

        if (bitmask_contains(flags, LocalInterpolationFlags::compute_p_loc))
          std::fill(p_loc.begin(), p_loc.end(), NumberType(0.0));

        if (bitmask_contains(flags, LocalInterpolationFlags::compute_u_ext_loc))
          std::fill(u_ext_loc.begin(),
                    u_ext_loc.end(),
                    Tensor<1, dim, double>());

        if (bitmask_contains(flags,
                             LocalInterpolationFlags::compute_u_star_loc))
          std::fill(u_star_loc.begin(),
                    u_star_loc.end(),
                    Tensor<1, dim, NumberType>());

        if (bitmask_contains(flags, LocalInterpolationFlags::compute_u_ale_loc))
          std::fill(u_ale_loc.begin(),
                    u_ale_loc.end(),
                    Tensor<1, dim, NumberType>());

        if (bitmask_contains(flags,
                             LocalInterpolationFlags::compute_grad_p_loc))
          std::fill(grad_p_loc.begin(),
                    grad_p_loc.end(),
                    Tensor<1, dim, NumberType>());

        if (bitmask_contains(flags,
                             LocalInterpolationFlags::compute_lapl_u_loc))
          std::fill(lapl_u_loc.begin(),
                    lapl_u_loc.end(),
                    Tensor<1, dim, NumberType>());

        if (stabilization_parameters_metric_tensors &&
            bitmask_contains(
              flags, LocalInterpolationFlags::compute_stabilization_terms))
          {
            std::fill(metric_tensor.begin(),
                      metric_tensor.end(),
                      Tensor<2, dim, double>());
            std::fill(metric_vector.begin(),
                      metric_vector.end(),
                      Tensor<1, dim, double>());
          }

        if (bitmask_contains(flags,
                             LocalInterpolationFlags::compute_grad_u_ext_loc))
          std::fill(grad_u_ext_loc.begin(),
                    grad_u_ext_loc.end(),
                    Tensor<2, dim, double>());

        if (bitmask_contains(flags,
                             LocalInterpolationFlags::compute_lapl_u_ext_loc))
          std::fill(lapl_u_ext_loc.begin(),
                    lapl_u_ext_loc.end(),
                    Tensor<1, dim, double>());

        if (bitmask_contains(flags,
                             LocalInterpolationFlags::compute_grad_p_ext_loc))
          std::fill(grad_p_ext_loc.begin(),
                    grad_p_ext_loc.end(),
                    Tensor<1, dim, double>());

        if (bitmask_contains(flags,
                             LocalInterpolationFlags::compute_riis_velocity))
          std::fill(riis_velocity_loc.begin(),
                    riis_velocity_loc.end(),
                    Tensor<1, dim, double>());

        if (bitmask_contains(flags, LocalInterpolationFlags::compute_riis))
          std::fill(riis_loc.begin(),
                    riis_loc.end(),
                    Tensor<1, dim, NumberType>());

        if (bitmask_contains(
              flags, LocalInterpolationFlags::compute_stabilization_terms))
          {
            std::fill(tau_M.begin(), tau_M.end(), NumberType(0.0));
            std::fill(tau_C.begin(), tau_C.end(), NumberType(0.0));
            std::fill(res_M_lhs.begin(),
                      res_M_lhs.end(),
                      Tensor<1, dim, NumberType>());

            if (semi_implicit_vms_les)
              std::fill(res_M_lhs_exp.begin(),
                        res_M_lhs_exp.end(),
                        Tensor<1, dim, NumberType>());

            std::fill(tauM_resM.begin(),
                      tauM_resM.end(),
                      Tensor<1, dim, NumberType>());

            std::fill(tauM_resM_u_star_loc.begin(),
                      tauM_resM_u_star_loc.end(),
                      Tensor<2, dim, NumberType>());
          }
      }

      // Define functions to retrieve content of variants according to the
      // template parameter given to this method.
      auto get_scalar = [](InterpolatedScalarAD &src,
                           const unsigned int &  q) -> NumberType & {
        return std::get<NumberType>(src[q]);
      };

      auto get_vector =
        [](InterpolatedVectorAD &src,
           const unsigned int &  q) -> Tensor<1, dim, NumberType> & {
        return std::get<Tensor<1, dim, NumberType>>(src[q]);
      };

      auto get_tensor =
        [](InterpolatedTensorAD &src,
           const unsigned int &  q) -> Tensor<2, dim, NumberType> & {
        return std::get<Tensor<2, dim, NumberType>>(src[q]);
      };

      // Interpolate local quantities at quadrature points
      // (fe_values.get_function_values() cannot be used for AD types).
      {
        const unsigned int dofs_per_cell = fe->dofs_per_cell;
        const unsigned int n_q_points    = quadrature_formula->size();

        NumberType sol_i     = 0.0;
        double     sol_bdf_i = 0.0;
        double     sol_ext_i = 0.0;

        for (unsigned int i = 0; i < dofs_per_cell; ++i)
          {
            component[i] = fe->system_to_component_index(i).first;

            // We extract the value of sol_i only if needed, to avoid exceptions
            // due to std::get.
            if (bitmask_contains(flags,
                                 LocalInterpolationFlags::compute_u_loc) ||
                bitmask_contains(flags,
                                 LocalInterpolationFlags::compute_grad_u_loc) ||
                bitmask_contains(
                  flags, LocalInterpolationFlags::compute_symgrad_u_loc) ||
                bitmask_contains(flags,
                                 LocalInterpolationFlags::compute_lapl_u_loc) ||
                bitmask_contains(flags,
                                 LocalInterpolationFlags::compute_div_u_loc) ||
                bitmask_contains(flags,
                                 LocalInterpolationFlags::compute_p_loc) ||
                bitmask_contains(flags,
                                 LocalInterpolationFlags::compute_grad_p_loc))
              sol_i = get_scalar(sol_dof, i);

            if (bitmask_contains(flags,
                                 LocalInterpolationFlags::compute_u_bdf_loc))
              sol_bdf_i = sol_bdf[dof_indices[i]];

            if (semi_implicit)
              sol_ext_i = sol_ext[dof_indices[i]];

            if (component[i] < dim) // Velocity DoF.
              {
                for (unsigned int q = 0; q < n_q_points; ++q)
                  {
                    // We manually loop on components (c1, c2 here and below)
                    // for efficiency, since it avoids the construction of
                    // temporaries.

                    // Local velocity.
                    if (bitmask_contains(
                          flags, LocalInterpolationFlags::compute_u_loc))
                      {
                        for (unsigned int c1 = 0; c1 < dim; ++c1)
                          get_vector(u_loc, q)[c1] +=
                            sol_i * fe_values[velocities].value(i, q)[c1];
                      }

                    // Local BDF velocity.
                    if (bitmask_contains(
                          flags, LocalInterpolationFlags::compute_u_bdf_loc))
                      {
                        for (unsigned int c1 = 0; c1 < dim; ++c1)
                          u_bdf_loc[q][c1] +=
                            sol_bdf_i * fe_values[velocities].value(i, q)[c1];
                      }

                    // Local extrapolated velocity.
                    if (bitmask_contains(
                          flags, LocalInterpolationFlags::compute_u_ext_loc))
                      {
                        for (unsigned int c1 = 0; c1 < dim; ++c1)
                          u_ext_loc[q][c1] +=
                            sol_ext_i * fe_values[velocities].value(i, q)[c1];
                      }

                    // Local velocity gradient.
                    if (bitmask_contains(
                          flags, LocalInterpolationFlags::compute_grad_u_loc))
                      {
                        for (unsigned int c1 = 0; c1 < dim; ++c1)
                          for (unsigned int c2 = 0; c2 < dim; ++c2)
                            get_tensor(grad_u_loc, q)[c1][c2] +=
                              sol_i *
                              fe_values[velocities].gradient(i, q)[c1][c2];
                      }

                    // Local extrapolated velocity gradient.
                    if (bitmask_contains(
                          flags,
                          LocalInterpolationFlags::compute_grad_u_ext_loc))
                      {
                        for (unsigned int c1 = 0; c1 < dim; ++c1)
                          for (unsigned int c2 = 0; c2 < dim; ++c2)
                            grad_u_ext_loc[q][c1][c2] +=
                              sol_ext_i *
                              fe_values[velocities].gradient(i, q)[c1][c2];
                      }

                    // Local symmetric gradient.
                    if (bitmask_contains(
                          flags,
                          LocalInterpolationFlags::compute_symgrad_u_loc))
                      {
                        for (unsigned int c1 = 0; c1 < dim; ++c1)
                          for (unsigned int c2 = 0; c2 < dim; ++c2)
                            get_tensor(symgrad_u_loc, q)[c1][c2] +=
                              sol_i * fe_values[velocities].symmetric_gradient(
                                        i, q)[c1][c2];
                      }

                    for (unsigned int c1 = 0; c1 < dim; ++c1)
                      {
                        // Local velocity laplacian.
                        if (bitmask_contains(
                              flags,
                              LocalInterpolationFlags::compute_lapl_u_loc))
                          get_vector(lapl_u_loc, q)[c1] +=
                            sol_i *
                            trace(fe_values[velocities].hessian(i, q)[c1]);

                        // Local extrapolated velocity laplacian.
                        if (bitmask_contains(
                              flags,
                              LocalInterpolationFlags::compute_lapl_u_ext_loc))
                          lapl_u_ext_loc[q][c1] +=
                            sol_ext_i *
                            trace(fe_values[velocities].hessian(i, q)[c1]);
                      }

                    // Local velocity divergence.
                    if (bitmask_contains(
                          flags, LocalInterpolationFlags::compute_div_u_loc))
                      get_scalar(div_u_loc, q) +=
                        sol_i * fe_values[velocities].divergence(i, q);
                  }
              }
            else if (component[i] == dim) // Pressure DoF.
              {
                for (unsigned int q = 0; q < n_q_points; ++q)
                  {
                    // Local pressure.
                    if (bitmask_contains(
                          flags, LocalInterpolationFlags::compute_p_loc))
                      get_scalar(p_loc, q) +=
                        sol_i * fe_values[pressure].value(i, q);

                    // Local pressure gradient.
                    if (bitmask_contains(
                          flags, LocalInterpolationFlags::compute_grad_p_loc))
                      {
                        for (unsigned int c1 = 0; c1 < dim; ++c1)
                          get_vector(grad_p_loc, q)[c1] +=
                            sol_i * fe_values[pressure].gradient(i, q)[c1];
                      }

                    // Local extrapolated pressure gradient.
                    if (bitmask_contains(
                          flags,
                          LocalInterpolationFlags::compute_grad_p_ext_loc))
                      {
                        for (unsigned int c1 = 0; c1 < dim; ++c1)
                          grad_p_ext_loc[q][c1] +=
                            sol_ext_i * fe_values[pressure].gradient(i, q)[c1];
                      }
                  }
              }
          }

        // Evaluate the forcing term at quadrature points.
        if (bitmask_contains(flags,
                             LocalInterpolationFlags::compute_rhs_values))
          forcing_term->vector_value_list(fe_values.get_quadrature_points(),
                                          rhs_values);

        // Compute other quantities at quadrature points.
        for (unsigned int q = 0; q < n_q_points; ++q)
          {
            if (bitmask_contains(
                  flags, LocalInterpolationFlags::compute_time_derivative))
              {
                const double alpha = bdf_handler.get_alpha();
                const bool   rhs   = !ale || !prm_ale_old_mesh_terms ||
                                 override_ale_old_mesh_terms;
                const double c =
                  (prm_density / prm_time_step * fe_values.JxW(q));

                for (unsigned int c1 = 0; c1 < dim; ++c1)
                  {
                    get_vector(time_derivative_loc, q)[c1] =
                      c * (alpha * get_vector(u_loc, q)[c1] -
                           rhs * u_bdf_loc[q][c1]);
                  }
              }

            if (bitmask_contains(flags,
                                 LocalInterpolationFlags::compute_diffusion))
              {
                const bool grad_grad =
                  prm_flag_diffusion_term == DiffusionTerm::Grad_Grad;
                const double k = grad_grad ?
                                   (prm_viscosity * fe_values.JxW(q)) :
                                   (2.0 * prm_viscosity * fe_values.JxW(q));
                const auto &grad = grad_grad ? get_tensor(grad_u_loc, q) :
                                               get_tensor(symgrad_u_loc, q);

                for (unsigned int c1 = 0; c1 < dim; ++c1)
                  for (unsigned int c2 = 0; c2 < dim; ++c2)
                    get_tensor(diffusion_loc, q)[c1][c2] += k * grad[c1][c2];
              }

            if (ale &&
                bitmask_contains(flags,
                                 LocalInterpolationFlags::compute_u_ale_loc))
              {
                get_vector(u_ale_loc, q) = (*quadrature_u_ale)(q);
              }

            // Forcing term: we ignore the pressure DoF since we only need the
            // velocity DoFs in the assembly.
            if (bitmask_contains(flags,
                                 LocalInterpolationFlags::compute_rhs_values))
              {
                for (unsigned int c1 = 0; c1 < dim; ++c1)
                  rhs_u_values[q][c1] = rhs_values[q][c1];
              }

            // Advection field: if we compute it without AD, we use u_loc;
            // otherwise, we choose according to nonlinearity treatment.
            if (bitmask_contains(flags,
                                 LocalInterpolationFlags::compute_u_star_loc))
              {
                if (semi_implicit)
                  get_vector(u_star_loc, q) = u_ext_loc[q];
                else
                  get_vector(u_star_loc, q) = get_vector(u_loc, q);

                if (ale)
                  get_vector(u_star_loc, q) -= get_vector(u_ale_loc, q);
              }

            if (bitmask_contains(flags,
                                 LocalInterpolationFlags::compute_advection))
              {
                for (unsigned int c1 = 0; c1 < dim; ++c1)
                  for (unsigned int c2 = 0; c2 < dim; ++c2)
                    get_vector(advection_loc, q)[c1] +=
                      (prm_density * fe_values.JxW(q)) *
                      get_tensor(grad_u_loc, q)[c1][c2] *
                      get_vector(u_star_loc, q)[c2];
              }

            // RIIS factor (only computed if RIIS is active).
            if (riis)
              {
                if (bitmask_contains(
                      flags, LocalInterpolationFlags::compute_riis_factor))
                  riis_factor_loc[q] = cached_riis_factor[c][q];

                if (bitmask_contains(
                      flags, LocalInterpolationFlags::compute_riis_velocity))
                  riis_handler.compute_velocity_loc(
                    fe_values.quadrature_point(q), riis_velocity_loc[q]);

                if (bitmask_contains(flags,
                                     LocalInterpolationFlags::compute_riis))
                  {
                    const double c = riis_factor_loc[q] * fe_values.JxW(q);
                    for (unsigned int d1 = 0; d1 < dim; ++d1)
                      {
                        get_vector(riis_loc, q)[d1] =
                          c * get_vector(u_loc, q)[d1];

                        if (prm_riis_use_velocity)
                          get_vector(riis_loc, q)[d1] -=
                            c * riis_velocity_loc[q][d1];

                        if (ale && riis_handler.get_move_surfaces_with_ale())
                          get_vector(riis_loc, q)[d1] -=
                            c * get_vector(u_ale_loc, q)[d1];
                      }
                  }
              }

            // Compute metric tensors and vectors if needed.
            if (prm_flag_stabilization != Stabilization::None &&
                stabilization_parameters_metric_tensors &&
                bitmask_contains(
                  flags, LocalInterpolationFlags::compute_stabilization_terms))
              {
                for (unsigned int c1 = 0; c1 < dim; ++c1)
                  for (unsigned int c2 = 0; c2 < dim; ++c2)
                    {
                      metric_vector[q][c1] +=
                        fe_values.inverse_jacobian(q)[c2][c1];

                      for (unsigned int c3 = 0; c3 < dim; ++c3)
                        {
                          metric_tensor[q][c1][c2] +=
                            (fe_values.inverse_jacobian(q)[c3][c1]) *
                            (fe_values.inverse_jacobian(q)[c3][c2]);
                        }
                    }
              }
          }

        // Stabilization terms.
        if (bitmask_contains(
              flags, LocalInterpolationFlags::compute_stabilization_terms))
          {
            for (unsigned int q = 0; q < n_q_points; ++q)
              {
                // Compute stabilization parameter tau_M using metric tensors
                // and vectors [Bazilevs, Calo, Cottrell, Hughes, Reali,
                // Scovazzi 2007].
                if (stabilization_parameters_metric_tensors)
                  get_scalar(tau_M, q) =
                    prm_density * prm_density * get_vector(u_star_loc, q) *
                      metric_tensor[q] * get_vector(u_star_loc, q) +
                    // Cr = 60 * 2^(r-2) = 15 * 2^r.
                    15 * (1 << prm_velocity_degree) * prm_viscosity *
                      prm_viscosity *
                      scalar_product(metric_tensor[q], metric_tensor[q]);

                // Compute stabilization parameter tau_M through cell
                // diameter [Forti, Dede' 2015].
                else
                  {
                    get_scalar(tau_M, q) = prm_density * prm_density *
                                             get_vector(u_star_loc, q) *
                                             get_vector(u_star_loc, q) /
                                             (cell_diameter * cell_diameter) +
                                           // Cr = 60 * 2^(r-2) = 15 * 2^r.
                                           15 * (1 << prm_velocity_degree) *
                                             prm_viscosity * prm_viscosity /
                                             (cell_diameter * cell_diameter *
                                              cell_diameter * cell_diameter);
                  }

                if (riis)
                  get_scalar(tau_M, q) +=
                    riis_factor_loc[q] * riis_factor_loc[q];

                // Static SUPG/VMS-LES
                get_scalar(tau_M, q) += prm_density * prm_density * order_bdf *
                                        order_bdf /
                                        (prm_time_step * prm_time_step);

                get_scalar(tau_M, q) = 1.0 / sqrt(get_scalar(tau_M, q));

                // Compute stabilization parameter tau_C through metric tensors
                // and vectors [Bazilevs, Calo, Cottrell, Hughes, Reali,
                // Scovazzi 2007].
                if (stabilization_parameters_metric_tensors)
                  get_scalar(tau_C, q) =
                    1.0 / (get_scalar(tau_M, q) * metric_vector[q] *
                           metric_vector[q]);

                // Compute stabilization parameter tau_C through cell
                // diameter [Forti, Dede' 2015].
                else
                  get_scalar(tau_C, q) =
                    cell_diameter * cell_diameter / get_scalar(tau_M, q);

                static Tensor<1, dim, NumberType> grad_u_u_star;
                grad_u_u_star =
                  get_tensor(grad_u_loc, q) * get_vector(u_star_loc, q);

                static Tensor<1, dim, NumberType> grad_u_ext_u_star;
                if (semi_implicit_vms_les)
                  grad_u_ext_u_star =
                    grad_u_ext_loc[q] * get_vector(u_star_loc, q);

                for (unsigned int c1 = 0; c1 < dim; ++c1)
                  {
                    get_vector(res_M_lhs, q)[c1] =
                      prm_density *
                        (alpha_bdf * get_vector(u_loc, q)[c1] / prm_time_step +
                         grad_u_u_star[c1]) -
                      prm_viscosity * get_vector(lapl_u_loc, q)[c1] +
                      get_vector(grad_p_loc, q)[c1];

                    res_M_rhs[q][c1] =
                      prm_density * u_bdf_loc[q][c1] / prm_time_step +
                      rhs_u_values[q][c1];

                    if (semi_implicit_vms_les)
                      get_vector(res_M_lhs_exp, q)[c1] =
                        prm_density *
                          (alpha_bdf * u_ext_loc[q][c1] / prm_time_step +
                           grad_u_ext_u_star[c1]) -
                        prm_viscosity * lapl_u_ext_loc[q][c1] +
                        grad_p_ext_loc[q][c1];

                    if (riis)
                      {
                        get_vector(res_M_lhs, q)[c1] +=
                          riis_factor_loc[q] *
                          (ale && riis_handler.get_move_surfaces_with_ale() ?
                             get_vector(u_loc, q)[c1] -
                               get_vector(u_ale_loc, q)[c1] :
                             get_vector(u_loc, q)[c1]);

                        if (prm_riis_use_velocity)
                          res_M_rhs[q][c1] +=
                            riis_factor_loc[q] * riis_velocity_loc[q][c1];

                        if (semi_implicit_vms_les)
                          get_vector(res_M_lhs_exp, q)[c1] +=
                            riis_factor_loc[q] * u_ext_loc[q][c1];
                      }

                    get_vector(tauM_resM, q)[c1] =
                      get_scalar(tau_M, q) *
                      (get_vector(res_M_lhs, q)[c1] - res_M_rhs[q][c1]);

                    for (unsigned int c2 = 0; c2 < dim; ++c2)
                      get_tensor(tauM_resM_u_star_loc, q)[c1][c2] =
                        (prm_stabilization_factor * prm_density *
                         fe_values.JxW(q)) *
                        get_vector(tauM_resM, q)[c1] *
                        get_vector(u_star_loc, q)[c2];
                  }
              }
          }
      }
    }

    /// Interpolate for current cell and face the local face pressure and
    /// (relative) velocity (relative to the boundary velocity, if ALE is
    /// active). If ALE is active, this assumes that ALE velocity has been
    /// interpolated to ale_velocity and ale_velocity_owned.
    template <class NumberType>
    inline void
    interpolate_face_local_quantities(
      const FEFaceValues<dim> &                   fe_face_values,
      const std::vector<types::global_dof_index> &dof_indices) const
    {
      const unsigned int dofs_per_cell = fe->dofs_per_cell;
      const unsigned int n_face_q_points =
        fe_face_values.get_quadrature().size();

      std::fill(u_loc_face.begin(),
                u_loc_face.end(),
                Tensor<1, dim, NumberType>());

      std::fill(p_loc_face.begin(), p_loc_face.end(), NumberType());

      if (prm_flag_non_linear_terms == Nonlinearity::SemiImplicit)
        std::fill(u_ext_loc_face.begin(),
                  u_ext_loc_face.end(),
                  Tensor<1, dim, double>());

      NumberType sol_i     = 0.0;
      double     sol_ext_i = 0.0;

      for (unsigned int i = 0; i < dofs_per_cell; ++i)
        {
          sol_i = std::get<NumberType>(sol_dof[i]);

          if (prm_flag_non_linear_terms == Nonlinearity::SemiImplicit)
            sol_ext_i = sol_ext[dof_indices[i]];

          if (fe->system_to_component_index(i).first < dim) // u.
            {
              for (unsigned int q = 0; q < n_face_q_points; ++q)
                for (unsigned int c = 0; c < dim; ++c)
                  {
                    std::get<Tensor<1, dim, NumberType>>(u_loc_face[q])[c] +=
                      (sol_i - (prm_ale ? ale_velocity[dof_indices[i]] : 0.0)) *
                      fe_face_values[velocities].value(i, q)[c];

                    if (prm_flag_non_linear_terms == Nonlinearity::SemiImplicit)
                      u_ext_loc_face[q][c] +=
                        (sol_ext_i -
                         (prm_ale ? ale_velocity[dof_indices[i]] : 0.0)) *
                        fe_face_values[velocities].value(i, q)[c];
                  }
            }
          else // p.
            {
              for (unsigned int q = 0; q < n_face_q_points; ++q)
                std::get<NumberType>(p_loc_face[q]) +=
                  sol_i * fe_face_values[pressure].value(i, q);
            }
        }
    }

    /**
     * @brief Compute the residual for current cell and given DoF.
     *
     * The function computes the contribution to the residual for one cell and
     * one DoF, tested against the given test function. The function assumes
     * that fe_values has been correctly initalized for current cell, and that
     * the relevant local quantities have been already interpolated by calling
     * @ref interpolate_local_quantities.
     *
     * @param[in] i Index of current DoF.
     * @param[in] test_velocity An abstract object representing the test function
     * for velocity. For system assembly, it is a FEValuesViews::Vector (as
     * obtained by (*fe_values)[velocities]).
     * @param[in] test_pressure An abstract object representing the test function
     * for pressure. For system assembly, it is a FEValuesViews::Scalar (as
     * obtained by (*fe_values)[pressure]).
     *
     * @return The contribution to the residual for current cell and DoF.
     */
    template <class NumberType,
              class QuadratureTestFunctionVelocity,
              class QuadratureTestFunctionPressure>
    inline NumberType
    compute_residual(const unsigned int &                  i,
                     const QuadratureTestFunctionVelocity &test_velocity,
                     const QuadratureTestFunctionPressure &test_pressure) const
    {
      // Define functions to retrieve content of variants according to the
      // template parameter given to this method.
      auto get_scalar = [](const InterpolatedScalarAD &src,
                           const unsigned int &q) -> const NumberType & {
        return std::get<NumberType>(src[q]);
      };

      auto get_vector =
        [](const InterpolatedVectorAD &src,
           const unsigned int &q) -> const Tensor<1, dim, NumberType> & {
        return std::get<Tensor<1, dim, NumberType>>(src[q]);
      };

      auto get_tensor =
        [](const InterpolatedTensorAD &src,
           const unsigned int &q) -> const Tensor<2, dim, NumberType> & {
        return std::get<Tensor<2, dim, NumberType>>(src[q]);
      };

      // Flags.
      // We redefine them locally as const to facilitate compiler optimization
      // (is it necessary?).
      const DiffusionTerm diffusion_term = prm_flag_diffusion_term;
      const Stabilization stabilization  = prm_flag_stabilization;
      const bool          riis           = prm_riis;

      const bool semi_implicit =
        prm_flag_non_linear_terms == Nonlinearity::SemiImplicit;

      const bool soft_semi_implicit_vms_les = prm_soft_semi_implicit_vms_les;

      const unsigned int n_q_points = quadrature_formula->size();

      NumberType res = 0.0;

      if (component[i] < dim) // u.
        {
          for (unsigned int q = 0; q < n_q_points; ++q)
            {
              const auto &test_grad =
                diffusion_term == DiffusionTerm::SymGrad_SymGrad ?
                  test_velocity.symmetric_gradient(i, q) :
                  test_velocity.gradient(i, q);

              // non-stationary term
              for (unsigned int d1 = 0; d1 < dim; ++d1)
                {
                  res += get_vector(time_derivative_loc, q)[d1] *
                         test_velocity.value(i, q)[d1];

                  // non-linear term
                  res += get_vector(advection_loc, q)[d1] *
                         test_velocity.value(i, q)[d1];

                  // diffusive term
                  for (unsigned int d2 = 0; d2 < dim; ++d2)
                    res +=
                      get_tensor(diffusion_loc, q)[d1][d2] * test_grad[d1][d2];

                  // forcing term
                  res -= rhs_u_values[q][d1] * test_velocity.value(i, q)[d1] *
                         fe_values->JxW(q);

                  // resistive term
                  if (riis)
                    res += get_vector(riis_loc, q)[d1] *
                           test_velocity.value(i, q)[d1];
                }

              // pressure term
              res -= get_scalar(p_loc, q) * test_velocity.divergence(i, q) *
                     fe_values->JxW(q);

              // stabilization and turbulence terms
              if (stabilization != Stabilization::None)
                {
                  if (stabilization == Stabilization::VMS_LES)
                    {
                      res +=
                        2.0 *
                        scalar_product(get_tensor(tauM_resM_u_star_loc, q),
                                       test_velocity.symmetric_gradient(i, q));

                      if (semi_implicit)
                        {
                          if (soft_semi_implicit_vms_les)
                            {
                              res -=
                                (prm_stabilization_factor * prm_density *
                                 fe_values->JxW(q)) *
                                scalar_product(
                                  outer_product(get_scalar(tau_M, q) *
                                                  (get_vector(res_M_lhs_exp,
                                                              q) -
                                                   res_M_rhs[q]),
                                                get_vector(tauM_resM, q)),
                                  test_velocity.gradient(i, q));
                            }
                          else // hard semi-implicit VMS-LES
                            {
                              res +=
                                (prm_stabilization_factor * prm_density *
                                 fe_values->JxW(q)) *
                                scalar_product(
                                  outer_product(get_vector(tauM_resM, q),
                                                get_scalar(tau_M, q) *
                                                  res_M_rhs[q]) -
                                    outer_product(get_scalar(tau_M, q) *
                                                    (get_vector(res_M_lhs_exp,
                                                                q) -
                                                     res_M_rhs[q]),
                                                  get_scalar(tau_M, q) *
                                                    get_vector(res_M_lhs, q)),
                                  test_velocity.gradient(i, q));
                            }
                        }
                      else // implicit
                        {
                          res -= (prm_stabilization_factor * prm_density *
                                  fe_values->JxW(q)) *
                                 scalar_product(
                                   outer_product(get_vector(tauM_resM, q),
                                                 get_vector(tauM_resM, q)),
                                   test_velocity.gradient(i, q));
                        }
                    }
                  else // classic SUPG term - momentum (advection)
                    {
                      for (unsigned int d1 = 0; d1 < dim; ++d1)
                        for (unsigned int d2 = 0; d2 < dim; ++d2)
                          res += get_tensor(tauM_resM_u_star_loc, q)[d1][d2] *
                                 test_velocity.gradient(i, q)[d1][d2];
                    }

                  // classic SUPG term - continuity
                  res += prm_stabilization_factor * get_scalar(tau_C, q) *
                         get_scalar(div_u_loc, q) *
                         test_velocity.divergence(i, q) * fe_values->JxW(q);
                }
            }
        }
      else if (component[i] == dim) // p.
        {
          for (unsigned int q = 0; q < n_q_points; ++q)
            {
              // mass continuity term
              res += get_scalar(div_u_loc, q) * test_pressure.value(i, q) *
                     fe_values->JxW(q);

              // classical SUPG term: momentum (pressure)
              if (stabilization != Stabilization::None)
                {
                  for (unsigned int d1 = 0; d1 < dim; ++d1)
                    res += (prm_stabilization_factor * fe_values->JxW(q)) *
                           get_vector(tauM_resM, q)[d1] *
                           test_pressure.gradient(i, q)[d1];
                }
            }
        }

      return res;
    }

    /**
     * @brief Compute the terms in the residual corresponding to backflow
     * stabilization.
     *
     * The function computes the contribution to the residual for one face and
     * one DoF, tested against the given test function. The function assumes
     * that fe_face_values has been correctly initalized for current cell and
     * face.
     *
     * @param[in] i Index of the DoF.
     * @param[in] test_velocity An abstract object representing the test function
     * for velocity. For system assembly, it is a FEValuesViews::Vector (as
     * obtained by (*fe_face_values)[velocities]).
     */
    template <class NumberType, class QuadratureTestFunctionVelocity>
    inline NumberType
    compute_residual_backflow_stabilization(
      const unsigned int &                  i,
      const QuadratureTestFunctionVelocity &test_velocity) const
    {
      const unsigned int n_face_q_points = face_quadrature_formula->size();
      NumberType         res             = 0.0;

      if (component[i] < dim) // u.
        {
          for (unsigned int q = 0; q < n_face_q_points; ++q)
            {
              if (prm_flag_non_linear_terms == Nonlinearity::Implicit)
                u_dot_n = scalar_product(std::get<Tensor<1, dim, NumberType>>(
                                           u_loc_face[q]),
                                         fe_face_values->normal_vector(q));
              else
                u_dot_n =
                  NumberType(scalar_product(u_ext_loc_face[q],
                                            fe_face_values->normal_vector(q)));

              u_dot_v = scalar_product(std::get<Tensor<1, dim, NumberType>>(
                                         u_loc_face[q]),
                                       test_velocity.value(i, q));

              res -= prm_beta_backflow_stabilization * prm_density *
                     std::min(std::get<NumberType>(u_dot_n), 0.0) *
                     std::get<NumberType>(u_dot_v) * fe_face_values->JxW(q);
            }
        }

      return res;
    }

    /// Standalone flag: if false, time discretization parameters will not be
    /// declared and will have to be set through the set_time_discretization
    /// method.
    bool standalone;

    /// Callback function to be invoked at the end of each timestep.
    std::function<void()> end_timestep_callback = {};

    /// @name Parameters read from file.
    /// @{

    unsigned int prm_velocity_degree; ///< degree of the velocity FE space.
    unsigned int prm_pressure_degree; ///< degree of the pressure FE space.

    double prm_density; ///< Density @f$[\mathrm{kg \cdot m^{-3}}]@f$.

    double prm_viscosity; ///< Viscosity for Newtonian model @f$[\mathrm{Pa
                          ///< \cdot s}]@f$.

    /// Run-time flag for the diffusive term formulation.
    DiffusionTerm prm_flag_diffusion_term = DiffusionTerm::SymGrad_Grad;

    /// Labels for mesh subvolumes and corresponding sets of volume tags.
    std::map<std::string, std::set<types::material_id>> prm_volume_tags;

    /// Toggle computation of average velocity in control volumes.
    std::vector<bool> prm_control_volumes_velocity;

    /// Toggle output of control volumes signed distance.
    bool prm_output_control_volume_distance;

    /// Toggle activate backflow stabilization.
    bool prm_backflow_stabilization;

    /// Tags of backflow stabilized Neumann boundaries.
    std::set<types::boundary_id> prm_tags_backflow_stabilization;

    /// Parameter beta for the backflow stabilization.
    double prm_beta_backflow_stabilization;

    /// Toggle activate periodic boundary conditions.
    /// @warning Only available for hexahedral meshes.
    bool prm_periodic_bcs;

    /// Run-time flag for the stabilization or turbulence model.
    Stabilization prm_flag_stabilization;

    /// Toggle using the soft or the hard semi-implicit VMS-LES formulation.
    bool prm_soft_semi_implicit_vms_les;

    /// Toggle computing the stabilization parameters through 'Metric-tensors'
    /// or 'Cell-diameter'
    bool prm_stabilization_parameters_metric_tensors;

    /// Constant factor @f$C_\text{S}@f$ multiplying all stabilization terms.
    double prm_stabilization_factor;

    /// Tags at which to compute outgoing flowrates.
    std::vector<types::boundary_id> prm_flowrates_tags;

    /// Names of flowrates for CSV exporting.
    std::vector<std::string> prm_flowrates_names;

    /// Tags at which to compute average pressures.
    std::vector<types::boundary_id> prm_pressures_tags;

    /// Names of pressures for CSV exporting.
    std::vector<std::string> prm_pressures_names;

    /// Toggle computation and exporting of integral fluid properties.
    bool prm_compute_fluid_energies;

    /// Toggle computation and exporting of space-averaged stabilization
    /// parameters.
    bool prm_compute_space_averaged_stabilization_parameters;

    /// Toggle activate RIIS term.
    bool prm_riis;

    /// Toggle use of surface velocity u_Gamma (if false, quasi-static
    /// approximation u_Gamma=0).
    bool prm_riis_use_velocity;

    /// Toggle using ALE formulation imposing the wall displacement.
    bool prm_ale;

    /// Toggle assembly of time-derivative rhs on old mesh
    /// (cf. @ref res_ale_old).
    bool prm_ale_old_mesh_terms;

    /// Toggle ALE displacement import from file.
    bool prm_ale_from_vtp;

    /// Toggle application of initial ALE displacement.
    bool prm_ale_apply_initial_displacement;

    /// Toggle saving the mesh in each output file.
    bool prm_export_mesh_always;

    unsigned int prm_bdf_order;  ///< BDF order.
    double       prm_time_init;  ///< Initial simulation time.
    double       prm_time_final; ///< Final simulation time.
    double       prm_time_step;  ///< Time step.
    double       prm_period;     ///< Period.

    bool         prm_restart; ///< Toggle restart from a previous simulation.
    std::string  prm_restart_basename; ///< Basename to be imported for restart.
    unsigned int prm_restart_timestep_init; ///< Initial timestep to be imported
                                            ///< for restart.

    std::string prm_preconditioner; ///< Preconditioner.

    /// Run-time flag for the non-linear term treatment.
    Nonlinearity prm_flag_non_linear_terms;

    bool   prm_enable_output;        ///< Toggle save results.
    double prm_output_min_time;      ///< Save results starting from a
                                     ///< user-set timestep.
    bool prm_enable_boundary_output; ///< Toggle output of solution on the
                                     ///< boundary.

    bool prm_serialize_solution; ///< Toggle serialization of the solution for a
                                 ///< possible restart.
    unsigned int
      prm_serialize_every_n_timesteps; ///< Serialize every n timesteps.

    unsigned int prm_output_every_n_timesteps; ///< Save every n timesteps.

    unsigned int
      prm_output_boundary_every_n_timesteps; ///< Save boundary solution every
                                             ///< n timesteps.

    std::string prm_output_basename; ///< Output basename.

    bool prm_enable_csv; ///< Boolean value to enable or disable the exporting
                         ///< of CSV file.
    std::string  prm_csv_filename;                ///< Filename for CSV output.
    unsigned int prm_print_csv_every_n_timesteps; ///< Frequency of CSV output.

    /// CSV filename to optionally read initial volumes when restart is enabled.
    std::string prm_input_csv_filename;

    /// @}

    /// @name Non-linear and linear system.
    /// @{

    /// Jacobian matrix coming from the linearization of the equation.
    LinAlg::MPI::BlockSparseMatrix jac;

    /// Residual vector coming from the linearization of the equation.
    LinAlg::MPI::BlockVector res;

    /// Sparsity pattern of the jacobian.
    BlockDynamicSparsityPattern dsp_jac;

    /// Set of locally relevant DoFs.
    IndexSet locally_relevant_dofs;

    /// @name Helpers.
    /// @{

    /// Non-linear solver handler.
    utils::NonLinearSolverHandler<LinAlg::MPI::BlockVector> non_linear_solver;

    /// Linear solver handler.
    utils::LinearSolverHandler<LinAlg::MPI::BlockVector> linear_solver;

    /// @}

    /// @name Dependencies.
    /// @{

    /// Handler for the RIIS term.
    RIISHandler riis_handler;

    /// Moves the mesh and provides ALE velocity.
    std::shared_ptr<ALEHandler> ale_handler;

    /// @}

    /// @name Finite element discretization.
    /// @{

    /// Mesh.
    std::shared_ptr<utils::MeshHandler> triangulation;

    /// Finite element space.
    std::unique_ptr<FESystem<dim>> fe;

    /// Dof handler.
    DoFHandler<dim> dof_handler;

    /// Dof handler for a scalar field. Used for turbulent viscosity of
    /// the sigma-LES model, projected onto the pressure finite element space.
    DoFHandler<dim> dof_handler_scalar;

    /// Dof handler for a piecewise linear scalar field. Used for
    /// control-volume-based quantities.
    DoFHandler<dim> dof_handler_scalar_linear;

    /// DoF handler callback, used to renumber dofs upon construction.
    std::function<void(DoFHandler<dim> &)> dof_renumbering = {};

    /// @}

    /// @name Recoverying fine-scale velocity at DoFs.
    ///
    /// Used for output purposes and for a possible simulation restart.
    /// @{

    /// Finite element space for the velocity block.
    std::unique_ptr<FESystem<dim>> fe_velocity;

    /// Dof handler for the velocity block.
    DoFHandler<dim> dof_handler_velocity;

    /// @f$L^2@f$ projector onto the velocity finite element space.
    /// Used for fine-scale velocities and/or for RIIS stress.
    std::unique_ptr<utils::ProjectionL2> project_l2_velocity_space;

    /// Fine-scale velocity projected on DoFs.
    LinAlg::MPI::Vector velocity_fine_scale_fem;

    /// Fine-scale velocity projected on DoFs, withouth ghost entries.
    LinAlg::MPI::Vector velocity_fine_scale_fem_owned;

    /// BDF fine-scale velocity projected on DoFs.
    std::vector<LinAlg::MPI::Vector> velocity_fine_scale_bdf_fem;

    /// Fine-scale velocity without ghost entries projected on DoFs.
    std::vector<LinAlg::MPI::Vector> velocity_fine_scale_bdf_fem_owned;

    /// @}

    /// Owned dofs per block.
    std::vector<IndexSet> owned_dofs;

    /// Relevant dofs per block.
    std::vector<IndexSet> relevant_dofs;

    /// Quadrature formula.
    std::unique_ptr<Quadrature<dim>> quadrature_formula;

    /// Quadrature formula for faces.
    std::unique_ptr<Quadrature<dim - 1>> face_quadrature_formula;

    /// Evaluation of @ref fe on a cell.
    std::unique_ptr<FEValues<dim>> fe_values;

    /// Evaluation of @ref fe on a face.
    std::unique_ptr<FEFaceValues<dim>> fe_face_values;

    /// Evaluation of @ref fe_velocity on a cell.
    std::unique_ptr<FEValues<dim>> fe_velocity_values;

    /// @name Boundary conditions.
    /// @{

    /// Constraints to impose homogeneous boundary conditions for Newton.
    AffineConstraints<double> zero_constraints;

    /// Constraints to impose Dirichlet normal or tangential flux boundary
    /// conditions to the initial Newton guess.
    AffineConstraints<double> dirichlet_flux_constraints;

    /// Constraints to impose periodic boundary conditions.
    AffineConstraints<double> periodicity_constraints;

    /// Dirichlet boundary conditions.
    std::vector<utils::BC<utils::FunctionDirichlet>> dirichlet_bcs;

    /// Defective flowrate boundary conditions.
    std::vector<utils::BC<utils::FunctionNeumann>> defective_flow_bcs;

    /// Number of defective flowrate boundary conditions.
    size_t n_defective_flowrate_bcs = 0;

    /// Neumann boundary conditions.
    std::vector<utils::BC<utils::FunctionNeumann>> neumann_bcs;

    /// Normal flux boundary conditions.
    std::vector<utils::BC<utils::FunctionDirichlet>> dirichlet_normal_flux_bcs;

    /// Tangential flux boundary conditions.
    std::vector<utils::BC<utils::FunctionDirichlet>>
      dirichlet_tangential_flux_bcs;

    /// Boolean variable to check if flux BC vectors are not empty.
    bool dirichlet_flux_bcs = false;

    /// Periodic boundary conditions.
    std::vector<PeriodicBC> periodic_bcs;

    /// Boundary conditions handler.
    utils::BCHandler bc_handler;

    /// Flag indicating whether set_BCs was called.
    bool bcs_are_set = false;

    /// Flag indicating whether vectors for Dirichlet BCs were initialized by a
    /// call to @ref utils::BCHandler::apply_dirichlet.
    bool bcs_dirichlet_initialized = false;

    /// Forcing term.
    std::shared_ptr<Function<dim>> forcing_term;

    /// Non-null initial condition.
    std::shared_ptr<Function<dim>> initial_condition;

    /// @}

    /// @name ALE utilities.
    /// @{

    /// Function to impose ALE motion.
    std::shared_ptr<Function<dim>> ale_displacement_function;

    /// Set of tags where to impose the no-slip condition with the ALE
    /// velocity.
    std::set<types::boundary_id> ale_tags;

    /// Component mask for ALE boundary conditions.
    ComponentMask ale_component_mask;

    /// @}

    /**
     * @brief Part of the residual vector that is assembled on the old mesh.
     * Used only if both @ref prm_ale and @ref prm_ale_old_mesh_terms
     * are @a true.
     *
     * It contains the term
     * @f[
     * - \left ( \rho \frac{\mathbf{u}^n_{\text{BDF}\sigma}}{\Delta t} ,
     * \mathbf{v} \right )_{\Omega^n},
     * @f]
     * which is strictly correct only for Implicit Euler.
     */
    LinAlg::MPI::BlockVector res_ale_old;

    /// @name Computation of submesh volumes.
    /// @{

    /// Labels for volumes in the CSV file: they are constructed from volume
    /// labels, making every letter lowercase and replacing spaces with
    /// underscores.
    std::map<std::string, std::string> volume_csv_labels;

    /// Map from the volume label to the deque of the corresponding volumes.
    /// The deque contains in i-th position the volume i timesteps ago (e.g.
    /// element 0 is the current volume, element 1 is the volume at previous
    /// timestep etc.). The number of stored volumes is dictated by @ref n_stored_volumes.
    std::map<std::string, std::deque<double>> volumes;

    /// Number of consecutive timesteps for which the volume is stored.
    static constexpr unsigned int n_stored_volumes = 3;

    /// @}

    /// Preconditioner instance.
    std::unique_ptr<FluidDynamicsPreconditioners::InexactBlockLU>
      preconditioner;

    /// Solution vector.
    LinAlg::MPI::BlockVector sol;

    /// Solution BDF.
    mutable LinAlg::MPI::BlockVector sol_bdf;

    /// Solution extrapolation.
    mutable LinAlg::MPI::BlockVector sol_ext;

    /// Solution vector, without ghost entries.
    LinAlg::MPI::BlockVector sol_owned;

    /// Fine-scale velocity at quadrature points.
    /// The two vector indices refer to the cell (local to current processor)
    /// and to the quadrature node.
    std::vector<std::vector<Tensor<1, dim, double>>> velocity_fine_scale;

    /// BDF time advancing handler.
    utils::BDFHandler<LinAlg::MPI::BlockVector> bdf_handler;

    double       time;                 ///< Current simulation time.
    unsigned int timestep_number;      ///< Index of current time step.
    unsigned int timestep_number_init; ///< Index of initial time step.

    utils::CSVWriter csv_writer; ///< log CSV writer.

    /// @name Space-averaged fluid properties.
    /// @{

    double total_kinetic_energy; ///< Total kinetic energy, for CSV output.
    double total_kinetic_energy_fine_scales; ///< Total kinetic energy computed
                                             ///< with fine scale velocity, for
                                             ///< CSV output.
    double enstrophy;                        ///< Enstrophy, for CSV output.
    double tau_M_avg; ///< Space-averaged stabilization parameter tau_M, for
                      ///< CSV output.
    double tau_C_avg; ///< Space-averaged stabilization parameter tau_C, for CSV
                      ///< output.

    /// Control volumes.
    std::vector<utils::ControlVolume> control_volumes;

    /// Average pressure in control volumes.
    std::vector<double> pressure_control_volumes;

    /// Average velocity magnitude in control volumes.
    std::vector<double> velocity_magnitude_control_volumes;

    /// Average velocity components in control volumes.
    std::vector<std::array<double, dim>> velocity_control_volumes;

    /// @}

    /** @name Preallocated variables for assembly.
     *  Variables that are used by the @ref assemble_callback_cell function (e.g.
     * to store evaluations on a cell of various finite element functions) are
     * declared here. The vectors are allocated by @ref setup_system.
     * Notice that most of these quantities are declared <kbd>mutable</kbd>,
     * because they are used mostly to store temporarily the interpolated
     * quantities, even by methods such as
     * @ref compute_boundary_flow_and_pressure that are inherently const.
     */
    /// @{

    /// Scalar quantity evaluated on quadrature nodes.
    using InterpolatedScalar = std::vector<double>;

    /// Scalar quantity evaluated on quadrature nodes, optionally with automatic
    /// differentiation.
    using InterpolatedScalarAD = std::vector<std::variant<double, double_AD>>;

    /// Vector quantity evaluated on quadrature nodes.
    using InterpolatedVector = std::vector<Tensor<1, dim, double>>;

    /// Vector quantity evaluated on quadrature nodes, optionally with automatic
    /// differentiation.
    using InterpolatedVectorAD = std::vector<
      std::variant<Tensor<1, dim, double>, Tensor<1, dim, double_AD>>>;

    /// Tensor quantity evaluated on quadrature nodes.
    using InterpolatedTensor = std::vector<Tensor<2, dim, double>>;

    /// Tensor quantity evaluated on quadrature nodes, optionally with automatic
    /// differentiation.
    using InterpolatedTensorAD = std::vector<
      std::variant<Tensor<2, dim, double>, Tensor<2, dim, double_AD>>>;

    /// FE values extractor for velocity.
    const FEValuesExtractors::Vector velocities;

    /// FE values extractor for pressure.
    const FEValuesExtractors::Scalar pressure;

    /// Component for each DoF.
    mutable std::vector<unsigned int> component;

    /// Residual vector.
    mutable InterpolatedScalarAD res_loc;

    /// Solution DoFs.
    mutable InterpolatedScalarAD sol_dof;

    /// Local velocity with automatic differentiation.
    mutable InterpolatedVectorAD u_loc;

    /// Local BDF velocity term.
    mutable InterpolatedVector u_bdf_loc;

    /// Local extrapolated velocity.
    mutable InterpolatedVector u_ext_loc;

    /// Local advection term.
    mutable InterpolatedVectorAD u_star_loc;

    /// Local ALE velocity.
    mutable InterpolatedVectorAD u_ale_loc;

    /// Local velocity gradient.
    mutable InterpolatedTensorAD grad_u_loc;

    /// Local velocity symmetric gradient.
    mutable InterpolatedTensorAD symgrad_u_loc;

    /// Local velocity divergence.
    mutable InterpolatedScalarAD div_u_loc;

    /// Local pressure.
    mutable InterpolatedScalarAD p_loc;

    /// Local velocity laplacian.
    mutable InterpolatedVectorAD lapl_u_loc;

    /// Local extrapolated velocity laplacian.
    mutable InterpolatedVector lapl_u_ext_loc;

    /// Local pressure gradient.
    mutable InterpolatedVectorAD grad_p_loc;

    /// Local extrapolated pressure gradient.
    mutable InterpolatedVector grad_p_ext_loc;

    /// Local extrapolated velocity gradient.
    mutable InterpolatedTensor grad_u_ext_loc;

    /// Metric tensor.
    mutable InterpolatedTensor metric_tensor;

    /// Metric vector.
    mutable InterpolatedVector metric_vector;

    /// Local evaluation of @f$\tau_M@f$.
    mutable InterpolatedScalarAD tau_M;

    /// Local evaluation of @f$\tau_C@f$.
    mutable InterpolatedScalarAD tau_C;

    /// @f$\mathbf r_M^{LHS}@f$.
    mutable InterpolatedVectorAD res_M_lhs;

    /// @f$\mathbf r_M^{LHS}@f$ in the explicit case.
    mutable InterpolatedVectorAD res_M_lhs_exp;

    /// @f$\mathbf r_M^{RHS}@f$.
    mutable InterpolatedVector res_M_rhs;

    /// @f$\tau_M\,\mathbf r_M@f$.
    mutable InterpolatedVectorAD tauM_resM;

    /// Local forcing term.
    mutable std::vector<Vector<double>> rhs_values;

    /// Local forcing term, velocity components.
    mutable InterpolatedVector rhs_u_values;

    /// Local RIIS factor.
    mutable InterpolatedScalar riis_factor_loc;

    /// Local RIIS velocity.
    mutable InterpolatedVector riis_velocity_loc;

    /// Local velocity on a face.
    mutable InterpolatedVectorAD u_loc_face;

    /// Local pressure on a face.
    mutable InterpolatedScalarAD p_loc_face;

    /// Extrapolated local velocity on a face.
    mutable InterpolatedVector u_ext_loc_face;

    /// Evaluator for RIIS factor on quadrature points.
    mutable std::unique_ptr<QuadratureRIIS<double>> quadrature_riis;

    /// Evaluator for ALE velocity on quadrature points.
    mutable std::unique_ptr<QuadratureALEVelocity> quadrature_u_ale;

    /// Velocity component normal to the boundary (for backflow stabilization).
    mutable std::variant<double_AD, double> u_dot_n;

    /// Scalar product of velocity times test function (for backflow
    /// stabilization).
    mutable std::variant<double_AD, double> u_dot_v;

    /// Vector to store the ALE velocity interpolated on FluidDynamics' DoF
    /// handler (needed for flow computation and backflow stabilization).
    mutable LinAlg::MPI::Vector ale_velocity;

    /// Vector to store the ALE velocity interpolated on FluidDynamics' DoF
    /// handler, owned DoFs.
    mutable LinAlg::MPI::Vector ale_velocity_owned;

    /// Flag that indicates whether ale_velocity and ale_velocity_owned have
    /// been initialized.
    mutable bool ale_velocity_initialized;

    /// Local values of alpha times u_loc.
    mutable InterpolatedVectorAD time_derivative_loc;

    /// Local values of grad_u times u_star.
    mutable InterpolatedVectorAD advection_loc;

    /// Local values of the diffusive term.
    mutable InterpolatedTensorAD diffusion_loc;

    /// Local evaluation of the RIIS related terms.
    mutable InterpolatedVectorAD riis_loc;

    /// Local values of the outer product of tauM_resM and u_star.
    mutable InterpolatedTensorAD tauM_resM_u_star_loc;

    /// Cached values of the RIIS factor on all cells and quadrature nodes.
    std::vector<std::vector<double>> cached_riis_factor;

    /// @}
  };
} // namespace lifex

#endif /* LIFEX_PHYSICS_FLUID_DYNAMICS_HPP_ */
