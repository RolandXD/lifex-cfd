/********************************************************************************
  Copyright (C) 2019 - 2023 by the lifex authors.

  This file is part of lifex.

  lifex is free software; you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  lifex is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with lifex.  If not, see <http://www.gnu.org/licenses/>.
********************************************************************************/

/**
 * @file
 *
 * @author Marco Fedele <marco.fedele@polimi.it>.
 * @author Ivan Fumagalli <ivan.fumagalli@polimi.it>.
 * @author Pasquale Claudio Africa <pasqualeclaudio.africa@polimi.it>.
 * @author Alberto Zingaro <alberto.zingaro@polimi.it>.
 * @author Michele Bucelli <michele.bucelli@polimi.it>.
 * @author Anna Peruso <anna.peruso@mail.polimi.it>.
 * @author Thomas Rimbot <thomas.rimbot@mail.polimi.it>.
 * @author Luca Crugnola <luca.crugnola@polimi.it>.
 */

#include "core/source/io/csv_reader.hpp"
#include "core/source/io/data_writer.hpp"
#include "core/source/io/serialization.hpp"

#include "core/source/numerics/tools.hpp"

#include "source/fluid_dynamics.hpp"

#include "source/helpers/fluid_dynamics_preconditioners.hpp"

#include <deal.II/dofs/dof_renumbering.h>

#include <deal.II/numerics/data_out_faces.h>

#include <limits>
#include <map>
#include <tuple>

namespace lifex
{
  FluidDynamics::FluidDynamics(const std::string &subsection,
                               const bool &       standalone_)
    : CoreModel(subsection)
    , standalone(standalone_)
    , prm_restart(false)
    , prm_enable_csv(false)
    , non_linear_solver(prm_subsection_path + " / Non-linear solver")
    , linear_solver(prm_subsection_path + " / Linear solver",
                    {"GMRES", "BiCGStab", "FGMRES"},
                    "GMRES")
    , riis_handler(prm_subsection_path +
                     " / Resistive Immersed Implicit Surface",
                   *this)
    , ale_handler(
        std::make_shared<ALEHandler>(prm_subsection_path +
                                       " / Arbitrary Lagrangian Eulerian",
                                     false))
    , triangulation(std::make_shared<utils::MeshHandler>(
        prm_subsection_path,
        mpi_comm,
        std::initializer_list<utils::MeshHandler::GeometryType>(
          {utils::MeshHandler::GeometryType::File,
           utils::MeshHandler::GeometryType::Hypercube,
           utils::MeshHandler::GeometryType::Cylinder,
           utils::MeshHandler::GeometryType::ChannelWithCylinder})))
    , bc_handler(dof_handler)
    , forcing_term(std::make_shared<Functions::ZeroFunction<dim>>(dim + 1))
    , initial_condition(std::make_shared<Functions::ZeroFunction<dim>>(dim + 1))
    , timestep_number(0)
    , timestep_number_init(0)
    , csv_writer(mpi_rank == 0)
    , velocities(0)
    , pressure(dim)
    , ale_velocity_initialized(false)
  {}

  void
  FluidDynamics::declare_parameters(ParamHandler &params) const
  {
    // Declare parameters.
    triangulation->declare_parameters(params);

    params.enter_subsection_path(prm_subsection_path);

    params.set_verbosity(VerbosityParam::Minimal);
    {
      params.enter_subsection("Mesh and space discretization");
      {
        params.declare_entry("Velocity FE space degree",
                             "1",
                             Patterns::Integer(1),
                             "Degree of the velocity FE space.");

        params.declare_entry("Pressure FE space degree",
                             "1",
                             Patterns::Integer(1),
                             "Degree of the pressure FE space.");
      }
      params.leave_subsection();

      params.enter_subsection("Physical constants and models");
      {
        params.declare_entry("Viscosity",
                             "3.5e-3",
                             Patterns::Double(0),
                             "Viscosity [Pa * s].");

        params.declare_entry("Density",
                             "1.06e3",
                             Patterns::Double(0),
                             "Density [kg * m^{-3}].");

        params.declare_entry_selection(
          "Diffusion term formulation",
          "SymGrad-Grad",
          "SymGrad-Grad | Grad-Grad | SymGrad-SymGrad",
          "Treatment of the Navier-Stokes diffusive term.");
      }
      params.leave_subsection();
    }
    params.reset_verbosity();

    params.set_verbosity(VerbosityParam::Standard);
    params.enter_subsection("Volume tags");
    {
      params.declare_entry("Labels",
                           "",
                           Patterns::List(Patterns::Anything()),
                           "A comma-separated list of labels for volume-tagged "
                           "regions whose volume is to be computed over time. "
                           "The regions are identified by the tags below.");

      params.declare_entry(
        "Material IDs",
        "",
        Patterns::List(Patterns::List(Patterns::Integer(0),
                                      0,
                                      Patterns::List::max_int_value,
                                      " "),
                       0),
        "A list-of-lists specifying for each volume in \"Labels of "
        "volume-tagged regions\" the corresponding material IDs. For "
        "example: 1 2 3, 4 5 indicates that tags 1, 2 and 3 correspond to the "
        "first volume while tags 4 and 5 correspond to the second.");
    }
    params.leave_subsection();
    params.reset_verbosity();

    params.set_verbosity(VerbosityParam::Minimal);
    params.enter_subsection("Backflow stabilization");
    {
      params.declare_entry("Active",
                           "false",
                           Patterns::Bool(),
                           "Toggle activation of the backflow stabilization.");

      params.declare_entry(
        "Tags of backflow stabilized Neumann boundaries",
        "",
        Patterns::List(Patterns::Integer(0),
                       0,
                       std::numeric_limits<unsigned int>::max(),
                       ","),
        "Neumann boundary tags where the backflow stabilization must be "
        "applied.");

      params.declare_entry("Beta",
                           "1.0",
                           Patterns::Double(),
                           "Coefficient beta for the backflow stabilization");
    }
    params.leave_subsection();
    params.reset_verbosity();

    params.enter_subsection("Stabilization and turbulence models");
    {
      params.set_verbosity(VerbosityParam::Minimal);
      params.declare_entry_selection("Model", "SUPG", "None | SUPG | VMS-LES");
      params.reset_verbosity();

      params.set_verbosity(VerbosityParam::Full);
      {
        params.declare_entry_selection("Semi-implicit VMS-LES formulation",
                                       "Soft",
                                       "Soft | Hard");

        params.declare_entry_selection("Stabilization parameters computation",
                                       "Metric-tensors",
                                       "Metric-tensors | Cell-diameter");

        params.declare_entry("Constant factor",
                             "1.0",
                             Patterns::Double(),
                             "Coefficient multiplying all stabilization terms");
      }
      params.reset_verbosity();
    }
    params.leave_subsection();

    params.enter_subsection("Resistive Immersed Implicit Surface");
    {
      params.set_verbosity(VerbosityParam::Minimal);
      {
        params.declare_entry("Active",
                             "false",
                             Patterns::Bool(),
                             "Toggle activation of the RIIS term.");

        params.declare_entry("Use surface velocity",
                             "true",
                             Patterns::Bool(),
                             "Toggle use of surface velocity (if false, "
                             "quasi-static approximation u_Gamma=0).");
      }
      params.reset_verbosity();
    }
    params.leave_subsection();

    params.enter_subsection("Arbitrary Lagrangian Eulerian");
    {
      params.set_verbosity(VerbosityParam::Minimal);
      params.declare_entry("Active",
                           "false",
                           Patterns::Bool(),
                           "Toggle activation of the ALE formulation.");

      params.declare_entry("Assemble time-derivative rhs on old mesh",
                           "true",
                           Patterns::Bool(),
                           "Toggle assembly of time-derivative rhs on "
                           "old mesh: int_{Omega_old} u_BDF . v / dt.");

      params.declare_entry(
        "Import displacement from file",
        "false",
        Patterns::Bool(),
        "Toggle import from a .vtp file. If false, a displacement Function "
        "should be given in the code.");

      params.declare_entry(
        "Initialize displacement",
        "true",
        Patterns::Bool(),
        "Toggle initializing the displacement and the mesh, so that the "
        "initial mesh conforms to the initial displacement.");

      params.declare_entry("Export mesh at every output",
                           "false",
                           Patterns::Bool(),
                           "Toggle the storage of the mesh in output files (if "
                           "false, only the initial mesh is exported).");
      params.reset_verbosity();
    }
    params.leave_subsection();

    params.enter_subsection("Time solver");
    {
      params.set_verbosity(VerbosityParam::Minimal);
      {
        params.declare_entry("BDF order",
                             "1",
                             Patterns::Integer(1, 3),
                             "BDF order.");

        params.declare_entry_selection("Non-linearity treatment",
                                       "Semi-implicit",
                                       "Implicit | Semi-implicit");

        if (standalone)
          {
            params.declare_entry("Initial time",
                                 "0",
                                 Patterns::Double(),
                                 "Initial time [s].");

            params.declare_entry("Final time",
                                 "0.05",
                                 Patterns::Double(),
                                 "Final time [s].");

            params.declare_entry("Time step",
                                 "1e-2",
                                 Patterns::Double(0),
                                 "Time step [s].");
          }

        params.declare_entry("Period", "1", Patterns::Double(0), "Period [s].");
      }
      params.reset_verbosity();

      params.set_verbosity(VerbosityParam::Standard);
      {
        if (standalone)
          {
            params.declare_entry(
              "Restart",
              "false",
              Patterns::Bool(),
              "Toggle starting the simulation from results of a previous run.");

            params.declare_entry(
              "Restart initial timestep",
              "0",
              Patterns::Integer(0),
              "If restart is enabled, specify the timestep k "
              "to import the solution from.");

            params.declare_entry(
              "Restart basename",
              "solution",
              Patterns::FileName(Patterns::FileName::FileType::input),
              "If restart is enabled, specify the basename to be imported.");
          }

        params.declare_entry("Input CSV filename",
                             "",
                             Patterns::FileName(
                               Patterns::FileName::FileType::input),
                             "If restart is enabled, specify the input file "
                             "to recover initial volumes from.");
      }
      params.reset_verbosity();
    }
    params.leave_subsection();

    // Non-linear and linear solver helpers.
    params.leave_subsection_path();
    non_linear_solver.declare_parameters(params);
    linear_solver.declare_parameters(params);
    params.enter_subsection_path(prm_subsection_path);

    params.enter_subsection("Preconditioner");
    {
      params.declare_entry_selection(
        "Preconditioner",
        FluidDynamicsPreconditioners::SIMPLE::label,
        FluidDynamicsPreconditioners::InexactBlockLU::Factory::
          get_registered_keys_prm(),
        "");
    }
    params.leave_subsection();

    params.enter_subsection("Post-processing");
    {
      params.set_verbosity(VerbosityParam::Standard);
      params.declare_entry(
        "Export fluid energies",
        "false",
        Patterns::Bool(),
        "Toggle exporting integral fluid properties to CSV: total kinetic "
        "energy, total kinetic energy computed with fine-scale velocity (only "
        "if VMS-LES model is active), and enstrophy.");
      params.reset_verbosity();

      params.set_verbosity(VerbosityParam::Full);
      params.declare_entry(
        "Export space-averaged stabilization parameters",
        "false",
        Patterns::Bool(),
        "Toggle exporting space-averaged stabilization parameters to CSV.");
      params.reset_verbosity();

      params.enter_subsection("Control volumes");
      {
        params.set_verbosity(VerbosityParam::Standard);
        params.declare_entry("Compute average velocity",
                             "",
                             Patterns::List(Patterns::Bool()),
                             "For each control volume, toggle computation of "
                             "average velocity (exported to CSV).");
        params.reset_verbosity();

        params.set_verbosity(VerbosityParam::Full);
        params.declare_entry(
          "Output control volume signed distance",
          "false",
          Patterns::Bool(),
          "Toggle exporting in the XDMF/HDF5 files the signed "
          "distance from control volumes.");
        params.reset_verbosity();
      }
      params.leave_subsection();

      params.set_verbosity(VerbosityParam::Minimal);
      params.enter_subsection("Compute boundary flowrates");
      {
        params.declare_entry(
          "Boundary tags",
          "",
          Patterns::List(Patterns::Integer(0),
                         0,
                         std::numeric_limits<unsigned int>::max(),
                         ","),
          "Tags of boundaries where to compute the outgoing flowrate.");

        params.declare_entry(
          "Boundary labels",
          "",
          Patterns::List(Patterns::Anything(),
                         0,
                         std::numeric_limits<unsigned int>::max(),
                         ","),
          "Labels for the boundaries: used to name columns in output CSV "
          "file.");
      }
      params.leave_subsection();

      params.enter_subsection("Compute average boundary pressures");
      {
        params.declare_entry(
          "Boundary tags",
          "",
          Patterns::List(Patterns::Integer(0),
                         0,
                         std::numeric_limits<unsigned int>::max(),
                         ","),
          "Tags of boundaries where to compute the average pressure.");

        params.declare_entry(
          "Boundary labels",
          "",
          Patterns::List(Patterns::Anything(),
                         0,
                         std::numeric_limits<unsigned int>::max(),
                         ","),
          "Labels for the boundaries: used to name columns in output CSV "
          "file.");
      }
      params.leave_subsection();
      params.reset_verbosity();
    }
    params.leave_subsection();

    params.enter_subsection("Output");
    {
      params.set_verbosity(VerbosityParam::Minimal);
      {
        params.declare_entry("Enable output",
                             "true",
                             Patterns::Bool(),
                             "Enable/disable output of HDF5 files.");

        params.declare_entry("Filename",
                             "solution",
                             Patterns::FileName(
                               Patterns::FileName::FileType::output),
                             "Output file.");

        params.declare_entry("Start saving from time",
                             "0",
                             Patterns::Double(0),
                             "Save results starting from a user-set time [s].");

        if (standalone)
          {
            params.declare_entry("Save every n timesteps",
                                 "1",
                                 Patterns::Integer(1),
                                 "Save every n timesteps.");
          }

        params.declare_entry(
          "Enable boundary output",
          "false",
          Patterns::Bool(),
          "If true, the solution is also written on an output "
          "file containing only the domain boundary.");

        params.declare_entry("Save boundary output every n timesteps",
                             "1",
                             Patterns::Integer(1),
                             "Frequency of boundary output.");

        params.declare_entry("Enable CSV output",
                             "true",
                             Patterns::Bool(),
                             "Toogle exporting of CSV file.");

        if (standalone)
          {
            params.declare_entry("CSV filename",
                                 "fluid_dynamics.csv",
                                 Patterns::FileName(
                                   Patterns::FileName::FileType::output),
                                 "Name for the CSV file.");

            if (standalone)
              {
                params.declare_entry("CSV filename",
                                     "fluid_dynamics.csv",
                                     Patterns::FileName(
                                       Patterns::FileName::FileType::output),
                                     "Name for the CSV file.");

                params.set_verbosity(VerbosityParam::Standard);
                params.declare_entry(
                  "Print CSV every n timesteps",
                  "1",
                  Patterns::Integer(1),
                  "CSV is written only once every n timesteps.");

                params.declare_entry(
                  "Serialize solution",
                  "false",
                  Patterns::Bool(),
                  "Enable/disable the serialization of the solution "
                  "for a possible restart.");

                params.declare_entry("Serialize every n timesteps",
                                     "1",
                                     Patterns::Integer(1),
                                     "Serialize every n timesteps.");
                params.reset_verbosity();
              }
          }
        params.leave_subsection();

        params.leave_subsection_path();

        // Dependencies.
        {
          // Control volumes.
          utils::ControlVolume dummy(prm_subsection_path +
                                       " / Post-processing / Control volumes",
                                     0);
          dummy.declare_parameters(params);

          // Preconditioners
          FluidDynamicsPreconditioners::InexactBlockLU::Factory::
            declare_children_parameters(
              params, prm_subsection_path + " / Preconditioner", *this);

          riis_handler.declare_parameters(params);
          ale_handler->declare_parameters(params);
        }
      }
    }
  }

  void
  FluidDynamics::parse_parameters(ParamHandler &params)
  {
    // Parse input file.
    params.parse();

    // Read input parameters.
    triangulation->parse_parameters(params);

    params.enter_subsection_path(prm_subsection_path);

    params.enter_subsection("Mesh and space discretization");
    {
      prm_velocity_degree = params.get_integer("Velocity FE space degree");
      prm_pressure_degree = params.get_integer("Pressure FE space degree");
    }
    params.leave_subsection();

    params.enter_subsection("Time solver");
    {
      prm_bdf_order = params.get_integer("BDF order");

      if (standalone)
        {
          prm_time_init  = params.get_double("Initial time");
          prm_time_final = params.get_double("Final time");
          prm_time_step  = params.get_double("Time step");
          prm_restart    = params.get_bool("Restart");
          prm_restart_timestep_init =
            params.get_integer("Restart initial timestep");

          time            = prm_time_init;
          timestep_number = prm_restart ? prm_restart_timestep_init : 0;

          prm_restart_basename = params.get("Restart basename");
        }

      prm_input_csv_filename = params.get("Input CSV filename");
      prm_period             = params.get_double("Period");

      const std::string &treatment = params.get("Non-linearity treatment");

      if (treatment == "Implicit")
        {
          prm_flag_non_linear_terms = Nonlinearity::Implicit;
        }
      else if (treatment == "Semi-implicit")
        {
          prm_flag_non_linear_terms = Nonlinearity::SemiImplicit;
        }
    }
    params.leave_subsection();

    params.enter_subsection("Physical constants and models");
    {
      prm_viscosity = params.get_double("Viscosity");
      prm_density   = params.get_double("Density");

      const std::string &term = params.get("Diffusion term formulation");

      // Diffusion term.
      if (term == "SymGrad-Grad")
        {
          prm_flag_diffusion_term = DiffusionTerm::SymGrad_Grad;
        }
      else if (term == "Grad-Grad")
        {
          prm_flag_diffusion_term = DiffusionTerm::Grad_Grad;
        }
      else if (term == "SymGrad-SymGrad")
        {
          prm_flag_diffusion_term = DiffusionTerm::SymGrad_SymGrad;
        }
    }
    params.leave_subsection();

    params.enter_subsection("Volume tags");
    {
      std::vector<std::string> labels =
        params.get_vector<std::string>("Labels");
      std::vector<std::string> sets_of_ids =
        params.get_vector<std::string>("Material IDs");

      AssertThrow(labels.size() == sets_of_ids.size(),
                  ExcMessage(
                    "You must specify as many volume labels as sets of tags."));

      for (unsigned int i = 0; i < labels.size(); ++i)
        {
          std::vector<int> ids_int = Utilities::string_to_int(
            Utilities::split_string_list(sets_of_ids[i], " "));

          prm_volume_tags[labels[i]] =
            std::set<types::material_id>(ids_int.begin(), ids_int.end());

          volume_csv_labels[labels[i]] = "volume_" + utils::mangle(labels[i]);

          // Initialize the map for volumes.
          volumes[labels[i]] = {};
        }
    }
    params.leave_subsection();

    params.enter_subsection("Backflow stabilization");
    {
      prm_backflow_stabilization = params.get_bool("Active");

      const auto tmp_tags =
        Utilities::string_to_int(Utilities::split_string_list(
          params.get("Tags of backflow stabilized Neumann boundaries"), ","));

      prm_tags_backflow_stabilization =
        std::set<types::boundary_id>(tmp_tags.begin(), tmp_tags.end());

      prm_beta_backflow_stabilization = params.get_double("Beta");
    }
    params.leave_subsection();

    params.enter_subsection("Stabilization and turbulence models");
    {
      const std::string &model = params.get("Model");

      if (model == "None")
        {
          prm_flag_stabilization = Stabilization::None;
        }
      else if (model == "SUPG")
        {
          prm_flag_stabilization = Stabilization::SUPG;
        }
      else // if (model == "VMS-LES")
        {
          prm_flag_stabilization = Stabilization::VMS_LES;
        }

      prm_stabilization_factor = params.get_double("Constant factor");

      if (params.get("Semi-implicit VMS-LES formulation") == "Soft")
        {
          prm_soft_semi_implicit_vms_les = true;
        }
      else if (params.get("Semi-implicit VMS-LES formulation") == "Hard")
        {
          prm_soft_semi_implicit_vms_les = false;
        }

      if (params.get("Stabilization parameters computation") ==
          "Metric-tensors")
        {
          prm_stabilization_parameters_metric_tensors = true;
        }
      else
        {
          prm_stabilization_parameters_metric_tensors = false;
        }
    }
    params.leave_subsection();

    params.enter_subsection("Resistive Immersed Implicit Surface");
    {
      prm_riis              = params.get_bool("Active");
      prm_riis_use_velocity = params.get_bool("Use surface velocity");
    }
    params.leave_subsection();

    params.enter_subsection("Arbitrary Lagrangian Eulerian");
    {
      prm_ale = params.get_bool("Active");
      prm_ale_old_mesh_terms =
        params.get_bool("Assemble time-derivative rhs on old mesh");
      prm_ale_from_vtp = params.get_bool("Import displacement from file");
      prm_ale_apply_initial_displacement =
        params.get_bool("Initialize displacement");
      prm_export_mesh_always = params.get_bool("Export mesh at every output");
    }
    params.leave_subsection();

    // Non-linear and linear solver helpers.
    params.leave_subsection_path();
    non_linear_solver.parse_parameters(params);
    linear_solver.parse_parameters(params);
    params.enter_subsection_path(prm_subsection_path);

    params.enter_subsection("Preconditioner");
    {
      prm_preconditioner = params.get("Preconditioner");
    }
    params.leave_subsection();

    params.enter_subsection("Post-processing");
    {
      prm_compute_fluid_energies = params.get_bool("Export fluid energies");
      prm_compute_space_averaged_stabilization_parameters =
        params.get_bool("Export space-averaged stabilization parameters");

      // Control volumes.
      params.enter_subsection("Control volumes");
      {
        const unsigned int n_volumes =
          params.get_vector<std::string>("Labels").size();

        prm_control_volumes_velocity =
          params.get_vector<bool>("Compute average velocity");
        AssertThrow(prm_control_volumes_velocity.size() == n_volumes,
                    ExcMessage(
                      "The list Compute average velocity is expected to have " +
                      std::to_string(n_volumes) + " entries, but it has " +
                      std::to_string(prm_control_volumes_velocity.size())));

        prm_output_control_volume_distance =
          params.get_bool("Output control volume signed distance");

        pressure_control_volumes.resize(n_volumes);
        velocity_magnitude_control_volumes.resize(n_volumes);
        velocity_control_volumes.resize(n_volumes);
      }
      params.leave_subsection();

      params.enter_subsection("Compute boundary flowrates");
      {
        prm_flowrates_tags =
          params.get_vector<types::boundary_id>("Boundary tags");

        const auto flowrates_labels =
          params.get_vector<std::string>("Boundary labels");
        AssertThrow(prm_flowrates_tags.size() == flowrates_labels.size(),
                    ExcDimensionMismatch(prm_flowrates_tags.size(),
                                         flowrates_labels.size()));

        for (const auto &label : flowrates_labels)
          {
            prm_flowrates_names.push_back("flowrate_boundary_" +
                                          utils::mangle(label));
          }
      }
      params.leave_subsection();

      params.enter_subsection("Compute average boundary pressures");
      {
        prm_pressures_tags =
          params.get_vector<types::boundary_id>("Boundary tags");

        const auto pressures_labels =
          params.get_vector<std::string>("Boundary labels");
        AssertThrow(prm_pressures_tags.size() == pressures_labels.size(),
                    ExcDimensionMismatch(prm_pressures_tags.size(),
                                         pressures_labels.size()));

        for (const auto &label : pressures_labels)
          {
            prm_pressures_names.push_back("pressure_boundary_" +
                                          utils::mangle(label));
          }
      }
      params.leave_subsection();
    }
    params.leave_subsection();

    params.enter_subsection("Output");
    {
      prm_enable_output          = params.get_bool("Enable output");
      prm_enable_boundary_output = params.get_bool("Enable boundary output");
      prm_output_min_time        = params.get_double("Start saving from time");
      prm_output_boundary_every_n_timesteps =
        params.get_integer("Save boundary output every n timesteps");
      prm_enable_csv = params.get_bool("Enable CSV output");

      if (standalone)
        {
          prm_serialize_solution = params.get_bool("Serialize solution");
          prm_output_every_n_timesteps =
            params.get_integer("Save every n timesteps");
          prm_serialize_every_n_timesteps =
            params.get_integer("Serialize every n timesteps");
          prm_print_csv_every_n_timesteps =
            params.get_integer("Print CSV every n timesteps");
        }

      prm_output_basename = params.get("Filename");

      if (standalone)
        {
          prm_csv_filename = params.get("CSV filename");
        }
    }
    params.leave_subsection();

    params.leave_subsection_path();

    // Dependencies.
    {
      preconditioner = FluidDynamicsPreconditioners::InexactBlockLU::Factory::
        parse_child_parameters(params,
                               prm_preconditioner,
                               prm_subsection_path + " / Preconditioner",
                               *this);

      if (prm_riis)
        riis_handler.parse_parameters(params);

      if (prm_ale)
        ale_handler->parse_parameters(params);

      for (unsigned int i = 0; i < pressure_control_volumes.size(); ++i)
        {
          control_volumes.emplace_back(prm_subsection_path +
                                         " / Post-processing / Control volumes",
                                       i);
          control_volumes[i].parse_parameters(params);
        }
    }

    // Validate parameters.
    {
      AssertThrow((prm_pressure_degree < prm_velocity_degree) ||
                    (prm_pressure_degree == prm_velocity_degree &&
                     prm_flag_stabilization != Stabilization::None),
                  ExcMessage(
                    "Pressure FE degree cannot be higher than velocity FE "
                    "degree. Moreover, if the same degree is selected, a "
                    "stabilization model must be enabled."));

      AssertThrow(
        prm_preconditioner != "PCD" ||
          prm_flag_stabilization == Stabilization::None,
        ExcMessage(
          "PCD preconditioner does not work in stabilized formulation."));

      AssertThrow((prm_output_min_time == 0) ||
                    (prm_output_min_time >= prm_time_init),
                  ExcMessage("Cannot start saving results from a time which "
                             "is smaller than the initial simulation time."));
    }

    // Compute local interpolation flags for different tasks, now that we know
    // the parameters.

    // System assembly.
    {
      local_interpolation_flags_assemble_system =
        LocalInterpolationFlags::compute_time_derivative |
        LocalInterpolationFlags::compute_advection |
        LocalInterpolationFlags::compute_diffusion |
        LocalInterpolationFlags::compute_div_u_loc |
        LocalInterpolationFlags::compute_p_loc |
        LocalInterpolationFlags::compute_u_star_loc |
        LocalInterpolationFlags::compute_rhs_values;

      if (prm_flag_diffusion_term != DiffusionTerm::Grad_Grad)
        local_interpolation_flags_assemble_system =
          local_interpolation_flags_assemble_system |
          LocalInterpolationFlags::compute_symgrad_u_loc;

      if (prm_flag_non_linear_terms == Nonlinearity::SemiImplicit)
        local_interpolation_flags_assemble_system =
          local_interpolation_flags_assemble_system |
          LocalInterpolationFlags::compute_u_ext_loc;

      if (prm_flag_stabilization != Stabilization::None)
        {
          local_interpolation_flags_assemble_system =
            local_interpolation_flags_assemble_system |
            LocalInterpolationFlags::compute_grad_p_loc |
            LocalInterpolationFlags::compute_lapl_u_loc |
            LocalInterpolationFlags::compute_stabilization_terms;

          if (prm_flag_stabilization == Stabilization::VMS_LES &&
              prm_flag_non_linear_terms == Nonlinearity::SemiImplicit)
            local_interpolation_flags_assemble_system =
              local_interpolation_flags_assemble_system |
              LocalInterpolationFlags::compute_grad_u_ext_loc |
              LocalInterpolationFlags::compute_lapl_u_ext_loc |
              LocalInterpolationFlags::compute_grad_p_ext_loc;
        }

      if (prm_ale)
        local_interpolation_flags_assemble_system =
          local_interpolation_flags_assemble_system |
          LocalInterpolationFlags::compute_u_ale_loc;

      if (prm_riis)
        {
          local_interpolation_flags_assemble_system =
            local_interpolation_flags_assemble_system |
            LocalInterpolationFlags::compute_riis;

          if (prm_riis_use_velocity)
            local_interpolation_flags_assemble_system =
              local_interpolation_flags_assemble_system |
              LocalInterpolationFlags::compute_riis_velocity;
        }
    }
  }


  void
  FluidDynamics::run()
  {
    setup_system();

    // Complete the setup of RIIS variables, but preventing overwrite if restart
    // is active.
    if (!prm_restart)
      {
        update_riis();
      }

    output_results();

    // Time loop.
    while (time < prm_time_final)
      {
        time_advance();

        if (prm_ale || prm_riis ||
            prm_flag_non_linear_terms == Nonlinearity::Implicit)
          pcout << "\n";

        pcout << prm_subsection_path << " timestep number " << std::setw(6)
              << timestep_number << " at t = " << std::setw(8) << std::fixed
              << std::setprecision(6) << time << " s:" << std::flush;

        {
          if (prm_ale)
            {
              TimerOutput::Scope timer_section(timer_output,
                                               prm_subsection_path +
                                                 " / ALE handling");

              pcout << "\n    ALE" << std::endl;
              ale_handler->update(time, prm_time_step, prm_period);

              if (prm_ale_old_mesh_terms)
                {
                  // Assemble time-derivative rhs on the old mesh.
                  assemble_ale_old();
                }
              // else: time-derivative rhs will be assembled on new mesh.

              ale_handler->move_mesh(prm_time_step);

              // In case of moved mesh, flux constraints have to be
              // reinitialized.
              if (dirichlet_flux_bcs)
                reinit_constraints();

              ale_handler->interpolate_dirichlet_vectors(
                ale_velocity, ale_velocity_owned, !ale_velocity_initialized);
              ale_velocity_initialized = true;
            }

          // Note for future warning: if update_riis() is moved from here, one
          // may consider calling it before the time loop also in the
          // prm_restart==true case.
          update_riis();

          apply_BCs();

          try
            {
              solve_time_step(true);
            }
          // If either the linear or non-linear solver do not converge, and
          // ALE is enabled, we print info about the mesh quality before
          // throwing the exception again. This might help identifying
          // issues due to mesh deformation.
          catch (const ExcNonlinearNotConverged & /*exc*/)
            {
              if (prm_ale)
                {
                  pcout << std::endl
                        << "Non-linear solver failure. Printing mesh quality "
                           "info for debugging:"
                        << std::endl;
                  triangulation->get_info().print_mesh_quality_info(true);
                }

              throw;
            }
          catch (const SolverControl::NoConvergence & /*exc*/)
            {
              if (prm_ale)
                {
                  pcout << std::endl
                        << "Linear solver failure. Printing mesh quality "
                           "info for debugging:"
                        << std::endl;
                  triangulation->get_info().print_mesh_quality_info(true);
                }

              throw;
            }
        }

        // These (non-const) method calls are here because they update members
        // of FluidDynamics (compartment volumes, control volume pressures, ...)
        // that may be useful to end_timestep_callback in many applications.
        compute_volumes();
        compute_integral_quantities();

        if (end_timestep_callback)
          end_timestep_callback();

        output_results();
        print_info();
      }
  }

  void
  FluidDynamics::solve_time_step(const bool &assemble_jac)
  {
    if (prm_ale || prm_riis)
      pcout << "\n    Fluid dynamics " << std::flush;

    auto assemble_fun = [this,
                         &assemble_jac](const bool &assemble_quasi_newton) {
      assemble_system(assemble_quasi_newton && assemble_jac);
      return res.l2_norm();
    };

    auto solve_fun = [this](const bool &              assemble_prec,
                            LinAlg::MPI::BlockVector &incr) {
      const double norm_sol = sol_owned.l2_norm();

      solve_system(assemble_prec, incr);

      const double       norm_incr           = incr.l2_norm();
      const unsigned int n_iterations_linear = linear_solver.get_n_iterations();

      return std::make_tuple(norm_sol, norm_incr, n_iterations_linear);
    };

    const bool converged = non_linear_solver.solve(assemble_fun, solve_fun);

    AssertThrow(converged, ExcNonlinearNotConverged());

    AssertThrow(prm_flag_non_linear_terms == Nonlinearity::Implicit ||
                  non_linear_solver.get_n_iterations() <= 1,
                ExcMessage(
                  "Nonlinear solver for non-implicit scheme required more than "
                  "one iteration: possible issues on tolerances or bugs."));
  }

  void
  FluidDynamics::solve_system(const bool &              assemble_prec,
                              LinAlg::MPI::BlockVector &incr)
  {
    // Finalize the preconditioner assembly.
    if (assemble_prec)
      {
        TimerOutput::Scope timer_section(timer_output,
                                         prm_subsection_path +
                                           " / Assemble preconditioner");

        preconditioner->assemble_callback_post();
      }

    TimerOutput::Scope timer_section(timer_output,
                                     prm_subsection_path +
                                       " / Preconditioning and solving");

    linear_solver.solve(jac, incr, res, *preconditioner);

    zero_constraints.distribute(incr);
  }

  void
  FluidDynamics::create_mesh()
  {
    TimerOutput::Scope timer_section(timer_output,
                                     prm_subsection_path + " / Create mesh");

    pcout << "Create the mesh..." << std::endl;

    if (prm_restart)
      {
        const std::string filename_deserialize_fluiddynamics =
          prm_restart_basename + "_" +
          utils::timestep_to_string(timestep_number_init);

        triangulation->set_refinement_from_file(
          filename_deserialize_fluiddynamics);
      }

    triangulation->create_mesh(false);

    // Collect periodic faces to set periodic boundary conditions.
    if (!periodic_bcs.empty())
      {
        AssertThrow(triangulation->is_hex(),
                    ExcMessage("Periodic boundary conditions are supported "
                               "only by hexahedral meshes."));

        parallel::distributed::Triangulation<dim> &triangulation_hex =
          dynamic_cast<parallel::distributed::Triangulation<dim> &>(
            triangulation->get());

        // Vector containing pairs of faces where periodic BCs will be set.
        std::vector<
          GridTools::PeriodicFacePair<utils::MeshHandler::cell_iterator>>
          periodicity_vector;

        // Collection of periodic faces.
        for (const auto &bc : periodic_bcs)
          {
            // We pass the triangulation as a p::d::T because
            // collect_periodic_faces is not instantiated for MeshHandler.
            GridTools::collect_periodic_faces(triangulation_hex,
                                              bc.tags[0],
                                              bc.tags[1],
                                              bc.axis,
                                              periodicity_vector);
          }

        triangulation_hex.add_periodicity(periodicity_vector);
      }

    triangulation->refine();
  }

  void
  FluidDynamics::restart()
  {
    time                 = prm_time_init;
    timestep_number_init = prm_restart_timestep_init;
    timestep_number      = timestep_number_init;

    const std::string filename_deserialize_fluiddynamics =
      prm_restart_basename + "_" +
      utils::timestep_to_string(timestep_number_init);

    // Fluid dynamics.
    {
      pcout << "Restarting fluid dynamics at timestep " << timestep_number
            << " (t = " << time << ") from "
            << filename_deserialize_fluiddynamics << "..." << std::endl;

      std::vector<LinAlg::MPI::BlockVector>   sol_init(prm_bdf_order);
      std::vector<LinAlg::MPI::BlockVector *> sol_init_ptr(prm_bdf_order);

      for (size_t i = 0; i < sol_init.size(); ++i)
        {
          sol_init[i].reinit(sol_owned);

          sol_init_ptr[i] = &(sol_init[i]);
        }

      utils::deserialize(filename_deserialize_fluiddynamics,
                         sol_init_ptr,
                         *triangulation,
                         dof_handler,
                         dof_renumbering);

      sol = sol_owned = sol_init.back();

      bdf_handler.initialize(prm_bdf_order, sol_init);
    }

    // ALE will be restarted by its setup methods.
  }

  void
  FluidDynamics::apply_BCs()
  {
    TimerOutput::Scope timer_section(timer_output,
                                     prm_subsection_path + " / Apply BCs");
    bc_handler.set_time(time, prm_ale);
    forcing_term->set_time(time);

    // Update solution with current Dirichlet data.
    bc_handler.apply_dirichlet<LinAlg::MPI::BlockVector>(
      sol_owned, sol, !bcs_dirichlet_initialized);
    bcs_dirichlet_initialized = true;

    if (dirichlet_flux_bcs)
      {
        // Update constraints at current time step.
        dirichlet_flux_constraints.clear();
        bc_handler.apply_dirichlet_normal_flux(dirichlet_flux_constraints,
                                               false);
        bc_handler.apply_dirichlet_tangential_flux(dirichlet_flux_constraints,
                                                   false);
        dirichlet_flux_constraints.close();

        bc_handler.apply_dirichlet_constraints(sol_owned,
                                               sol,
                                               dirichlet_flux_constraints);
      }

    // Apply Dirichlet condition u = u_ALE to the velocity block.
    // They must be applied as last, because they overwrite wall
    // conditions.
    if (prm_ale)
      {
        ale_handler->apply_dirichlet(sol_owned.block(0),
                                     sol.block(0),
                                     ale_tags);
      }
  }

  void
  FluidDynamics::time_advance()
  {
    time += prm_time_step;
    ++timestep_number;

    bdf_handler.time_advance(sol_owned,
                             prm_flag_non_linear_terms ==
                               Nonlinearity::SemiImplicit);
  }

  void
  FluidDynamics::make_sparsity()
  {
    TimerOutput::Scope timer_section(timer_output,
                                     prm_subsection_path +
                                       " / Setup sparsity pattern");

    std::vector<unsigned int> fluid_sub_blocks(dim + 1, 0);
    fluid_sub_blocks[dim] = 1;

    std::vector<types::global_dof_index> dofs_per_block =
      DoFTools::count_dofs_per_fe_block(dof_handler, fluid_sub_blocks);

    jac.clear();
    Table<2, DoFTools::Coupling> coupling(dim + 1, dim + 1);
    for (unsigned int c = 0; c < dim + 1; ++c)
      for (unsigned int d = 0; d < dim + 1; ++d)
        {
          if (prm_flag_stabilization == Stabilization::None)
            {
              if (c == dim && d == dim)
                {
                  coupling[c][d] = DoFTools::none;
                }
              else
                {
                  coupling[c][d] = DoFTools::always;
                }
            }
          else
            {
              coupling[c][d] = DoFTools::always;
            }
        }

    dsp_jac.reinit(dofs_per_block, dofs_per_block);

    DoFTools::make_sparsity_pattern(
      dof_handler, coupling, dsp_jac, zero_constraints, false);

    SparsityTools::distribute_sparsity_pattern(dsp_jac,
                                               dof_handler.locally_owned_dofs(),
                                               mpi_comm,
                                               locally_relevant_dofs);

    jac.reinit(owned_dofs, dsp_jac, mpi_comm);
  }

  void
  FluidDynamics::setup_system()
  {
    TimerOutput::Scope timer_section(timer_output,
                                     prm_subsection_path + " / Setup system");

    Assert(bcs_are_set,
           ExcMessage(
             "Boundary conditions must be set before calling setup_system. You "
             "must call at least one among set_BCs_dirichlet, "
             "set_BCs_neumann, and set_BCs_periodic."));

    create_mesh();

    // Initialize DoF handlers and finite elements.
    {
      // FE and quadrature.
      // NOTE:
      // an alternative for fe construction is to use get_fe_by_name with the
      // correct string that could be directly read by datafile
      // NOTE 2:
      // the velocity FE space can be replaced by another FESystem, cfr.
      // BlockMask glossary
      const auto fe_scalar_velocity =
        triangulation->get_fe_lagrange(prm_velocity_degree);
      const auto fe_pressure =
        triangulation->get_fe_lagrange(prm_pressure_degree);

      fe = std::make_unique<FESystem<dim>>(*fe_scalar_velocity,
                                           dim,
                                           *fe_pressure,
                                           1);

      // Gaussian quadrature with n quadrature points in each spatial direction
      // is exact for polynomial of degree 2 * n - 1, thus fixing n =
      // prm_velocity_degree + 1 the mass matrix is assembled exactly.
      quadrature_formula =
        triangulation->get_quadrature_gauss(prm_velocity_degree + 1);
      face_quadrature_formula =
        triangulation->get_quadrature_gauss<dim - 1>(prm_velocity_degree + 1);

      if (prm_flag_stabilization == Stabilization::None)
        {
          fe_values =
            std::make_unique<FEValues<dim>>(*fe,
                                            *quadrature_formula,
                                            update_values | update_gradients |
                                              update_quadrature_points |
                                              update_JxW_values);
        }
      else
        {
          fe_values = std::make_unique<FEValues<dim>>(
            *fe,
            *quadrature_formula,
            update_inverse_jacobians | update_values | update_gradients |
              update_hessians | update_quadrature_points | update_JxW_values);
        }

      fe_face_values =
        std::make_unique<FEFaceValues<dim>>(*fe,
                                            *face_quadrature_formula,
                                            update_values |
                                              update_normal_vectors |
                                              update_quadrature_points |
                                              update_JxW_values);
      dof_handler.reinit(triangulation->get());
      dof_handler.distribute_dofs(*fe);
    }

    std::vector<unsigned int> fluid_sub_blocks(dim + 1, 0);
    fluid_sub_blocks[dim] = 1;

    dof_renumbering = [&fluid_sub_blocks](DoFHandler<dim> &dof_handler_) {
      DoFRenumbering::component_wise(dof_handler_, fluid_sub_blocks);
    };

    if (dof_renumbering)
      dof_renumbering(dof_handler);

    std::vector<types::global_dof_index> dofs_per_block =
      DoFTools::count_dofs_per_fe_block(dof_handler, fluid_sub_blocks);

    const unsigned int n_u = dofs_per_block[0], n_p = dofs_per_block[1];

    const std::string dof_str = std::to_string(dof_handler.n_dofs()) +
                                " (u: " + std::to_string(n_u) +
                                ", p: " + std::to_string(n_p) + ")";

    triangulation->get_info().print(prm_subsection_path, dof_str, true);

    std::string polynomial_type_char = triangulation->is_hex() ? "Q" : "P";
    pcout << polynomial_type_char << prm_velocity_degree << "-"
          << polynomial_type_char << prm_pressure_degree << " finite elements"
          << std::endl;

    pcout << "BDF" << prm_bdf_order << " time discretization" << std::endl;

    if (prm_flag_non_linear_terms == Nonlinearity::SemiImplicit)
      pcout << "Semi-implicit";
    else // if (prm_flag_non_linear_terms == Nonlinearity::Implicit)
      pcout << "Implicit";
    pcout << " treatment of non-linearities" << std::endl;

    if (prm_flag_stabilization != Stabilization::None)
      {
        std::string model_str;
        if (prm_flag_stabilization == Stabilization::SUPG)
          model_str = "SUPG";
        else // if (prm_flag_stabilization == Stabilization::VMSLES)
          model_str = "VMS-LES";

        pcout << model_str << " stabilization. " << std::endl;
      }

    if (prm_backflow_stabilization)
      {
        pcout << std::endl << "Backflow stabilization is active on tag(s) ";

        for (const auto &tag : prm_tags_backflow_stabilization)
          pcout << tag << " ";
        pcout << "with beta = " << prm_beta_backflow_stabilization << std::endl;
      }

    pcout << utils::log::separator_section << std::endl;

    owned_dofs.resize(2);
    owned_dofs[0] = dof_handler.locally_owned_dofs().get_view(0, n_u);
    owned_dofs[1] = dof_handler.locally_owned_dofs().get_view(n_u, n_u + n_p);

    relevant_dofs.resize(2);
    DoFTools::extract_locally_relevant_dofs(dof_handler, locally_relevant_dofs);
    relevant_dofs[0] = locally_relevant_dofs.get_view(0, n_u);
    relevant_dofs[1] = locally_relevant_dofs.get_view(n_u, n_u + n_p);

    bc_handler.initialize(dirichlet_bcs,
                          neumann_bcs,
                          *face_quadrature_formula,
                          dirichlet_normal_flux_bcs,
                          dirichlet_tangential_flux_bcs);

    // Initialize solution vectors.
    {
      sol.reinit(owned_dofs, relevant_dofs, mpi_comm);
      sol_bdf.reinit(owned_dofs, relevant_dofs, mpi_comm);
      sol_owned.reinit(owned_dofs, mpi_comm);
      res.reinit(owned_dofs, mpi_comm);

      if (prm_ale && prm_ale_old_mesh_terms)
        res_ale_old.reinit(owned_dofs, mpi_comm);

      if (prm_flag_non_linear_terms == Nonlinearity::SemiImplicit ||
          prm_backflow_stabilization)
        sol_ext.reinit(owned_dofs, relevant_dofs, mpi_comm);

      if (!prm_restart)
        {
          Assert(initial_condition != nullptr,
                 ExcMessage("Initial conditions not set."));

          std::vector<LinAlg::MPI::BlockVector> sol_init;

          /// Interpolate the initial condition on the solution. The initial
          /// condition is required for the velocity only; however, for the
          /// sake of output visualization, we interpolate also the pressure
          /// at the initial time.
          for (unsigned int i = 0; i < prm_bdf_order; ++i)
            {
              initial_condition->set_time(time - prm_time_step *
                                                   (prm_bdf_order - i - 1));

              sol_init.emplace_back(owned_dofs, mpi_comm);
              VectorTools::interpolate(dof_handler,
                                       *initial_condition,
                                       sol_init.back(),
                                       ComponentMask({true, true, true, true}));
            }
          bdf_handler.initialize(prm_bdf_order, sol_init);

          sol = sol_owned = sol_init.back();
        }
      else
        {
          restart();
        }
    }

    // Initialize ALE. Must be done before calling reinit_constraints, which in
    // turn must be done before initializing the matrices. Moreover, it must be
    // done after possible call to restart.
    if (prm_ale)
      {
        std::optional<std::string> filename_ale_deserialize = {};

        if (prm_restart)
          {
            // Only filename, without the directory path: this will be managed
            // within ALEHandler::restart().
            filename_ale_deserialize =
              prm_restart_basename + "_ALE_" +
              utils::timestep_to_string(prm_restart_timestep_init);
          }

        if (prm_ale_from_vtp)
          {
            // ALE displacement from file.
            ale_handler->setup_from_file(triangulation,
                                         fe->get_sub_fe(0, 1),
                                         prm_time_init,
                                         prm_ale_apply_initial_displacement,
                                         prm_ale_apply_initial_displacement,
                                         filename_ale_deserialize);
          }
        else if (ale_displacement_function != nullptr)
          {
            // ALE displacement from function.
            ale_displacement_function->set_time(time);
            ale_handler->setup_from_function(triangulation,
                                             fe->get_sub_fe(0, 1),
                                             prm_time_init,
                                             ale_displacement_function,
                                             prm_ale_apply_initial_displacement,
                                             prm_ale_apply_initial_displacement,
                                             ale_component_mask,
                                             filename_ale_deserialize);
          }
        else
          {
            // If ALE displacement comes neither from file nor from a
            // specified function, we still need to setup its displacement
            // vectors in case we are solving a FSI problem, so that ALE
            // displacement comes from the structure problem.
            ale_handler->setup_displacement_vectors(
              triangulation,
              fe->get_sub_fe(0, 1),
              prm_ale_apply_initial_displacement,
              prm_ale_apply_initial_displacement,
              filename_ale_deserialize);
          }

        ale_handler->interpolate_dirichlet_vectors(ale_velocity,
                                                   ale_velocity_owned,
                                                   !ale_velocity_initialized);
        ale_velocity_initialized = true;

        quadrature_u_ale =
          std::make_unique<QuadratureALEVelocity>(*ale_handler,
                                                  *quadrature_formula);
      }

    // Initialize constraints, sparsity pattern and jacobian.
    reinit_constraints();
    make_sparsity();

    // Initialize the preconditioner
    preconditioner->initialize();

    // We do not assert here that the input CSV filename is not empty if
    // restarting. Indeed, the file is only needed if some of the boundary
    // conditions require volumes at previous timesteps, but this class is not
    // aware of the boundary conditions. The check should be performed in
    // classes responsible for managing boundary conditions, as is done e.g. in
    // FluidDynamicsHeart.
    if (prm_restart && !prm_input_csv_filename.empty())
      {
        pcout << "FluidDynamics: importing initial volumes from "
              << prm_input_csv_filename << std::endl;

        for (auto &volume : volumes)
          {
            for (unsigned int i = 0; i < n_stored_volumes; ++i)
              {
                const double initial_volume_ml =
                  utils::csv_read_time_variable(prm_input_csv_filename,
                                                time - i * prm_time_step,
                                                volume_csv_labels.at(
                                                  volume.first));

                // conversion from mL to m^3.
                volume.second.push_front(initial_volume_ml *
                                         utils::convert::mL_to_m3);
              }
          }
      }
    else
      {
        compute_volumes();
      }

    // Initialize non-linear solver handlers.
    non_linear_solver.initialize(&sol_owned, &sol);

    // Initialize vectors for the interpolation of local quantities.
    // NB: some of them must be initialized before the initialization of RIIS.
    {
      const unsigned int dofs_per_cell   = fe->dofs_per_cell;
      const unsigned int n_q_points      = quadrature_formula->size();
      const unsigned int n_face_q_points = face_quadrature_formula->size();

      component.resize(dofs_per_cell);

      res_loc.resize(dofs_per_cell);
      sol_dof.resize(dofs_per_cell);

      u_ale_loc.resize(n_q_points);
      u_loc.resize(n_q_points);
      u_bdf_loc.resize(n_q_points);
      u_ext_loc.resize(n_q_points);
      u_star_loc.resize(n_q_points);
      grad_u_loc.resize(n_q_points);
      symgrad_u_loc.resize(n_q_points);
      div_u_loc.resize(n_q_points);
      p_loc.resize(n_q_points);
      lapl_u_loc.resize(n_q_points);
      lapl_u_ext_loc.resize(n_q_points);
      grad_p_loc.resize(n_q_points);
      grad_p_ext_loc.resize(n_q_points);
      grad_u_ext_loc.resize(n_q_points);
      metric_tensor.resize(n_q_points);
      metric_vector.resize(n_q_points);
      tau_M.resize(n_q_points);
      tau_C.resize(n_q_points);
      res_M_lhs.resize(n_q_points);
      res_M_lhs_exp.resize(n_q_points);
      res_M_rhs.resize(n_q_points);
      tauM_resM.resize(n_q_points);

      rhs_values =
        std::vector<Vector<double>>(n_q_points, Vector<double>(dim + 1));
      rhs_u_values.resize(n_q_points);

      if (prm_riis)
        {
          riis_factor_loc.resize(n_q_points);
          riis_velocity_loc.resize(n_q_points);
        }

      u_loc_face.resize(n_face_q_points);
      p_loc_face.resize(n_face_q_points);
      u_ext_loc_face.resize(n_face_q_points);

      time_derivative_loc.resize(n_q_points);
      diffusion_loc.resize(n_q_points);
      advection_loc.resize(n_q_points);
      riis_loc.resize(n_q_points);
      tauM_resM_u_star_loc.resize(n_q_points);
    }

    // Control volumes.
    if (control_volumes.size() > 0)
      {
        // Hardcoded piecewise linear FE to avoid dependence on
        // velocity/pressure FE degree.
        const auto fe_scalar = triangulation->get_fe_lagrange(1);

        dof_handler_scalar_linear.reinit(triangulation->get());
        dof_handler_scalar_linear.distribute_dofs(*fe_scalar);

        for (auto &control_volume : control_volumes)
          control_volume.initialize(ale_handler->get_reference_mapping(),
                                    dof_handler_scalar_linear);
      }

    // Initialize RIIS.
    if (prm_riis)
      {
        riis_handler.setup(triangulation,
                           prm_time_init,
                           prm_time_final,
                           prm_time_step,
                           prm_period,
                           prm_ale ? ale_handler : nullptr);

        // Restart RIIS.
        if (prm_restart)
          riis_handler.restart(prm_input_csv_filename, time, prm_time_step);
      }

    compute_integral_quantities();

    // Open the CSV output file.
    if (prm_enable_csv && standalone)
      {
        declare_entries_csv(csv_writer);
        csv_writer.open(get_output_csv_filename(), ',');
      }
  }


  void
  FluidDynamics::assemble_ale_old()
  {
    const unsigned int                   dofs_per_cell = fe->dofs_per_cell;
    std::vector<types::global_dof_index> dof_indices(dofs_per_cell);
    Vector<double>                       cell_res(dofs_per_cell);
    unsigned int                         c = 0; // Cell index.

    res_ale_old = 0;

    assemble_callback_pre_ale_old();

    for (const auto &cell : dof_handler.active_cell_iterators())
      {
        if (cell->is_locally_owned())
          {
            cell->get_dof_indices(dof_indices);
            assemble_callback_cell_ale_old(cell, dof_indices, cell_res);
            res_ale_old.add(dof_indices, cell_res);

            ++c;
          }
      }

    res_ale_old.compress(VectorOperation::add);
    zero_constraints.distribute(res_ale_old);
  }

  void
  FluidDynamics::assemble_callback_cell_ale_old(
    const DoFHandler<dim>::active_cell_iterator &cell,
    const std::vector<types::global_dof_index> & dof_indices,
    Vector<double> &                             cell_res)
  {
    const unsigned int dofs_per_cell = fe->dofs_per_cell;
    const unsigned int n_q_points    = quadrature_formula->size();

    cell_res = 0;
    fe_values->reinit(cell);

    // Interpolate local quantities: we pass 0 as cell index, since it is not
    // needed for the interpolation of u_bdf_loc.
    interpolate_local_quantities<double>(
      LocalInterpolationFlags::compute_u_bdf_loc, *fe_values, dof_indices, 0);

    // Assembly cycle for all the dofs of the current cell.
    for (unsigned int i = 0; i < dofs_per_cell; ++i)
      {
        if (component[i] < dim) // u.
          {
            for (unsigned int q = 0; q < n_q_points; ++q)
              {
                // rhs of the non-stationary term
                cell_res[i] -= prm_density * u_bdf_loc[q] / prm_time_step *
                               (*fe_values)[velocities].value(i, q) *
                               fe_values->JxW(q);
              }
          }
      }
  }

  void
  FluidDynamics::assemble_system(const bool &assemble_jac)
  {
    TimerOutput::Scope timer_section(timer_output,
                                     prm_subsection_path +
                                       " / System assembly");

    if (assemble_jac)
      {
        jac = 0;
      }
    res = 0;

    const unsigned int dofs_per_cell = fe->dofs_per_cell;

    std::vector<types::global_dof_index> dof_indices(dofs_per_cell);

    FullMatrix<double> cell_matrix(dofs_per_cell, dofs_per_cell);
    Vector<double>     cell_res(dofs_per_cell);

    unsigned int c = 0; // Cell index.

    assemble_callback_pre();

    // Reinitialize the preconditioners' blocks
    preconditioner->assemble_callback_pre();

    for (const auto &cell : dof_handler.active_cell_iterators())
      {
        if (cell->is_locally_owned())
          {
            cell->get_dof_indices(dof_indices);

            // Beware that the assemble_callback_cell method is called before
            // distributing with zero_constraints below, so that the affine
            // constraints may override the terms added by
            // assemble_callback_cell.
            assemble_callback_cell(
              cell, c, dof_indices, cell_matrix, cell_res, assemble_jac);

            // Assemble the chosen preconditioner by calling the class method.
            // IMPORTANT: this should be called AFTER assemble_callback_cell()
            // so that the fe_values are correctly initialized
            preconditioner->assemble_callback_cell(cell, c);

            if (assemble_jac)
              {
                zero_constraints.distribute_local_to_global(
                  cell_matrix, cell_res, dof_indices, jac, res);
              }
            else
              {
                zero_constraints.distribute_local_to_global(cell_res,
                                                            dof_indices,
                                                            res);
              }

            ++c;
          }
      }

    assemble_callback_post();

    jac.compress(VectorOperation::add);
    res.block(0).compress(VectorOperation::add);
    res.block(1).compress(VectorOperation::add);

    if (prm_ale && prm_ale_old_mesh_terms)
      {
        // Add rhs of the non-stationary term, assembled in
        // assemble_ale_old().
        res.add(+1, res_ale_old);
      }
  }

  void
  FluidDynamics::output_results()
  {
    TimerOutput::Scope timer_section(timer_output,
                                     prm_subsection_path + " / Output results");

    if (prm_enable_output &&
        ((timestep_number - timestep_number_init) %
         prm_output_every_n_timesteps) == 0 &&
        (prm_output_min_time - time) <= 0.5 * prm_time_step)
      // The last condition is true when time is greater than the time when user
      // wants to start saving results. The RHS is needed to guarantee a correct
      // functionality in case of Finite Arithmetic errors in evaluating the LHS
      // difference.
      {
        DataOut<dim> data_out;

        attach_output(data_out);
        if (prm_ale)
          ale_handler->attach_output(data_out);
        if (prm_riis)
          riis_handler.attach_output(data_out, time, timestep_number);

        data_out.build_patches();

        // Computes the time step at which the user wants to export the mesh. If
        // the max is equal to zero then the mesh is exported at the first time
        // step (timestep_number_init), otherwise the mesh is exported at
        // timestep_number_init + the number of time steps needed to reach
        // prm_output_min_time.
        const unsigned int timestep_export_mesh =
          static_cast<unsigned int>(std::max(
            std::round((prm_output_min_time - prm_time_init) / prm_time_step),
            0.0)) +
          timestep_number_init;

        // Choose if always writing the mesh, or only at time step
        // timestep_export_mesh. Exporting always can be useful only if ALE is
        // active.
        const bool export_mesh = (prm_ale && prm_export_mesh_always) ||
                                 (timestep_number == timestep_export_mesh);

        const unsigned int filename_mesh_index =
          export_mesh ? timestep_number : timestep_export_mesh;

        utils::dataout_write_hdf5(data_out,
                                  prm_output_basename,
                                  timestep_number,
                                  filename_mesh_index,
                                  time);

        data_out.clear();
      }

    output_serialize();

    // Output solution on the boundary.
    output_boundary();

    // Open the CSV output file.
    if (prm_enable_csv && standalone &&
        timestep_number % prm_print_csv_every_n_timesteps == 0)
      {
        set_entries_csv(csv_writer);
        csv_writer.write_line();
      }
  }

  void
  FluidDynamics::output_boundary() const
  {
    TimerOutput::Scope timer_section(timer_output,
                                     prm_subsection_path +
                                       " / Output results / Boundary");

    if (prm_enable_boundary_output &&
        ((timestep_number - timestep_number_init) %
         prm_output_boundary_every_n_timesteps) == 0)
      {
        DataOutFaces<dim> data_out;

        std::vector<std::string> solution_names(dim, "velocity");
        solution_names.push_back("pressure");

        std::vector<DataComponentInterpretation::DataComponentInterpretation>
          data_component_interpretation(
            dim, DataComponentInterpretation::component_is_part_of_vector);
        data_component_interpretation.push_back(
          DataComponentInterpretation::component_is_scalar);

        data_out.add_data_vector(dof_handler,
                                 sol,
                                 solution_names,
                                 data_component_interpretation);

        data_out.build_patches();

        utils::dataout_write_hdf5(data_out,
                                  prm_output_basename + "_boundary",
                                  timestep_number,
                                  timestep_number_init,
                                  time);
      }
  }

  void
  FluidDynamics::output_serialize() const
  {
    if (prm_serialize_solution && ((timestep_number - timestep_number_init) %
                                   prm_serialize_every_n_timesteps) == 0)
      {
        const std::string filename_serialize_fluiddynamics =
          prm_output_basename + "_" +
          utils::timestep_to_string(timestep_number);

        if (prm_ale)
          ale_handler->output_serialize(prm_output_basename, timestep_number);

        // Copy BDF solutions into ghosted vectors and serialize.
        // Please recall that solutions.size() == prm_bdf_order - 1,
        // so we need to manually attach the current solution.
        const auto &solutions = bdf_handler.get_solutions();

        std::vector<LinAlg::MPI::BlockVector> solutions_ghosted;
        solutions_ghosted.reserve(prm_bdf_order - 1);
        std::vector<const LinAlg::MPI::BlockVector *> solutions_ghosted_ptr;
        solutions_ghosted_ptr.reserve(prm_bdf_order);

        for (size_t i = 0; i < solutions.size(); ++i)
          {
            solutions_ghosted.emplace_back();
            solutions_ghosted.back().reinit(sol);
            solutions_ghosted.back() = solutions[i];

            solutions_ghosted_ptr.emplace_back(&(solutions_ghosted.back()));
          }

        solutions_ghosted_ptr.emplace_back(&sol);

        utils::serialize(filename_serialize_fluiddynamics,
                         solutions_ghosted_ptr,
                         *triangulation,
                         dof_handler);
      }
  }

  void
  FluidDynamics::assemble_callback_cell(
    const DoFHandler<dim>::active_cell_iterator &cell,
    const unsigned int &                         c,
    const std::vector<types::global_dof_index> & dof_indices,
    FullMatrix<double> &                         cell_matrix,
    Vector<double> &                             cell_res,
    const bool &                                 assemble_jac)
  {
    const bool ale = prm_ale;

    const unsigned int dofs_per_cell           = fe->dofs_per_cell;
    const unsigned int n_independent_variables = dofs_per_cell;
    const unsigned int n_dependent_variables   = dofs_per_cell;

    // Reset local matrix and vector.
    if (assemble_jac)
      cell_matrix = 0.0;
    cell_res = 0.0;

    if (ale)
      quadrature_u_ale->reinit(cell);

    fe_values->reinit(cell);

    ADHelper ad_helper(n_independent_variables, n_dependent_variables);

    // If we need to assemble the jacobian, hence to compute derivatives, we use
    // double_ADs and setup the ADHelper.
    if (assemble_jac)
      {
        std::fill(res_loc.begin(), res_loc.end(), double_AD(0.0));

        ad_helper.register_dof_values(sol, dof_indices);
        for (unsigned int i = 0; i < dofs_per_cell; ++i)
          sol_dof[i] = ad_helper.get_sensitive_dof_values()[i];

        // Interpolate local quantities.
        interpolate_local_quantities<double_AD>(
          local_interpolation_flags_assemble_system,
          *fe_values,
          dof_indices,
          c);
      }
    // Otherwise, we use standard doubles.
    else
      {
        std::fill(res_loc.begin(), res_loc.end(), 0.0);

        // Here we must explicitly cast to double the right hand side of the
        // assignment: otherwise, the call sol[dof_indices[i]] returns an object
        // of type dealii::TrilinosWrappers::internal::VectorReference, which
        // cannot be implicitly assigned to an std::variant<double, double_AD>.
        for (unsigned int i = 0; i < dofs_per_cell; ++i)
          sol_dof[i] = static_cast<double>(sol[dof_indices[i]]);

        interpolate_local_quantities<double>(
          local_interpolation_flags_assemble_system,
          *fe_values,
          dof_indices,
          c);
      }

    // Assembly cycle for all the dofs of the current cell.
    for (unsigned int i = 0; i < dofs_per_cell; ++i)
      {
        if (assemble_jac)
          std::get<double_AD>(res_loc[i]) +=
            compute_residual<double_AD>(i,
                                        (*fe_values)[velocities],
                                        (*fe_values)[pressure]);
        else
          std::get<double>(res_loc[i]) +=
            compute_residual<double>(i,
                                     (*fe_values)[velocities],
                                     (*fe_values)[pressure]);
      }

    if (prm_backflow_stabilization)
      {
        for (unsigned int face = 0; face < cell->n_faces(); ++face)
          if (cell->face(face)->at_boundary() &&
              std::find(prm_tags_backflow_stabilization.begin(),
                        prm_tags_backflow_stabilization.end(),
                        cell->face(face)->boundary_id()) !=
                prm_tags_backflow_stabilization.end())
            {
              fe_face_values->reinit(cell, face);

              if (assemble_jac)
                interpolate_face_local_quantities<double_AD>(*fe_face_values,
                                                             dof_indices);
              else
                interpolate_face_local_quantities<double>(*fe_face_values,
                                                          dof_indices);

              // Assemble boundary integral.
              for (unsigned int i = 0; i < dofs_per_cell; ++i)
                {
                  if (assemble_jac)
                    std::get<double_AD>(res_loc[i]) +=
                      compute_residual_backflow_stabilization<double_AD>(
                        i, (*fe_face_values)[velocities]);
                  else
                    std::get<double>(res_loc[i]) +=
                      compute_residual_backflow_stabilization<double>(
                        i, (*fe_face_values)[velocities]);
                }
            }
      }

    if (assemble_jac)
      {
        // Extract the residual as a vector of double_ADs (instead of a vector
        // of variants).
        std::vector<double_AD> res_AD(dofs_per_cell);
        for (unsigned int i = 0; i < dofs_per_cell; ++i)
          res_AD[i] = std::get<double_AD>(res_loc[i]);
        ad_helper.register_residual_vector(res_AD);

        ad_helper.compute_residual(cell_res);
        ad_helper.compute_linearization(cell_matrix);
      }
    else
      {
        for (unsigned int i = 0; i < dofs_per_cell; ++i)
          cell_res[i] = std::get<double>(res_loc[i]);
      }

    bc_handler.apply_neumann(cell_res, cell);
  }

  std::pair<std::vector<double>, std::vector<double>>
  FluidDynamics::compute_boundary_flow_and_pressure(
    const std::vector<std::set<types::boundary_id>> &tags_flowrate,
    const std::vector<std::set<types::boundary_id>> &tags_pressure,
    const bool &                                     normalize_flowrates) const
  {
    TimerOutput::Scope timer_section(timer_output,
                                     prm_subsection_path +
                                       " / Compute flow and pressure");

    // Check that the provided IDs are all defined on the mesh.
    std::set<types::boundary_id> existing_ids =
      triangulation->get_info().get_ids_face();
    for (const auto &set : tags_flowrate)
      for (const auto &id : set)
        AssertThrow(existing_ids.find(id) != existing_ids.end(),
                    ExcMessage("The tag " + std::to_string(id) +
                               " is not present on the triangulation."));
    for (const auto &set : tags_pressure)
      for (const auto &id : set)
        AssertThrow(existing_ids.find(id) != existing_ids.end(),
                    ExcMessage("The tag " + std::to_string(id) +
                               " is not present on the triangulation."));

    const unsigned int n_face_q_points = face_quadrature_formula->size();
    const unsigned int dofs_per_cell   = fe->dofs_per_cell;
    std::vector<types::global_dof_index> dof_indices(dofs_per_cell);

    std::vector<double> flowrates(tags_flowrate.size(), 0.0);
    std::vector<double> pressures(tags_pressure.size(), 0.0);
    std::vector<double> areas_flowrate(tags_flowrate.size(), 0);
    std::vector<double> areas_pressure(tags_pressure.size(), 0);

    for (const auto &cell : dof_handler.active_cell_iterators())
      {
        if (!cell->is_locally_owned() || !cell->at_boundary())
          continue;

        for (unsigned int face = 0; face < cell->n_faces(); ++face)
          {
            if (!cell->face(face)->at_boundary())
              continue;

            // Find if the face belongs to one of the sets in tags_flowrate.
            unsigned int j_flowrate = 0;
            for (; j_flowrate < tags_flowrate.size(); ++j_flowrate)
              if (tags_flowrate[j_flowrate].find(
                    cell->face(face)->boundary_id()) !=
                  tags_flowrate[j_flowrate].end())
                break;

            // Find if the face belongs to one of the sets in tags_pressure.
            unsigned int j_pressure = 0;
            for (; j_pressure < tags_pressure.size(); ++j_pressure)
              if (tags_pressure[j_pressure].find(
                    cell->face(face)->boundary_id()) !=
                  tags_pressure[j_pressure].end())
                break;

            // If the boundary ID was not found, go to the next face.
            if (j_flowrate == tags_flowrate.size() &&
                j_pressure == tags_pressure.size())
              continue;

            cell->get_dof_indices(dof_indices);
            fe_face_values->reinit(cell, face);

            for (unsigned int i = 0; i < dofs_per_cell; ++i)
              sol_dof[i] = static_cast<double>(sol[dof_indices[i]]);

            // Evaluate u at face quadrature nodes.
            interpolate_face_local_quantities<double>(*fe_face_values,
                                                      dof_indices);

            // Sum flowrate and pressure on this cell.
            for (unsigned int q = 0; q < n_face_q_points; ++q)
              {
                if (j_flowrate != tags_flowrate.size())
                  {
                    flowrates[j_flowrate] +=
                      std::get<Tensor<1, dim, double>>(u_loc_face[q]) *
                      fe_face_values->normal_vector(q) * fe_face_values->JxW(q);
                    areas_flowrate[j_flowrate] += fe_face_values->JxW(q);
                  }

                if (j_pressure != tags_pressure.size())
                  {
                    pressures[j_pressure] +=
                      std::get<double>(p_loc_face[q]) * fe_face_values->JxW(q);
                    areas_pressure[j_pressure] += fe_face_values->JxW(q);
                  }
              }
          }
      }

    // Sum over the different parallel processes.
    for (unsigned int j = 0; j < tags_flowrate.size(); ++j)
      {
        flowrates[j]      = Utilities::MPI::sum(flowrates[j], mpi_comm);
        areas_flowrate[j] = Utilities::MPI::sum(areas_flowrate[j], mpi_comm);

        if (normalize_flowrates)
          {
            flowrates[j] /= areas_flowrate[j];
          }
      }
    for (unsigned int j = 0; j < tags_pressure.size(); ++j)
      {
        pressures[j]      = Utilities::MPI::sum(pressures[j], mpi_comm);
        areas_pressure[j] = Utilities::MPI::sum(areas_pressure[j], mpi_comm);

        pressures[j] /= areas_pressure[j];
      }

    return {flowrates, pressures};
  }

  std::pair<std::vector<double>, std::vector<double>>
  FluidDynamics::compute_boundary_flow_and_pressure(
    const std::vector<types::boundary_id> &tags_flowrate,
    const std::vector<types::boundary_id> &tags_pressure,
    const bool &                           normalize_flowrates) const
  {
    // Convert the input to a vector of sets of one element each.
    std::vector<std::set<types::boundary_id>> tags_flowrate_sets;
    for (const auto &id : tags_flowrate)
      tags_flowrate_sets.push_back({id});
    std::vector<std::set<types::boundary_id>> tags_pressure_sets;
    for (const auto &id : tags_pressure)
      tags_pressure_sets.push_back({id});

    return compute_boundary_flow_and_pressure(tags_flowrate_sets,
                                              tags_pressure_sets,
                                              normalize_flowrates);
  }

  std::pair<double, double>
  FluidDynamics::compute_boundary_flow_and_pressure(
    const std::set<types::boundary_id> &tags_flowrate,
    const std::set<types::boundary_id> &tags_pressure,
    const bool &                        normalize_flowrates) const
  {
    const auto result = compute_boundary_flow_and_pressure(
      std::vector<std::set<types::boundary_id>>{{tags_flowrate}},
      std::vector<std::set<types::boundary_id>>{{tags_pressure}},
      normalize_flowrates);

    return {result.first[0], result.second[0]};
  }

  std::pair<double, double>
  FluidDynamics::compute_boundary_flow_and_pressure(
    const types::boundary_id &tag_flowrate,
    const types::boundary_id &tag_pressure,
    const bool &              normalize_flowrate) const
  {
    return compute_boundary_flow_and_pressure(
      std::set<types::boundary_id>{tag_flowrate},
      std::set<types::boundary_id>{tag_pressure},
      normalize_flowrate);
  }

  void
  FluidDynamics::reinit_constraints()
  {
    zero_constraints.reinit(locally_relevant_dofs);

    bc_handler.apply_dirichlet(zero_constraints, true);
    bc_handler.apply_dirichlet_normal_flux(zero_constraints, true);
    bc_handler.apply_dirichlet_tangential_flux(zero_constraints, true);

    if (prm_ale)
      ale_handler->apply_dirichlet(zero_constraints, ale_tags, true);

    // Periodic BCs.
    if (!periodic_bcs.empty())
      {
        // Vector containing pairs of faces where periodic BCs will be set.
        std::vector<
          GridTools::PeriodicFacePair<typename DoFHandler<dim>::cell_iterator>>
          periodicity_vector;

        // Collection of periodic faces.
        for (const auto &bc : periodic_bcs)
          {
            GridTools::collect_periodic_faces(
              dof_handler, bc.tags[0], bc.tags[1], bc.axis, periodicity_vector);
          }

        DoFTools::make_periodicity_constraints<dim, dim, double>(
          periodicity_vector,
          periodicity_constraints,
          fe->component_mask(velocities));

        periodicity_constraints.close();

        zero_constraints.merge(periodicity_constraints,
                               AffineConstraints<double>::
                                 MergeConflictBehavior::no_conflicts_allowed,
                               true);
      }

    zero_constraints.close();

    dirichlet_flux_constraints.clear();
    bc_handler.apply_dirichlet_normal_flux(dirichlet_flux_constraints, false);
    bc_handler.apply_dirichlet_tangential_flux(dirichlet_flux_constraints,
                                               false);
    dirichlet_flux_constraints.close();
  }

  void
  FluidDynamics::initialize_assembly() const
  {
    const bool semi_implicit =
      prm_flag_non_linear_terms == Nonlinearity::SemiImplicit;

    // Copy sol_bdf and sol_extrapolation into relevant_dofs.
    sol_bdf = bdf_handler.get_sol_bdf();
    if (semi_implicit || prm_backflow_stabilization)
      sol_ext = bdf_handler.get_sol_extrapolation();

    // Evaluator for u_ale, possibly having different polynomial degree than
    // u.
    if (prm_ale)
      {
        quadrature_u_ale->init();
      }
  }

  void
  FluidDynamics::assemble_callback_pre()
  {
    initialize_assembly();
  }

  void
  FluidDynamics::assemble_callback_pre_ale_old()
  {
    sol_bdf = bdf_handler.get_sol_bdf();
  }

  void
  FluidDynamics::declare_entries_csv(utils::CSVWriter &csv_writer) const
  {
    if (!prm_enable_csv)
      return;

    std::vector<std::string> csv_names;

    if (standalone)
      csv_names.emplace_back("time");

    // Volumes.
    for (const auto &label : volume_csv_labels)
      csv_names.emplace_back(label.second);

    // Flowrates.
    csv_names.insert(csv_names.end(),
                     prm_flowrates_names.begin(),
                     prm_flowrates_names.end());

    // Pressures.
    csv_names.insert(csv_names.end(),
                     prm_pressures_names.begin(),
                     prm_pressures_names.end());

    // Space-averaged fluid properties.
    if (prm_compute_fluid_energies)
      {
        csv_names.emplace_back("total_kinetic_energy");
        // If no VMS-LES model is used, fine-scale kinetic energy has little
        // meaning.
        if (prm_flag_stabilization == Stabilization::VMS_LES)
          {
            csv_names.emplace_back("total_kinetic_energy_fine_scales");
          }
        csv_names.emplace_back("enstrophy");
      }

    // Space-averaged stabilization parameters.
    if (prm_compute_space_averaged_stabilization_parameters)
      {
        csv_names.emplace_back("tau_M_avg");
        csv_names.emplace_back("tau_C_avg");
      }

    // Average pressures and velocity magnitudes in control volumes.
    for (unsigned int k = 0; k < control_volumes.size(); ++k)
      {
        csv_names.emplace_back("pressure_" +
                               utils::mangle(control_volumes[k].get_label()));

        if (prm_control_volumes_velocity[k])
          {
            csv_names.emplace_back(
              "velocity_magnitude" +
              utils::mangle(control_volumes[k].get_label()));
            for (unsigned int d = 0; d < dim; ++d)
              csv_names.emplace_back(
                "velocity_" + std::to_string(d) + "_" +
                utils::mangle(control_volumes[k].get_label()));
          }
      }

    csv_writer.declare_entries(csv_names);

    if (prm_riis)
      riis_handler.declare_entries_csv(csv_writer);
  }

  void
  FluidDynamics::set_entries_csv(utils::CSVWriter &csv_writer) const
  {
    std::map<std::string, utils::CSVWriter::value_type> csv_map;

    if (standalone)
      csv_map.emplace("time", time);

    // Volumes, in ml.
    for (const auto &volume : volumes)
      csv_map.emplace(volume_csv_labels.at(volume.first),
                      utils::convert::m3_to_mL * volume.second.back());

    // Flowrates in m^3/s and pressures in Pa.
    std::vector<double> flowrates, pressures;
    std::tie(flowrates, pressures) =
      compute_boundary_flow_and_pressure(prm_flowrates_tags,
                                         prm_pressures_tags);
    for (unsigned int i = 0; i < prm_flowrates_tags.size(); ++i)
      {
        csv_map.emplace(prm_flowrates_names[i], flowrates[i]);
      }
    for (unsigned int i = 0; i < prm_pressures_tags.size(); ++i)
      {
        csv_map.emplace(prm_pressures_names[i], pressures[i]);
      }

    // Fluid energies.
    if (prm_compute_fluid_energies)
      {
        csv_map.emplace("total_kinetic_energy", total_kinetic_energy);
        // If no VMS-LES model is used, fine-scale kinetic energy has little
        // meaning.
        if (prm_flag_stabilization == Stabilization::VMS_LES)
          {
            csv_map.emplace("total_kinetic_energy_fine_scales",
                            total_kinetic_energy_fine_scales);
          }
        csv_map.emplace("enstrophy", enstrophy);
      }

    // Space-averaged stabilization parameters.
    if (prm_compute_space_averaged_stabilization_parameters)
      {
        csv_map.emplace("tau_M_avg", tau_M_avg);
        csv_map.emplace("tau_C_avg", tau_C_avg);
      }

    // Average pressure in control volumes.
    // Average pressures and velocity magnitudes in control volumes.
    for (unsigned int k = 0; k < control_volumes.size(); ++k)
      {
        csv_map.emplace("pressure_" +
                          utils::mangle(control_volumes[k].get_label()),
                        pressure_control_volumes[k]);

        if (prm_control_volumes_velocity[k])
          {
            csv_map.emplace("velocity_magnitude" +
                              utils::mangle(control_volumes[k].get_label()),
                            velocity_magnitude_control_volumes[k]);
            for (unsigned int d = 0; d < dim; ++d)
              csv_map.emplace("velocity_" + std::to_string(d) + "_" +
                                utils::mangle(control_volumes[k].get_label()),
                              velocity_control_volumes[k][d]);
          }
      }

    csv_writer.set_entries(csv_map);

    if (prm_riis)
      riis_handler.set_entries_csv(csv_writer);
  }

  void
  FluidDynamics::compute_integral_quantities()
  {
    if (!prm_compute_fluid_energies &&
        !prm_compute_space_averaged_stabilization_parameters &&
        control_volumes.size() == 0)
      return;

    TimerOutput::Scope timer_section(timer_output,
                                     prm_subsection_path +
                                       " / Compute integral quantities");

    const unsigned int n_q_points = quadrature_formula->size();

    if (prm_compute_fluid_energies)
      {
        total_kinetic_energy             = 0.0;
        total_kinetic_energy_fine_scales = 0.0;
        enstrophy                        = 0.0;
      }

    // Coarse-scale velocity
    std::vector<Tensor<1, dim, double>> sol_velocity_loc(
      n_q_points, Tensor<1, dim, double>());

    // Coarse-scale vorticity.
    std::vector<FEValuesViews::Vector<dim>::curl_type> sol_vorticity_loc(
      n_q_points, FEValuesViews::Vector<dim>::curl_type());

    // Pressure.
    std::vector<double> sol_pressure_loc(n_q_points);

    double volume = 0.0;

    if (prm_compute_space_averaged_stabilization_parameters)
      {
        tau_M_avg = 0.0;
        tau_C_avg = 0.0;
      }

    // Control volumes.
    std::vector<double> volume_control_volumes(control_volumes.size(), 0.0);
    std::vector<std::unique_ptr<utils::ControlVolume::QuadratureEvaluation>>
      quadrature_control_volumes;

    for (unsigned int k = 0; k < control_volumes.size(); ++k)
      {
        pressure_control_volumes[k] = 0.0;

        if (prm_control_volumes_velocity[k])
          {
            for (unsigned int d = 0; d < dim; ++d)
              velocity_control_volumes[k][d] = 0.0;

            velocity_magnitude_control_volumes[k] = 0.0;
          }

        quadrature_control_volumes.push_back(
          std::make_unique<utils::ControlVolume::QuadratureEvaluation>(
            dof_handler_scalar_linear,
            *quadrature_formula,
            control_volumes[k]));
        quadrature_control_volumes[k]->init();
      }

    const unsigned int                   dofs_per_cell = fe->dofs_per_cell;
    std::vector<types::global_dof_index> dof_indices(dofs_per_cell);

    unsigned int c = 0;

    LocalInterpolationFlags flags = LocalInterpolationFlags::none;

    // Stabilization terms are computed only when VMS-LES model is active, and
    // if energies are requested, because tauM and tauC are needed for fine
    // scale energy.
    //
    // NB: if no stabilization/turbulence is active, fe_values does not update
    // hessians, therefore calling interpolate_local_quantities() with
    // LocalInterpolationFlags::compute_stabilization_term active results in
    // seg-fault.
    if (prm_flag_stabilization == Stabilization::VMS_LES &&
        (prm_compute_fluid_energies ||
         prm_compute_space_averaged_stabilization_parameters))
      {
        flags = flags | LocalInterpolationFlags::compute_stabilization_terms;

        if (prm_ale)
          {
            quadrature_u_ale->init();
            flags = flags | LocalInterpolationFlags::compute_u_ale_loc;
          }
      }

    for (const auto &cell : dof_handler.active_cell_iterators())
      {
        if (!cell->is_locally_owned())
          continue;

        fe_values->reinit(cell);
        cell->get_dof_indices(dof_indices);

        if (prm_ale && (prm_compute_fluid_energies ||
                        prm_compute_space_averaged_stabilization_parameters))
          quadrature_u_ale->reinit(cell);

        for (unsigned int i = 0; i < dofs_per_cell; ++i)
          sol_dof[i] = static_cast<double>(sol[dof_indices[i]]);

        if (prm_compute_fluid_energies)
          {
            (*fe_values)[velocities].get_function_values(sol, sol_velocity_loc);
            (*fe_values)[velocities].get_function_curls(sol, sol_vorticity_loc);
          }

        if (control_volumes.size() > 0)
          {
            (*fe_values)[pressure].get_function_values(sol, sol_pressure_loc);
            (*fe_values)[velocities].get_function_values(sol, sol_velocity_loc);

            for (unsigned int i = 0; i < control_volumes.size(); ++i)
              quadrature_control_volumes[i]->reinit(cell);
          }

        // Interpolate local quantities.
        interpolate_local_quantities<double>(flags, *fe_values, dof_indices, c);

        for (unsigned int q = 0; q < n_q_points; ++q)
          {
            if (prm_compute_fluid_energies)
              {
                for (unsigned int i = 0; i < dim; ++i)
                  {
                    total_kinetic_energy += sol_velocity_loc[q][i] *
                                            sol_velocity_loc[q][i] *
                                            fe_values->JxW(q);

                    enstrophy += sol_vorticity_loc[q][i] *
                                 sol_vorticity_loc[q][i] * fe_values->JxW(q);
                  }

                // If no VMS-LES model is used, fine-scale kinetic energy has
                // little meaning.
                if (prm_flag_stabilization == Stabilization::VMS_LES)
                  {
                    total_kinetic_energy_fine_scales +=
                      std::get<Tensor<1, dim, double>>(tauM_resM[q]) *
                      std::get<Tensor<1, dim, double>>(tauM_resM[q]) *
                      fe_values->JxW(q);
                  }
              }

            if (prm_compute_space_averaged_stabilization_parameters)
              {
                volume += fe_values->JxW(q);
                tau_M_avg += std::get<double>(tau_M[q]) * fe_values->JxW(q);
                tau_C_avg += std::get<double>(tau_C[q]) * fe_values->JxW(q);
              }

            for (unsigned int k = 0; k < control_volumes.size(); ++k)
              {
                pressure_control_volumes[k] +=
                  sol_pressure_loc[q] *
                  (*quadrature_control_volumes[k])(q)*fe_values->JxW(q);
                volume_control_volumes[k] +=
                  (*quadrature_control_volumes[k])(q)*fe_values->JxW(q);

                if (prm_control_volumes_velocity[k])
                  {
                    for (unsigned int d = 0; d < dim; ++d)
                      velocity_control_volumes[k][d] +=
                        sol_velocity_loc[q][d] *
                        (*quadrature_control_volumes[k])(q)*fe_values->JxW(q);

                    velocity_magnitude_control_volumes[k] +=
                      sol_velocity_loc[q].norm() *
                      (*quadrature_control_volumes[k])(q)*fe_values->JxW(q);
                  }
              }
          }

        ++c;
      }

    if (prm_compute_fluid_energies)
      {
        total_kinetic_energy =
          Utilities::MPI::sum(total_kinetic_energy, mpi_comm) * 0.5 *
          prm_density;
        // If no VMS-LES model is used, fine-scale kinetic energy has little
        // meaning.
        if (prm_flag_stabilization == Stabilization::VMS_LES)
          {
            total_kinetic_energy_fine_scales =
              Utilities::MPI::sum(total_kinetic_energy_fine_scales, mpi_comm) *
              0.5 * prm_density;
          }
        enstrophy =
          Utilities::MPI::sum(enstrophy, mpi_comm) * 0.5 * prm_density;
      }

    if (prm_compute_space_averaged_stabilization_parameters)
      {
        volume    = Utilities::MPI::sum(volume, mpi_comm);
        tau_M_avg = Utilities::MPI::sum(tau_M_avg, mpi_comm) / volume;
        tau_C_avg = Utilities::MPI::sum(tau_C_avg, mpi_comm) / volume;
      }

    if (control_volumes.size() > 0)
      {
        for (unsigned int k = 0; k < control_volumes.size(); ++k)
          {
            const double volume =
              Utilities::MPI::sum(volume_control_volumes[k], mpi_comm);

            pressure_control_volumes[k] =
              Utilities::MPI::sum(pressure_control_volumes[k], mpi_comm) /
              volume;

            if (prm_control_volumes_velocity[k])
              {
                for (unsigned int d = 0; d < dim; ++d)
                  velocity_control_volumes[k][d] =
                    Utilities::MPI::sum(velocity_control_volumes[k][d],
                                        mpi_comm) /
                    volume;
                velocity_magnitude_control_volumes[k] =
                  Utilities::MPI::sum(velocity_magnitude_control_volumes[k],
                                      mpi_comm) /
                  volume;
              }
          }
      }
  }

  void
  FluidDynamics::compute_volumes()
  {
    for (auto &volume : volumes)
      {
        const double new_volume =
          triangulation->get_info().compute_submesh_volume(
            prm_volume_tags[volume.first]);

        // If there is at least one stored volume, we pop the oldest one to
        // make room for the new one.
        if (!volume.second.empty())
          volume.second.pop_front();

        // Then we insert the new volume at back, until we reach the desired
        // size. This makes sure that volumes are correctly initialized the
        // first time.
        while (volume.second.size() < n_stored_volumes)
          volume.second.push_back(new_volume);
      }
  }

  void
  FluidDynamics::update_riis()
  {
    if (prm_riis)
      {
        pcout << "\n    RIIS" << std::endl;
        const bool riis_moved = riis_handler.update();

        // Recompute RIIS factors if needed.
        static bool first_call = true;
        if (first_call || riis_moved)
          {
            cache_riis_factor();
            first_call = false;
          }
      }
  }

  void
  FluidDynamics::attach_output(DataOut<dim> &data_out) const
  {
    if (prm_enable_output)
      {
        std::vector<std::string> solution_names(dim, "velocity");
        solution_names.push_back("pressure");

        std::vector<DataComponentInterpretation::DataComponentInterpretation>
          data_component_interpretation(
            dim, DataComponentInterpretation::component_is_part_of_vector);
        data_component_interpretation.push_back(
          DataComponentInterpretation::component_is_scalar);

        data_out.add_data_vector(dof_handler,
                                 sol,
                                 solution_names,
                                 data_component_interpretation);

        if (prm_output_control_volume_distance)
          {
            for (const auto &control_volume : control_volumes)
              {
                data_out.add_data_vector(
                  dof_handler_scalar_linear,
                  control_volume.get_ghosted(),
                  "control_volume_" + utils::mangle(control_volume.get_label()),
                  {DataComponentInterpretation::component_is_scalar});
              }
          }
      }
  }


  void
  FluidDynamics::print_info() const
  {
    // Backup stream flags and manipulators.
    const boost::io::ios_all_saver iostream_backup(pcout.get_stream());
    pcout << std::setprecision(3);

    if (!volumes.empty())
      {
        pcout << "\n    Compartment volumes" << std::endl;

        for (const auto &volume : volumes)
          {
            pcout << "        Volume of " << volume.first << " = "
                  << utils::convert::m3_to_mL * volume.second.back() << " ml"
                  << std::endl;
          }
      }

    if (!control_volumes.empty())
      {
        pcout << "\n    Control volume pressures" << std::endl;

        for (unsigned int k = 0; k < control_volumes.size(); ++k)
          {
            pcout << "        Pressure of " << control_volumes[k].get_label()
                  << " = "
                  << utils::convert::Pa_to_mmHg * pressure_control_volumes[k]
                  << " mmHg" << std::endl;
          }
      }

    if (!control_volumes.empty() || !volumes.empty())
      pcout << std::endl;
  }


  Tensor<1, dim, double>
  FluidDynamics::compute_force(const types::boundary_id &tag) const
  {
    // Test functions for velocity.
    // Represents the three test functions used for the computation of the
    // residual. Each function corresponds to one component (x, y, z), and
    // evaluates to zero everywhere on the boundary except on the boundary
    // with given tag, where the corresponding component is one and the
    // others are zero (e.g. phi_x = (1, 0, 0) on boundary with given tag).
    // Note: we use a different interface from QuadratureEvaluationFEM
    // because we want the interface to be the same as that exposed by
    // fe_values[velocities].
    class TestFunctionVelocity : public QuadratureEvaluationFEM<double>
    {
    public:
      // Constructor.
      TestFunctionVelocity(
        const std::vector<LinAlg::MPI::BlockVector> &fe_functions_,
        const DoFHandler<dim> &                      dof_handler_,
        const Quadrature<dim> &                      quadrature)
        : QuadratureEvaluationFEM(dof_handler_,
                                  quadrature,
                                  update_values | update_gradients)
        , fe_functions(fe_functions_)
        , velocities(0)
      {}

      // Evaluate i-th test function value at quadrature node q.
      Tensor<1, dim, double>
      value(const unsigned int &i, const unsigned int &q) const
      {
        sol_q = 0.0;

        for (unsigned int j = 0; j < dof_indices.size(); ++j)
          sol_q += fe_functions[i][dof_indices[j]] *
                   (*fe_values)[velocities].value(j, q);

        return sol_q;
      }

      // Evaluate i-th test function gradient at quadrature node q.
      Tensor<2, dim, double>
      gradient(const unsigned int &i, const unsigned int &q) const
      {
        grad_q = 0.0;

        for (unsigned int j = 0; j < dof_indices.size(); ++j)
          grad_q += fe_functions[i][dof_indices[j]] *
                    (*fe_values)[velocities].gradient(j, q);

        return grad_q;
      }

      // Evaluate i-th test function symmetric gradient at quadrature node
      // q.
      Tensor<2, dim, double>
      symmetric_gradient(const unsigned int &i, const unsigned int &q) const
      {
        symgrad_q = 0.0;

        for (unsigned int j = 0; j < dof_indices.size(); ++j)
          symgrad_q += fe_functions[i][dof_indices[j]] *
                       (*fe_values)[velocities].symmetric_gradient(j, q);

        return symgrad_q;
      }

      // Evaluate i-th test function divergence at quadrature node q.
      double
      divergence(const unsigned int &i, const unsigned int &q) const
      {
        div_q = 0.0;

        for (unsigned int j = 0; j < dof_indices.size(); ++j)
          div_q += fe_functions[i][dof_indices[j]] *
                   (*fe_values)[velocities].divergence(j, q);

        return div_q;
      }

      // Disable the call operator.
      virtual double
      operator()(const unsigned int & /*q_index*/,
                 const double & /*t*/       = 0,
                 const Point<dim> & /*x_q*/ = Point<dim>()) override
      {
        AssertThrow(false, ExcLifexInternalError());
        return 0.0;
      }

    protected:
      // Finite element representations of the three test functions.
      const std::vector<LinAlg::MPI::BlockVector> &fe_functions;

      // Extractor for velocities.
      FEValuesExtractors::Vector velocities;

      // Temporary for storing function evaluation.
      mutable Tensor<1, dim, double> sol_q;

      // Temporary for storing function gradient evaluation.
      mutable Tensor<2, dim, double> grad_q;

      // Temporary for storing function symmetric gradient evaluation.
      mutable SymmetricTensor<2, dim, double> symgrad_q;

      // Temporary for storing function divergence evaluation.
      mutable double div_q;
    };

    // Test function for pressure: always returns zero.
    // Note: we use a different interface from QuadratureEvaluationFEM
    // because we want the interface to be the same as that exposed by
    // fe_values[pressure].
    class TestFunctionPressure : public QuadratureEvaluationFEM<double>
    {
    public:
      // Constructor.
      TestFunctionPressure(const DoFHandler<dim> &dof_handler_,
                           const Quadrature<dim> &quadrature)
        : QuadratureEvaluationFEM(dof_handler_, quadrature, update_default)
      {}

      // Evaluation.
      double
      value(const unsigned int & /*i*/, const unsigned int & /*q*/) const
      {
        return 0.0;
      }

      // Evaluation for gradient.
      Tensor<1, dim, double>
      gradient(const unsigned int & /*i*/, const unsigned int & /*q*/) const
      {
        return Tensor<1, dim, double>();
      }

      // Disable the call operator.
      virtual double
      operator()(const unsigned int & /*q_index*/,
                 const double & /*t*/       = 0,
                 const Point<dim> & /*x_q*/ = Point<dim>()) override
      {
        AssertThrow(false, ExcLifexInternalError());
        return 0.0;
      }
    };

    // Construct the finite element representation of the test functions: we
    // interpolate the boundary values and then copy them onto the finite
    // element vector.
    std::vector<LinAlg::MPI::BlockVector> phi(dim);
    for (unsigned int i = 0; i < dim; ++i)
      {
        LinAlg::MPI::BlockVector phi_owned(owned_dofs, mpi_comm);
        std::map<types::global_dof_index, double> boundary_values;

        VectorTools::interpolate_boundary_values(
          dof_handler,
          tag,
          ComponentSelectFunction<dim>(i, 1.0, dim + 1),
          boundary_values);

        for (const auto &v : boundary_values)
          if (phi_owned.in_local_range(v.first))
            phi_owned[v.first] = v.second;

        phi[i].reinit(owned_dofs, relevant_dofs, mpi_comm);
        phi[i] = phi_owned;
      }

    Tensor<1, dim, double> force;

    const unsigned int                   dofs_per_cell = fe->dofs_per_cell;
    std::vector<types::global_dof_index> dof_indices(dofs_per_cell);

    unsigned int c = 0;

    TestFunctionVelocity test_velocity(phi, dof_handler, *quadrature_formula);
    TestFunctionPressure test_pressure(dof_handler, *quadrature_formula);

    initialize_assembly();

    test_velocity.init();
    test_pressure.init();

    for (const auto &cell : dof_handler.active_cell_iterators())
      {
        if (!cell->is_locally_owned())
          continue;

        cell->get_dof_indices(dof_indices);

        fe_values->reinit(cell);
        test_velocity.reinit(cell);
        test_pressure.reinit(cell);

        if (prm_riis)
          quadrature_riis->reinit(cell);

        if (prm_ale)
          quadrature_u_ale->reinit(cell);

        for (unsigned int i = 0; i < dofs_per_cell; ++i)
          sol_dof[i] = static_cast<double>(sol[dof_indices[i]]);

        // Interpolate the local quantities.
        interpolate_local_quantities<double>(
          local_interpolation_flags_assemble_system,
          *fe_values,
          dof_indices,
          c,
          true);

        // Compute the residual to get the force.
        for (unsigned int i = 0; i < dim; ++i)
          force[i] += compute_residual<double>(i, test_velocity, test_pressure);

        if (prm_backflow_stabilization)
          {
            for (unsigned int face = 0; face < cell->n_faces(); ++face)
              if (cell->face(face)->at_boundary() &&
                  std::find(prm_tags_backflow_stabilization.begin(),
                            prm_tags_backflow_stabilization.end(),
                            cell->face(face)->boundary_id()) !=
                    prm_tags_backflow_stabilization.end())
                {
                  fe_face_values->reinit(cell, face);

                  interpolate_face_local_quantities<double>(*fe_face_values,
                                                            dof_indices);

                  // Assemble boundary integral.
                  for (unsigned int i = 0; i < dim; ++i)
                    force[i] += compute_residual_backflow_stabilization<double>(
                      i, test_velocity);
                }
          }

        ++c;
      }

    force = Utilities::MPI::sum(force, mpi_comm);

    // Force exerted by the fluid on the surface.
    force *= -1.0;

    return force;
  }

  void
  FluidDynamics::cache_riis_factor()
  {
    const unsigned int n_cells =
      triangulation->get().n_locally_owned_active_cells();
    const unsigned int n_q_points = quadrature_formula->size();

    cached_riis_factor.resize(n_cells);

    unsigned int c = 0;

    quadrature_riis = std::make_unique<QuadratureRIISFactor>(
      riis_handler,
      *quadrature_formula,
      QuadratureRIIS<double>::RIISUpdateFlags::update_riis_factor);
    quadrature_riis->init();
    cached_riis_factor.resize(n_cells);

    for (const auto &cell : dof_handler.active_cell_iterators())
      {
        if (!cell->is_locally_owned())
          continue;

        quadrature_riis->reinit(cell);

        cached_riis_factor[c].resize(n_q_points);
        for (unsigned int q = 0; q < n_q_points; ++q)
          cached_riis_factor[c][q] = (*quadrature_riis)(q);

        ++c;
      }
  }

  std::tuple<double, double, double>
  FluidDynamics::integrate_difference(Function<dim> &exact_solution,
                                      const bool &   normalize)
  {
    TimerOutput::Scope timer_section(timer_output,
                                     prm_subsection_path + " / Compute error");

    std::tuple<double, double, double> errors;

    const ComponentSelectFunction<dim> velocity_mask(std::make_pair(0, dim),
                                                     dim + 1);
    const ComponentSelectFunction<dim> pressure_mask(dim, dim + 1);

    std::unique_ptr<Quadrature<dim>> quadrature_error =
      triangulation->get_quadrature_gauss(prm_velocity_degree + 4);

    exact_solution.set_time(time);

    const auto mapping = triangulation->get_linear_mapping();

    Vector<double> difference(
      triangulation->get().n_locally_owned_active_cells());

    // Compute L2 error of velocity.
    VectorTools::integrate_difference(*mapping,
                                      dof_handler,
                                      sol,
                                      exact_solution,
                                      difference,
                                      *quadrature_error,
                                      VectorTools::L2_norm,
                                      &velocity_mask);

    std::get<0>(errors) =
      VectorTools::compute_global_error(triangulation->get(),
                                        difference,
                                        VectorTools::L2_norm);

    VectorTools::integrate_difference(*mapping,
                                      dof_handler,
                                      sol,
                                      exact_solution,
                                      difference,
                                      *quadrature_error,
                                      VectorTools::H1_seminorm,
                                      &velocity_mask);

    // Compute H1 error of velocity.
    std::get<1>(errors) =
      VectorTools::compute_global_error(triangulation->get(),
                                        difference,
                                        VectorTools::H1_seminorm);

    VectorTools::integrate_difference(*mapping,
                                      dof_handler,
                                      sol,
                                      exact_solution,
                                      difference,
                                      *quadrature_error,
                                      VectorTools::L2_norm,
                                      &pressure_mask);

    // Compute L2 error of pressure.
    std::get<2>(errors) =
      VectorTools::compute_global_error(triangulation->get(),
                                        difference,
                                        VectorTools::L2_norm);

    if (normalize)
      {
        LinAlg::MPI::BlockVector zero_vector;
        zero_vector.reinit(owned_dofs, relevant_dofs, mpi_comm);
        zero_vector = 0;

        // Compute the norms of exact solutions and divide the errors norm by
        // them.
        VectorTools::integrate_difference(*mapping,
                                          dof_handler,
                                          zero_vector,
                                          exact_solution,
                                          difference,
                                          *quadrature_error,
                                          VectorTools::L2_norm,
                                          &velocity_mask);

        std::get<0>(errors) /=
          VectorTools::compute_global_error(triangulation->get(),
                                            difference,
                                            VectorTools::L2_norm);

        VectorTools::integrate_difference(*mapping,
                                          dof_handler,
                                          zero_vector,
                                          exact_solution,
                                          difference,
                                          *quadrature_error,
                                          VectorTools::H1_seminorm,
                                          &velocity_mask);

        std::get<1>(errors) /=
          VectorTools::compute_global_error(triangulation->get(),
                                            difference,
                                            VectorTools::H1_seminorm);

        VectorTools::integrate_difference(*mapping,
                                          dof_handler,
                                          zero_vector,
                                          exact_solution,
                                          difference,
                                          *quadrature_error,
                                          VectorTools::L2_norm,
                                          &pressure_mask);

        std::get<2>(errors) /=
          VectorTools::compute_global_error(triangulation->get(),
                                            difference,
                                            VectorTools::L2_norm);
      }

    return errors;
  }
} // namespace lifex
