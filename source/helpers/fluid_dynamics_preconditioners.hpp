/********************************************************************************
  Copyright (C) 2019 - 2023 by the lifex authors.

  This file is part of lifex.

  lifex is free software; you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  lifex is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with lifex.  If not, see <http://www.gnu.org/licenses/>.
********************************************************************************/

/**
 * @file
 *
 * @author Marco Fedele <marco.fedele@polimi.it>.
 * @author Ivan Fumagalli <ivan.fumagalli@polimi.it>.
 * @author Pasquale Claudio Africa <pasqualeclaudio.africa@polimi.it>.
 * @author Michele Bucelli <michele.bucelli@polimi.it>.
 * @author Thomas Rimbot <thomas.rimbot@mail.polimi.it>.
 * @author Anna Peruso <anna.peruso@mail.polimi.it>.
 */

#ifndef LIFEX_HELPER_FLUID_DYNAMICS_PRECONDITIONERS_HPP_
#define LIFEX_HELPER_FLUID_DYNAMICS_PRECONDITIONERS_HPP_

#include "core/source/core_model.hpp"
#include "core/source/generic_factory.hpp"

#include "core/source/numerics/preconditioner_handler.hpp"

#include <memory>
#include <string>
#include <tuple>

namespace lifex
{
  /// Forward declaration.
  class FluidDynamics;

  /**
   * @brief Compute the sparsity for the approximate Schur complement of a block
   * matrix.
   *
   * For a block matrix in the form
   * @f[
   * A = \begin{bmatrix}A_{00} & A_{01} \\ A_{10} & A_{11} \end{bmatrix}
   * @f]
   * computes the sparsity of the approximate Schur complement
   * @f[
   * \widetilde\Sigma = A_{11} - A_{10}\widetilde{A_{00}}^{-1}A_{01}\,,
   * @f]
   * in which @f$\widetilde{A_{00}}@f$ is a diagonal approximation of
   * @f$A_{00}@f$. For the assembly of the Schur complement, see @ref schur_complement.
   *
   * @param[out] dst The sparsity pattern that will be filled with the result.
   * Its previous content will be overwritten. This function does not call
   * SparsityTools::distribute_sparsity_pattern on the output pattern: you
   * should call it if needed.
   * @param[in] src The block matrix @f$A@f$.
   *
   * @note This function is needed because the sparsity pattern of
   * @f$A_{10}\widetilde{A_{00}}^{-1}A_{01}@f$ is not necessarily consistent
   * with that of @f$A_{11}@f$, so that their sum might throw an exception if
   * the sparsity of @f$\widetilde\Sigma@f$ is not properly initialized.
   */
  inline void
  schur_complement_sparsity(DynamicSparsityPattern &              dst,
                            const LinAlg::MPI::BlockSparseMatrix &src)
  {
    LinAlg::MPI::SparseMatrix tmp_mat;
    src.block(1, 0).mmult(tmp_mat, src.block(0, 1));

    dst.reinit(tmp_mat.m(), tmp_mat.n());

#if defined(LIN_ALG_TRILINOS)
    for (auto entry = tmp_mat.begin(); entry != tmp_mat.end(); ++entry)
      dst.add(entry->row(), entry->column());
#elif defined(LIN_ALG_PETSC)
    // This solution is valid for both Trilinos and PETSc, although
    // significantly less efficient than the Trilinos version above.
    for (const auto &row : tmp_mat.locally_owned_range_indices())
      for (auto entry = tmp_mat.begin(row); entry != tmp_mat.end(row); ++entry)
        dst.add(entry->row(), entry->column());
#endif

      // Add entries for the (1, 1) block.
#if defined(LIN_ALG_TRILINOS)
    for (auto entry = src.block(1, 1).begin(); entry != src.block(1, 1).end();
         ++entry)
      dst.add(entry->row(), entry->column());
#elif defined(LIN_ALG_PETSC)
    for (const auto &row : src.block(1, 1).locally_owned_range_indices())
      for (auto entry = src.block(1, 1).begin(row);
           entry != src.block(1, 1).end(row);
           ++entry)
        dst.add(entry->row(), entry->column());
#endif
  }

  /**
   * @brief Compute the approximate Schur complement of a block matrix.
   *
   * For a block matrix in the form
   * @f[
   * A = \begin{bmatrix}A_{00} & A_{01} \\ A_{10} & A_{11} \end{bmatrix}
   * @f]
   * computes the approximate Schur complement
   * @f[
   * \widetilde\Sigma = A_{11} - A_{10}\widetilde{A_{00}}^{-1}A_{01}\,,
   * @f]
   * in which @f$\widetilde{A_{00}}@f$ is a diagonal approximation of
   * @f$A_{00}@f$.
   *
   * @param[in] dst The matrix where the Schur complement will be stored. Its
   * contents will be overwritten; the sparsity pattern of the matrix should
   * be initialized in advance with @ref schur_complement_sparsity
   * @param[in] src The matrix @f$A@f$. If it has more than two blocks, the
   * extra ones will be ignored.
   * @param[in] diag_A00_inverse The diagonal of @f$\widetilde{A_{00}}^{-1}@f$.
   */
  inline void
  schur_complement(LinAlg::MPI::SparseMatrix &           dst,
                   const LinAlg::MPI::BlockSparseMatrix &src,
                   const LinAlg::MPI::Vector &           diag_A00_inverse)
  {
    LinAlg::MPI::SparseMatrix tmp_mat;
    src.block(1, 0).mmult(tmp_mat, src.block(0, 1), diag_A00_inverse);

    dst = 0.0;
    dst.add(1.0, tmp_mat);
    dst.add(-1.0, src.block(1, 1));
  }

  /// Namespace that implements preconditioners
  /// for the @ref FluidDynamics solver.
  namespace FluidDynamicsPreconditioners
  {
    /**
     * @brief Abstract class for inexact-block-LU preconditioners for Navier-Stokes equations.
     *
     * The matrix arising from the discretization of the fluid dynamics problem
     * has the general block structure
     * @f[ \mathcal{J} = \begin{bmatrix}F & B^T \\ -B & S\end{bmatrix}\;.@f]
     * This class implements preconditioners based on an inexact block-LU
     * factorization, starting from an LU factorization @f[ P = LU =
     * \begin{bmatrix} L_{1} & 0 \\ L_{2} & L_{3} \end{bmatrix} \begin{bmatrix}
     * U_{1} & U_{2} \\ 0 & U_{3} \end{bmatrix}\;,@f] and introducing suitable
     * approximations for some of the blocks of this decomposition. See the
     * derived classes for more informations about the actual preconditioners
     * that are implemented.
     */
    class InexactBlockLU : public Subscriptor, public CoreModel
    {
    public:
      /// Alias for the preconditioner factory.
      using Factory = GenericFactory<InexactBlockLU,
                                     const std::string &,
                                     const FluidDynamics &>;

      /// Constructor.
      /// @param[in] subsection       parameter subsection path for this class.
      /// @param[in] fluid_dynamics_  reference to the FluidDynamics instance that owns this object.
      /// @param[in] schur_subsection parameter subsection path for the Schur complement object.
      InexactBlockLU(const std::string &  subsection,
                     const FluidDynamics &fluid_dynamics_,
                     const std::string &  schur_subsection)
        : CoreModel(subsection)
        , fluid_dynamics(fluid_dynamics_)
        , velocities(0)
        , pressure(dim)
        , prec_F_inv(subsection + " / Inverse of F")
        , prec_schur_inv(subsection + schur_subsection)
      {}

      /// Destructor.
      virtual ~InexactBlockLU()
      {}

      /// Apply the preconditioner, that is @f$U^{-1}L^{-1}\mathrm{src}@f$.
      void
      vmult(LinAlg::MPI::BlockVector &      dst,
            const LinAlg::MPI::BlockVector &src) const
      {
        tmp_LU = src;
        vmult_L(tmp_LU, src);
        vmult_U(dst, tmp_LU);
      }

      /// Initialize the blocks of the preconditioner.
      virtual void
      initialize() = 0;

      /// Reset the preconditioner before assembly.
      virtual void
      assemble_callback_pre() = 0;

      /// Assemble the local contribution on a cell.
      virtual void
      assemble_callback_cell(const DoFHandler<dim>::active_cell_iterator &,
                             const unsigned int) = 0;

      /// Finalize the assembly of the blocks of the preconditioner.
      virtual void
      assemble_callback_post() = 0;

      /// Get the preconditioner for the velocity block.
      utils::PreconditionerHandler &
      get_prec_F()
      {
        return prec_F_inv;
      }

      /// Get the preconditioner for the Schur complement.
      utils::PreconditionerHandler &
      get_prec_schur()
      {
        return prec_schur_inv;
      }

    protected:
      /// Initialize the Schur complement.
      virtual void
      initialize_schur() final;

      /// Apply the L factor, that is @f$L^{-1}\mathrm{src}@f$.
      virtual void
      vmult_L(LinAlg::MPI::BlockVector &      dst,
              const LinAlg::MPI::BlockVector &src) const = 0;

      /// Apply the U factor, that is @f$U^{-1}\mathrm{src}@f$.
      virtual void
      vmult_U(LinAlg::MPI::BlockVector &      dst,
              const LinAlg::MPI::BlockVector &src) const = 0;

      /// Reference to FluidDynamics class.
      const FluidDynamics &fluid_dynamics;

      /// Extractor for the velocity.
      const FEValuesExtractors::Vector velocities;

      /// Extractor for the pressure.
      const FEValuesExtractors::Scalar pressure;

      /// Approximate Schur complement.
      LinAlg::MPI::SparseMatrix approximate_schur;

      /// Preconditioner for the velocity block.
      utils::PreconditionerHandler prec_F_inv;

      /// Preconditioner for the approximate Schur complement.
      utils::PreconditionerHandler prec_schur_inv;

      /// Flag indicating if the sparsity of the Schur complement is
      /// already initialized. It is used to initialize it once and for
      /// all at the beginning of the simulation instead of doing it at each
      /// time step.
      bool dsp_schur_initialized;

      /// Temporary vector.
      mutable LinAlg::MPI::BlockVector tmp_LU;
    };

    /**
     * @brief aSIMPLE preconditioner.
     *
     * The matrix arising from the discretization of the fluid dynamics problem
     * has the form
     * @f[ \mathcal{J} = \begin{bmatrix}F & B^T \\ -B & S\end{bmatrix}\;.@f]
     *
     * The SIMPLE preconditioner is defined as:
     * @f[ P_\mathrm{SIMPLE} = \begin{bmatrix} F & 0 \\ -B & \widetilde{\Sigma}
     * \end{bmatrix} \begin{bmatrix} I & D^{-1}B^T \\ 0 & I \end{bmatrix}\;,@f]
     * where @f$D = \mathrm{diag}(F)@f$ is a diagonal matrix obtained from the
     * diagonal entries of @f$F@f$, and @f$\widetilde{\Sigma} = S +
     * B\,D^{-1}\,B^T@f$ is an approximation of the Schur complement, in which
     * replacing @f$F^{-1}@f$ with @f$D^{-1}@f$ allows explicit assembly of the
     * matrix.
     *
     * In practice, to use the preconditioner we need to compute the application
     * of its inverse to a vector, @a i.e. @f$P_\mathrm{SIMPLE}^{-1}\mathbf
     * x@f$, which in turn requires to compute the inverses of @f$F@f$ and
     * @f$\widetilde{\Sigma}@f$. To do so, we approximate @f$F@f$ and
     * @f$\widetilde{\Sigma}@f$ with suitable preconditioners, passed as
     * constructor arguments to this class. In @ref FluidDynamics, both @f$F@f$ and
     * @f$\widetilde{S}@f$ are approximated by means of @ref utils::PreconditionerHandler.
     * With these approximations, the preconditioner is more appropriately
     * referred to as aSIMPLE (approximate SIMPLE).
     *
     * In case of defective flow BC treatment the matrix arising from the
     * discretization of the fluid dynamics problem has an extra block
     * associated with the Lagrange multipliers. Thus, in such cases
     * the SIMPLE preconditioner is extended with a diagonal identity block.
     *
     * @todo Implement a more suitable extension of the SIMPLE preconditioner in case of multiple defective flowrate boundary conditions.
     *
     * **References**: @refcite{Deparis2014, Deparis et al. (2014)}
     */
    class SIMPLE : public InexactBlockLU
    {
    public:
      /// Label for this class.
      static inline constexpr auto label = "SIMPLE";

      /// Constructor.
      SIMPLE(const std::string &  subsection,
             const FluidDynamics &fluid_dynamics_);

      /// Declare parameters.
      virtual void
      declare_parameters(ParamHandler &) const override;

      /// Parse parameters.
      virtual void
      parse_parameters(ParamHandler &) override;

      /// Initialization.
      virtual void
      initialize() override;

      /// Reset the preconditioner before assembly. Does nothing, and is defined
      /// for consistency with the interface of the base class.
      virtual void
      assemble_callback_pre() override;

      /// Assemble the local contribution on a cell. Does nothing, and is
      /// defined for consistency with the interface of the base class.
      virtual void
      assemble_callback_cell(const DoFHandler<dim>::active_cell_iterator &,
                             const unsigned int) override;

      /// Finalize the assembly. Constructs the preconditioner from the
      /// assembled system matrix for Navier-Stokes.
      virtual void
      assemble_callback_post() override;

    protected:
      /// Apply the L factor.
      virtual void
      vmult_L(LinAlg::MPI::BlockVector &      dst,
              const LinAlg::MPI::BlockVector &src) const override;

      /// Apply the U factor.
      virtual void
      vmult_U(LinAlg::MPI::BlockVector &      dst,
              const LinAlg::MPI::BlockVector &src) const override;

      /// Inverse diagonal of F.
      LinAlg::MPI::Vector diag_F_inv;

      /// Temporary vector.
      mutable LinAlg::MPI::BlockVector tmp;
    };

    // Register preconditioners into the factory.
    LIFEX_REGISTER_CHILD_INTO_FACTORY(InexactBlockLU,
                                      SIMPLE,
                                      SIMPLE::label,
                                      const std::string &,
                                      const FluidDynamics &);
  } // namespace FluidDynamicsPreconditioners
} // namespace lifex

#endif /* LIFEX_HELPER_FLUID_DYNAMICS_PRECONDITIONERS_HPP_ */
