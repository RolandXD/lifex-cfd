/********************************************************************************
  Copyright (C) 2019 - 2023 by the lifex authors.

  This file is part of lifex.

  lifex is free software; you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  lifex is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with lifex.  If not, see <http://www.gnu.org/licenses/>.
********************************************************************************/

/**
 * @file
 *
 * @author Marco Fedele <marco.fedele@polimi.it>.
 * @author Ivan Fumagalli <ivan.fumagalli@polimi.it>.
 * @author Pasquale Claudio Africa <pasqualeclaudio.africa@polimi.it>.
 * @author Michele Bucelli <michele.bucelli@polimi.it>.
 * @author Thomas Rimbot <thomas.rimbot@mail.polimi.it>.
 * @author Anna Peruso <anna.peruso@mail.polimi.it>.
 */

#include "source/fluid_dynamics.hpp"

#include "source/helpers/fluid_dynamics_preconditioners.hpp"

#include <deal.II/lac/solver_cg.h>
#include <deal.II/lac/solver_control.h>

#include <memory>
#include <string>
#include <tuple>
#include <vector>

namespace lifex::FluidDynamicsPreconditioners
{
  void
  InexactBlockLU::initialize_schur()
  {
    const std::vector<IndexSet> &owned_dofs(fluid_dynamics.get_owned_dofs());
    const std::vector<IndexSet> &relevant_dofs(
      fluid_dynamics.get_relevant_dofs());
    const LinAlg::MPI::BlockSparseMatrix &jac = fluid_dynamics.get_jac();

    DynamicSparsityPattern dsp_schur;
    schur_complement_sparsity(dsp_schur, jac);

    SparsityTools::distribute_sparsity_pattern(dsp_schur,
                                               owned_dofs[1],
                                               mpi_comm,
                                               relevant_dofs[1]);


    lifex::utils::initialize_matrix(approximate_schur,
                                    owned_dofs[1],
                                    dsp_schur);
  }

  SIMPLE::SIMPLE(const std::string &  subsection,
                 const FluidDynamics &fluid_dynamics_)
    : InexactBlockLU(subsection,
                     fluid_dynamics_,
                     " / Inverse of Schur complement (SIMPLE only)")
  {}

  void
  SIMPLE::declare_parameters(ParamHandler &params) const
  {
    prec_F_inv.declare_parameters(params);
    prec_schur_inv.declare_parameters(params);
  }

  void
  SIMPLE::parse_parameters(ParamHandler &params)
  {
    params.parse();
    prec_F_inv.parse_parameters(params);
    prec_schur_inv.parse_parameters(params);
  }

  void
  SIMPLE::initialize()
  {
    const std::vector<IndexSet> &owned_dofs(fluid_dynamics.get_owned_dofs());

    dsp_schur_initialized = false;
    diag_F_inv.reinit(owned_dofs[0], mpi_comm);

    tmp.reinit(owned_dofs, mpi_comm);
  }

  void
  SIMPLE::assemble_callback_pre()
  {}

  void
  SIMPLE::assemble_callback_cell(const DoFHandler<dim>::active_cell_iterator &,
                                 const unsigned int)
  {}

  void
  SIMPLE::assemble_callback_post()
  {
    // Initialize the sparsity pattern before the first call. This is done
    // here instead of in initialize() because the jacobian is not ready
    // before that.
    if (!dsp_schur_initialized)
      {
        initialize_schur();
        dsp_schur_initialized = true;
      }

    const unsigned int start = diag_F_inv.local_range().first;

    const unsigned int end = diag_F_inv.local_range().second;

    for (unsigned int j = start; j < end; ++j)
      {
        AssertThrow(fluid_dynamics.get_jac().block(0, 0).diag_element(j) != 0.0,
                    ExcDivideByZero());

        diag_F_inv(j) =
          1.0 / fluid_dynamics.get_jac().block(0, 0).diag_element(j);
      }

    diag_F_inv.compress(VectorOperation::insert);

    schur_complement(approximate_schur, fluid_dynamics.get_jac(), diag_F_inv);

    const DoFHandler<dim> &dof_handler(fluid_dynamics.get_dof_handler());
    prec_F_inv.initialize(fluid_dynamics.get_jac().block(0, 0),
                          &dof_handler,
                          dof_handler.get_fe_collection().component_mask(
                            velocities));

    prec_schur_inv.initialize(approximate_schur,
                              &dof_handler,
                              dof_handler.get_fe_collection().component_mask(
                                pressure));
  }

  void
  SIMPLE::vmult_L(LinAlg::MPI::BlockVector &      dst,
                  const LinAlg::MPI::BlockVector &src) const
  {
    // d0 = F^{-1} * s0
    prec_F_inv.vmult(dst.block(0), src.block(0));

    // t1 = (-B) * d0
    fluid_dynamics.get_jac().block(1, 0).vmult(tmp.block(1), dst.block(0));

    // t1 -= s1
    tmp.block(1).add(-1.0, src.block(1));

    // d1 = (-Sigma^{-1}) * t1
    prec_schur_inv.vmult(dst.block(1), tmp.block(1));

    // In case of defective flow BC treatment the Navier-Stokes system matrix
    // has an extra block associated with the Lagrange multipliers (n_blocks=3).
    // Thus, in case of defective flow BCs we add an extra identity block.
    if (src.n_blocks() == 3)
      dst.block(2) = src.block(2);
  }

  void
  SIMPLE::vmult_U(LinAlg::MPI::BlockVector &      dst,
                  const LinAlg::MPI::BlockVector &src) const
  {
    // d1 = s1
    dst.block(1) = src.block(1);

    // d0 = B^T * s1
    fluid_dynamics.get_jac().block(0, 1).vmult(dst.block(0), src.block(1));

    // d0 = D^{-1} * d0
    dst.block(0).scale(diag_F_inv);

    // d0 = s0 - d0
    dst.block(0).sadd(-1.0, src.block(0));

    // In case of defective flow BC treatment the Navier-Stokes system matrix
    // has an extra block associated with the Lagrange multipliers (n_blocks=3).
    // Thus, in case of defective flow BCs we add an extra identity block.
    if (src.n_blocks() == 3)
      dst.block(2) = src.block(2);
  }
} // namespace lifex::FluidDynamicsPreconditioners
