/********************************************************************************
Copyright (C) 2020 - 2023 by the lifex authors.

This file is part of lifex.

lifex is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

lifex is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with lifex.  If not, see <http://www.gnu.org/licenses/>.
********************************************************************************/

/**
 * @file
 *
 * @author Ivan Fumagalli <ivan.fumagalli@polimi.it>.
 * @author Alberto Zingaro <alberto.zingaro@polimi.it>.
 * @author Michele Bucelli <michele.bucelli@polimi.it>.
 */

#ifndef LIFEX_HELPER_RIIS_SURFACE_HPP_
#define LIFEX_HELPER_RIIS_SURFACE_HPP_

#include "core/source/core_model.hpp"

#include "core/source/io/csv_writer.hpp"
#include "core/source/io/vtk_function.hpp"

#include "source/helpers/ale_handler.hpp"

#include <memory>
#include <optional>
#include <string>
#include <utility>
#include <vector>

namespace lifex
{
  class RIISHandler;
  class FluidDynamics;

  /**
   * @brief Immersed surface class.
   *
   * # Table of contents
   * - @ref riis_surface_movement
   *   - @ref riis_surface_movement_warp_by_vector
   *   - @ref riis_surface_onoff
   *   - @ref riis_surface_variable_epsilon
   * - @ref riis_pressure_jumps
   *
   * This class represents an immersed surface in the context of the Resistive
   * Immersed Implicit Surface model (see @ref RIISHandler). A surface can either
   * be still or moving in time. Different options are available for moving
   * surfaces, and still surfaces can be obtained as particular cases of moving
   * surfaces, as detailed below.
   *
   * **References:** @refcite{Fedele2017, Fedele et al. (2017)},
   * @refcite{Fumagalli2020, Fumagalli et al. (2020)}.
   *
   * @section riis_surface_movement Moving immersed surfaces
   * The movement of the immersed surface can be obtained by warping according
   * to a vector defined on the surface.
   *
   * In this section we refer to movement in the sense of the deformation over
   * time of the immersed surface, with reference in particular to the opening
   * and closing of cardiovascular valves. Movement of the immersed surface
   * following the ALE displacement of the fluid domain (see @ref ale) is
   * accounted for separately. Refer to @ref riis_ale for more details.
   *
   * @subsection riis_surface_movement_warp_by_vector Warping the surface
   * A single VTP input file is used. On the input file, an array @f$\mathbf
   * g(\hat{\mathbf{x}})@f$ defines the displacement from the input
   * configuration to the fully displaced configuration. For a cardiac valve,
   * for instance, the input configuration might be the valve in open
   * configuration, and
   * @f$\mathbf{g}(\hat{\mathbf{x}})@f$ defines the displacement at every point
   * of the surface from the open to the closed configuration.
   *
   * The evolution over time of the position of a point @f$\hat{\mathbf{x}}@f$
   * of the surface is defined through
   * @f[
   * \mathbf{x}(\hat{\mathbf{x}}, t) = \mathbf{g}(\hat{\mathbf{x}}) c(t)
   * @f]
   * where the opening coefficient @f$c(t)@f$ is given by a displacement law,
   * for which several options are available (see
   * @ref RIISSurface::DisplacementLaw).
   *
   * A still surface can be obtained in this framework by providing a VTP file
   * in which @f$g(\hat{\mathbf{x}}) = \mathbf{0} \forall \hat{\mathbf{x}}@f$.
   *
   * @subsection riis_surface_onoff On-off behavior
   * Alternatively, the surface may be made to appear and disappear
   * instantaneously over time. To do so, set the associated <kbd>On-off
   * behavior</kbd> parameter to true. Doing so, the opening coefficient
   * @f$c(t)@f$ is still computed according to the chosen displacement law.
   * However, the surface is never moved. Instead, the surface disappears if
   * @f$c(t) > 0.5@f$.
   *
   * @subsection riis_surface_variable_epsilon Time-varying surface thickness
   * It may be useful for testing purposes to have thicker surfaces (large
   * @f$\varepsilon@f$) when they are closed, but thinner when they are open.
   * This is the case, for instance, when testing cardiac valves with a coarse
   * mesh: when the valve is closed, @f$\varepsilon@f$ must be large enough
   * (typically larger than one element) to prevent flow through it. Conversely,
   * when the valve is open, a too large value of @f$\epsilon@f$ results in an
   * orifice that is too small.
   *
   * To this end, two coefficients @f$K_\mathrm{open}@f$ and
   * @f$K_\mathrm{closed}@f$ are defined, and @f$\varepsilon@f$ varies in time
   * as
   * @f[ \varepsilon(t) = \varepsilon_0 \left[K_\mathrm{open}c(t) +
   * K_\mathrm{closed}(1 - c(t))\right] @f]
   * Both the coefficients are set through the parameter file (<kbd>Epsilons
   * factors in open configuration</kbd> and <kbd>Epsilons factors in closed
   * configuration</kbd>) and are set to 1 (no change in either configuration)
   * by default.
   *
   * @section riis_pressure_jumps Computation of pressure jumps
   * It is possible to compute the pressure jump across an immersed surface.
   * Such quantity is relevant both for postprocessing purposes and for
   * modelling purposes, as it can be used to determine when to open and close
   * the surface in the context of cardiac valve modeling (see
   * @ref DisplacementLaw::FlowDriven).
   *
   * To compute pressure jumps, the user must provide for each immersed surface
   * two control volumes (upwind and downwind to the surface). The volumes must
   * be defined within the <kbd>Fluid dynamics / Control volumes</kbd> parameter
   * section (see also @ref control-volumes), and for each surface you have to
   * provide the labels of the upwind and downwind volumes in the section
   * <kbd>Resistive Immersed Implicit Surface / Control volumes</kbd>. At every
   * timestep, the difference between the upwind and downwind average pressures
   * is used as a proxy for the pressure jump through the surface.
   *
   * The use of control volumes is mandatory when using displacement laws that
   * make use of the pressure jump, such as @ref DisplacementLaw::FlowDriven.
   */
  class RIISSurface : public CoreModel
  {
  public:
    template <class NumberType>
    friend class QuadratureRIIS;

    friend class RIISHandler;

    /// Valve configuration.
    enum class ValveConfiguration
    {
      Open,    ///< Open configuration.
      Closing, ///< Transition from open to closed.
      Closed,  ///< Closed configuration.
      Opening  ///< Transition from closed to open.
    };

    /// Displacement laws.
    enum class DisplacementLaw
    {
      /**
       * @brief Open and close at prescribed times, periodically.
       *
       * At a given time @f$t@f$, the opening coefficient @f$c(t)@f$ is given by
       * the function
       * @f[
       * c(t) = \begin{cases}
       * c_0 & \text{if } t < t_0 \\
       * c_0 + (c_1 - c_0) r\left(\frac{t - t_0}{t_1 - t_0}\right) & \text{if
       * } t_0 \leq t < t_1
       * \\
       * c_1 & \text{if } t_1 \leq t < t_2 \\
       * c_1 + (c_0 - c_1) r\left(\frac{t - t_2}{t_3 - t_2}\right)& \text{if }
       * t_2 \leq t < t_3 \\ c_0 & \text{if } t \geq t_3 \end{cases}
       * @f]
       * where the function @f$r@f$ is one of the functions described in
       * @ref RampShape, selected through the parameter file, and @f$c_0 = 0, c_1
       * = 1@f$ if the initial configuration is closed, @f$c_0 = 1, c_1 = 0@f$
       * if it is open. The times @f$t_0, t_1, t_2, t_3@f$ are manually set
       * through the parameter file.
       */
      Prescribed,

      /**
       * @brief Open and close according to pressure jump and flowrate.
       *
       * This option is specific for cardiac valves: the function @f$c(t)@f$ is
       * the same as that of the Prescribed option. However, the times @f$t_i@f$
       * are not set manually and a priori. Instead, they are determined as a
       * result of the fluid dynamics simulation, computing the pressure jump
       * across the valve and the flowrate through it:
       * - the time @f$t_1@f$ at which the valve opens is
       * determined as the time at which the pressure jump through the valve
       * becomes positive (i.e. the upwind pressure becomes larger than the
       * downwind pressure); to safeguard against spurious pressure
       * oscillations, the pressure jump must be positive for two consequent
       * timesteps to trigger valve opening;
       * - the time @f$t_3@f$ at which the valve closes can be determined in
       * multiple ways, as documented in @ref FlowDrivenClosing; the method can
       * be selected by setting <kbd>Displacement law / Flow driven / Closing
       * law</kbd>;
       * - times @f$t_2@f$ and @f$t_4@f$ are determined by prescribing the
       * duration of the opening and closing of the valve through the parameters
       * <kbd>Displacement law / Flow driven / Duration of the opening
       * ramp</kbd> and <kbd>Displacement law / Flow driven / Duration of the
       * closing ramp</kbd>.
       */
      FlowDriven
    };

    /**
     * Shapes for opening and closing ramps when using Prescribed or FlowDriven
     * displacement laws.
     */
    enum class RampShape
    {
      /**
       * Linear ramp: @f$ r_\text{lin}(t) = t @f$.
       */
      Linear,

      /**
       * Cosinusoidal ramp:
       * @f$ r_\text{cos}(t) = \frac{1}{2}(1 - \cos(\pi t) @f$.
       */
      Cosine,

      /**
       * Exponential ramp:
       * @f$ r_\text{exp}(t) = \frac{1 - e^{-p t}}{1 - e^{-p}} @f$. The
       * coefficient @f$p@f$ is defined in @ref prm_exp_ramp_coefficient.
       */
      Exponential,

      /**
       * Combination of cosine and exponential:
       * @f$ r_\text{cosexp} = r_\text{cos}(r_\text{exp}(t)) @f$.
       */
      CosineExponential
    };

    /**
     * Valve closing approach for the @ref DisplacementLaw::FlowDriven displacement law.
     */
    enum class FlowDrivenClosing
    {
      /**
       * Closing starts at a prescribed time.
       */
      Prescribed,

      /**
       * Closing starts as soon as the pressure jump through the valve becomes
       * negative. To safeguard against spurious pressure oscillations, pressure
       * jump must be negative for two consequent timesteps to trigger valve
       * closing.
       */
      PressureJump,

      /**
       * Closing is triggered according to the volume derivative of some
       * compartment of the fluid domain (see @ref volumes-flows). This assumes
       * that, when the valve is open, it is the only open boundary of the
       * compartment, so that flowrate through it can be evaluated as the
       * derivative of the compartment's volume.
       *
       * More formally, if @f$V@f$ denotes the volume of the compartment, the
       * value of @f$\frac{dV}{dt}@f$ is retrieved
       * (@ref FluidDynamics::get_volume_derivative). If <kbd>Displacement law /
       * Flow driven / Volume derivative closing / Flow direction through the
       * valve</kbd> is set to <kbd>Inflow</kbd>, the valve starts closing as
       * soon as @f$\frac{dV}{dt} > 0@f$ (i.e. as soon as reverse flow occurs
       * through the surface). Conversely, if <kbd>Flow direction through the
       * valve</kbd> is set to <kbd>Outflow</kbd>, closing starts when
       * @f$\frac{dV}{dt} < 0@f$.
       *
       * To safeguard against spurious flow effects (especially immediately
       * after valve opening), closing is triggered only if the closing
       * condition is met for two consecutive timesteps.
       */
      VolumeDerivative
    };

    /**
     * Constructor.
     *
     * @param[in] subsection Parameter subsection for the RIIS handler.
     * @param[in] riis_handler_ Reference to the @ref RIISHandler owning this surface.
     * @param[in] fluid_dynamics_    reference to the FluidDynamics instance
     *   that owns riis_handler_.
     * @param[in] surface_index_ The index for this surface within the @ref RIISHandler.
     */
    RIISSurface(const std::string &  subsection,
                const RIISHandler &  riis_handler_,
                const FluidDynamics &fluid_dynamics_,
                const unsigned int & surface_index_);

    /**
     * @brief Declare parameters related to immersed surfaces.
     *
     * Since we do not know a priori how many surfaces are going to be present,
     * this declares all parameters as lists: in other words, the method
     * declares parameters not for a single instance of RIISSurface, but for
     * multiple ones. When parsing parameters, the entry in each list
     * corresponding to @ref surface_index is retrieved.
     *
     * Parameters that are not specific to the surface but are related to the
     * general handling of the RIIS model are declared by
     * @ref RIISHandler::declare_parameters.
     */
    virtual void
    declare_parameters(ParamHandler &params) const override;

    /**
     * @brief Parse parameters.
     *
     * On top of the parameters declared in @ref declare_parameters, it parses
     * some of the general ones declared in @ref RIISHandler::declare_parameters
     * that are useful for the handling of a single surface.
     */
    virtual void
    parse_parameters(ParamHandler &params) override;

    /**
     * @brief Setup the immersed surface.
     *
     * Reads all relevant files and sets up signed distance functions and
     * vectors and time interpolations.
     */
    void
    setup(const DoFHandler<dim> &dof_handler,
          const double &         time_init_,
          const double &         time_final_,
          const double &         time_step_,
          const double &         period_);

    /**
     * @brief Update the immersed surface.
     *
     * Displaces the immersed surface according to the displacement law. Returns
     * true if the surface moved since last call to update, false otherwise.
     */
    bool
    update();

    /**
     * @brief Compute surface velocity term at quadrature point.
     *
     * Adds to @p velocity_loc the evaluation of @f$\mathbf{u}_{\Gamma}@f$ for
     * this surface at the point @p x.
     *
     * @param[in]      x Coordinates of the point.
     * @param[in, out] velocity_loc Vector to be filled.
     */
    void
    compute_velocity_loc(const Point<dim> &      x,
                         Tensor<1, dim, double> &velocity_loc) const;

    /**
     * @brief Attach output related to this surface to a DataOut object.
     */
    void
    attach_output(DataOut<dim> &         data_out,
                  const DoFHandler<dim> &dof_handler) const;

    /**
     * @brief Export VTP of the surface.
     */
    void
    export_vtp(const double &time, const unsigned int &timestep_number);

    /**
     * @brief Declare CSV entries for this surface.
     */
    void
    declare_entries_csv(utils::CSVWriter &csv_writer) const;

    /**
     * @brief Set CSV entries for this surface.
     */
    void
    set_entries_csv(utils::CSVWriter &csv_writer) const;

    /**
     * Evaluates smoothed Dirac delta function for the immersed surface.
     *
     * @param[in] signed_distance Value of the signed distance at which
     * to evaluate the delta.
     */
    double
    delta_function(const double &signed_distance) const
    {
      const double epsilon = get_epsilon();

      return std::abs(signed_distance) < epsilon ?
               0.5 / epsilon *
                 (1. + std::cos(M_PI * signed_distance / epsilon)) :
               0;
    }

    /**
     * @brief Read state from CSV file, for restart.
     */
    void
    restart(const std::string &csv_filename,
            const double &     time,
            const double &     time_step);

    /// @name Getters.
    /// @{

    /// Get displacement law.
    const DisplacementLaw &
    get_displacement_law() const
    {
      return prm_displacement_law;
    }

    /// Get current signed distance function.
    const utils::InterpolatedSignedDistance &
    get_current_signed_distance() const
    {
      return *current_signed_distance;
    }

    /// Get displacement function.
    const utils::VTKFunction &
    get_displacement_g() const
    {
      return *displacement_g;
    }


    /// Get whether the surface is moving with ALE.
    bool
    get_move_with_ale() const
    {
      return prm_move_with_ale;
    }

    /// Get epsilon.
    double
    get_epsilon() const
    {
      const double epsilon_closed = prm_epsilon * prm_epsilon_factor_closed;
      const double epsilon_open   = prm_epsilon * prm_epsilon_factor_open;

      return utils::cosine_ramp(
        opening_coeff, 0.0, 1.0, epsilon_closed, epsilon_open);
    }

    /// Get pressure jump.
    double
    get_pressure_jump() const
    {
      return pressure_jump;
    }

    /// Get current configuration.
    ValveConfiguration
    get_configuration() const
    {
      return current_configuration;
    }

    /// Get current opening coefficient.
    double
    get_opening_coeff() const
    {
      return opening_coeff;
    }

    /// Get current velocity coefficient.
    double
    get_velocity_coeff() const
    {
      return velocity_coeff;
    }

    /// Get resistance.
    double
    get_resistance() const
    {
      const double resistance_closed =
        prm_resistance * prm_resistance_factor_closed;
      const double resistance_open =
        prm_resistance * prm_resistance_factor_open;

      return utils::cosine_ramp(
        opening_coeff, 0.0, 1.0, resistance_closed, resistance_open);
    }

    /// @}

  protected:
    /// Cutoff distance factor for signed distance evaluation (see
    /// @ref utils::VTKFunction::setup_as_signed_distance). The actual cutoff distance
    /// is computed as prm_epsilon * max(prm_epsilon_factor_open,
    /// prm_epsilon_factor_closed) * cutoff_distance_factor. This amounts to
    /// slightly enlarging the bounding box for the surface, with the aim of
    /// facilitating postprocessing.
    static inline constexpr double cutoff_distance_factor = 1.1;

    /// Evaluate the ramp at a given time, according to the value of @ref prm_ramp_shape.
    double
    evaluate_ramp(const double &t) const;

    /// Evaluate the inverse of the ramp for a given coefficient value. Used
    /// when restarting with Flow driven displacement law, to determine the ramp
    /// initial time.
    double
    evaluate_ramp_inverse(const double &c) const;

    /// Reference to the RIIS handler owning this surface.
    const RIISHandler &riis_handler;

    /// Reference to the FluidDynamics instance.
    const FluidDynamics &fluid_dynamics;

    /// Index of this surface.
    unsigned int surface_index;

    /// @name Time solver info.
    /// @{

    /// Initial time.
    double time_init;

    /// Final time.
    double time_final;

    /// Time step.
    double time_step;

    /// Period.
    double period;

    /// @}

    /// @name Immersed surface.
    /// @{

    /// Current signed distance function. Obtained by interpolating the
    /// imported distance functions and/or warping them according to the
    /// displacement law.
    std::unique_ptr<utils::InterpolatedSignedDistance> current_signed_distance;

    /// Displacement component @f$\mathbf{g}@f$. Loads the same VTK file as
    /// @ref current_signed_distance, but extracts the spatial component of the
    /// displacement.
    std::unique_ptr<utils::VTKFunction> displacement_g;

    /// @}

    /// Current opening coefficient. 0 stands for the fully closed
    /// configuration, and 1 stands for the fully open one.
    double opening_coeff;

    /// Current configuration.
    ValveConfiguration current_configuration;

    /// Coefficient for surface velocity.
    double velocity_coeff;

    /// @name Control volumes.
    /// @{

    /// Pressure jump across the surface (upwind - downwind).
    double pressure_jump;

    /// Pressure jump across the surface at previous timestep.
    double pressure_jump_old;

    /// @}

    /// Volume derivative of the compartment associated to the surface.
    double volume_derivative;

    /// Volume derivative at previous timestep.
    double volume_derivative_old;

    /// Opening start time, for the flow driven displacement law. The optional
    /// has value if the displacement law is flow driven, and if the surface has
    /// opened at least once since the start of the simulation.
    ///
    /// @todo The opening time is not loaded upon restart, and thus refractory
    /// time does not work when restarting.
    std::optional<double> flow_driven_opening_time;

    /// Closing start time, for the flow driven displacement law. The optional
    /// has value if the displacement law is flow driven, and if the surface has
    /// closed at least once since the start of the simulation.
    ///
    /// @todo The opening time is not loaded upon restart, and thus refractory
    /// time does not work when restarting.
    std::optional<double> flow_driven_closing_time;

    /// Used to export immersed surface VTP files.
    std::vector<std::pair<double, std::string>> vtp_output_times_and_names;

    /// Nearest neighboring DoFs, used if moving the surface with ALE.
    std::vector<std::vector<std::pair<types::global_dof_index, double>>>
      closest_dofs;

    /// @name Parameters read from file.
    /// @{

    /// Label identifying the surface. It is used mostly for output readability,
    /// but it can also be used to retrieve a specific surface in
    /// @ref RIISHandler::get_surface.
    std::string prm_label;

    /// Mangled label.
    std::string prm_label_mangled;

    /// Basename of input files.
    std::string prm_basename;

    /// Name of the displacement vector within the input file. The vector is
    /// assumed to bring the surface from the input configuration to the other
    /// configuration: if the input configuration is Open, the vector should
    /// bring in Closed configuration, and vice versa.
    std::string prm_displacement_name;

    /// Factor multiplying the surface velocity u_Gamma.
    double prm_surface_velocity_factor;

    /// Toggle flipping the normal vector. Changes the sign of the normal as
    /// computed by QuadratureRIIS.
    bool prm_flip_normal;

    /// Toggle moving immersed surface with ALE.
    bool prm_move_with_ale;

    /// @name Resistive parameters.
    /// @{

    /// Half-thickness of the smeared Dirac delta.
    double prm_epsilon;

    /// Resistance coefficient [kg/(m s)].
    double prm_resistance;

    /// Epsilon reduction factor in open configuration.
    double prm_epsilon_factor_open;

    /// Epsilon reduction factor in closed configuration.
    double prm_epsilon_factor_closed;

    /// Resistance reduction factor in open configuration.
    double prm_resistance_factor_open;

    /// Resistance reduction factor in closed configuration.
    double prm_resistance_factor_closed;

    /// @}

    /// @name Control volumes.
    /// @{

    /// Toggle control volumes.
    bool prm_enable_control_volumes;

    /// Upwind control volume label.
    std::string prm_control_volume_upwind;

    /// Downwind control volume label.
    std::string prm_control_volume_downwind;

    /// @}

    /// @name Displacement law.
    /// @{

    /// Configuration of the input VTP file.
    ValveConfiguration prm_input_configuration;

    /// Configuration to be set at the beginning of the simulation.
    ValveConfiguration prm_initial_configuration;

    /// Displacement law.
    DisplacementLaw prm_displacement_law;

    /// Ramp shape.
    RampShape prm_ramp_shape;

    /// Coefficient for exponential and cosine-exponential ramp shapes.
    double prm_exp_ramp_coefficient;

    /// Toggle on-off behavior of the valve. If enabled, the surface does not
    /// move over time. Instead, the surface is present if $c < 0.5$, and
    /// disappears instantaneously as soon as @f$c \geq 0.5@f$.
    bool prm_on_off;

    /// @name Linear displacement law.
    /// @{

    /// Start time for the first ramp (from initial to final configuration).
    double prm_prescribed_t0;

    /// End time for the first ramp (from initial to final configuration).
    double prm_prescribed_t1;

    /// Start time for the second ramp (back from final to initial
    /// configuration).
    double prm_prescribed_t2;

    /// End time for the second ramp (back from final to initial
    /// configuration).
    double prm_prescribed_t3;

    /// @}

    /// @name Flow driven displacement law.
    /// @{

    /// Valve closing approach.
    FlowDrivenClosing prm_flow_driven_closing;

    /// Duration of the opening ramp [s].
    double prm_flow_driven_duration_opening;

    /// Duration of the closing ramp [s].
    double prm_flow_driven_duration_closing;

    /// Refractory time for the flow driven displacement law. After the valve
    /// has closed (or opened), it cannot open (or close) again before this
    /// amount of time has passed [s].
    double prm_flow_driven_refractory_time;

    /// Label of the volume compartment to be checked, if
    /// prm_flow_driven_closing set to VolumeDerivative. The label has to be
    /// defined in the Volume tags section of FluidDynamics parameters.
    std::string prm_flow_driven_volume_compartment;

    /// Whether the valve should be at the inflow or at the outflow of the
    /// associated compartment.
    std::string prm_flow_driven_volume_flow_direction;

    /// Explicitly set closing start time.
    double prm_flow_driven_prescribed_closing_time;

    /// @}

    /// @name Korakianitis-Shi model.
    /// @{

    /// KS open angle.
    double prm_model_KS_theta_max;

    /// @}

    /// @}

    /// @}
  };
} // namespace lifex

#endif
