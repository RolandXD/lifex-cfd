/********************************************************************************
  Copyright (C) 2020 - 2023 by the lifex authors.

  This file is part of lifex.

  lifex is free software; you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  lifex is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with lifex.  If not, see <http://www.gnu.org/licenses/>.
********************************************************************************/

/**
 * @file
 *
 * @author Ivan Fumagalli <ivan.fumagalli@polimi.it>.
 * @author Alberto Zingaro <alberto.zingaro@polimi.it>.
 * @author Michele Bucelli <michele.bucelli@polimi.it>.
 */

#ifndef LIFEX_HELPER_RIIS_HANDLER_HPP_
#define LIFEX_HELPER_RIIS_HANDLER_HPP_

#include "core/source/core_model.hpp"

#include "core/source/geometry/mesh_handler.hpp"

#include "core/source/io/csv_writer.hpp"
#include "core/source/io/vtk_function.hpp"

#include "core/source/numerics/projection.hpp"
#include "core/source/numerics/time_interpolation.hpp"

#include "source/helpers/ale_handler.hpp"
#include "source/helpers/riis_surface.hpp"

#include <deal.II/base/time_stepping.h>

#include <algorithm>
#include <limits>
#include <map>
#include <memory>
#include <set>
#include <string>
#include <utility>
#include <vector>

namespace lifex
{
  // Forward declarations.
  class FluidDynamics;

  /// @brief Class to account for one or more immersed surface(s) by the RIIS method.
  ///
  /// For each immersed surface @f$\Gamma_k, k=1,\dots,K@f$, the RIIS method
  /// consists of introducing an additional term
  /// @f[
  /// \left(\frac{R_k}{\varepsilon_k}(\mathbf u - \mathbf u_{\Gamma_k})
  /// \delta_{\Gamma_k,\varepsilon_k}(\varphi_{\Gamma_k}),
  /// \mathbf v \right)_\Omega
  /// @f]
  /// to the momentum equation (see @ref FluidDynamics), where @f$R_k@f$ is a
  /// resistance coefficient, @f$\varepsilon_k@f$ represents the thickness of
  /// the smeared Dirac delta function
  /// @f[
  /// \delta_{\Gamma_k,\varepsilon_k}(s) =
  /// \begin{cases}
  /// \frac{1+\cos(s\,\pi/\varepsilon_k)}{2\varepsilon_k} & \text{if }
  /// |s|\leq\varepsilon_k\\ 0 & \text{if } |s|>\varepsilon_k
  /// \end{cases}
  /// @f]
  /// and @f$\varphi_{\Gamma_k}@f$ is a signed distance function from
  /// @f$\Gamma_k@f$
  /// imported as @ref utils::VTKFunction.
  ///
  /// Each individual surface is described by an instance of the @ref RIISSurface class.
  /// Refer to the documentation of that class for the description of all the
  /// available options for the displacement of immersed surface over time.
  ///
  /// @section riis_ale Moving domain case
  /// In case the domain is moving (see @ref ale and @ref ALEHandler), it is
  /// possible to move the immersed surfaces following the displacement of the
  /// fluid domain.
  ///
  /// Surfaces can be moved with the fluid domain if the parameter <kbd>Move
  /// surfaces with ALE</kbd> is set to true (it is false by default). In such
  /// case, the surface displacement will actually be (see above):
  /// - static case:
  /// @f$\mathbf d_\Gamma(t,\mathbf x) = \mathbf d_\text{ALE}(t,\mathbf x)@f$;
  /// - moving case: @f$\mathbf d_\Gamma(t,\mathbf x) =
  /// \frac{t}{T_\text{final}}\mathbf g(\mathbf x) + \mathbf
  /// d_\text{ALE}(t,\mathbf x)@f$ (or a periodic, piecewise linear time
  /// interpolation);
  ///
  /// @note If the immersed surfaces are displaced along with ALE, the ALE
  /// displacement field is given a physical interpretation (whereas in general
  /// it is arbitrary) and can have a significant impact on the energy balance
  /// of the fluid momentum equation. This might lead to time-instabilities in
  /// some cases, mostly involving FSI simulations.
  ///
  /// **References**: @refcite{Fedele2017, Fedele et al. (2017)},
  /// @refcite{Fumagalli2020, Fumagalli et al. (2020)}
  class RIISHandler : public CoreModel
  {
  public:
    /**
     * This class needs to access private members
     * such as @ref dof_handler and the RIIS factor vector,
     * to compute the latter at quadrature points.
     */
    template <class NumberType>
    friend class QuadratureRIIS;

    /// Constructor.
    /// @param[in] subsection         parameter subsection path for this class.
    /// @param[in] fluid_dynamics_    reference to the FluidDynamics instance
    ///   that owns this object.
    RIISHandler(const std::string &  subsection,
                const FluidDynamics &fluid_dynamics_);

    /// Set up finite element spaces and degrees of freedom, and setup immersed
    /// surfaces.
    /// @param[in] triangulation_ Triangulation on which to interpolate the signed distance functions.
    /// @param[in] time_init_ Initial time of the simulation.
    /// @param[in] time_final_ Final time of the simulation.
    /// @param[in] time_step_ Timestep length of the simulation.
    /// @param[in] period_ Period of the simulation.
    /// @param[in] ale_handler_ ALE handler for moving surfaces with ALE. Leave
    /// to nullptr if ALE is not enabled.
    void
    setup(const std::shared_ptr<const utils::MeshHandler> &triangulation_,
          const double &                                   time_init_,
          const double &                                   time_final_,
          const double &                                   time_step_,
          const double &                                   period_,
          const std::shared_ptr<const ALEHandler> &ale_handler_ = nullptr);

    /// Update: updates the position of immersed surfaces and the related signed
    /// distance vectors. Returns true if any surface has moved since last call
    /// to update, false otherwise.
    bool
    update();

    /// Compute the normal vectors to each immersed surface, as the
    /// gradients of the distance functions.
    ///
    /// Compute the @f$L^2@f$ projection of the gradient of the finite element
    /// vector of the distance functions of all surfaces,
    /// by @ref utils::ProjectionL2.
    ///
    /// @note The resulting normal vectors (one for each immersed surface)
    /// have values in the whole domain.
    void
    recover_normal_and_curvature();

    /// Compute surface velocity term at quadrature point.
    /// To be used in external assembling cycle
    /// (cf. e.g. @ref FluidDynamics::assemble_callback_cell() ).
    ///
    /// Fills @p velocity_loc with the evaluation of @f$\sum_k \mathbf
    /// u_{\Gamma_k}@f$ at the point @p x.
    ///
    /// @param[in]      x Coordinates of the point.
    /// @param[in, out] velocity_loc Vector to be filled.
    void
    compute_velocity_loc(const Point<dim> &      x,
                         Tensor<1, dim, double> &velocity_loc) const;

    /// Attach output data to a DataOut object and export surface vtp if
    /// @ref prm_enable_surface_output <kbd>== true</kbd>.
    ///
    /// @brief Save current signed distance function, normal vectors and
    /// curvature.
    ///
    /// @param[in, out] data_out DataOut exporter.
    /// @param[in]      time Current simulation time.
    /// @param[in]      timestep_number Current time-step iteration.
    void
    attach_output(DataOut<dim> &      data_out,
                  const double &      time            = 0.0,
                  const unsigned int &timestep_number = 0);

    /// Override of @ref CoreModel::declare_parameters.
    virtual void
    declare_parameters(ParamHandler &params) const override;

    /// Override of @ref CoreModel::parse_parameters.
    virtual void
    parse_parameters(ParamHandler &params) override;

    /// Declare entries for the CSV log.
    void
    declare_entries_csv(utils::CSVWriter &csv_writer) const;

    /// Set entries for the CSV log.
    void
    set_entries_csv(utils::CSVWriter &csv_writer) const;

    /// Get DoF handler.
    const DoFHandler<dim> &
    get_dof_handler() const
    {
      return dof_handler;
    }

    /// Triangulation.
    const utils::MeshHandler &
    get_triangulation() const
    {
      return *triangulation;
    }

    /// Get ALE handler.
    const std::shared_ptr<const ALEHandler> &
    get_ale_handler() const
    {
      return ale_handler;
    }

    /// Get k-th immersed surface.
    const RIISSurface &
    get_surface(const unsigned int &k) const
    {
      return surfaces[k];
    }

    /// Get immersed surface based on label.
    const RIISSurface &
    get_surface(const std::string &label) const
    {
      for (const auto &surface : surfaces)
        if (surface.prm_label == label)
          return surface;

      AssertThrow(false,
                  ExcMessage("No immersed surface with label \"" + label +
                             "\" is present."));
    }

    /// Check whether a surface with a given label is present.
    bool
    has_surface(const std::string &label) const
    {
      for (const auto &surface : surfaces)
        if (surface.prm_label == label)
          return true;

      return false;
    }

    /// Get whether at least one of the RIIS surfaces is moving with ALE.
    bool
    get_move_surfaces_with_ale() const
    {
      for (const auto &surface : surfaces)
        {
          if (surface.get_move_with_ale())
            return true;
        }

      return false;
    }

    /// Get ALE support points.
    const std::vector<std::map<types::global_dof_index, Point<dim>>>
    get_ale_support_points() const
    {
      Assert(ale_handler && get_move_surfaces_with_ale(),
             ExcMessage(
               "ALE must be active and surfaces must be moving with ALE."));

      return ale_support_points;
    }

    /// Get whether control volumes are enabled.
    bool
    get_control_volumes_enabled() const
    {
      return prm_enable_control_volumes;
    }

    /// Get number of immersed surfaces.
    unsigned int
    n_resistive_surfaces() const
    {
      return surfaces.size();
    }

    /// Get whether the displacement law of at least one immersed surface is set
    /// to pressure jump.
    const bool &
    displacement_law_flow_driven() const
    {
      return prm_displacement_law_flow_driven;
    }

    /// Get vector of pressure jumps.
    std::vector<double>
    get_pressure_jumps() const
    {
      std::vector<double> result;

      for (const auto &surface : surfaces)
        result.push_back(surface.get_pressure_jump());

      return result;
    }

    /**
     * @brief Read state from CSV file, for restart.
     */
    void
    restart(const std::string &csv_filename,
            const double &     time,
            const double &     time_step);

    /**
     * @brief Trigger interpolation of signed distance for all immersed surfaces.
     *
     * Actual interpolation happens only if necessary (i.e. if surface position
     * has changed since last interpolation). See also @ref utils::InterpolatedSignedDistance.
     */
    void
    interpolate_signed_distances() const
    {
      for (const auto &surface : surfaces)
        surface.get_current_signed_distance().interpolate();
    }

  protected:
    /// Reference to the FluidDynamics instance.
    const FluidDynamics &fluid_dynamics;

    /// Mesh.
    std::shared_ptr<const utils::MeshHandler> triangulation;

    /// Finite element space.
    std::unique_ptr<FiniteElement<dim>> fe;

    /// DoFHandler.
    DoFHandler<dim> dof_handler;

    /// Quadrature formula for L2 projections.
    std::unique_ptr<Quadrature<dim>> quadrature_projection;

    /// Immersed surfaces.
    std::vector<RIISSurface> surfaces;

    /// Initial time, set at setup().
    double time_init;

    /// Final time, set at setup().
    double time_final;

    /// Simulation time step, set at setup().
    double time_step;

    /// Simulation period, set at setup().
    double period;

    /// @name Displacement of surfaces with ALE.
    /// @{

    /// Pointer to the ALE handler. It is left to nullptr if ALE is not enabled.
    std::shared_ptr<const ALEHandler> ale_handler;

    /// Support points of displacement DoFs in the reference configuration, with
    /// one entry for each displacement component. Used to interpolate ALE
    /// displacement onto surfaces.
    std::vector<std::map<types::global_dof_index, Point<dim>>>
      ale_support_points;

    /// @}

    /// @name Computation of curvature and normal vector.
    /// @{

    /// FE for the normal vector: it is the vectorial version of @ref fe, used
    /// to compute the normal vector projection.
    std::unique_ptr<FESystem<dim>> fe_normal;

    /// DoFHandler for the normal vector.
    DoFHandler<dim> dof_handler_normal;

    /// L2-projection operator (vectorial).
    /// @warning Assumption: the mesh is not moving.
    std::unique_ptr<utils::ProjectionL2> projection_normal;

    /// L2 projection (ghosted) of current normals
    /// (cf. @ref recover_normal_and_curvature()).
    LinAlg::MPI::Vector current_normal_vector;

    /// L2 projection (non-ghosted) of current normals
    /// (cf. @ref recover_normal_and_curvature()).
    LinAlg::MPI::Vector current_normal_vector_owned;

    /// L2-projection operator (scalar).
    /// @warning Assumption: the mesh is not moving.
    std::unique_ptr<utils::ProjectionL2> projection_curvature;

    /// L2 projection (ghosted) of current curvatures
    /// (cf. @ref recover_normal_and_curvature()).
    LinAlg::MPI::Vector current_curvature_vector;

    /// L2 projection (non-ghosted) of current curvatures
    /// (cf. @ref recover_normal_and_curvature()).
    LinAlg::MPI::Vector current_curvature_vector_owned;

    /// @}

    /// @name Parameters read from file.
    /// @{

    /// Use unsigned distance when assembling RIIS factor.
    bool prm_unsigned_in_assembly;

    /// Toggle control volumes.
    bool prm_enable_control_volumes;

    /// Toggle save vtp of immersed surfaces.
    bool prm_enable_surface_output;

    /// Toggle save L2-projected normals and curvatures.
    bool prm_enable_normal_and_curvature_output;

    /// @}

    /// Flag: true if displacement of immersed surface is flow driven for <b>at
    /// least one</b> surface.
    bool prm_displacement_law_flow_driven;

    /// Vector containing the characteristic times for the opening and closure
    /// of immersed surfaces (effectively used when displacement law selection
    /// is Pressure jump).
    std::shared_ptr<std::vector<std::pair<double, double>>>
      valves_characteristic_times;
  };

  /**
   * @brief Class to evaluate several RIIS-related quantities on quadrature nodes.
   */
  template <class NumberType>
  class QuadratureRIIS : public QuadratureEvaluationFEM<NumberType>
  {
  public:
    /**
     * Enumeration of the different quantities that this class can compute.
     */
    enum class RIISUpdateFlags
    {
      /// Update nothing.
      update_default = 0,

      /// Compute the RIIS factor
      /// @f$\sum_k\frac{R_k}{\varepsilon_k}\delta_{\Gamma_k,\varepsilon_k}
      /// (\varphi_{\Gamma_k})@f$.
      update_riis_factor = 1 << 0,

      /// Compute the surface normal extended to @f$\mathbb R^\text{dim}@f$,
      /// @f[\widetilde{\mathbf n} =
      /// \frac{\nabla\varphi_{\Gamma_k}}{|\nabla\varphi_{\Gamma_k}|}@f]
      update_normal = 1 << 1,

      /// Compute the surface curvature extended to @f$\mathbb
      /// R^\text{dim}@f$, according to Weingarten map:
      /// @f[\widetilde{H} =
      /// -{\rm div}_{\Gamma_k}\widetilde{\mathbf n} =
      /// -\frac{\Delta\varphi_{\Gamma_k}}{|\nabla\varphi_{\Gamma_k}|} +
      /// \frac{\nabla^2\varphi_{\Gamma_k}\colon(\nabla\varphi_{\Gamma_k}\otimes\nabla\varphi_{\Gamma_k})}{|\nabla\varphi_{\Gamma_k}|^3}@f].
      update_curvature = 1 << 2
    };

    /**
     * @brief Constructor.
     *
     * @param[in] riis_handler_ Handler providing RIIS variables
     * @param[in] quadrature Quadrature formula
     * @param[in] riis_update_flags_ Bitwise flags indicating the quantities to be computed.
     */
    QuadratureRIIS(const RIISHandler &    riis_handler_,
                   const Quadrature<dim> &quadrature,
                   const RIISUpdateFlags &riis_update_flags_)
      : QuadratureEvaluationFEM<NumberType>(
          riis_handler_.get_dof_handler(),
          quadrature,
          update_values | update_quadrature_points |
            (bitmask_contains(riis_update_flags_,
                              RIISUpdateFlags::update_normal) ?
               update_gradients :
               update_default) |
            (bitmask_contains(riis_update_flags_,
                              RIISUpdateFlags::update_curvature) ?
               update_gradients | update_hessians :
               update_default))
      , riis_handler(riis_handler_)
      , riis_update_flags(riis_update_flags_)
    {
      AssertThrow(riis_handler.ale_handler ||
                    !riis_handler.get_move_surfaces_with_ale(),
                  ExcMessage("ALE must be active and initialized to move "
                             "immersed surfaces with ALE displacement."));
    }

    /// Call operator. Calling this throws an exception: to access computed
    /// quantities use the @ref factor, @ref normal, @ref curvature methods
    /// or one of the derived classes.
    NumberType
    operator()(const unsigned int & /*q*/,
               const double & /*t*/       = 0,
               const Point<dim> & /*x_q*/ = Point<dim>()) override
    {
      AssertThrow(
        false,
        ExcMessage(
          "To access quantities computed by QuadratureRIIS, use either the "
          "dedicated getter methods or one of the derived classes."));

      return NumberType();
    }

    /// Get distance from nearest surface.
    double
    distance(const unsigned int &q) const
    {
      return precomputed_distance[q];
    }

    /// Get RIIS factor.
    double
    factor(const unsigned int &q) const
    {
      return precomputed_factor[q];
    }

    /// Get normal.
    const Tensor<1, dim> &
    normal(const unsigned int &q) const
    {
      return precomputed_normal[q];
    }

    /// Get curvature.
    double
    curvature(const unsigned int &q) const
    {
      return precomputed_curvature[q];
    }

  protected:
    /// Construct and initialize the quadrature evaluation for ALE
    /// displacement, if needed.
    virtual void
    post_init_callback() override
    {
      const unsigned int n_q_points = this->fe_values->get_quadrature().size();

      precomputed_distance.resize(n_q_points);

      if (bitmask_contains(riis_update_flags,
                           RIISUpdateFlags::update_riis_factor))
        precomputed_factor.resize(n_q_points);

      if (bitmask_contains(riis_update_flags, RIISUpdateFlags::update_normal))
        precomputed_normal.resize(n_q_points);

      if (bitmask_contains(riis_update_flags,
                           RIISUpdateFlags::update_curvature))
        precomputed_curvature.resize(n_q_points);

      // If either normal or curvature are required, we trigger the
      // interpolation of signed distances, to prevent deadlocks.
      if (bitmask_contains(riis_update_flags, RIISUpdateFlags::update_normal) ||
          bitmask_contains(riis_update_flags,
                           RIISUpdateFlags::update_curvature))
        riis_handler.interpolate_signed_distances();
    }

    /// Precompute quantities requested by riis_update_flags, and call reinit
    /// on the quadrature evaluation for ALE displacement, if needed.
    virtual void
    post_reinit_callback(
      const DoFHandler<dim>::active_cell_iterator & /*cell_other*/) override
    {
      const unsigned int n_q_points = this->fe_values->get_quadrature().size();
      const unsigned int n_surfaces = riis_handler.n_resistive_surfaces();

      for (unsigned int q = 0; q < n_q_points; ++q)
        {
          x_q = this->fe_values->quadrature_point(q);

          precomputed_distance[q] = std::numeric_limits<int>::max();

          if (bitmask_contains(riis_update_flags,
                               RIISUpdateFlags::update_riis_factor))
            precomputed_factor[q] = 0.0;

          if (bitmask_contains(riis_update_flags,
                               RIISUpdateFlags::update_normal))
            precomputed_normal[q] = 0.0;

          if (bitmask_contains(riis_update_flags,
                               RIISUpdateFlags::update_curvature))
            precomputed_curvature[q] = 0.0;

          for (unsigned int k = 0; k < n_surfaces; ++k)
            {
              const RIISSurface &surface = riis_handler.get_surface(k);

              signed_distance = surface.current_signed_distance->value(x_q);

              if (std::abs(signed_distance) < std::abs(precomputed_distance[q]))
                precomputed_distance[q] = signed_distance;

              // If the distance is greater than epsilon, we compute no
              // further: no contributions must be made to the RIIS factor,
              // normal vector or curvature.
              if (std::abs(signed_distance) > surface.get_epsilon())
                continue;

              // Compute RIIS factor.
              if (bitmask_contains(riis_update_flags,
                                   RIISUpdateFlags::update_riis_factor))
                precomputed_factor[q] +=
                  surface.get_resistance() / surface.get_epsilon() *
                  surface.delta_function(signed_distance);

              // Compute normalized gradient of the distance function.
              if (bitmask_contains(riis_update_flags,
                                   RIISUpdateFlags::update_normal) ||
                  bitmask_contains(riis_update_flags,
                                   RIISUpdateFlags::update_curvature))
                {
                  distance_gradient = 0.0;

                  for (unsigned int i = 0; i < this->dof_indices.size(); ++i)
                    distance_gradient +=
                      surface.get_current_signed_distance()
                        .get_ghosted()[this->dof_indices[i]] *
                      this->fe_values->shape_grad(i, q);

                  distance_gradient_norm = distance_gradient.norm();

                  // If the norm of the gradient is zero, we compute no
                  // further: no contributions must be made to the normal and
                  // curvature
                  if (distance_gradient_norm <= 0)
                    continue;

                  distance_gradient /= distance_gradient_norm;
                }

              // Compute normal vector.
              if (bitmask_contains(riis_update_flags,
                                   RIISUpdateFlags::update_normal))
                precomputed_normal[q] +=
                  (surface.prm_flip_normal ? -1.0 : 1.0) * distance_gradient;

              // Compute hessian and curvature.
              if (bitmask_contains(riis_update_flags,
                                   RIISUpdateFlags::update_curvature))
                {
                  distance_hessian = 0.0;

                  for (unsigned int i = 0; i < this->dof_indices.size(); ++i)
                    distance_hessian += surface.get_current_signed_distance()
                                          .get_ghosted()[this->dof_indices[i]] *
                                        this->fe_values->shape_hessian(i, q);

                  // We explicitly loop over the components to avoid creation
                  // of temporaries. What we are actually computing is:
                  // -(trace(distance_hessian) -
                  // scalar_product(distance_hessian,
                  // outer_product(distance_gradient, distance_gradient))) /
                  // distance_gradient_norm
                  for (unsigned int d1 = 0; d1 < dim; ++d1)
                    {
                      precomputed_curvature[q] -=
                        (surface.prm_flip_normal ? -1.0 : 1.0) *
                        distance_hessian[d1][d1] / distance_gradient_norm;

                      for (unsigned int d2 = 0; d2 < dim; ++d2)
                        precomputed_curvature[q] +=
                          (surface.prm_flip_normal ? -1.0 : 1.0) *
                          distance_hessian[d1][d2] * distance_gradient[d1] *
                          distance_gradient[d2] / distance_gradient_norm;
                    }
                }
            }
        }
    }

    /// Reference to the RIIS handler.
    const RIISHandler &riis_handler;

    /// Flags indicating what to quantities to update upon calling reinit.
    RIISUpdateFlags riis_update_flags;

    /// @name Quantities for preallocation.
    /// @{

    /// Current quadrature point.
    Point<dim> x_q;

    /// Current signed distance functions.
    double signed_distance;

    /// Gradient of the distance functions.
    Tensor<1, dim> distance_gradient;

    /// Norm of the gradient of the distance function.
    double distance_gradient_norm;

    /// Hessian of the distance function.
    Tensor<2, dim> distance_hessian;

    /// Distance from nearest surface.
    std::vector<double> precomputed_distance;

    /// RIIS factor.
    std::vector<double> precomputed_factor;

    /// Normal vector.
    std::vector<Tensor<1, dim>> precomputed_normal;

    /// Curvature.
    std::vector<double> precomputed_curvature;

    /// @}
  };

  /// @brief Decorator of @ref QuadratureRIIS, to have operator()
  /// returning the RIIS factor.
  class QuadratureRIISFactor : public QuadratureRIIS<double>
  {
  public:
    /// Constructor.
    QuadratureRIISFactor(
      const RIISHandler &    riis_handler,
      const Quadrature<dim> &quadrature,
      const RIISUpdateFlags &update_flags = RIISUpdateFlags::update_riis_factor)
      : QuadratureRIIS(riis_handler, quadrature, update_flags)
    {}

    /// Call operator: returns the RIIS factor.
    virtual double
    operator()(const unsigned int &q,
               const double & /*t*/       = 0,
               const Point<dim> & /*x_q*/ = Point<dim>()) override
    {
      return precomputed_factor[q];
    }

  protected:
  };

  /// @brief Decorator of @ref QuadratureRIIS, to have operator()
  /// returning the normal vector.
  class QuadratureRIISNormal : public QuadratureRIIS<Tensor<1, dim, double>>
  {
  public:
    /// Constructor.
    QuadratureRIISNormal(
      const RIISHandler &    riis_handler,
      const Quadrature<dim> &quadrature,
      const RIISUpdateFlags &update_flags = RIISUpdateFlags::update_default)
      : QuadratureRIIS(riis_handler,
                       quadrature,
                       update_flags | RIISUpdateFlags::update_normal)
    {}

    /// Returns the normal vector.
    virtual Tensor<1, dim, double>
    operator()(const unsigned int &q,
               const double & /*t*/       = 0,
               const Point<dim> & /*x_q*/ = Point<dim>()) override
    {
      return precomputed_normal[q];
    }
  };

  /// @brief Decorator of @ref QuadratureRIIS, to have operator()
  /// returning the curvature.
  class QuadratureRIISCurvature : public QuadratureRIIS<double>
  {
  public:
    /// Constructor.
    QuadratureRIISCurvature(
      const RIISHandler &    riis_handler,
      const Quadrature<dim> &quadrature,
      const RIISUpdateFlags &update_flags = RIISUpdateFlags::update_default)
      : QuadratureRIIS(riis_handler,
                       quadrature,
                       update_flags | RIISUpdateFlags::update_curvature)
    {}

    /// Returns curvature.
    double
    operator()(const unsigned int &q,
               const double & /*t*/       = 0,
               const Point<dim> & /*x_q*/ = Point<dim>()) override
    {
      return precomputed_curvature[q];
    }
  };

} // namespace lifex
#endif /* LIFEX_HELPER_RIIS_HANDLER_HPP_ */
