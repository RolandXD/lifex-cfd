/********************************************************************************
  Copyright (C) 2020 - 2023 by the lifex authors.

  This file is part of lifex.

  lifex is free software; you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  lifex is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with lifex.  If not, see <http://www.gnu.org/licenses/>.
********************************************************************************/

/**
 * @file
 *
 * @author Ivan Fumagalli <ivan.fumagalli@polimi.it>.
 * @author Alberto Zingaro <alberto.zingaro@polimi.it>.
 * @author Michele Bucelli <michele.bucelli@polimi.it>.
 */


#include "core/source/io/data_writer.hpp"

#include "core/source/numerics/numbers.hpp"

#include "source/fluid_dynamics.hpp"

#include "source/helpers/riis_handler.hpp"

#include <boost/io/ios_state.hpp>

namespace lifex
{
  RIISHandler::RIISHandler(const std::string &  subsection,
                           const FluidDynamics &fluid_dynamics_)
    : CoreModel(subsection)
    , fluid_dynamics(fluid_dynamics_)
  {}

  void
  RIISHandler::declare_parameters(ParamHandler &params) const
  {
    // Declare parameters.
    params.enter_subsection_path(prm_subsection_path);

    params.set_verbosity(VerbosityParam::Full);
    {
      params.declare_entry(
        "Use unsigned distance in assembly",
        "false",
        Patterns::Bool(),
        "Use unsigned distance, instead of signed one, when assembling the "
        "smeared Dirac delta of the RIIS factor.");
    }
    params.reset_verbosity();
    params.leave_subsection_path();

    // Declare surface-related parameters: we instantiate a dummy surface for
    // that. The reason is that a single surface declares lists of parameters,
    // whose length determines the number of surfaces that will be included.
    // Individual surfaces then parse only one item of those lists. Refer to
    // the documentation of RIISSurface::declare_parameters and
    // RIISSurface::parse_parameters for more details.

    RIISSurface dummy_surface(prm_subsection_path, *this, fluid_dynamics, 0);
    dummy_surface.declare_parameters(params);

    params.enter_subsection_path(prm_subsection_path);
    {
      params.enter_subsection("Displacement law");
      params.set_verbosity(VerbosityParam::Standard);
      {
        params.enter_subsection("Flow driven");
        {
          params.declare_entry_selection(
            "Closing law",
            "Volume derivative",
            "Volume derivative|Pressure jump|Prescribed",
            "How the closing of the valve is triggered. Volume derivative "
            "means "
            "that the valve is closed based on a condition on the flow rate. "
            "Prescribed closes the valve at a specified time.");
        }
        params.leave_subsection(); // Flow driven.
      }
      params.reset_verbosity();
      params.leave_subsection(); // Displacement law.

      params.enter_subsection("Control volumes");
      params.set_verbosity(VerbosityParam::Standard);
      {
        params.declare_entry(
          "Enable control volumes",
          "false",
          Patterns::Bool(),
          "Toggle the use of control volumes for computing upwind and downwind "
          "pressures for immersed surfaces. If enabled, lists of upwind and "
          "downwind control volumes must be provided, one for each surface. It "
          "is required by the pressure jump displacement law.");
      }
      params.reset_verbosity();
      params.leave_subsection();

      params.enter_subsection("Output");
      {
        params.set_verbosity(VerbosityParam::Standard);
        params.declare_entry("Enable surface output",
                             "false",
                             Patterns::Bool(),
                             "Enable/disable output of immersed vtp files.");
        params.reset_verbosity();

        params.set_verbosity(VerbosityParam::Full);
        params.declare_entry(
          "Enable normals and curvatures output",
          "false",
          Patterns::Bool(),
          "Enable/disable output of L2-projected normals and "
          "curvatures of immersed surfaces.");
        params.reset_verbosity();
      }
      params.leave_subsection();
    }

    params.leave_subsection_path();
  }

  void
  RIISHandler::parse_parameters(ParamHandler &params)
  {
    // Parse input file.
    params.parse();

    // Read input parameters.
    unsigned int n_surfaces = 0;
    params.enter_subsection_path(prm_subsection_path);
    {
      // Use the number of entries in Surface labels to determine the number
      // of immersed surfaces.
      n_surfaces =
        Utilities::split_string_list(params.get("Surface labels")).size();
    }
    params.leave_subsection_path();

    // Parse parameters for the immersed surfaces.
    surfaces.reserve(n_surfaces);
    for (unsigned int k = 0; k < n_surfaces; ++k)
      {
        surfaces.emplace_back(prm_subsection_path, *this, fluid_dynamics, k);
        surfaces[k].parse_parameters(params);
      }

    params.enter_subsection_path(prm_subsection_path);
    {
      prm_unsigned_in_assembly =
        params.get_bool("Use unsigned distance in assembly");

      params.enter_subsection("Control volumes");
      prm_enable_control_volumes = params.get_bool("Enable control volumes");
      params.leave_subsection();

      params.enter_subsection("Output");
      {
        prm_enable_surface_output = params.get_bool("Enable surface output");
        prm_enable_normal_and_curvature_output =
          params.get_bool("Enable normals and curvatures output");
      }
      params.leave_subsection();

      // Check whether there exists at least one immersed surface whose
      // incremental displacement is based on pressure jump.
      prm_displacement_law_flow_driven = false;

      for (const auto &surface : surfaces)
        if (surface.get_displacement_law() ==
            RIISSurface::DisplacementLaw::FlowDriven)
          {
            prm_displacement_law_flow_driven = true;
            break;
          }
    }
    params.leave_subsection_path();
  }

  void
  RIISHandler::setup(
    const std::shared_ptr<const utils::MeshHandler> &triangulation_,
    const double &                                   time_init_,
    const double &                                   time_final_,
    const double &                                   time_step_,
    const double &                                   period_,
    const std::shared_ptr<const ALEHandler> &        ale_handler_)
  {
    pcout << utils::log::separator_section << std::endl
          << prm_subsection_path << std::endl
          << std::endl;

    time_init  = time_init_;
    time_final = time_final_;
    time_step  = time_step_;
    period     = period_;

    triangulation = triangulation_;

    /// @todo Generalize to other degrees.
    fe = triangulation->get_fe_lagrange(2);

    dof_handler.reinit(triangulation->get());
    dof_handler.distribute_dofs(*fe);

    quadrature_projection = triangulation->get_quadrature_gauss(fe->degree + 1);

    ale_handler = ale_handler_;

    // Setup utilities to move surfaces with ALE.
    if (ale_handler && get_move_surfaces_with_ale())
      {
        ale_support_points.resize(dim);

        for (unsigned int d = 0; d < dim; ++d)
          {
            ComponentMask mask(dim, false);
            mask.set(d, true);

            DoFTools::map_dofs_to_support_points(
              *ale_handler->get_reference_mapping(),
              ale_handler->get_lifting().get_dof_handler(),
              ale_support_points[d],
              mask);
          }
      }

    for (auto &surface : surfaces)
      surface.setup(dof_handler, time_init, time_final, time_step, period);

    // Setup utilities for projection of normal and curvature.
    if (prm_enable_normal_and_curvature_output)
      {
        fe_normal = std::make_unique<FESystem<dim>>(*fe, dim);
        dof_handler_normal.reinit(triangulation->get());
        dof_handler_normal.distribute_dofs(*fe_normal);

        IndexSet owned_dofs = dof_handler.locally_owned_dofs();
        IndexSet relevant_dofs;
        DoFTools::extract_locally_relevant_dofs(dof_handler, relevant_dofs);

        IndexSet owned_dofs_normal = dof_handler_normal.locally_owned_dofs();
        IndexSet relevant_dofs_normal;
        DoFTools::extract_locally_relevant_dofs(dof_handler_normal,
                                                relevant_dofs_normal);

        projection_normal =
          std::make_unique<utils::ProjectionL2>(dof_handler_normal,
                                                *quadrature_projection);
        projection_curvature =
          std::make_unique<utils::ProjectionL2>(dof_handler,
                                                *quadrature_projection);

        current_normal_vector_owned.reinit(owned_dofs_normal, mpi_comm);
        current_normal_vector.reinit(owned_dofs_normal,
                                     relevant_dofs_normal,
                                     mpi_comm);

        current_curvature_vector_owned.reinit(owned_dofs, mpi_comm);
        current_curvature_vector.reinit(owned_dofs, relevant_dofs, mpi_comm);
      }

    pcout << utils::log::separator_section << std::endl;
  }

  bool
  RIISHandler::update()
  {
    TimerOutput::Scope timer_section(timer_output,
                                     prm_subsection_path +
                                       " / Update to current time");

    bool result = false;

    // The "or" operator below must be bitwise, to avoid short-circuited
    // evaluation.
    for (auto &surface : surfaces)
      result |= surface.update();

    // If ALE is active, something has moved regardless of whether valves are
    // moving or not.
    result |= (ale_handler != nullptr);

    return result;
  }

  void
  RIISHandler::recover_normal_and_curvature()
  {
    TimerOutput::Scope timer_section(timer_output,
                                     prm_subsection_path +
                                       " / Recover normal vectors");

    // Since ProjectionL2 internally uses 1e-12 relative tolerance for its
    // linear solver, giving a solution that already is good in some cases
    // causes the solver to never converge (since we start with a very low
    // residual, and we want to reduce it by 12 orders of magnitude).
    // Resetting the initial guess prevents that.
    current_normal_vector_owned    = 0.0;
    current_curvature_vector_owned = 0.0;

    QuadratureRIISNormal quadrature_normal(
      *this,
      *quadrature_projection,
      QuadratureRIIS<Tensor<1, dim, double>>::RIISUpdateFlags::update_normal);
    projection_normal->project<QuadratureEvaluationVector>(
      quadrature_normal, current_normal_vector_owned);
    current_normal_vector = current_normal_vector_owned;

    QuadratureRIISCurvature quadrature_curvature(
      *this,
      *quadrature_projection,
      QuadratureRIIS<double>::RIISUpdateFlags::update_curvature);
    projection_curvature->project<QuadratureEvaluationScalar>(
      quadrature_curvature, current_curvature_vector_owned);
    current_curvature_vector = current_curvature_vector_owned;
  }

  void
  RIISHandler::compute_velocity_loc(const Point<dim> &      x,
                                    Tensor<1, dim, double> &velocity_loc) const
  {
    velocity_loc = 0;

    for (const auto &surface : surfaces)
      surface.compute_velocity_loc(x, velocity_loc);
  }

  void
  RIISHandler::attach_output(DataOut<dim> &      data_out,
                             const double &      time,
                             const unsigned int &timestep_number)
  {
    TimerOutput::Scope timer_section(timer_output,
                                     prm_subsection_path + " / Output results");

    for (const auto &surface : surfaces)
      surface.attach_output(data_out, dof_handler);

    if (prm_enable_normal_and_curvature_output)
      {
        recover_normal_and_curvature();

        std::vector<DataComponentInterpretation::DataComponentInterpretation>
          data_component_interpretation(
            dim, DataComponentInterpretation::component_is_part_of_vector);
        std::vector<std::string> normal_names(dim, "n_surf");
        data_out.add_data_vector(dof_handler_normal,
                                 current_normal_vector,
                                 normal_names,
                                 data_component_interpretation);
        data_out.add_data_vector(
          dof_handler,
          current_curvature_vector,
          "H_surf",
          {DataComponentInterpretation::component_is_scalar});
      }

    // Export vtp of immersed surfaces.
    if (prm_enable_surface_output)
      {
        for (auto &surface : surfaces)
          surface.export_vtp(time, timestep_number);
      }
  }

  void
  RIISHandler::declare_entries_csv(utils::CSVWriter &csv_writer) const
  {
    for (const auto &surface : surfaces)
      surface.declare_entries_csv(csv_writer);
  }

  void
  RIISHandler::set_entries_csv(utils::CSVWriter &csv_writer) const
  {
    for (const auto &surface : surfaces)
      surface.set_entries_csv(csv_writer);
  }

  void
  RIISHandler::restart(const std::string &csv_filename,
                       const double &     time,
                       const double &     time_step)
  {
    for (auto &surface : surfaces)
      surface.restart(csv_filename, time, time_step);
  }
} // namespace lifex
