/********************************************************************************
  Copyright (C) 2019 - 2023 by the lifex authors.

  This file is part of lifex.

  lifex is free software; you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  lifex is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with lifex.  If not, see <http://www.gnu.org/licenses/>.
********************************************************************************/

/**
 * @file
 *
 * @author Ivan Fumagalli <ivan.fumagalli@polimi.it>.
 * @author Michele Bucelli <michele.bucelli@polimi.it>.
 * @author Alberto Zingaro <alberto.zingaro@polimi.it>.
 */

#ifndef LIFEX_HELPER_ALE_HANDLER_HPP_
#define LIFEX_HELPER_ALE_HANDLER_HPP_

#include "core/source/core_model.hpp"

#include "core/source/io/data_writer.hpp"
#include "core/source/io/vtk_function.hpp"

#include "core/source/numerics/interface_handler.hpp"
#include "core/source/numerics/time_interpolation.hpp"

#include "source/helpers/lifting/lifting.hpp"

#include <deal.II/fe/mapping_fe_field.h>

#include <functional>
#include <memory>
#include <set>
#include <sstream>
#include <string>
#include <utility>
#include <vector>

namespace lifex
{
  /**
   * @brief Class to deal with ALE functionalities.
   *
   * Accounts for:
   * - mesh motion
   * - computation of ALE velocity
   *
   * The configuration @f$\Omega^n@f$ of the domain at time @f$t^n@f$ is defined
   * in terms of the reference configuration @f$\hat\Omega@f$
   * (@a i.e. the input mesh) and of
   * the total displacement @f$\mathbf D^n@f$, as
   * @f[
   * \Omega^n = (I + \mathbf D^n)(\hat\Omega).
   * @f]
   * Notice that the initial configuration @f$\Omega^0@f$ can be different
   * from @f$\hat\Omega@f$, if @f$\mathbf D^0\neq\mathbf 0@f$
   * (see @ref setup_from_file(), @ref setup_from_function()).
   *
   * If the displacement is read from file, as a sequence of @f$\widehat{\mathbf
   * D}^m@f$ corresponding to times @f$\widehat t^m@f$ (typically with
   * @f$\Delta\widehat t > \Delta t@f$), the displacement @f$\mathbf D^n@f$ is
   * the time interpolation of @f$\widehat{\mathbf D}^m@f$ on the time grid
   * @f$t^n@f$.
   *
   * The actual motion of the mesh is performed incrementally, as follows:
   * @f[
   * \Omega^{n+1} = (I + \mathbf d^{n+1})(\Omega^{n}),
   * \text{ where }
   * \mathbf d^{n+1} = \mathbf D^{n+1} - \mathbf D^n.
   * @f]
   *
   * The domain velocity @f$\mathbf u_\text{ALE}@f$ is then computed
   * by a first-order finite difference:
   * @f[
   * \mathbf u_\text{ALE}^{n+1} = \frac{\mathbf D^{n+1} - \mathbf D^n}{\Delta
   * t}.
   * @f]
   *
   * This velocity field can be employed from outside the class in two ways:
   *   - Evaluation in quadrature points, via QuadratureALEVelocity,
   *     @a e.g. for assembling purposes.
   *   - Enforcement of a Dirichlet condition
   *     @f$\mathbf u^{n+1} = \mathbf u_\text{ALE}^{n+1}@f$
   *     on an external velocity (@a e.g. as wall condition in a fluid problem),
   *     via the method apply_dirichlet().
   */
  class ALEHandler : public CoreModel
  {
  public:
    /**
     * This class needs to access private members
     * such as Lifting::dof_handler and the ALE velocity,
     * to compute the latter at quadrature points.
     */
    friend class QuadratureALEVelocity;

    /// Constructor.
    ALEHandler(const std::string &subsection, const bool &standalone_);

    /**
     * @brief Set-up lifting and ALE objects, relying on the given triangulation.
     *
     * Boundary displacements values are read from file and interpolated on the
     * triangulation.
     *
     * Then, setup_displacement_vectors() is called to setup displacements and
     * velocity.
     *
     * If @ref prm_use_time_interpolation = <kbd>true</kbd>,
     * @ref time_interpolation is set-up to time-interpolate the
     * input displacement data (cf. @ref utils::TimeInterpolationFEM);
     * otherwise, piecewise linear time interpolation is employed.
     *
     * @warning
     * In the time-interpolation case, the imported data is assumed to have
     * instances ranging from @f$t=0@f$ to the final time @f$T@f$ of the
     * simulation, irrespectively of @p time_init_ (which is the left end of
     * the actual simulation timespan, and assumed to be @f$\geq 0@f$), and the
     * time interpolation is performed over @f$[0,T]@f$, <b>even if restart is
     * enabled</b> (in such case, @p time_init_ is the restarting time).
     * This ensures that the resulting time approximation (which is actually
     * not interpolant in the splines/Fourier case) is always the same,
     * independently of other parameters.
     *
     * @param[in] triangulation_ The mesh that is moved.
     * @param[in] u_fe_scalar A scalar FE used in apply_dirichlet().
     * @param[in] time_init_ Simulation initial time.
     * @param[in] compute_initial_displacement Toggle lifting
     * to set the initial displacement @f$\mathbf D^0@f$ from data.
     * @param[in] apply_initial_displacement Toggle moving the mesh so that
     * @f$\Omega^0 = (I + \mathbf D^0)(\hat\Omega)@f$
     * (if @a false, @f$\mathbf D^0 = \mathbf 0@f$
     * and thus @f$\Omega^0=\hat\Omega@f$).
     * The reference surface of the @ref utils::VTKFunction "VTKFunction"s
     * reading the displacement from file are moved (or not) accordingly.
     * @param[in] filename_deserialize Filename for restart from previous
     * simulation. If empty, no restart occurs.
     *
     * @note If no restart is considered, the initial ALE velocity is set as
     * @f$\mathbf u_\text{ALE}^0 = \mathbf 0@f$.
     */
    void
    setup_from_file(
      const std::shared_ptr<utils::MeshHandler> &triangulation_,
      const FiniteElement<dim> &                 u_fe_scalar,
      const double &                             time_init_,
      const bool &                      compute_initial_displacement = true,
      const bool &                      apply_initial_displacement   = true,
      const std::optional<std::string> &filename_deserialize         = {});

    /**
     * @brief Set-up lifting and ALE objects, relying on the given triangulation.
     *
     * Boundary displacements values imposed by given function.
     *
     * Then, setup_displacement_vectors() is called to setup displacements and
     * velocity.
     *
     * @param[in] triangulation_ The mesh that is moved.
     * @param[in] u_fe_scalar A scalar FE used in apply_dirichlet().
     * @param[in] w_bd Function with boundary values.
     * @param[in] time_init_ Simulation initial time.
     * @param[in] compute_initial_displacement Toggle lifting
     * to set the initial displacement @f$\mathbf D^0@f$ from data.
     * @param[in] apply_initial_displacement Toggle moving the mesh so that
     * @f$\Omega^0 = (I + \mathbf D^0)(\hat\Omega)@f$
     * (if @a false, @f$\mathbf D^0 = \mathbf 0@f$
     * and thus @f$\Omega^0=\hat\Omega@f$).
     * @param[in] component_mask_ A component mask specifying to which displacement
     * components the Dirichlet condition given by the function w_bd should be
     * applied.
     * @param[in] filename_deserialize Filename for restart from previous
     * simulation. If empty, no restart occurs.
     *
     * If @ref utils::VTKFunction "VTKFunction"s are active, their
     * reference surface is moved (or not) accordingly.
     *
     * @note If no restart is considered, the initial ALE velocity is set as
     * @f$\mathbf u_\text{ALE}^0 = \mathbf 0@f$.
     */
    void
    setup_from_function(
      const std::shared_ptr<utils::MeshHandler> &triangulation_,
      const FiniteElement<dim> &                 u_fe_scalar,
      const double &                             time_init_,
      const std::shared_ptr<Function<dim>> &     w_bd,
      const bool &                      compute_initial_displacement = true,
      const bool &                      apply_initial_displacement   = true,
      const ComponentMask &             component_mask_      = ComponentMask(),
      const std::optional<std::string> &filename_deserialize = {});

    /// Allocate and possibly initialize displacement vectors.
    ///
    /// If @p compute_initial_displacement = <kbd>true</kbd>, initialization
    /// is done by calling @ref compute_initial_displacement().
    void
    setup_displacement_vectors(
      const std::shared_ptr<utils::MeshHandler> &triangulation_,
      const FiniteElement<dim> &                 u_fe_scalar,
      const bool &                      compute_initial_displacement = true,
      const bool &                      apply_initial_displacement   = true,
      const std::optional<std::string> &filename_deserialize         = {});

    /// Setup lifting's interface handler and interface conditions (for FSI).
    ///
    /// @note Only Dirichlet interface data can be specified, since this is meant
    /// to extract lifting's boundary conditions from the mechanics problem.
    void
    setup_interface(
      const DoFHandler<dim> &other_dof_handler,
      const std::vector<
        std::shared_ptr<Lifting::FSIInterfaceHandler::InterfaceDataDirichlet>>
        &interface_data_dirichlet);

    /**
     * @brief Update boundary displacement, solve lifting problem and compute velocity.
     *
     * -# For time-dependent boundary displacements, update current values:
     *     - If read from file (cf. setup_from_file()):
     *       - If @ref prm_use_time_interpolation = <kbd>true</kbd>
     *         - evaluate @ref time_interpolation on current @p time
     *       - else
     *         -# identify the time subinterval
     *            @f$[\widehat{t}^m,\widehat{t}^{m+1}]@f$
     *            of the input data where @p time lays;
     *         -# if needed, import @f$\widehat{\mathbf D}^{m+1}@f$ from file
     *            and solve the associated lifting problem;
     *         -# perform piecewise linear time interpolation between
     *            @f$\widehat{\mathbf D}^m,\widehat{\mathbf D}^{m+1}@f$.
     *     - If imposed by Function (provided in setup_from_function()),
     *       <kbd>set_time(time)</kbd> is called on that Function, and then
     *       the corresponding lifting problem is solved.
     * -# Compute velocity from displacement.
     *
     * @param[in] time      Current time of the simulation.
     * @param[in] time_step Time-step length.
     * @param[in] period    Simulation period.
     */
    void
    update(const double &time, const double &time_step, const double &period);

    /**
     * @brief Move mesh and surfaces according to given displacement.
     *
     * This method calls:
     * - @ref utils::move_mesh() to move the mesh;
     * - @ref utils::VTKFunction::warp_by_array_combination() to move the
     * reference surfaces of @ref displacement_function_new and
     * @ref displacement_function_old (if displacement is read from .vtp).
     *
     * @param[in] time_step Time-step length.
     * @param[in] incremental_displacement Displacement field.
     **/
    void
    move_mesh(const double &             time_step,
              const LinAlg::MPI::Vector &incremental_displacement);

    /**
     * @brief Move mesh and surfaces according to
     * @ref displacement_increment_owned.
     *
     * This method calls:
     * - @ref utils::move_mesh() to move the mesh;
     * - @ref utils::VTKFunction::warp_by_array_combination() to move the
     * reference surfaces of @ref displacement_function_new and
     * @ref displacement_function_old (if displacement is read from .vtp).
     *
     * @param[in] time_step Time-step length.
     **/
    void
    move_mesh(const double &time_step)
    {
      move_mesh(time_step, *displacement_increment_owned);
    }

    /**
     * @brief Initialize ALE displacements and velocity,
     * and possibly apply initial displacement.
     *
     * If no restart is considered:
     * - solves lifting problem to compute initial displacements
     * (total and incremental);
     * - sets ALE velocity to 0.
     *
     * In the restart case, reads initial velocity and displacements from file.
     *
     * @param[in] apply_initial_displacement
     * Toggle application of initial displacement.
     * @param[in] filename_deserialize Filename for restart from previous
     * simulation. If empty, no restart occurs.
     */
    void
    compute_initial_displacement(
      const bool &                      apply_initial_displacement,
      const std::optional<std::string> &filename_deserialize = {});

    /**
     * @brief Apply Dirichlet condition @f$\mathbf u = \mathbf u_\text{ALE}@f$
     * to an external problem in ALE formulation.
     *
     * @f$\mathbf u_\text{ALE}@f$ is the @ref velocity member of this class.
     *
     * @note The in/out parameters @p u, @p u_owned are assumed to belong to
     * a FE space made of @p dim copies of @p u_fe_scalar
     * (@a e.g. FESystem<dim>(u_fe_scalar, dim, another_fe, 1) ),
     * where @p u_fe_scalar has been set during setup_from_file() / setup_from_function().
     *
     * @param[in, out] u_owned The external velocity @f$\mathbf u@f$ (non-ghosted).
     * @param[in, out] u The external velocity @f$\mathbf u@f$ (ghosted).
     * @param[in] u_dirichlet_tags The boundary tags
     * where to enforce the condition.
     */
    void
    apply_dirichlet(LinAlg::MPI::Vector &               u_owned,
                    LinAlg::MPI::Vector &               u,
                    const std::set<types::boundary_id> &u_dirichlet_tags) const;

    /**
     * @brief Apply Dirichlet condition @f$\mathbf u = \mathbf u_\text{ALE}@f$
     * to an external problem in ALE formulation.
     *
     * @f$\mathbf u_\text{ALE}@f$ is the @ref velocity member of this class.
     *
     * @param[in, out] constraints An AffineConstraints object in which constraints
     * for the Dirichlet conditions will be inserted.
     * @param[in] u_dirichlet_tags The boundary tags where to enforce the condition.
     * @param[in] homogeneous If true, will apply homogeneous conditions @f$\mathbf u = 0@f$;
     * otherwise, will apply non-homogeneous conditions @f$\mathbf u = \mathbf
     * u_\text{ALE}@f$.
     */
    void
    apply_dirichlet(AffineConstraints<double> &         constraints,
                    const std::set<types::boundary_id> &u_dirichlet_tags,
                    const bool &homogeneous = false) const;

    /**
     * @brief Save current ALE velocity @f$\mathbf u_\text{ALE}^{n+1}@f$
     * and displacements @f$\mathbf D^{n+1},\mathbf d^{n+1}@f$ on file.
     * @param[in] time Current simulation time.
     * @param[in] timestep_number Current time-step iteration.
     */
    void
    output_results(const double &      time            = 0.0,
                   const unsigned int &timestep_number = 0);

    /**
     * @brief Serialize current ALE velocity @f$\mathbf u_\text{ALE}^{n+1}@f$
     * and displacements @f$\mathbf D^{n+1},\mathbf d^{n+1}@f$ to file for
     * future restart.
     */
    void
    output_serialize(const std::string & basename,
                     const unsigned int &timestep_number) const;

    /// Restart from serialized solution.
    void
    restart(const std::string &filename);

    /// Override of @ref CoreModel::declare_parameters.
    virtual void
    declare_parameters(ParamHandler &params) const override;

    /// Override of @ref CoreModel::parse_parameters.
    virtual void
    parse_parameters(ParamHandler &params) override;

    /** @brief Interpolate the ALE velocity from the Lifting FE space to the
     * FluidDynamics FE space.
     *
     * @param[out] dest Vector to store the interpolated velocity, with ghost elements.
     * @param[out] dest_owned Vector to store the interpolated velocity, without ghost elements.
     * @param[in] reinit If true, dest and dest_owned will be reinitialized with the
     * appropriate locally owned and ghosted elements.
     */
    void
    interpolate_dirichlet_vectors(LinAlg::MPI::Vector &dest,
                                  LinAlg::MPI::Vector &dest_owned,
                                  bool                 reinit = true) const;

    /// Get lifting problem.
    const Lifting &
    get_lifting() const
    {
      return *lifting;
    }

    /// Get DoF handler.
    const DoFHandler<dim> &
    get_u_dof_handler() const
    {
      return u_dof_handler;
    }

    /// Get whether the displacement is total or incremental.
    const bool &
    get_displacement_is_total() const
    {
      return displacement_is_total;
    }

    /// Get displacement vector, owned dofs.
    const LinAlg::MPI::Vector &
    get_displacement_owned() const
    {
      return *displacement_owned;
    }

    /// Get displacement vector.
    const LinAlg::MPI::Vector &
    get_displacement() const
    {
      return displacement;
    }

    /// Get velocity vector, owned dofs.
    const LinAlg::MPI::Vector &
    get_velocity_owned() const
    {
      return *velocity_owned;
    }

    /// Check whether this has been initialized.
    bool
    initialized() const
    {
      return (displacement_owned != nullptr);
    }

    /// Increment velocity vector by given vector, and update ghosted values.
    /// Used in FSI simulations with implicit geometry.
    void
    add_to_velocity(const LinAlg::MPI::Vector &delta_w)
    {
      *velocity_owned += delta_w;
      velocity = *velocity_owned;
    }

    /// Attach output data to given DataOut object.
    void
    attach_output(DataOut<dim> &data_out) const;

    /// Set how often output must be saved.
    void
    set_output_every_n_timesteps(const unsigned int &output_every_n_timesteps)
    {
      prm_output_every_n_timesteps = output_every_n_timesteps;
    }

    /// Get mapping representing the reference configuration.
    std::shared_ptr<Mapping<dim>>
    get_reference_mapping() const
    {
      return mapping_reference;
    }

  protected:
    /// Standalone output: if false, output filename parameters are not
    /// declared, output_results will not save the solution and output should be
    /// done via attach_output.
    bool standalone;

    /// @name Parameters read from file.
    /// @{

    /**
     * @brief Input displacements filename.
     *
     * If empty, the displacement is not read from file,
     * but from a Function implemented outside this class,
     * and passed to it in setup_from_function().
     */
    std::string prm_boundary_disp_filename;
    /// Boundary displacement field basename.
    std::string prm_boundary_disp_field_basename;
    /**
     * @brief Number of figures of the index string of boundary displacement.
     *
     * @a e.g. @p prm_boundary_disp_field_basename = 2 for
     * displacement00, displacement01, displacement02, ...
     */
    unsigned int prm_boundary_disp_num_figures_idx;
    /// Initial index of the input time-dependent boundary
    /// displacement @f$\widehat{\mathbf D}^m@f$.
    unsigned int prm_boundary_disp_initial_idx;
    /**
     * @brief Number @f$M@f$ of time subintervals
     * @f$\{[\widehat{t}^m,\widehat{t}^{m+1}]\}_{m=0}^{M-1}@f$
     * of the input time-dependent
     * boundary displacement @f$\{\widehat{\mathbf D}^m\}_{m=0}^{M}@f$.
     *
     * If == 0, @p prm_boundary_disp_field_basename is taken as
     * the full name of the displacement field.
     * If != 0, the full name of the displacement field is made by
     * appending the subinterval index @f$m@f$ (possibly with stride: see
     * @ref prm_boundary_disp_subintervals_stride) to
     * @p prm_boundary_disp_field_basename.
     */
    unsigned int prm_boundary_disp_num_subintervals;
    /**
     * Stride between the indices of two consecutive time instances
     * of the input time-dependent boundary displacement
     * @f$\widehat{\mathbf D}^m,\widehat{\mathbf D}^{m+1}@f$.
     */
    unsigned int prm_boundary_disp_subintervals_stride;
    /**
     * Duration @f$\widehat t^{m+1}-\widehat t^m@f$ of the subinterval between
     * consecutive instances of the input time-dependent boundary displacement
     * @f$\widehat{\mathbf D}^m,\widehat{\mathbf D}^{m+1}@f$.
     */
    double prm_boundary_disp_subinterval_duration;

    /// Displacement mode.
    std::string prm_disp_mode;

    /// Displacement scaling factor, when read from a .vtp file.
    double prm_disp_scaling_factor;

    /// Geometry scaling factor, when the displacement is read from a .vtp file.
    double prm_geometry_scaling_factor;

    /// Lifting operator.
    std::string prm_lifting_op;

    bool        prm_enable_output;   ///< Toggle save results.
    std::string prm_output_basename; ///< Output basename.

    bool prm_use_time_interpolation; ///< Toggle time interpolation.
    utils::TimeInterpolation::Mode
         prm_time_interpolation_mode; ///< Interpolation mode flag.
    bool prm_time_spline_der_zero;    ///< Impose zero derivative at both ends.
    double
      prm_time_smoothing_spline_lambda; ///< Regularization weight for smoothing
                                        ///< spline interpolation.
    bool prm_time_interpolation_periodic; ///< Periodic time interpolation.

    std::string  prm_total_disp_name; ///< Total displacement name.
    std::string  prm_incr_disp_name;  ///< Incremental displacement name.
    std::string  prm_velocity;        ///< Velocity.
    unsigned int prm_output_every_n_timesteps; ///< Save every n timesteps.
    bool         prm_export_mesh_always;       ///< Always export mesh.
    bool         prm_enable_surface_output; ///< Toggle save disp vtp surfaces.

    /// @}

    /// Moving mesh @f$\Omega^{n+1}@f$.
    std::shared_ptr<utils::MeshHandler> triangulation;

    /**
     * Input displacement field @f$\mathbf D^{n+1}_\text{new}@f$, when read from
     * file and @ref prm_use_time_interpolation = <kbd>false</kbd>:
     * right-hand end of (piecewise) linear interpolation in time.
     * Boundary function.
     */
    std::shared_ptr<utils::VTKFunction> displacement_function_new;
    /**
     * Input displacement field @f$\mathbf D^{n+1}_\text{new}@f$, when read from
     * file and @ref prm_use_time_interpolation = <kbd>false</kbd>:
     * right-hand end of (piecewise) linear interpolation in time.
     * Non-ghosted finite element vector.
     */
    std::shared_ptr<LinAlg::MPI::Vector> displacement_from_file_new_owned;
    /**
     * Input displacement field @f$\mathbf D^{n+1}_\text{old}@f$, when read from
     * file and @ref prm_use_time_interpolation = <kbd>false</kbd>:
     * left-hand end of (piecewise) linear interpolation in time. Boundary
     * function.
     *
     * @p nullptr if @p prm_boundary_disp_num_subintervals < 2.
     */
    std::shared_ptr<utils::VTKFunction> displacement_function_old;
    /**
     * Input displacement field @f$\mathbf D^{n+1}_\text{old}@f$, when read from
     * file and @ref prm_use_time_interpolation = <kbd>false</kbd>:
     * left-hand end of (piecewise) linear interpolation in time.
     * Non-ghosted finite element vector.
     *
     * Uninitialized if @p prm_boundary_disp_num_subintervals < 2.
     */
    std::shared_ptr<LinAlg::MPI::Vector> displacement_from_file_old_owned;
    /**
     * Input displacement fields @f$\widehat{\mathbf D}^{m}, m=1,2,\ldots@f$,
     * when read
     * from file and @ref prm_use_time_interpolation = <kbd>true </kbd>
     * Non-ghosted finite element vector.
     */
    std::vector<LinAlg::MPI::Vector> imported_displacements_owned;
    /**
     * Utility for time interpolation among input displacements.
     */
    std::unique_ptr<utils::TimeInterpolationFEM<LinAlg::MPI::Vector>>
      time_interpolation;

    /// Times and names for pvd output of @ref displacement_function_new
    /// (used if @ref prm_enable_surface_output is true).
    std::vector<std::pair<double, std::string>>
      displacement_new_times_and_names;

    /// Times and names for pvd output of @ref displacement_function_old
    /// (used if @ref prm_enable_surface_output is true).
    std::vector<std::pair<double, std::string>>
      displacement_old_times_and_names;

    /// Initial time of the simulation (set at
    /// setup_from_file() / setup_from_function()).
    double time_init;

    /**
     * @brief Coefficient for piecewise linear time interpolation.
     *
     * Used when the boundary displacement is read from .vtp file
     * and @ref prm_use_time_interpolation = <kbd>false</kbd>.
     *
     * The employed displacement is
     * @f[\begin{cases}
     * \mathbf D^{n+1} = \text{interpolation_coeff} * \mathbf
     * D^{n+1}_\text{new}
     * +(1-\text{interpolation_coeff}) * \mathbf D^{n+1}_\text{old},
     * & \text{if displacement_is_total = true},\\
     * \mathbf d^{n+1} = \text{interpolation_coeff} * \mathbf
     * d^{n+1}_\text{new}, & \text{if displacement_is_total = false}.
     * \end{cases}@f].
     */
    double interpolation_coeff;

    /// Index of current subinterval of input time-dependent boundary
    /// displacement.
    unsigned int current_subinterval;

    /// Index of previous subinterval of input time-dependent boundary
    /// displacement.
    unsigned int previous_subinterval;

    /**
     * @brief Utility to access the correct displacement field
     * when given as input from file
     * (considering also @ref prm_boundary_disp_subintervals_stride).
     */
    std::function<std::string()> str_current_subinterval_with_stride;

    /**
     * @brief Utility to access the correct (old) displacement field
     * when given as input from file
     * (considering also @ref prm_boundary_disp_subintervals_stride).
     */
    std::function<std::string()> str_previous_subinterval_with_stride;

    /// Lifting operator: pointer to allow polymorphism.
    std::unique_ptr<Lifting> lifting;

    /**
     * @brief %Flag for ALE displacement imposition.
     *
     * The imposed displacement that is read from file/Function
     * and then lifted is
     * @f[\begin{cases}
     * \mathbf D^{n+1}, \text{ the total displacement, } & \text{if
     * displacement_is_total = true},\\ \mathbf d^{n+1}, \text{ the
     * incremental displacement, } & \text{if displacement_is_total = false}.
     * \end{cases}@f]
     */
    bool displacement_is_total;

    /// Total displacement @f$\mathbf D^{n+1}@f$.
    LinAlg::MPI::Vector displacement;
    /// Total displacement @f$\mathbf D^{n+1}@f$ (non-ghosted).
    std::shared_ptr<LinAlg::MPI::Vector> displacement_owned;
    /// Incremental displacement @f$\mathbf d^{n+1} = \mathbf D^{n+1} - \mathbf
    /// D^{n}@f$.
    LinAlg::MPI::Vector displacement_increment;
    /// Incremental displacement @f$\mathbf d^{n+1} = \mathbf D^{n+1} - \mathbf
    /// D^{n}@f$ (non-ghosted).
    std::shared_ptr<LinAlg::MPI::Vector> displacement_increment_owned;
    /// Initial displacement @f$\mathbf D^0@f$.
    std::unique_ptr<LinAlg::MPI::Vector> initial_displacement_owned;
    /// Mesh velocity @f$\mathbf u_\text{ALE}^{n+1} = \frac{\mathbf
    /// d^{n+1}}{\Delta t}@f$.
    LinAlg::MPI::Vector velocity;
    /// Mesh velocity @f$\mathbf u_\text{ALE}^{n+1} = \frac{\mathbf
    /// d^{n+1}}{\Delta t}@f$ (non-ghosted).
    std::shared_ptr<LinAlg::MPI::Vector> velocity_owned;

    /**
     * @brief Input displacement field, when NOT read from file.
     *
     * Stored at setup, it undergoes set_time() at every update().
     *
     * Interpreted as total or incremental displacement,
     * depending on @p displacement_is_total.
     */
    std::shared_ptr<Function<dim>> boundary_function;

    /// Component mask for the application of boundary_function.
    ComponentMask component_mask;

    /// Temporary vector to store Dirichlet values interpolated onto the fluid
    /// dynamics DoF handler.
    mutable LinAlg::MPI::Vector tmp_u_bc;

    /// Temporary vector to store Dirichlet values interpolated onto the fluid
    /// dynamics DoF handler, owned version.
    mutable LinAlg::MPI::Vector tmp_u_bc_owned;

    /// Flag that indicates whether tmp_u_bc and tmp_u_bc_owned have been
    /// initialized.
    mutable bool tmp_u_bc_initialized;

    /// @name Utilities for apply_dirichlet. Created at setup from u_fe_scalar.

    /// @{
    /// DoFHandler to deal with dofs of the external velocity on which the
    /// conditions are imposed.
    DoFHandler<dim> u_dof_handler;
    /// Interpolation matrix from the FE of this class to the FE of the external
    /// velocity.
    std::shared_ptr<FullMatrix<double>> interpolation_matrix;
    /// @}

    /// Position vector in reference configuration, owned elements.
    LinAlg::MPI::Vector position_reference_owned;

    /// Position vector in reference configuration.
    LinAlg::MPI::Vector position_reference;

    /// Mapping from representing the reference, undeformed configuration.
    std::shared_ptr<MappingFEField<dim, dim, LinAlg::MPI::Vector>>
      mapping_reference;
  };

  /**
   * @brief Class to evaluate ALE velocity at quadrature points.
   */
  class QuadratureALEVelocity : public QuadratureEvaluationFEMVector
  {
  public:
    /**
     * @brief Constructor.
     *
     * @param[in] ale_handler_ Handler providing ALE variables.
     * @param[in] quadrature_ Quadrature rule w.r.t. which ALE velocity is evaluated.
     */
    QuadratureALEVelocity(const ALEHandler &     ale_handler_,
                          const Quadrature<dim> &quadrature_)
      : QuadratureEvaluationFEMVector(ale_handler_.lifting->get_dof_handler(),
                                      quadrature_,
                                      update_values)
      , ale_handler(ale_handler_)
    {}

    Tensor<1, dim, double>
    operator()(const unsigned int &q,
               const double & /*t*/       = 0,
               const Point<dim> & /*x_q*/ = Point<dim>()) override
    {
      const DoFHandler<dim> &dof_handler(
        ale_handler.lifting->get_dof_handler());

      velocity.clear();

      for (unsigned int i = 0; i < dof_indices.size(); ++i)
        {
          velocity[dof_handler.get_fe().system_to_component_index(i).first] +=
            ale_handler.velocity[dof_indices[i]] * fe_values->shape_value(i, q);
        }

      return velocity;
    };

  protected:
    const ALEHandler &ale_handler; ///< ALE handler.

    Tensor<1, dim, double> velocity; ///< Used for pre-allocation.
  };

} // namespace lifex

#endif /* LIFEX_HELPER_ALE_HANDLER_HPP_ */
