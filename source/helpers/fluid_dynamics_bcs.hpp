/********************************************************************************
  Copyright (C) 2019 - 2023 by the lifex authors.

  This file is part of lifex.

  lifex is free software; you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  lifex is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with lifex.  If not, see <http://www.gnu.org/licenses/>.
********************************************************************************/

/**
 * @file
 *
 * @author Michele Bucelli <michele.bucelli@polimi.it>.
 * @author Alberto Zingaro <alberto.zingaro@polimi.it>.
 * @author Ivan Fumagalli <ivan.fumagalli@polimi.it>.
 */

#ifndef LIFEX_HELPER_FLUID_DYNAMICS_BCS_HPP_
#define LIFEX_HELPER_FLUID_DYNAMICS_BCS_HPP_

#include "core/source/generic_factory.hpp"

#include "core/source/numerics/bc_handler.hpp"
#include "core/source/numerics/time_interpolation.hpp"

#include "source/fluid_dynamics.hpp"

#include <algorithm>
#include <cmath>
#include <complex>
#include <memory>
#include <set>
#include <string>
#include <vector>

/**
 * @brief Namespace for handling common boundary conditions for @ref FluidDynamics problems.
 *
 * The namespace provides a set of predefined boundary conditions for fluid
 * dynamics problems, including pressure boundary conditions, parabolic or
 * constant velocity profiles and resistance conditions. Each class exposes
 * method that allow it to be configured through parameters, as well as to be
 * manually adjusted within the code.
 *
 * #### Basic usage example
 * As an example, suppose we want to impose velocity at an
 * inlet boundary, with a parabolic (see @ref Parabolic) or flat (see @ref UniformVelocity) profile,
 * and with the total flow that varies in time either with a ramp
 * (see @ref Ramp) or with a sinusoidal pulsation (see @ref Pulsatile), according
 * to parameters selected by the user. Then, within the class defining the test
 * case (typically the class that wraps the @ref FluidDynamics instance, such as
 * @ref lifex::tests::TestFluidDynamicsCylinder), we define a member:
 * @code{.cpp}
 * std::shared_ptr<FluidDynamicsBCs::FlowBC> inlet_bc;
 * @endcode
 * This member has to be initialized within the constructor of the class,
 * specifying its subsection in the parameter file and what types of time
 * evolutions and space distributions are allowed:
 * @code{.cpp}
 * // Within the constructor of the example class
 * inlet_bc(std::make_shared<FluidDynamicsBCs::FlowBC>(
 *   prm_subsection_path + " / Boundary conditions / Inlet", // BC subsection.
 *   {Ramp::label, Pulsatile::label}, // Set of valid time-dependency functions.
 *   Ramp::label, // Default time-dependency function.
 *   {Parabolic::label, UniformVelocity::label},
 *       // Set of valid space-dependency functions.
 *   Parabolic::label, // Default space-dependency function.
 *   fluid_dynamics)); // FluidDynamics class.
 * @endcode
 * Then, we declare and parse the parameters of the boundary condition as with
 * any other dependency, by calling the declare_parameters and
 * parse_parameters methods. Finally, we use the <kbd>inlet_bc</kbd> member to
 * construct the @ref utils::BC object you pass to @ref FluidDynamics.
 *
 * Concrete use examples can be found in @ref lifex::tests::TestFluidDynamicsCylinder.
 *
 * #### Custom time or space dependency
 * The namespace implements the following generic functions of time:
 * - @ref Constant (constant in time);
 * - @ref Ramp (cosinusoidal ramp);
 * - @ref Pulsatile (sinusoidal oscillation);
 * - @ref CSVFunction (data read from a CSV file);
 *
 * and following generic functions of space:
 * - @ref Parabolic (parabolic profile, for a circular section);
 * - @ref UniformVelocity (flat profile).
 *
 * In case a different function <b>of time</b> is wanted, that function should
 * be implemented by deriving from @ref TimeFunction, overriding the evaluate,
 * get_info, declare_parameters and parse_parameters methods.
 *
 * In case a different function <b>of space</b> is wanted, that function
 * should be implemented by deriving from @ref SpaceFunction, overriding the
 * vector_value, get_info, declare_parameters and parse_parameters methods
 * (and possibly other methods of utils::FunctionDirichlet)
 *
 * The new functions must be registered in the time/space function factory, so
 * that their parameters will be declared and parsed as needed. To do so,
 * following the
 * procedure described in the documentation for @ref GenericFactory, the following
 * macro can be called:
 * @code{.cpp}
 * LIFEX_REGISTER_CHILD_INTO_FACTORY(TimeFunction, // Or SpaceFunction,
 *                                   MyFunction,
 *                                   MyFunction::label,
 *                                   const std::string &,
 *                                   const std::string &,
 *                                   const std::string &,
 *                                   const FluidDynamics &);
 * @endcode
 *
 * where <kbd>MyFunction::label</kbd> is a string label defined as a static
 * member of MyFunction.
 *
 * The custom function and its parameters will then be available in the
 * parameter file; it is identified in parameters by means of the string
 * stored in its <kbd>label</kbd> member.
 */
namespace lifex::FluidDynamicsBCs
{
  /**
   * @brief Abstract class to represent a generic function of time.
   *
   * This class and its derivatives are used by FluidDynamicsBCs classes to
   * describe the evolution in time of relevant quantities (e.g. pressure or
   * flow).
   *
   * Derived classes should declare:
   * - the @ref evaluate method to indicate how the function is computed;
   * - the methods declare_parameters and parse_parameters;
   * - a get_info method providing a string with information abount the boundary
   * condition, for logging purposes;
   * - a member <kbd>static inline constexpr auto label</kbd> containing the
   * label to be used for the condition in parameter files.
   *
   * To obtain an evaluation of the function, one
   * should call first the @ref update_value method, to compute and store the
   * value, and then the @ref get_value method to retrieve it. This is useful
   * for functions whose evaluation is not trivial (see e.g. @ref FlowIntegralEvaluation).
   *
   * See also @ref FluidDynamicsBCs for a more detailed usage guide.
   */
  class TimeFunction : public CoreModel
  {
  public:
    /// Alias for the generic time function factory.
    using TimeFunctionFactory = GenericFactory<TimeFunction,
                                               const std::string &,
                                               const std::string &,
                                               const std::string &,
                                               const FluidDynamics &>;

    /// Constructor.
    TimeFunction(const std::string &  subsection,
                 const std::string &  variable_name_,
                 const std::string &  unit_of_measure_,
                 const FluidDynamics &fluid_dynamics_);

    /// Virtual destructor.
    virtual ~TimeFunction() = default;

    /// Returns the value of the function. In order to be correct, the value
    /// must have been updated with the @ref update_value method.
    virtual double
    get_value(const double &t) const final
    {
      // We assert that the value has been updated for the requested time.
      Assert(initialized && utils::is_equal(t, time),
             ExcMessage("Before calling this method, the value must be updated "
                        "through update_value."));

      // Silence compiler warning.
      (void)t;

      return tmp_value;
    }

    /// Get a string containing information about the function, for logging
    /// purposes.
    virtual std::string
    get_info() const = 0;

    /// Update value. Pre-computes the value and stores it.
    /// @note The time passed as first argument to this method is cached internally,
    /// and subsequent calls with the same time will have no effect, to avoid
    /// useless computations.
    virtual void
    update_value(const double &t)
    {
      // If the currently stored time already equals the given argument, this
      // method does nothing.
      if (utils::is_equal(time, t))
        return;

      time      = t;
      tmp_value = evaluate(t);

      initialized = true;
    }

  protected:
    /// Evaluate: compute the value at given time.
    virtual double
    evaluate(const double &t) const = 0;

    /// Flag indicating whether @ref update_value has been called at least once.
    bool initialized;

    /// Current time. This is updated by calls to @ref update_value.
    double time;

    /// Temporarily stored evaluation of the function, updated by calling
    /// @ref update_value.
    double tmp_value;

    /// Name of the variable whose evolution is described by this function (used
    /// to construct meaningful names and documentation for parameters).
    const std::string variable_name;

    /// Unit of measure of the variable whose evolution is described by this
    /// function (used to construct meaningful documentation for parameters).
    const std::string unit_of_measure;

    /// Capitalized variable name.
    std::string variable_name_capitalized;

    /// Reference to the fluid dynamics instance.
    const FluidDynamics &fluid_dynamics;
  };

  /**
   * @brief Class to represent a constant function of time.
   */
  class Constant : public TimeFunction
  {
  public:
    /// Label for this function.
    static inline constexpr auto label = "Constant";

    /// Constructor.
    Constant(const std::string &  subsection,
             const std::string &  variable_name,
             const std::string &  unit_of_measure,
             const FluidDynamics &fluid_dynamics)
      : TimeFunction(subsection + " / " + label,
                     variable_name,
                     unit_of_measure,
                     fluid_dynamics)
    {}

    /// Get info.
    virtual std::string
    get_info() const override
    {
      return "constant " + variable_name + " with value " +
             std::to_string(prm_value) + " " + unit_of_measure;
    }

    /// Declare parameters.
    virtual void
    declare_parameters(ParamHandler &params) const override
    {
      params.set_verbosity(VerbosityParam::Minimal);
      params.enter_subsection_path(prm_subsection_path);
      {
        params.declare_entry("Value",
                             "0.0",
                             Patterns::Double(),
                             "Constant value for " + variable_name + " " +
                               unit_of_measure + ".");
      }
      params.leave_subsection_path();
      params.reset_verbosity();
    }

    /// Parse parameters.
    virtual void
    parse_parameters(ParamHandler &params)
    {
      params.enter_subsection_path(prm_subsection_path);
      {
        prm_value = params.get_double("Value");
      }
      params.leave_subsection_path();
    }

  protected:
    /// Evaluate.
    virtual double
    evaluate(const double & /*t*/) const override
    {
      return prm_value;
    }

    /// @name Parameters read from file.
    /// @{

    /// Constant value.
    double prm_value;

    /// @}
  };

  /**
   * @brief Class to represent a ramp in time.
   *
   * Given @f$a_\mathrm{start}@f$, @f$a_\mathrm{end}@f$, @f$t_\mathrm{start}@f$
   * and @f$t_\mathrm{load}@f$, defines the function
   * @f[
   * a(t) = \left\{
   * \begin{alignedat}{1}
   * a_\mathrm{start} &\quad& t < t_\mathrm{start} \\
   * a_\mathrm{start} + (a_\mathrm{end} - a_\mathrm{start})\frac{1}{2}\left(1 -
   * \cos\left(\frac{\pi(t - t_\mathrm{start})}{t_\mathrm{load}}\right)\right)
   * &\quad&
   * t_\mathrm{start} \leq t < t_\mathrm{start} + t_\mathrm{load} \\
   * a_\mathrm{end} &\quad& t_\mathrm{start} + t_\mathrm{load} \leq t
   * \end{alignedat} \right.
   * @f]
   * corresponding to a sinusoidal ramp from @f$a_\mathrm{start}@f$ to
   * @f$a_\mathrm{end}@f$.
   */
  class Ramp : public TimeFunction
  {
  public:
    /// Label for this function.
    static inline constexpr auto label = "Ramp";

    /// Constructor.
    Ramp(const std::string &  subsection,
         const std::string &  variable_name,
         const std::string &  unit_of_measure,
         const FluidDynamics &fluid_dynamics);

    /// Get info.
    virtual std::string
    get_info() const override
    {
      return variable_name + " ramp from " + std::to_string(prm_initial_value) +
             " " + unit_of_measure + " to " + std::to_string(prm_final_value) +
             " " + unit_of_measure + " in " + std::to_string(prm_time_load) +
             " s, starting at " + std::to_string(prm_time_start) + " s";
    }

    /// Declare parameters.
    virtual void
    declare_parameters(ParamHandler &params) const override;

    /// Parse parameters.
    virtual void
    parse_parameters(ParamHandler &params) override;

  protected:
    /// Compute value.
    virtual double
    evaluate(const double &t) const override;

    /// @name Parameters read from file.
    /// @{

    /// Initial value.
    double prm_initial_value;

    /// End value.
    double prm_final_value;

    /// Start time [s].
    double prm_time_start;

    /// End time [s].
    double prm_time_load;

    /// @}
  };

  /**
   * @brief Class to represent a pulsatile evolution in time.
   *
   * Given @f$a_\mathrm{min}@f$, @f$a_\mathrm{max}@f$ and @f$T@f$, defines the
   * function
   * @f[
   * a(t) = a_\mathrm{min} + (a_\mathrm{max} - a_\mathrm{min})\frac{1}{2}\left(1
   * - \cos\left(\frac{2 \pi t}{T}\right)\right)
   * @f]
   * corresponding to a sinusoidal function between @f$a_\mathrm{min}@f$ and
   * @f$a_\mathrm{max}@f$ of period @f$T@f$.
   */
  class Pulsatile : public TimeFunction
  {
  public:
    /// Label for this function.
    static inline constexpr auto label = "Pulsatile";

    /// Constructor.
    Pulsatile(const std::string &  subsection,
              const std::string &  variable_name,
              const std::string &  unit_of_measure,
              const FluidDynamics &fluid_dynamics);

    /// Get info.
    virtual std::string
    get_info() const override
    {
      return "pulsatile " + variable_name + " with minimum value " +
             std::to_string(prm_value_min) + " " + unit_of_measure +
             ", maximum value " + std::to_string(prm_value_max) + " " +
             unit_of_measure + " and period " + std::to_string(prm_period) +
             " s";
    }

    /// Declare parameters.
    virtual void
    declare_parameters(ParamHandler &params) const override;

    /// Parse parameters.
    virtual void
    parse_parameters(ParamHandler &params) override;

  protected:
    /// Compute value.
    virtual double
    evaluate(const double &t) const override;

    /// @name Parameters read from file.
    /// @{

    /// Minimum value.
    double prm_value_min;

    /// Maximum value.
    double prm_value_max;

    /// Period [s].
    double prm_period;

    /// @}
  };

  /**
   * @brief Interpolation in time of data from a CSV file.
   *
   * Relies on @ref utils::csv_read and @ref utils::TimeInterpolation.
   *
   * @note The CSV file is read, and its data interpolated, upon the first call
   * to the @ref update_value method. This way, if a @ref CSVFunction object is
   * instantiated but never used and the corresponding file does not exist,
   * no exception is raised.
   */
  class CSVFunction : public TimeFunction
  {
  public:
    /// Label for this function.
    static inline constexpr auto label = "CSV function";

    /// Constructor.
    CSVFunction(const std::string &  subsection,
                const std::string &  variable_name,
                const std::string &  unit_of_measure,
                const FluidDynamics &fluid_dynamics);

    /// Get info.
    virtual std::string
    get_info() const override
    {
      const double period = fluid_dynamics.get_period();
      return variable_name + " from CSV file " + prm_file_name +
             " with scaling factor " + std::to_string(prm_scaling_factor) +
             (utils::is_positive(period) ?
                (" and period " + std::to_string(period) + " s") :
                "");
    }


    /// Method to return the time interpolation function.
    const utils::TimeInterpolation &
    get_time_interpolation() const
    {
      return time_interpolation;
    }

    /// Method to return the time interval of the CSV (@a i.e.
    /// the difference between the last and first time).
    double
    get_time_interval() const
    {
      return time_final - time_initial;
    }

    /// Declare parameters.
    virtual void
    declare_parameters(ParamHandler &params) const override;

    /// Parse parameters.
    virtual void
    parse_parameters(ParamHandler &params) override;

    /// The first time this method is called, load the CSV data from file an
    /// setup the TimeInterpolation object.
    virtual void
    update_value(const double &time) override;

  protected:
    /// Compute value.
    virtual double
    evaluate(const double &t) const override;

    /// Flag indicating whether the data has been read from file and the
    /// TimeInterpolation has been setup.
    bool initialized;

    /// Time interpolation object.
    utils::TimeInterpolation time_interpolation;

    /// @name Parameters read from file.
    /// @{

    /// File name.
    std::string prm_file_name;

    /// Column for the time in the CSV file.
    std::string prm_time_column;

    /// Column for the variable in the CSV file.
    std::string prm_value_column;

    /// First time instant in the CSV file.
    double time_initial;

    /// Last time instant in the CSV file.
    double time_final;

    /// Delimiter for the CSV file.
    char prm_delimiter;

    /// Interpolation method.
    utils::TimeInterpolation::Mode prm_interpolation_method;

    /// Scaling factor.
    double prm_scaling_factor;

    /// @}
  };

  /**
   * @brief Class for the evaluation of flow by integration.
   *
   * Wraps the method @ref FluidDynamics::compute_boundary_flow_and_pressure
   * within a class whose interface makes it suitable to be used within fluid
   * boundary condition classes. It computes:
   * @f[ Q(t) = \int_{\Gamma}\mathbf u\cdot\mathbf n d\sigma @f]
   * where @f$\Gamma\subset\partial\Omega@f$ is a portion of the boundary
   * specified by means of a set of tags.
   *
   * Notice that the flow is computed for the last available solution of the
   * FluidDynamics problem instance; this function does not allow to implement
   * implicit conditions based on the computed flow (i.e. conditions in which
   * the flow is an unknown of the problem).
   */
  class FlowIntegralEvaluation : public TimeFunction
  {
  public:
    /// Label for this function.
    static inline constexpr auto label = "Flow integral evaluation";

    /// Constructor.
    FlowIntegralEvaluation(const std::string &  subsection,
                           const std::string &  variable_name,
                           const std::string &  unit_of_measure,
                           const FluidDynamics &fluid_dynamics);

    /// Get info.
    virtual std::string
    get_info() const override
    {
      std::string tag_list = "";
      for (const auto tag : prm_outlet_tags)
        tag_list += std::to_string(tag) + ", ";
      tag_list.resize(tag_list.size() - 2);

      return "flow evaluated by integrating over tags " + tag_list;
    }

    /// Declare parameters.
    virtual void
    declare_parameters(ParamHandler &params) const override;

    /// Parse parameters.
    virtual void
    parse_parameters(ParamHandler &params) override;

  protected:
    /// Evaluate.
    double
    evaluate(const double &t) const;

    /// @name Parameters read from file.
    /// @{

    /// Outlet tags.
    std::set<types::boundary_id> prm_outlet_tags;

    /// @}

    mutable double flow; ///< Pre-allocated variable.
  };

  /**
   * @brief Pressure boundary condition.
   *
   * Represents a Neumann boundary condition of the type @f$\sigma(\mathbf u,
   * p)\mathbf n = -p_{\Gamma_\mathrm{N}}(t)\mathbf n@f$ on
   * @f$\Gamma_\mathrm{N}@f$, where @f$p_{\Gamma_\mathrm{N}}(t)@f$ is a given
   * pressure, constant in space (over the selected boundary portion) but
   * possibly varying in time.
   *
   * When calling the evaluate method of the @ref TimeFunction representing
   * pressure over time, this class does not pass any additional parameters (the
   * argument params of evaluate is left empty).
   */
  class PressureBC : public CoreModel, public utils::FunctionNeumann
  {
  public:
    /// Constructor.
    PressureBC(const std::string &          subsection,
               const std::set<std::string> &valid_pressure_functions_,
               const std::string &          default_pressure_function_,
               const FluidDynamics &        fluid_dynamics_);

    /// Override of value method.
    virtual void
    vector_value(const Point<dim> &p, Vector<double> &values) const override;

    /// Declare parameters.
    virtual void
    declare_parameters(ParamHandler &params) const override;

    /// Parse parameters.
    virtual void
    parse_parameters(ParamHandler &params) override;

    /// Get time evolution of pressure.
    std::shared_ptr<TimeFunction> &
    get_pressure_over_time()
    {
      return pressure_over_time;
    }

    /// Get time evolution of pressure, const overload.
    const std::shared_ptr<TimeFunction> &
    get_pressure_over_time() const
    {
      return pressure_over_time;
    }

    /// Get an informative string, for logging purposes.
    std::string
    get_info() const
    {
      return "prescribed pressure, " + pressure_over_time->get_info();
    }

    /// Override set_time to update the pressure value.
    virtual void
    set_time(const double time) override
    {
      Assert(pressure_over_time,
             ExcMessage("Custom pressure over time was chosen, but no pressure "
                        "over time function was set."));

      FunctionNeumann::set_time(time);
      pressure_over_time->update_value(time);
    }

  protected:
    /// Valid functions for pressure over time.
    std::set<std::string> valid_pressure_functions;

    /// Default function for pressure over time.
    std::string default_pressure_function;

    /// Object representing the pressure over time.
    std::shared_ptr<TimeFunction> pressure_over_time;

    /// Reference to the fluid dynamics instance.
    const FluidDynamics &fluid_dynamics;
  };

  /**
   * @brief Abstract class to represent a generic space profile of a flow BC.
   *
   * This class and its derivatives are used by FluidDynamicsBCs classes to
   * describe the space distribution of flow.
   *
   * Derived classes should declare:
   * - the vector_value method (inherited from utils::FunctionDirichlet)
   * to indicate how the function is computed;
   * - the methods declare_parameters and parse_parameters;
   * - a get_info method providing a string with information abount the boundary
   * condition, for logging purposes;
   * - a member <kbd>static inline constexpr auto label</kbd> containing the
   * label to be used for the condition in parameter files.
   *
   * To obtain a correct evaluation of the function, one
   * should call the set_normal/set_surface_area/set_barycenter methods when
   * needed (@a e.g. when time or geometry change). If the SpaceFunction is used
   * through a FlowBC, such calls are already taken into account.
   *
   * See also @ref FluidDynamicsBCs for a more detailed usage guide.
   */
  class SpaceFunction : public CoreModel, public utils::FunctionDirichlet
  {
  public:
    /// Type of normalization represented by the class.
    /// Used to set normalization type in constructor.
    enum class Normalization
    {
      /// The function has a flowrate equal to 1.
      UnitFlowrate,
      /// The function has magnitude 1 in each point.
      UnitVelocity
    };

    /// Alias for the generic space function factory.
    using SpaceFunctionFactory = GenericFactory<SpaceFunction,
                                                const std::string &,
                                                const std::string &,
                                                const std::string &,
                                                const FluidDynamics &>;

    /// Constructor.
    SpaceFunction(
      const std::string &  subsection,
      const std::string &  variable_name_,
      const std::string &  unit_of_measure_,
      const FluidDynamics &fluid_dynamics_,
      const Normalization &normalization_ = Normalization::UnitFlowrate);

    /// Virtual destructor.
    virtual ~SpaceFunction() = default;

    /// Get a string containing information about the function, for logging
    /// purposes.
    virtual std::string
    get_info() const = 0;

    /// Get the type of normalization considered in the function. Useful when
    /// combined with a TimeFunction, within FlowBC.
    virtual const Normalization &
    get_normalization() const
    {
      return normalization;
    }

  protected:
    /// Name of the variable whose evolution is described by this function (used
    /// to construct meaningful names and documentation for parameters).
    const std::string variable_name;

    /// Unit of measure of the variable whose evolution is described by this
    /// function (used to construct meaningful documentation for parameters).
    const std::string unit_of_measure;

    /// Capitalized variable name.
    std::string variable_name_capitalized;

    /// Reference to the fluid dynamics instance.
    const FluidDynamics &fluid_dynamics;

    /// Run-time flag for type of normalization considered.
    const Normalization normalization;
  };

  /**
   * @brief Parabolic velocity profile.
   *
   * For a given circular boundary @f$\Gamma_\mathrm{D}@f$ of radius @f$R@f$,
   * represent the Dirichlet boundary condition @f$ \mathbf u = -2\frac{R^2 -
   * r^2}{\pi R^4} \mathbf n @f$, where @f$r@f$ is the distance from
   * the center of @f$\Gamma_\mathrm{D}@f$
   * and @f$\mathbf n@f$ is the outgoing normal.
   * The condition represents a parabolic velocity profile through
   * @f$\Gamma_\mathrm{D}@f$ with a unit flowrate.
   *
   * A time-dependent flowrate can be enforced by using this class within a
   FlowBC, where it is combined with a TimeFunction.

   * @warning
   * The present implementation is meant to be used for circular sections only.
   * The class works also for non-circular sections, but the
   * resulting profile is not null on the boundary of such sections, in
   * general, and backflow may occur in those cases.
   */
  class Parabolic : public SpaceFunction
  {
  public:
    /// Label for this function.
    static inline constexpr auto label = "Parabolic";

    /// Constructor.
    Parabolic(const std::string &  subsection,
              const std::string &  variable_name_,
              const std::string &  unit_of_measure_,
              const FluidDynamics &fluid_dynamics_);

    /// Override of vector_value method.
    virtual void
    vector_value(const Point<dim> &p, Vector<double> &values) const override;

    virtual void
    declare_parameters(ParamHandler & /*params*/) const override
    {}

    virtual void
    parse_parameters(ParamHandler & /*params*/) override
    {}

    /// Get an informative string, for logging purposes.
    std::string
    get_info() const
    {
      return "parabolic profile with prescribed flowrate";
    }
  };

  /**
   * @brief Uniform velocity boundary condition.
   *
   * Implements the boundary condition @f$\mathbf u =
   * -\mathbf n@f$, corresponding to a unit prescribed inflow
   * velocity that is normal to the boundary, constant in space.
   *
   * A time-dependent velocity magnitude can be enforced by using this class
   * within a FlowBC, where it is combined with a TimeFunction.
   */
  class UniformVelocity : public SpaceFunction
  {
  public:
    /// Label for this function.
    static inline constexpr auto label = "Uniform";

    /// Constructor.
    UniformVelocity(const std::string &  subsection,
                    const std::string &  variable_name_,
                    const std::string &  unit_of_measure_,
                    const FluidDynamics &fluid_dynamics_);

    /// Override of vector_value method.
    virtual void
    vector_value(const Point<dim> &p, Vector<double> &values) const override;

    virtual void
    declare_parameters(ParamHandler & /*params*/) const override
    {}

    virtual void
    parse_parameters(ParamHandler & /*params*/) override
    {}

    /// Get an informative string, for logging purposes.
    std::string
    get_info() const
    {
      return "prescribed constant velocity";
    }
  };

  /**
   * @brief Flow boundary condition.
   *
   * For a given boundary @f$\Gamma_\mathrm{D}@f$,
   * represent the Dirichlet boundary condition @f$ \mathbf u(t, \mathbf x) =
   * \theta\,g(t)\,\mathbf f(\mathbf x)@f$, where @f$\mathbf f@f$ is the space
   * distribution of the velocity profile (see @ref SpaceFunction), @f$g@f$
   * its time evolution (see @ref TimeFunction), and @f$\theta@f$ is a
   * repartition factor (used when the total flow rate is known for multiple
   * boundaries).
   *
   * If the FluidDynamics instance that uses this boundary condition has volume
   * tags and volume computation enabled, when calling the evaluate method of
   * the @ref TimeFunction representing flow over time, this class passes as
   * extra parameters the variations in volume of the chambers. Otherwise, the
   * extra parameters to evaluate are ignored.
   */
  class FlowBC : public CoreModel, public utils::FunctionDirichlet
  {
  public:
    /// Constructor.
    FlowBC(const std::string &          subsection,
           const std::set<std::string> &valid_time_functions_,
           const std::string &          default_time_function_,
           const std::set<std::string> &valid_space_functions_,
           const std::string &          default_space_function_,
           const FluidDynamics &        fluid_dynamics_);

    /// Override of vector_value method.
    virtual void
    vector_value(const Point<dim> &p, Vector<double> &values) const override;

    /// Overridden to pass information to @ref SpaceFunction.
    virtual void
    set_normal_vector(const Tensor<1, dim, double> &normal_vector_) override
    {
      Assert(space_function != nullptr,
             ExcMessage("Empty space function in FlowBC class"));

      FunctionDirichlet::set_normal_vector(normal_vector_);
      space_function->set_normal_vector(normal_vector_);
    };

    /// Overridden to pass information to @ref SpaceFunction.
    virtual void
    set_surface_area(const double &surface_area_) override
    {
      Assert(space_function != nullptr,
             ExcMessage("Empty space function in FlowBC class"));

      FunctionDirichlet::set_surface_area(surface_area_);
      space_function->set_surface_area(surface_area_);
    };

    /// Overridden to pass information to @ref SpaceFunction.
    virtual void
    set_barycenter(const Point<dim> &barycenter_) override
    {
      Assert(space_function != nullptr,
             ExcMessage("Empty space function in FlowBC class"));

      FunctionDirichlet::set_barycenter(barycenter_);
      space_function->set_barycenter(barycenter_);
    };

    /// Get information about the SpaceFunction.
    std::string
    get_space_distribution() const
    {
      return prm_space_distribution;
    }

    virtual void
    declare_parameters(ParamHandler &params) const override;

    virtual void
    parse_parameters(ParamHandler &params) override;

    /// Get repartition: obtains a copy of this object, but scaling the flow
    /// repartition by the given argument. Can be used to construct several BC
    /// functions that correspond to splitting the same flow across
    /// multiple inlets.
    ///
    /// @note Makes sense only if <b>flowrate</b> is imposed (currently, only
    /// if space_function is Parabolic).
    std::shared_ptr<FlowBC>
    get_repartition(const double &k) const
    {
      AssertThrow(
        space_function->get_normalization() ==
          SpaceFunction::Normalization::UnitFlowrate,
        ExcMessage(
          "Trying to distribute flowrate in a function that is not "
          "normalized w.r.t. flowrate (see SpaceFunction::Normalization)."));

      std::shared_ptr<FlowBC> result =
        std::make_shared<FlowBC>(prm_subsection_path,
                                 valid_time_functions,
                                 default_time_function,
                                 valid_space_functions,
                                 default_space_function,
                                 fluid_dynamics);

      // Same time function, representing the total flowrate.
      result->time_function = time_function;

      // A new instance must be created, to avoid overwriting of geometric
      // information (surface normal, barycenter, ...) among the different BCs
      // into which the current one is repartitioned.
      // Indeed, space_function is a shared_ptr and copying it would result
      // in different classes pointing to the same function, so modifications to
      // space_function in one class would propagate to other classes as well.
      ParamHandler tmp;
      result->space_function =
        SpaceFunction::SpaceFunctionFactory::parse_child_parameters(
          tmp,
          prm_space_distribution,
          prm_subsection_path,
          "space distribution",
          "[-]",
          fluid_dynamics);

      result->flow_repartition = flow_repartition * k;

      return result;
    }

    /// Get time evolution of flowrate.
    ///
    /// Meaningful only if space_function has unit flowrate (currently, only
    /// Parabolic).
    std::shared_ptr<TimeFunction> &
    get_flow_over_time()
    {
      AssertThrow(
        space_function->get_normalization() ==
          SpaceFunction::Normalization::UnitFlowrate,
        ExcMessage(
          "Trying to access flowrate time evolution of a function that is not "
          "normalized w.r.t. flowrate (see SpaceFunction::Normalization)."));

      return time_function;
    }

    /// Get time evolution of flowrate, const overload.
    ///
    /// Meaningful only if space_function has unit flowrate (currently, only
    /// Parabolic).
    const std::shared_ptr<TimeFunction> &
    get_flow_over_time() const
    {
      AssertThrow(
        space_function->get_normalization() ==
          SpaceFunction::Normalization::UnitFlowrate,
        ExcMessage(
          "Trying to access flowrate time evolution of a function that is not "
          "normalized w.r.t. flowrate (see SpaceFunction::Normalization)."));

      return time_function;
    }

    /// Get time evolution of velocity.
    ///
    /// Meaningful only if space_function has unit characteristic velocity
    /// (currently, only UniformVelocity).
    std::shared_ptr<TimeFunction> &
    get_velocity_over_time()
    {
      AssertThrow(
        space_function->get_normalization() ==
          SpaceFunction::Normalization::UnitVelocity,
        ExcMessage(
          "Trying to access velocity time evolution of a function that is not "
          "normalized w.r.t. velocity (see SpaceFunction::Normalization)."));

      return time_function;
    }

    /// Get time evolution of velocity, const overload.
    ///
    /// Meaningful only if space_function has unit characteristic velocity
    /// (currently, only UniformVelocity).
    const std::shared_ptr<TimeFunction> &
    get_velocity_over_time() const
    {
      AssertThrow(
        space_function->get_normalization() ==
          SpaceFunction::Normalization::UnitVelocity,
        ExcMessage(
          "Trying to access velocity time evolution of a function that is not "
          "normalized w.r.t. velocity (see SpaceFunction::Normalization)."));

      return time_function;
    }

    /// Get type of normalization of the underlying space function.
    ///
    /// If SpaceFunction::Normalization::UnitFlowrate, the underlying time
    /// function is a flowrate [m^3/s].
    ///
    /// If SpaceFunction::Normalization::UnitVelocity, the underlying time
    /// function is a velocity evolution [m/s].
    virtual const SpaceFunction::Normalization &
    get_space_function_normalization() const
    {
      return space_function->get_normalization();
    }

    /// Get an informative string, for logging purposes.
    std::string
    get_info() const
    {
      return "flow condition: " + space_function->get_info() +
             ", time dependence: " + time_function->get_info() +
             ", flow repartition = " + std::to_string(flow_repartition);
    }

    /// Override set_time to update the flow value.
    virtual void
    set_time(const double time) override
    {
      Assert(
        time_function,
        ExcMessage(
          "Custom time evolution was chosen, but no time function was set."));

      FunctionDirichlet::set_time(time);
      space_function->set_time(time);

      time_function->update_value(time);
    }

  protected:
    /// Valid functions for time evolution.
    std::set<std::string> valid_time_functions;

    /// Default function for time evolution.
    std::string default_time_function;

    /// Function for time evolution.
    std::shared_ptr<TimeFunction> time_function;

    /// Valid functions for space evolution.
    std::set<std::string> valid_space_functions;

    /// Default function for space evolution.
    std::string default_space_function;

    /// Space distribution label.
    std::string prm_space_distribution;

    /// Function for space distributions.
    std::shared_ptr<SpaceFunction> space_function;

    /// Fraction of total flow assigned to this boundary.
    double flow_repartition;

    /// Reference to the fluid dynamics instance.
    const FluidDynamics &fluid_dynamics;
  };


  /**
   * @brief Womersley velocity profile as boundary condition.
   *
   * For a given circular boundary @f$\Gamma_\mathrm{D}@f$ of radius @f$R@f$,
   * represent the Dirichlet boundary condition that imposes the Womersley
   * velocity profile corresponding to the flow-rate time evolution provided
   * by a .csv file. This profile is computed solving the Inverse Womersley
   * problem as described in
   * @refcite{inverse_womersley, Berselli et al. (2012)}.
   * A time-dependent periodic flowrate must be provided through a .csv file and
   * the selected time-evolution must be CSVFunction.
   * This class is used instead of FlowBC when the Womersley boundary condition
   * flag is on.
   *
   * @warning The present implementation is meant to be used for circular sections only.
   */
  class WomersleyBC : public CoreModel, public utils::FunctionDirichlet
  {
  public:
    /// Class that represent a mode of the Fourier decomposition of the flow,
    /// used to reconstruct the Womersley profile through the inverse Womersley
    /// problem.
    class WomersleyMode : public Function<dim, std::complex<double>>
    {
    public:
      /// Constructor.
      WomersleyMode(const double &       an,
                    const double &       bn,
                    const size_t &       index_,
                    const FluidDynamics &fluid_dynamics_)
        : q_n(an, bn)
        , index(index_)
        , fluid_dynamics(fluid_dynamics_)
      {}

      /// Override of value method used to compute and return the Fourier
      /// coefficient @f[v_n@f] for the axial velocity.
      std::complex<double>
      value(const Point<dim> & p,
            const unsigned int component = 0) const override;

      /// Overridden to pass information to @ref SpaceFunction.
      void
      set_surface_area(const double &surface_area_)
      {
        surface_area = surface_area_;

        // Precompute all quantities that depend only on the surface area.
        {
          using namespace std::complex_literals;

          boundary_radius = std::sqrt(surface_area / M_PI);

          const double omega_n = 2 * M_PI * index / fluid_dynamics.get_period();

          wo_R = std::sqrt(boundary_radius * boundary_radius * omega_n *
                           fluid_dynamics.get_density() /
                           fluid_dynamics.get_viscosity());

          const std::complex<double> arg_bessel =
            std::complex<double>(0.0, (wo_R * wo_R) / 4.0);

          f_11 = utils::bessel_j(0, 2.0 * std::sqrt(-arg_bessel));
          f_12 = (utils::bessel_j(1, 2.0 * std::sqrt(-arg_bessel))) /
                 std::sqrt(-arg_bessel);

          sigma_n =
            (1.0i * omega_n * q_n) /
            ((M_PI * boundary_radius * boundary_radius) * (1 - f_12 / f_11));

          j_0R = utils::bessel_j(
            0, 0.5 * (std::complex<double>(-M_SQRT2, M_SQRT2)) * wo_R);
        }
      };

      /// Overridden to pass information to @ref SpaceFunction.
      void
      set_barycenter(const Point<dim> &barycenter_)
      {
        barycenter = barycenter_;
      };

    protected:
      /// Complex coefficient of the Fourier decomposition of the flow.
      const std::complex<double> q_n;

      /// Index of the coefficient of the Fourier decomposition of the flow.
      const size_t index;

      /// Reference to the fluid dynamics instance.
      const FluidDynamics &fluid_dynamics;

      /// @name Geometric information.
      /// @{

      /// Inlet surface area.
      double surface_area;

      /// Inlet surface barycenter.
      Point<dim> barycenter;

      /// Boundary radius.
      double boundary_radius;

      /// @}

      /// @name Quantities precomputed for efficiency.
      /// @{

      /// @todo Documentation?
      double wo_R;

      /// Fourier coefficient associated to the velocity profile.
      std::complex<double> f_11;

      /// Fourier coefficient associated to the velocity profile.
      std::complex<double> f_12;

      /// @todo Documentation?
      std::complex<double> sigma_n;

      /// @todo Documentation?
      std::complex<double> j_0R;

      /// @}
    };

    /// Constructor.
    WomersleyBC(const std::string &  subsection,
                const FluidDynamics &fluid_dynamics_);


    /// Override of vector_value method used to solve the inverse Womersley
    /// problems. Currently, the implementation of this function assumes that
    /// the boundary never moves
    virtual void
    vector_value(const Point<dim> &p, Vector<double> &values) const override;

    /// Declare parameters.
    virtual void
    declare_parameters(ParamHandler & /*params*/) const override;

    /// Parse parameters.
    virtual void
    parse_parameters(ParamHandler &params) override;

    /// Get repartition: obtains a copy of this object, but scaling the flow
    /// repartition by the given argument. Can be used to construct several BC
    /// functions that correspond to splitting the same flow across
    /// multiple inlets.
    ///
    /// @todo To test.
    std::shared_ptr<WomersleyBC>
    get_repartition(const double &k) const
    {
      std::shared_ptr<WomersleyBC> result =
        std::make_shared<WomersleyBC>(prm_subsection_path, fluid_dynamics);

      // Same time function, representing the total flowrate.
      result->flowrate_over_time = flowrate_over_time;

      // A new instance must be created, to avoid overwriting of geometric
      // information (surface normal, barycenter, ...) among the different BCs
      // into which the current one is repartitioned.

      result->initialized = initialized;

      result->flowrate_over_time = flowrate_over_time;

      for (const auto &m : modes)
        result->modes.emplace_back(m);

      result->flow_repartition = flow_repartition * k;

      return result;
    }

    /// Get time evolution of flowrate.
    std::shared_ptr<CSVFunction> &
    get_flow_over_time()
    {
      return flowrate_over_time;
    }

    /// Get time evolution of flowrate, const overload.
    const std::shared_ptr<CSVFunction> &
    get_flow_over_time() const
    {
      return flowrate_over_time;
    }

    /// Get an informative string, for logging purposes.
    std::string
    get_info() const
    {
      return "flow condition: Womersley profile with prescribed flowrate"
             ", time dependence: " +
             flowrate_over_time->get_info() +
             ", flow repartition = " + std::to_string(flow_repartition);
    }

    /// Override set_time to update the flow value.
    virtual void
    set_time(const double time) override
    {
      Assert(
        flowrate_over_time,
        ExcMessage(
          "Custom time evolution was chosen, but no time function was set."));

      FunctionDirichlet::set_time(time);

      flowrate_over_time->update_value(time);

      AssertThrow(flowrate_over_time->get_time_interval() ==
                    fluid_dynamics.get_period(),
                  ExcMessage(
                    "The time period imposed must coincide with the time "
                    "interval in the CSV file."));

      // Upon first call, setup the vector of modes.
      if (!initialized)
        {
          const std::vector<double> &re_ck =
            flowrate_over_time->get_time_interpolation().get_re_ck();
          const std::vector<double> &im_ck =
            flowrate_over_time->get_time_interpolation().get_im_ck();

          const unsigned int mu = (re_ck.size() + 1) % 2;
          const size_t       M  = (re_ck.size() - mu - 1) / 2;

          AssertThrow(
            flowrate_over_time->get_time_interpolation().get_mode() ==
              utils::TimeInterpolation::Mode::FourierSeries,
            ExcMessage(
              "Womersley requires Fourier series as interpolation method"));

          for (size_t n = 1; n <= M; ++n)
            {
              modes.push_back(
                WomersleyMode(re_ck[M + n], im_ck[M + n], n, fluid_dynamics));
              modes.back().set_barycenter(get_barycenter());
              modes.back().set_surface_area(get_surface_area());
            }

          if (mu == 1)
            {
              modes.push_back(WomersleyMode(
                re_ck[2 * M + 1], im_ck[2 * M + 1], 2 * M + 1, fluid_dynamics));
              modes.back().set_barycenter(get_barycenter());
              modes.back().set_surface_area(get_surface_area());
            }

          initialized = true;
        }
    }

  protected:
    /// Boolean to tell whether if @ref set_time has been called at least once.
    bool initialized;

    /// Vector of WomersleyMode for every index in Fourier decomposition.
    std::vector<WomersleyMode> modes;

    /// Function for CSV evolution.
    std::shared_ptr<CSVFunction> flowrate_over_time;

    /// Fraction of total flow assigned to this boundary.
    double flow_repartition;

    /// Reference to the fluid dynamics instance.
    const FluidDynamics &fluid_dynamics;
  };

  /**
   * @brief Resistance boundary condition.
   *
   * Represents the condition @f$\sigma(\mathbf u, p)\mathbf n =
   * -(p_{\Gamma_\mathrm{N}}(t) + C\,Q_{\Gamma_\mathrm{N}}(t))\mathbf n@f$ on
   * @f$\Gamma_\mathrm{N}@f$, where @f$p_{\Gamma_\mathrm{N}}@f$ is a given
   * pressure, @f$Q_{\Gamma_\mathrm{N}}(t)@f$ is a (known a priori) proxy for
   * flow through the boundary @f$\Gamma_\mathrm{N}@f$,
   * and @f$C\ \left[\frac{kg}{s\,m^4}\right]@f$ a given constant.
   *
   * Notice that the class assumes the flow to be known (for example from
   * previous time step - see @ref FlowIntegralEvaluation - or from a CSV file - see
   * @ref CSVFunction); implicit resistance conditions, i.e. conditions where
   * the flow is an unknown of the problem are not implemented as of now.
   *
   * When calling the evaluate method of the @ref TimeFunction instances representing
   * flow and pressure over time, this class does not pass any additional
   * parameters (the argument params of evaluate is left empty).
   */
  class ResistanceBC : public CoreModel, public utils::FunctionNeumann
  {
  public:
    /// Constructor.
    ResistanceBC(const std::string &          subsection,
                 const std::set<std::string> &valid_pressure_functions_,
                 const std::string &          default_pressure_function_,
                 const std::set<std::string> &valid_flow_functions_,
                 const std::string &          default_flow_function_,
                 const FluidDynamics &        fluid_dynamics_);

    /// Override of vector_value method.
    virtual void
    vector_value(const Point<dim> & /*p*/,
                 Vector<double> &values) const override;

    /// Declare parameters.
    virtual void
    declare_parameters(ParamHandler &params) const override;

    /// Parse parameters.
    virtual void
    parse_parameters(ParamHandler &params) override;

    /// Get imposed pressure value at current time. set_time must be called
    /// before calling this method.
    double
    get_pressure() const
    {
      return pressure_over_time->get_value(this->get_time()) +
             resistance_constant * flow_over_time->get_value(this->get_time());
    }

    /// Get time evolution of pressure.
    std::shared_ptr<TimeFunction> &
    get_pressure_over_time()
    {
      return pressure_over_time;
    }

    /// Get time evolution of pressure, const overload.
    const std::shared_ptr<TimeFunction> &
    get_pressure_over_time() const
    {
      return pressure_over_time;
    }

    /// Get time evolution of flow.
    std::shared_ptr<TimeFunction> &
    get_flow_over_time()
    {
      return flow_over_time;
    }

    /// Get time evolution of flow, const overload.
    const std::shared_ptr<TimeFunction> &
    get_flow_over_time() const
    {
      return flow_over_time;
    }

    /// Get an informative string, for logging purposes.
    std::string
    get_info() const
    {
      return "resistance condition, pressure: " +
             pressure_over_time->get_info() +
             ", flow: " + flow_over_time->get_info() +
             ", resistance constant: " + std::to_string(resistance_constant);
    }

    /// Override set_time to update the pressure and flow values.
    virtual void
    set_time(const double time) override
    {
      Assert(pressure_over_time,
             ExcMessage("Custom pressure over time was chosen, but no pressure "
                        "over time function was set."));
      Assert(flow_over_time,
             ExcMessage("Custom flow over time was chosen, but no flow "
                        "over time function was set."));

      FunctionNeumann::set_time(time);

      pressure_over_time->update_value(time);
      flow_over_time->update_value(time);
    }

  protected:
    /// Valid functions for pressure over time.
    std::set<std::string> valid_pressure_functions;

    /// Default function for pressure over time.
    std::string default_pressure_function;

    /// Valid functions for flow over time.
    std::set<std::string> valid_flow_functions;

    /// Default function for flow over time.
    std::string default_flow_function;

    /// Object representing the pressure over time.
    std::shared_ptr<TimeFunction> pressure_over_time;

    /// Object representing the flux over time.
    std::shared_ptr<TimeFunction> flow_over_time;

    /// Resistance constant [kg*s^{-1}*m^{-4}].
    double resistance_constant;

    /// Reference to the fluid dynamics instance.
    const FluidDynamics &fluid_dynamics;
  };

  // Register time functions into their factory.
  LIFEX_REGISTER_CHILD_INTO_FACTORY(TimeFunction,
                                    Constant,
                                    Constant::label,
                                    const std::string &,
                                    const std::string &,
                                    const std::string &,
                                    const FluidDynamics &);
  LIFEX_REGISTER_CHILD_INTO_FACTORY(TimeFunction,
                                    Ramp,
                                    Ramp::label,
                                    const std::string &,
                                    const std::string &,
                                    const std::string &,
                                    const FluidDynamics &);
  LIFEX_REGISTER_CHILD_INTO_FACTORY(TimeFunction,
                                    Pulsatile,
                                    Pulsatile::label,
                                    const std::string &,
                                    const std::string &,
                                    const std::string &,
                                    const FluidDynamics &);
  LIFEX_REGISTER_CHILD_INTO_FACTORY(TimeFunction,
                                    CSVFunction,
                                    CSVFunction::label,
                                    const std::string &,
                                    const std::string &,
                                    const std::string &,
                                    const FluidDynamics &);
  LIFEX_REGISTER_CHILD_INTO_FACTORY(TimeFunction,
                                    FlowIntegralEvaluation,
                                    FlowIntegralEvaluation::label,
                                    const std::string &,
                                    const std::string &,
                                    const std::string &,
                                    const FluidDynamics &);

  // Register flow space functions into their factory.
  LIFEX_REGISTER_CHILD_INTO_FACTORY(SpaceFunction,
                                    Parabolic,
                                    Parabolic::label,
                                    const std::string &,
                                    const std::string &,
                                    const std::string &,
                                    const FluidDynamics &);
  LIFEX_REGISTER_CHILD_INTO_FACTORY(SpaceFunction,
                                    UniformVelocity,
                                    UniformVelocity::label,
                                    const std::string &,
                                    const std::string &,
                                    const std::string &,
                                    const FluidDynamics &);
} // namespace lifex::FluidDynamicsBCs

#endif /* LIFEX_HELPER_FLUID_DYNAMICS_BCS_HPP_ */
