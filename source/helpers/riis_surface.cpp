/********************************************************************************
  Copyright (C) 2020 - 2023 by the lifex authors.

  This file is part of lifex.

  lifex is free software; you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  lifex is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with lifex.  If not, see <http://www.gnu.org/licenses/>.
********************************************************************************/

/**
 * @file
 *
 * @author Ivan Fumagalli <ivan.fumagalli@polimi.it>.
 * @author Alberto Zingaro <alberto.zingaro@polimi.it>.
 * @author Michele Bucelli <michele.bucelli@polimi.it>.
 */

#include "core/source/io/csv_reader.hpp"

#include "core/source/numerics/numbers.hpp"

#include "source/fluid_dynamics.hpp"

#include "source/helpers/riis_handler.hpp"
#include "source/helpers/riis_surface.hpp"

#include <deal.II/fe/mapping_fe_field.h>

#include <vtkXMLPolyDataWriter.h>

#include <boost/io/ios_state.hpp>

#include <algorithm>
#include <map>

namespace lifex
{
  RIISSurface::RIISSurface(const std::string &  subsection,
                           const RIISHandler &  riis_handler_,
                           const FluidDynamics &fluid_dynamics_,
                           const unsigned int & surface_index_)
    : CoreModel(subsection)
    , riis_handler(riis_handler_)
    , fluid_dynamics(fluid_dynamics_)
    , surface_index(surface_index_)
    , pressure_jump(0)
    , pressure_jump_old(0)
    , volume_derivative(0)
    , volume_derivative_old(0)
  {}

  void
  RIISSurface::declare_parameters(ParamHandler &params) const
  {
    params.enter_subsection_path(prm_subsection_path);

    params.set_verbosity(VerbosityParam::Minimal);
    {
      params.declare_entry("Surface labels",
                           "surface",
                           Patterns::List(Patterns::Anything()),
                           "Labels used to identify the surfaces in the output "
                           "(one for each surface).");

      params.declare_entry("Immersed surfaces basenames",
                           "surface",
                           Patterns::List(Patterns::FileName(
                             Patterns::FileName::FileType::input)),
                           "Files basenames (no extension) for the immersed "
                           "surfaces (one for each surface).");
    }
    params.reset_verbosity();

    params.set_verbosity(VerbosityParam::Standard);
    params.declare_entry(
      "Move surfaces with ALE",
      "false",
      Patterns::List(Patterns::Bool()),
      "Toggles movement of each immersed surface with the ALE displacement of "
      "the fluid domain (one for each surface, or a single value applied to "
      "all surfaces).");
    params.reset_verbosity();

    params.set_verbosity(VerbosityParam::Full);
    params.declare_entry("Flip normals",
                         "false",
                         Patterns::List(Patterns::Bool()),
                         "Flip the normal vector for immersed surfaces.");
    params.reset_verbosity();

    params.set_verbosity(VerbosityParam::Minimal);
    {
      params.declare_entry("On-off behavior",
                           "false",
                           Patterns::List(Patterns::Bool()),
                           "For each surface, toggle on-off behavior.");

      params.declare_entry("Displacement names",
                           "dVecGamma0",
                           Patterns::Anything(),
                           "Name of the displacement fields stored in the "
                           "immersed surface files (one for each surface).");

      params.declare_entry("Input configurations",
                           "Open",
                           Patterns::List(Patterns::Selection("Open | Closed")),
                           "For each immersed surface, the configuration of "
                           "its input VTP file: Open | Closed.");

      params.declare_entry(
        "Initial configurations",
        "Open",
        Patterns::List(Patterns::Selection("Open | Closed")),
        "Initial immersed surfaces configurations, for each surface: Open | "
        "Closed.");

      params.declare_entry(
        "Epsilons",
        "0.001",
        Patterns::List(Patterns::Double(0), 0),
        "The half-thickness of the smooth Dirac function, "
        "one for each RIIS term [m] (one for each surface).");

      params.declare_entry("Resistances",
                           "1000",
                           Patterns::List(Patterns::Double(0), 0),
                           "The resistances of the RIIS terms [kg * m^{-1} * "
                           "s^{-1}] (one for each surface).");
    }
    params.reset_verbosity();

    params.set_verbosity(VerbosityParam::Standard);
    {
      params.declare_entry(
        "Epsilons factors in open configuration",
        "1.0",
        Patterns::List(Patterns::Double(0), 0),
        "Factor multiplying epsilon when the surface is in open configuration "
        "(one for each surface).");

      params.declare_entry("Epsilons factors in closed configuration",
                           "1.0",
                           Patterns::List(Patterns::Double(0), 0),
                           "Factor multiplying epsilon when the surface is in "
                           "closed configuration (one for each surface).");

      params.declare_entry(
        "Resistances factors in open configuration",
        "1.0",
        Patterns::List(Patterns::Double(0), 0),
        "Factor multiplying resistance when the surface is in "
        "open configuration (one for each surface).");

      params.declare_entry(
        "Resistances factors in closed configuration",
        "1.0",
        Patterns::List(Patterns::Double(0), 0),
        "Factor multiplying resistance when the surface is in "
        "closed configuration (one for each surface).");
    }
    params.reset_verbosity();

    params.set_verbosity(VerbosityParam::Full);
    {
      params.declare_entry(
        "Surface velocities factors",
        "1.0",
        Patterns::List(Patterns::Double(0), 0),
        "Factor multiplying the surface velocity u_Gamma in Navier-Stokes "
        "momentum equation (one for each surface) [-].");
    }
    params.reset_verbosity();

    params.enter_subsection("Displacement law");
    {
      params.set_verbosity(VerbosityParam::Minimal);
      {
        params.declare_entry(
          "Displacement laws",
          "Prescribed",
          Patterns::List(Patterns::Selection("Prescribed | Flow driven")),
          "Displacement law for immersed surface: Prescribed | Flow driven.");

        params.declare_entry(
          "Ramp shapes",
          "Cosine",
          Patterns::List(Patterns::Selection(
            "Linear | Cosine | Exponential | CosineExponential")),
          "For the Prescribed and Flow driven displacement laws, shapes of the "
          "opening and closing ramps (one for each surface). Linear | Cosine | "
          "Exponential | "
          "CosineExponential");
      }
      params.reset_verbosity();

      params.set_verbosity(VerbosityParam::Standard);
      params.declare_entry(
        "Ramp exponential coefficients",
        "-3.0",
        Patterns::List(Patterns::Double()),
        "Coefficient for Exponential and CosineExponential ramp shapes, for "
        "the Prescribed and Flow driven displacement laws (one for each "
        "surface).");
      params.reset_verbosity();

      params.set_verbosity(VerbosityParam::Minimal);
      params.enter_subsection("Prescribed");
      {
        params.declare_entry(
          "First ramp: start time",
          "0.10",
          Patterns::List(Patterns::Double(0), 0),
          "Starting first ramp time (one for each surface) [s].");

        params.declare_entry(
          "First ramp: end time",
          "0.11",
          Patterns::List(Patterns::Double(0), 0),
          "Ending first ramp time (one for each surface) [s].");

        params.declare_entry(
          "Second ramp: start time",
          "0.20",
          Patterns::List(Patterns::Double(0), 0),
          "Starting second ramp time (one for each surface) [s].");

        params.declare_entry(
          "Second ramp: end time",
          "0.21",
          Patterns::List(Patterns::Double(0), 0),
          "Ending second ramp time (one for each surface) [s].");
      }
      params.leave_subsection();
      params.reset_verbosity();

      params.set_verbosity(VerbosityParam::Standard);
      params.enter_subsection("Flow driven");
      {
        params.declare_entry("Duration of the opening ramp",
                             "1e-3",
                             Patterns::List(Patterns::Double(0), 0),
                             "Time required to go from closed to open "
                             "configuration (one for each surface) [s].");

        params.declare_entry("Duration of the closing ramp",
                             "1e-3",
                             Patterns::List(Patterns::Double(0), 0),
                             "Time required to go from open to closed "
                             "configuration (one for each surface) [s].");

        params.declare_entry(
          "Refractory time",
          "0",
          Patterns::List(Patterns::Double(0), 0),
          "Refractory time [s]. After the valve has opened (or closed), this "
          "amount of time must pass before it can close (or open) again.");

        params.enter_subsection("Volume derivative closing");
        {
          params.declare_entry(
            "Volume compartments",
            "",
            Patterns::List(Patterns::Anything()),
            "For each immersed surface, the volume compartment whose "
            "derivative must be checked to trigger the closing of the "
            "valve.");

          params.declare_entry(
            "Flow direction through the valve",
            "",
            Patterns::List(Patterns::Selection("Inflow|Outflow")),
            "For each immersed surface, the expected direction of the flow "
            "through it, when in open configuration. When the derivative of "
            "the associated volume compartment has opposite sign to the flow "
            "direction specified here, the closing of the valve is "
            "triggered.");
        }
        params.leave_subsection();

        params.enter_subsection("Prescribed closing");
        {
          params.declare_entry(
            "Closing start times",
            "",
            Patterns::List(Patterns::Double(0), 0),
            "Immersed surface closing: start time (one for each surface) [s] "
            "(if immersed surface closing law is Prescribed).");
        }
        params.leave_subsection();
      }
      params.leave_subsection(); // Flow driven.
      params.reset_verbosity();
    }
    params.leave_subsection(); // Displacement law.

    params.enter_subsection("Control volumes");
    params.set_verbosity(VerbosityParam::Standard);
    {
      params.declare_entry(
        "Upwind control volume labels",
        "",
        Patterns::List(Patterns::Anything()),
        "Labels of control volumes upwind to the surfaces (one for each "
        "surface). These must have been defined in the Control volumes section "
        "of Fluid dynamics parameters.");

      params.declare_entry(
        "Downwind control volume labels",
        "",
        Patterns::List(Patterns::Anything()),
        "Labels of control volumes downwind to the surfaces (one for each "
        "surface). These must have been defined in the Control volumes section "
        "of Fluid dynamics parameters.");
    }
    params.reset_verbosity();
    params.leave_subsection();

    params.leave_subsection_path();
  }

  void
  RIISSurface::parse_parameters(ParamHandler &params)
  {
    // Parse input file.
    params.parse();

    // Read input parameters.
    params.enter_subsection_path(prm_subsection_path);

    std::string tmp_string;

    const unsigned int n_surfaces =
      Utilities::split_string_list(params.get("Surface labels")).size();

    prm_label =
      params.get_vector_item("Surface labels", surface_index, n_surfaces);
    prm_label_mangled = utils::mangle(prm_label);

    prm_basename = params.get_vector_item("Immersed surfaces basenames",
                                          surface_index,
                                          n_surfaces);

    prm_flip_normal =
      params.get_vector_item<bool>("Flip normals", surface_index, n_surfaces);

    prm_move_with_ale =
      params.get_vector_item<bool>("Move surfaces with ALE",
                                   Utilities::split_string_list(
                                     params.get("Move surfaces with ALE"))
                                         .size() == 1 ?
                                     0 :
                                     surface_index,
                                   n_surfaces);

    prm_epsilon =
      params.get_vector_item<double>("Epsilons", surface_index, n_surfaces);
    prm_epsilon_factor_open =
      params.get_vector_item<double>("Epsilons factors in open configuration",
                                     surface_index,
                                     n_surfaces);
    prm_epsilon_factor_closed =
      params.get_vector_item<double>("Epsilons factors in closed configuration",
                                     surface_index,
                                     n_surfaces);
    prm_resistance =
      params.get_vector_item<double>("Resistances", surface_index, n_surfaces);
    prm_on_off = params.get_vector_item<bool>("On-off behavior",
                                              surface_index,
                                              n_surfaces);

    prm_resistance_factor_open = params.get_vector_item<double>(
      "Resistances factors in open configuration", surface_index, n_surfaces);

    prm_resistance_factor_closed = params.get_vector_item<double>(
      "Resistances factors in closed configuration", surface_index, n_surfaces);

    prm_displacement_name =
      params.get_vector_item("Displacement names", surface_index, n_surfaces);

    prm_surface_velocity_factor =
      params.get_vector_item<double>("Surface velocities factors",
                                     surface_index,
                                     n_surfaces);

    params.enter_subsection("Displacement law");
    {
      tmp_string =
        params.get_vector_item("Displacement laws", surface_index, n_surfaces);
      if (tmp_string == "Prescribed")
        prm_displacement_law = DisplacementLaw::Prescribed;
      else if (tmp_string == "Flow driven")
        prm_displacement_law = DisplacementLaw::FlowDriven;

      tmp_string =
        params.get_vector_item("Ramp shapes", surface_index, n_surfaces);

      if (tmp_string == "Linear")
        prm_ramp_shape = RampShape::Linear;
      else if (tmp_string == "Cosine")
        prm_ramp_shape = RampShape::Cosine;
      else if (tmp_string == "Exponential")
        prm_ramp_shape = RampShape::Exponential;
      else // if (tmp_string == "CosineExponential")
        prm_ramp_shape = RampShape::CosineExponential;

      if (prm_ramp_shape == RampShape::Exponential ||
          prm_ramp_shape == RampShape::CosineExponential)
        prm_exp_ramp_coefficient =
          params.get_vector_item<double>("Ramp exponential coefficients",
                                         surface_index,
                                         n_surfaces);

      if (prm_displacement_law == DisplacementLaw::Prescribed)
        {
          params.enter_subsection("Prescribed");

          prm_prescribed_t0 =
            params.get_vector_item<double>("First ramp: start time",
                                           surface_index,
                                           n_surfaces);
          prm_prescribed_t1 =
            params.get_vector_item<double>("First ramp: end time",
                                           surface_index,
                                           n_surfaces);
          prm_prescribed_t2 =
            params.get_vector_item<double>("Second ramp: start time",
                                           surface_index,
                                           n_surfaces);
          prm_prescribed_t3 =
            params.get_vector_item<double>("Second ramp: end time",
                                           surface_index,
                                           n_surfaces);

          params.leave_subsection();
        }
      else if (prm_displacement_law == DisplacementLaw::FlowDriven)
        {
          params.enter_subsection("Flow driven");

          prm_flow_driven_duration_opening =
            params.get_vector_item<double>("Duration of the opening ramp",
                                           surface_index,
                                           n_surfaces);

          prm_flow_driven_duration_closing =
            params.get_vector_item<double>("Duration of the closing ramp",
                                           surface_index,
                                           n_surfaces);

          prm_flow_driven_refractory_time =
            params.get_vector_item<double>("Refractory time",
                                           surface_index,
                                           n_surfaces);

          tmp_string = params.get("Closing law");
          if (tmp_string == "Volume derivative")
            {
              prm_flow_driven_closing = FlowDrivenClosing::VolumeDerivative;

              params.enter_subsection("Volume derivative closing");
              prm_flow_driven_volume_compartment =
                params.get_vector_item("Volume compartments",
                                       surface_index,
                                       n_surfaces);
              prm_flow_driven_volume_flow_direction =
                params.get_vector_item("Flow direction through the valve",
                                       surface_index,
                                       n_surfaces);
              params.leave_subsection();
            }
          else if (tmp_string == "Pressure jump")
            {
              prm_flow_driven_closing = FlowDrivenClosing::PressureJump;
            }
          else // if (tmp_string == "Prescribed")
            {
              prm_flow_driven_closing = FlowDrivenClosing::Prescribed;

              params.enter_subsection("Prescribed closing");
              prm_flow_driven_prescribed_closing_time =
                params.get_vector_item<double>("Closing start times",
                                               surface_index,
                                               n_surfaces);
              params.leave_subsection();
            }

          params.leave_subsection();
        }
    }
    params.leave_subsection();

    tmp_string =
      params.get_vector_item("Input configurations", surface_index, n_surfaces);
    if (tmp_string == "Open")
      prm_input_configuration = ValveConfiguration::Open;
    else // if (tmp_string == "Close")
      prm_input_configuration = ValveConfiguration::Closed;

    tmp_string = params.get_vector_item("Initial configurations",
                                        surface_index,
                                        n_surfaces);
    if (tmp_string == "Open")
      {
        prm_initial_configuration = ValveConfiguration::Open;
        opening_coeff             = 1.0;
      }
    else // if (tmp_string == "Close")
      {
        prm_initial_configuration = ValveConfiguration::Closed;
        opening_coeff             = 0.0;
      }

    current_configuration = prm_initial_configuration;

    params.enter_subsection("Control volumes");
    {
      prm_enable_control_volumes = params.get_bool("Enable control volumes");

      if (prm_enable_control_volumes)
        {
          prm_control_volume_upwind =
            params.get_vector_item("Upwind control volume labels",
                                   surface_index,
                                   n_surfaces);
          prm_control_volume_downwind =
            params.get_vector_item("Downwind control volume labels",
                                   surface_index,
                                   n_surfaces);
        }
    }
    params.leave_subsection();

    params.leave_subsection_path();

    if (prm_displacement_law == DisplacementLaw::FlowDriven)
      AssertThrow(prm_enable_control_volumes,
                  ExcMessage("Control volumes must be enabled to use "
                             "flow-driven displacement law."));
  }

  void
  RIISSurface::setup(const DoFHandler<dim> &dof_handler,
                     const double &         time_init_,
                     const double &         time_final_,
                     const double &         time_step_,
                     const double &         period_)
  {
    IndexSet owned_dofs = dof_handler.locally_owned_dofs();
    IndexSet relevant_dofs;
    DoFTools::extract_locally_relevant_dofs(dof_handler, relevant_dofs);

    time_init  = time_init_;
    time_final = time_final_;
    time_step  = time_step_;
    period     = period_;

    const std::string vtp_filename = prm_basename + ".vtp";
    const bool        move_with_ale =
      riis_handler.get_ale_handler() && prm_move_with_ale;

    // Distance bounding box enlarged by 10% to facilitate postprocessing.
    current_signed_distance =
      std::make_unique<utils::InterpolatedSignedDistance>(
        vtp_filename,
        dof_handler,
        nullptr,
        prm_epsilon *
            std::max(prm_epsilon_factor_open, prm_epsilon_factor_closed) +
          riis_handler.get_triangulation().get_info().get_diameter_max() *
            cutoff_distance_factor,
        riis_handler.get_triangulation().get_info().get_diameter_tot());

    displacement_g = std::make_unique<utils::VTKFunction>(
      vtp_filename, utils::VTKDataType::PolyData, 1.0, 1.0, true);
    displacement_g->setup_as_linear_projection(prm_displacement_name);

    // If we are not using the on/off mode, and input and initial
    // configurations are different, we have to warp the valve to bring it
    // in initial configuration. (In on/off mode, the surface is never
    // warped).
    if (!prm_on_off && prm_initial_configuration != prm_input_configuration)
      {
        current_signed_distance->warp_by_array_combination(
          {{prm_displacement_name, 1.0}});

        displacement_g->warp_by_array_combination(
          {{prm_displacement_name, 1.0}});
      }

    if (move_with_ale)
      {
        closest_dofs = current_signed_distance->find_closest_owned_dofs(
          riis_handler.get_ale_handler()->get_lifting().get_dof_handler(),
          riis_handler.get_ale_support_points());

        current_signed_distance->warp_by_pointwise_vectors(
          utils::VTKFunction::extract_nearest_neighbor_values(
            closest_dofs, riis_handler.get_ale_handler()->get_displacement()));
      }

    current_signed_distance->update();

    velocity_coeff = 0.0;

    // Log information.
    if (prm_displacement_law == DisplacementLaw::Prescribed)
      {
        pcout << "   " << prm_label
              << " - opening and closing at prescribed times" << std::endl;
      }
    else if (prm_displacement_law == DisplacementLaw::FlowDriven)
      {
        pcout << "   " << prm_label << " - opening based on pressure jump in "
              << prm_flow_driven_duration_opening << "s, " << std::flush;

        if (prm_flow_driven_closing == FlowDrivenClosing::Prescribed)
          pcout << "closing at prescribed time" << std::flush;
        else if (prm_flow_driven_closing == FlowDrivenClosing::PressureJump)
          pcout << "closing based on pressure jump" << std::flush;
        else // if (prm_flow_driven_closing ==
             // FlowDrivenClosing::VolumeDerivative)
          pcout << "closing based on volume derivative" << std::flush;

        pcout << " in " << prm_flow_driven_duration_closing << "s."
              << std::endl;
      }
  }

  bool
  RIISSurface::update()
  {
    // Backup stream flags and manipulators.
    const boost::io::ios_all_saver iostream_backup(pcout.get_stream());
    pcout << std::setprecision(3);

    const double time =
      std::fmod(fluid_dynamics.get_time(), fluid_dynamics.get_period());

    // Compute pressure jump.
    if (prm_enable_control_volumes)
      {
        pressure_jump_old = pressure_jump;
        pressure_jump     = fluid_dynamics.get_pressure_control_volume(
                          prm_control_volume_upwind) -
                        fluid_dynamics.get_pressure_control_volume(
                          prm_control_volume_downwind);
      }

    double opening_coeff_old = opening_coeff;

    if (prm_displacement_law == DisplacementLaw::Prescribed)
      {
        if (utils::is_definitely_less_than(time, prm_prescribed_t0))
          opening_coeff = 0.0;
        else if (utils::is_definitely_less_than(time, prm_prescribed_t1))
          opening_coeff =
            evaluate_ramp((time - prm_prescribed_t0) /
                          (prm_prescribed_t1 - prm_prescribed_t0));
        else if (utils::is_definitely_less_than(time, prm_prescribed_t2))
          opening_coeff = 1.0;
        else if (utils::is_definitely_less_than(time, prm_prescribed_t3))
          opening_coeff =
            1.0 - evaluate_ramp((time - prm_prescribed_t2) /
                                (prm_prescribed_t3 - prm_prescribed_t2));
        else
          opening_coeff = 0.0;

        // If the initial configuration is open, ramps are inverted.
        if (prm_initial_configuration == ValveConfiguration::Open)
          opening_coeff = 1.0 - opening_coeff;

        // Valve configuration can now be determined according to the value
        // of opening_coeff.
        if (utils::is_zero(opening_coeff))
          current_configuration = ValveConfiguration::Closed;
        else if (utils::is_equal(opening_coeff, 1.0))
          current_configuration = ValveConfiguration::Open;
        else if (opening_coeff > opening_coeff_old)
          current_configuration = ValveConfiguration::Opening;
        else
          current_configuration = ValveConfiguration::Closing;
      }

    else if (prm_displacement_law == DisplacementLaw::FlowDriven)
      {
        // First, we update the current configuration based on pressure jump
        // and volume derivative.
        const double time         = fluid_dynamics.get_time();
        const double time_step    = fluid_dynamics.get_time_step();
        const double period       = fluid_dynamics.get_period();
        const double period_count = std::floor(time / period);

        if (prm_flow_driven_closing == FlowDrivenClosing::VolumeDerivative)
          {
            volume_derivative_old = volume_derivative;
            volume_derivative     = fluid_dynamics.get_volume_derivative(
              prm_flow_driven_volume_compartment);
          }

        const bool refractory_time_has_passed =
          (current_configuration == ValveConfiguration::Open &&
           (!flow_driven_opening_time.has_value() ||
            time > flow_driven_opening_time.value() +
                     prm_flow_driven_duration_opening +
                     prm_flow_driven_refractory_time)) ||
          (current_configuration == ValveConfiguration::Closed &&
           (!flow_driven_closing_time.has_value() ||
            time > flow_driven_closing_time.value() +
                     prm_flow_driven_duration_closing +
                     prm_flow_driven_refractory_time));

        if (current_configuration == ValveConfiguration::Closed &&
            utils::is_positive(pressure_jump) &&
            utils::is_positive(pressure_jump_old) &&
            utils::is_positive(time - time_step) && refractory_time_has_passed)
          {
            current_configuration = ValveConfiguration::Opening;

            flow_driven_opening_time = time - time_step;
            pcout << "\t" << prm_label << " - start opening at time "
                  << flow_driven_opening_time.value() << std::endl;
          }

        else if (current_configuration == ValveConfiguration::Opening &&
                 utils::is_equal(opening_coeff, 1.0))
          {
            current_configuration = ValveConfiguration::Open;

            if (prm_flow_driven_closing == FlowDrivenClosing::Prescribed)
              flow_driven_closing_time =
                period_count * period + prm_flow_driven_prescribed_closing_time;
          }

        else if (current_configuration == ValveConfiguration::Open &&
                 refractory_time_has_passed)
          {
            if (prm_flow_driven_closing == FlowDrivenClosing::Prescribed &&
                !utils::is_definitely_less_than(
                  time, flow_driven_closing_time.value()))
              current_configuration = ValveConfiguration::Closing;

            else if (prm_flow_driven_closing ==
                       FlowDrivenClosing::VolumeDerivative &&
                     ((utils::is_negative(volume_derivative) &&
                       utils::is_negative(volume_derivative_old) &&
                       prm_flow_driven_volume_flow_direction == "Inflow") ||
                      (utils::is_positive(volume_derivative) &&
                       utils::is_positive(volume_derivative_old) &&
                       prm_flow_driven_volume_flow_direction == "Outflow")))
              {
                flow_driven_closing_time = time - time_step;
                current_configuration    = ValveConfiguration::Closing;

                pcout << "\t" << prm_label << " - start closing at time "
                      << flow_driven_closing_time.value() << std::endl;
              }

            else if (prm_flow_driven_closing ==
                       FlowDrivenClosing::PressureJump &&
                     utils::is_negative(pressure_jump) &&
                     utils::is_negative(pressure_jump_old) &&
                     utils::is_positive(time - time_step))
              {
                flow_driven_closing_time = time - time_step;
                current_configuration    = ValveConfiguration::Closing;

                pcout << "\t" << prm_label << " - start closing at time "
                      << flow_driven_closing_time.value() << std::endl;
              }
          }

        else if (current_configuration == ValveConfiguration::Closing &&
                 utils::is_equal(opening_coeff, 0.0))
          {
            current_configuration = ValveConfiguration::Closed;
          }

        // Then, we update the opening coefficient according to the current
        // configuration.
        if (current_configuration == ValveConfiguration::Closed)
          opening_coeff = 0.0;
        else if (current_configuration == ValveConfiguration::Opening)
          opening_coeff =
            evaluate_ramp((time - flow_driven_opening_time.value()) /
                          prm_flow_driven_duration_opening);
        else if (current_configuration == ValveConfiguration::Open)
          opening_coeff = 1.0;
        else if (current_configuration == ValveConfiguration::Closing)
          opening_coeff =
            1.0 - evaluate_ramp((time - flow_driven_closing_time.value()) /
                                prm_flow_driven_duration_closing);
      }

    const double coeff = opening_coeff - opening_coeff_old;
    velocity_coeff     = coeff / time_step;

    // Now update the current signed distance function and associated
    // vectors.
    if (prm_on_off)
      current_signed_distance->set_to_infinity(
        utils::is_positive(opening_coeff - 0.5));

    else if (riis_handler.get_ale_handler() && prm_move_with_ale)
      {
        const double warp_coeff =
          prm_input_configuration == ValveConfiguration::Closed ?
            opening_coeff :
            (1.0 - opening_coeff);

        // Reload from file, to return in reference configuration.
        current_signed_distance->reset();

        // Warp according to opening coefficient.
        current_signed_distance->warp_by_array_combination(
          {{prm_displacement_name, warp_coeff}});

        // If the opening coefficient has changed since last update, we need
        // to update the nearest-neighbor information. We don't need to do
        // it if the coefficient didn't change, beause nearest-neighbor
        // interpolation is done on the reference mesh.
        if (!utils::is_zero(coeff))
          {
            closest_dofs = current_signed_distance->find_closest_owned_dofs(
              riis_handler.get_ale_handler()->get_lifting().get_dof_handler(),
              riis_handler.get_ale_support_points());
          }

        // Warp according to total ALE displacement.
        current_signed_distance->warp_by_pointwise_vectors(
          utils::VTKFunction::extract_nearest_neighbor_values(
            closest_dofs,
            riis_handler.get_ale_handler()->get_displacement_owned()));

        current_signed_distance->update();
      }

    else if (!utils::is_zero(coeff))
      {
        // If the input configuration is Open, the displacement vector
        // defined on the surface brings from Open to Closed configuration.
        // Therefore, the surface must be warped according to the
        // incremental coefficient changed of sign, in that case, since such
        // coefficient is positive when opening and negative when closing.
        const double warp_coeff =
          prm_input_configuration == ValveConfiguration::Closed ?
            coeff :
            (-1.0 * coeff);

        current_signed_distance->warp_by_array_combination(
          {{prm_displacement_name, warp_coeff}});
        current_signed_distance->update();

        displacement_g->warp_by_array_combination(
          {{prm_displacement_name, warp_coeff}});
        displacement_g->setup_as_linear_projection(prm_displacement_name);
      }

    // Log current state.
    pcout << "\t" << prm_label << " - configuration = " << std::flush;
    if (current_configuration == ValveConfiguration::Open)
      pcout << "Open";
    else if (current_configuration == ValveConfiguration::Closing)
      pcout << "Closing, coeff = " << opening_coeff;
    else if (current_configuration == ValveConfiguration::Closed)
      pcout << "Closed";
    else if (current_configuration == ValveConfiguration::Opening)
      pcout << "Opening, coeff = " << opening_coeff;

    if (prm_enable_control_volumes)
      pcout << ", pressure jump = "
            << pressure_jump * utils::convert::Pa_to_mmHg << " mmHg";

    pcout << std::endl;

    return !utils::is_zero(coeff);
  }

  void
  RIISSurface::compute_velocity_loc(const Point<dim> &      x,
                                    Tensor<1, dim, double> &velocity_loc) const
  {
    static Vector<double> velocity_tmp(dim);

    displacement_g->vector_value(x, velocity_tmp);

    for (unsigned int d = 0; d < dim; ++d)
      velocity_loc[d] +=
        prm_surface_velocity_factor * velocity_coeff * velocity_tmp[d] *
        (prm_input_configuration == ValveConfiguration::Open ? -1.0 : 1.0);
  }

  void
  RIISSurface::attach_output(DataOut<dim> &         data_out,
                             const DoFHandler<dim> &dof_handler) const
  {
    data_out.add_data_vector(
      dof_handler,
      current_signed_distance->get_ghosted(),
      "distance_" + prm_label_mangled,
      {DataComponentInterpretation::component_is_scalar});
  }

  void
  RIISSurface::export_vtp(const double &      time,
                          const unsigned int &timestep_number)
  {
    const std::string output_basename = "immersed_" + prm_label_mangled;
    const std::string vtp_output_filename =
      output_basename + "_" + utils::timestep_to_string(timestep_number) +
      ".vtp";
    const std::string pvd_output_filename = output_basename + ".pvd";

    // Write vtp.
    vtkSmartPointer<vtkXMLPolyDataWriter> writer =
      vtkSmartPointer<vtkXMLPolyDataWriter>::New();
    writer->SetFileName(
      (Core::prm_output_directory + vtp_output_filename).c_str());
    writer->SetInputData(current_signed_distance->get_vtk_surface());
    writer->Write();

    // Write pvd for Paraview visualization.
    // immersed_surfaces_times_and_names[k] accumulates all times.
    vtp_output_times_and_names.push_back(
      std::make_pair(time, vtp_output_filename));
    std::ofstream pvd_output(Core::prm_output_directory + pvd_output_filename);
    DataOutBase::write_pvd_record(pvd_output, vtp_output_times_and_names);
  }

  void
  RIISSurface::declare_entries_csv(utils::CSVWriter &csv_writer) const
  {
    csv_writer.declare_entries({"opening_coeff_" + prm_label_mangled,
                                "velocity_coeff_" + prm_label_mangled});

    if (prm_enable_control_volumes)
      csv_writer.declare_entries({"p_jump_" + prm_label_mangled});

    if (prm_displacement_law == DisplacementLaw::FlowDriven &&
        prm_flow_driven_closing == FlowDrivenClosing::VolumeDerivative)
      csv_writer.declare_entries({"volume_derivative_" + prm_label_mangled});
  }

  void
  RIISSurface::set_entries_csv(utils::CSVWriter &csv_writer) const
  {
    csv_writer.set_entries(
      {{"opening_coeff_" + prm_label_mangled, opening_coeff},
       {"velocity_coeff_" + prm_label_mangled, velocity_coeff}});

    if (prm_enable_control_volumes)
      csv_writer.set_entries({{"p_jump_" + prm_label_mangled, pressure_jump}});

    if (prm_displacement_law == DisplacementLaw::FlowDriven &&
        prm_flow_driven_closing == FlowDrivenClosing::VolumeDerivative)
      csv_writer.set_entries(
        {{"volume_derivative_" + prm_label_mangled, volume_derivative}});
  }

  void
  RIISSurface::restart(const std::string &csv_filename,
                       const double &     time,
                       const double &     time_step)
  {
    std::vector<std::string> variable_names = {"opening_coeff_" +
                                                 prm_label_mangled, // 0
                                               "velocity_coeff_" +
                                                 prm_label_mangled}; // 1

    if (prm_enable_control_volumes)
      {
        variable_names.push_back("p_jump_" + prm_label_mangled); // 2
      }

    if (prm_displacement_law == DisplacementLaw::FlowDriven &&
        prm_flow_driven_closing == FlowDrivenClosing::VolumeDerivative)
      variable_names.push_back("volume_derivative_" + prm_label_mangled); // 3

    // Load variables at current time.
    {
      const std::map<std::string, double> csv_input =
        utils::csv_read_time_variables(csv_filename, time, variable_names);

      opening_coeff  = csv_input.at("opening_coeff_" + prm_label_mangled);
      velocity_coeff = csv_input.at("velocity_coeff_" + prm_label_mangled);

      if (prm_enable_control_volumes)
        {
          pressure_jump = csv_input.at("p_jump_" + prm_label_mangled);
        }

      if (prm_displacement_law == DisplacementLaw::FlowDriven &&
          prm_flow_driven_closing == FlowDrivenClosing::VolumeDerivative)
        volume_derivative =
          csv_input.at("volume_derivative_" + prm_label_mangled);

      // Deduce current configuration from input variables.
      if (utils::is_zero(opening_coeff))
        current_configuration = ValveConfiguration::Closed;
      else if (utils::is_equal(opening_coeff, 1.0))
        current_configuration = ValveConfiguration::Open;
      else if (utils::is_positive(velocity_coeff))
        current_configuration = ValveConfiguration::Opening;
      else
        current_configuration = ValveConfiguration::Closing;
    }

    // Load variables at previous time.
    {
      const std::map<std::string, double> csv_input =
        utils::csv_read_time_variables(csv_filename,
                                       time - time_step,
                                       variable_names);

      if (prm_enable_control_volumes)
        pressure_jump_old = csv_input.at("p_jump_" + prm_label_mangled);

      if (prm_displacement_law == DisplacementLaw::FlowDriven &&
          prm_flow_driven_closing == FlowDrivenClosing::VolumeDerivative)
        volume_derivative_old =
          csv_input.at("volume_derivative_" + prm_label_mangled);
    }

    // Now we have to warp the surface to bring it in the configuration that was
    // just loaded. To do so, we retrieve the coefficient corresponding to the
    // input configuration, and warp according to the difference between current
    // coefficient (after restart) and input coefficient.
    const double input_coefficient =
      prm_input_configuration == ValveConfiguration::Closed ? 0.0 : 1.0;
    const double warp_coefficient =
      (prm_input_configuration == ValveConfiguration::Open ? -1.0 : 1.0) *
      (opening_coeff - input_coefficient);

    displacement_g->warp_by_array_combination(
      {{prm_displacement_name, warp_coefficient}});

    // If moving with ALE, we also warp according to the restarted ALE
    // displacement.
    if (riis_handler.get_ale_handler() && prm_move_with_ale)
      {
        // Reload from file, to return in reference configuration.
        current_signed_distance->reset();

        // Warp according to opening coefficient.
        current_signed_distance->warp_by_array_combination(
          {{prm_displacement_name, warp_coefficient}});

        closest_dofs = current_signed_distance->find_closest_owned_dofs(
          riis_handler.get_ale_handler()->get_lifting().get_dof_handler(),
          riis_handler.get_ale_support_points());

        current_signed_distance->warp_by_pointwise_vectors(
          utils::VTKFunction::extract_nearest_neighbor_values(
            closest_dofs, riis_handler.get_ale_handler()->get_displacement()));
      }
    else
      {
        current_signed_distance->warp_by_array_combination(
          {{prm_displacement_name, warp_coefficient}});
      }

    current_signed_distance->update();

    // In the case of Flow driven displacement law, if we are in intermediate
    // configuration (opening and closing) we need to find the ramp start and
    // end times. This is done by inverting the function describing the ramp for
    // the ramp initial time.
    if (prm_displacement_law == DisplacementLaw::FlowDriven)
      {
        if (current_configuration == ValveConfiguration::Opening)
          flow_driven_opening_time =
            time - prm_flow_driven_duration_opening *
                     evaluate_ramp_inverse(opening_coeff);

        else if (current_configuration == ValveConfiguration::Closing)
          flow_driven_closing_time =
            time - prm_flow_driven_duration_closing *
                     evaluate_ramp_inverse(1.0 - opening_coeff);
      }
  }

  double
  RIISSurface::evaluate_ramp(const double &t) const
  {
    if (prm_ramp_shape == RampShape::Linear)
      return t;

    else if (prm_ramp_shape == RampShape::Cosine)
      return utils::cosine_ramp(t);

    else if (prm_ramp_shape == RampShape::Exponential)
      return (1.0 - std::exp(-prm_exp_ramp_coefficient * t)) /
             (1.0 - std::exp(-prm_exp_ramp_coefficient));

    else // if (prm_ramp_shape == RampShape::CosineExponential)
      return utils::cosine_ramp(
        (1.0 - std::exp(-prm_exp_ramp_coefficient * t)) /
        (1.0 - std::exp(-prm_exp_ramp_coefficient)));
  }

  double
  RIISSurface::evaluate_ramp_inverse(const double &c) const
  {
    if (prm_ramp_shape == RampShape::Linear)
      return c;

    else if (prm_ramp_shape == RampShape::Cosine)
      return std::acos(1.0 - 2 * c) / M_PI;

    else if (prm_ramp_shape == RampShape::Exponential)
      return -std::log(1.0 - c * (1.0 - std::exp(-prm_exp_ramp_coefficient))) /
             prm_exp_ramp_coefficient;

    else // if (prm_ramp_shape == RampShape::CosineExponential)
      return -std::log(1.0 - std::acos(1.0 - 2 * c) / M_PI *
                               (1.0 - std::exp(-prm_exp_ramp_coefficient))) /
             prm_exp_ramp_coefficient;
  }
} // namespace lifex
