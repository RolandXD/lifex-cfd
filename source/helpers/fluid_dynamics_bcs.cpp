/********************************************************************************
  Copyright (C) 2019 - 2023 by the lifex authors.

  This file is part of lifex.

  lifex is free software; you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  lifex is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with lifex.  If not, see <http://www.gnu.org/licenses/>.
********************************************************************************/

/**
 * @file
 *
 * @author Michele Bucelli <michele.bucelli@polimi.it>.
 * @author Alberto Zingaro <alberto.zingaro@polimi.it>.
 * @author Ivan Fumagalli <ivan.fumagalli@polimi.it>.
 */

#include "core/source/io/csv_reader.hpp"

#include "core/source/numerics/numbers.hpp"

#include "source/helpers/fluid_dynamics_bcs.hpp"

namespace lifex::FluidDynamicsBCs
{
  TimeFunction::TimeFunction(const std::string &  subsection,
                             const std::string &  variable_name_,
                             const std::string &  unit_of_measure_,
                             const FluidDynamics &fluid_dynamics_)
    : CoreModel(subsection)
    , initialized(false)
    , variable_name(variable_name_)
    , unit_of_measure(unit_of_measure_)
    , fluid_dynamics(fluid_dynamics_)
  {
    AssertThrow(!variable_name.empty(),
                ExcMessage("The variable name cannot be empty."));

    variable_name_capitalized = variable_name;

    variable_name_capitalized.front() =
      std::toupper(variable_name_capitalized.front());
  }

  Ramp::Ramp(const std::string &  subsection,
             const std::string &  variable_name,
             const std::string &  unit_of_measure,
             const FluidDynamics &fluid_dynamics)
    : TimeFunction(subsection + " / " + label,
                   variable_name,
                   unit_of_measure,
                   fluid_dynamics)
  {}

  void
  Ramp::declare_parameters(ParamHandler &params) const
  {
    params.set_verbosity(VerbosityParam::Minimal);
    params.enter_subsection_path(prm_subsection_path);
    {
      params.declare_entry("Initial value",
                           "0.0",
                           Patterns::Double(),
                           "Initial value of the " + variable_name + " ramp " +
                             unit_of_measure + ".");

      params.declare_entry("Final value",
                           "1.0",
                           Patterns::Double(),
                           "Final value of the " + variable_name + " ramp " +
                             unit_of_measure + ".");

      params.declare_entry("Start time",
                           "0.0",
                           Patterns::Double(0),
                           "Time at which the " + variable_name +
                             " ramp starts [s].");

      params.declare_entry("Load time",
                           "0.1",
                           Patterns::Double(0),
                           "Duration of the " + variable_name +
                             " ramp [s]; the ramp will have reached the final "
                             "value at start time plus load time.");
    }
    params.leave_subsection_path();
    params.reset_verbosity();
  }

  void
  Ramp::parse_parameters(ParamHandler &params)
  {
    params.enter_subsection_path(prm_subsection_path);
    {
      prm_initial_value = params.get_double("Initial value");
      prm_final_value   = params.get_double("Final value");
      prm_time_start    = params.get_double("Start time");
      prm_time_load     = params.get_double("Load time");
    }
    params.leave_subsection_path();
  }

  double
  Ramp::evaluate(const double &t) const
  {
    return utils::cosine_ramp(t,
                              prm_time_start,
                              prm_time_start + prm_time_load,
                              prm_initial_value,
                              prm_final_value);
  }

  Pulsatile::Pulsatile(const std::string &  subsection,
                       const std::string &  variable_name,
                       const std::string &  unit_of_measure,
                       const FluidDynamics &fluid_dynamics)
    : TimeFunction(subsection + " / " + label,
                   variable_name,
                   unit_of_measure,
                   fluid_dynamics)
  {}

  void
  Pulsatile::declare_parameters(ParamHandler &params) const
  {
    params.set_verbosity(VerbosityParam::Minimal);
    params.enter_subsection_path(prm_subsection_path);
    {
      params.declare_entry("Minimum value",
                           "0.0",
                           Patterns::Double(),
                           "Minimum " + variable_name + " " + unit_of_measure +
                             ".");

      params.declare_entry("Maximum value",
                           "1.0",
                           Patterns::Double(),
                           "Maximum " + variable_name + " " + unit_of_measure +
                             ".");

      params.declare_entry("Period",
                           "1.0",
                           Patterns::Double(0),
                           "Period of the " + variable_name +
                             " oscillations [s].");
    }
    params.leave_subsection_path();
    params.reset_verbosity();
  }

  void
  Pulsatile::parse_parameters(ParamHandler &params)
  {
    params.enter_subsection_path(prm_subsection_path);
    {
      prm_value_min = params.get_double("Minimum value");
      prm_value_max = params.get_double("Maximum value");
      prm_period    = params.get_double("Period");
    }
    params.leave_subsection_path();
  }

  double
  Pulsatile::evaluate(const double &t) const
  {
    return prm_value_min + (prm_value_max - prm_value_min) * 0.5 *
                             (1.0 - std::cos(2 * M_PI * t / prm_period));
  }

  CSVFunction::CSVFunction(const std::string &  subsection,
                           const std::string &  variable_name,
                           const std::string &  unit_of_measure,
                           const FluidDynamics &fluid_dynamics)
    : TimeFunction(subsection + " / " + label,
                   variable_name,
                   unit_of_measure,
                   fluid_dynamics)
    , initialized(false)
  {}

  void
  CSVFunction::declare_parameters(ParamHandler &params) const
  {
    params.enter_subsection_path(prm_subsection_path);
    {
      params.set_verbosity(VerbosityParam::Minimal);
      {
        params.declare_entry("CSV file name",
                             "",
                             Patterns::FileName(
                               Patterns::FileName::FileType::input),
                             "Name of the CSV file that " + variable_name +
                               "-over-time will be read from.");

        params.declare_entry(
          "Time column",
          "time",
          Patterns::Anything(),
          "Label of the time column in the CSV file. The first row of the CSV "
          "file is interpreted as a header.");

        params.declare_entry(variable_name_capitalized + " column",
                             utils::mangle(variable_name),
                             Patterns::Anything(),
                             "Label of " + variable_name +
                               " column in the CSV file. The first row of the "
                               "CSV file is interpreted as a header.");

        params.declare_entry("Delimiter",
                             ",",
                             Patterns::Anything(),
                             "Delimiter of the CSV file.");

        params.declare_entry(
          "Scaling factor",
          "1.0",
          Patterns::Double(),
          "Multiplicative scaling factor applied to the data.");
      }
      params.reset_verbosity();

      params.set_verbosity(VerbosityParam::Standard);
      {
        params.declare_entry_selection(
          "Interpolation",
          "Linear",
          "Linear|Cubic spline|Fourier series",
          "The interpolation method used to interpolate the data. Beware that "
          "for "
          "cubic spline interpolation, times are assumed to be equispaced, and "
          "the "
          "contents of the time column in the CSV file will only be used to "
          "determine initial and final time.");
      }
      params.reset_verbosity();
    }
    params.leave_subsection_path();
  }

  void
  CSVFunction::parse_parameters(ParamHandler &params)
  {
    params.enter_subsection_path(prm_subsection_path);
    {
      prm_file_name      = params.get("CSV file name");
      prm_time_column    = params.get("Time column");
      prm_value_column   = params.get(variable_name_capitalized + " column");
      prm_delimiter      = params.get("Delimiter")[0];
      prm_scaling_factor = params.get_double("Scaling factor");

      const std::string interpolation_mode_str = params.get("Interpolation");

      if (interpolation_mode_str == "Linear")
        prm_interpolation_method =
          utils::TimeInterpolation::Mode::LinearInterpolation;
      else if (interpolation_mode_str == "Cubic spline")
        prm_interpolation_method = utils::TimeInterpolation::Mode::CubicSpline;
      else if (interpolation_mode_str == "Fourier series")
        prm_interpolation_method =
          utils::TimeInterpolation::Mode::FourierSeries;
    }
    params.leave_subsection_path();
  }

  void
  CSVFunction::update_value(const double &t)
  {
    // The first time this method is called, read the CSV file and setup the
    // TimeInterpolation object.
    if (!initialized)
      {
        const std::vector<std::string> header =
          utils::csv_read_header(prm_file_name, prm_delimiter);

        // Find time and value column indices.
        unsigned int time_column_index  = header.size();
        unsigned int value_column_index = header.size();

        for (unsigned int i = 0; i < header.size(); ++i)
          {
            if (header[i] == prm_time_column)
              time_column_index = i;
            if (header[i] == prm_value_column)
              value_column_index = i;
          }

        AssertThrow(time_column_index < header.size(),
                    ExcMessage("Variable \"" + prm_time_column +
                               "\" not found in header of CSV file " +
                               prm_file_name + "."));
        AssertThrow(value_column_index < header.size(),
                    ExcMessage("Variable \"" + prm_value_column +
                               "\" not found in header of CSV file " +
                               prm_file_name + "."));

        std::vector<std::vector<double>> data = utils::csv_transpose(
          utils::csv_read(prm_file_name, prm_delimiter, true));

        time_initial = data[time_column_index].front();
        time_final   = data[time_column_index].back();

        if (prm_interpolation_method ==
            utils::TimeInterpolation::Mode::LinearInterpolation)
          {
            time_interpolation.setup_as_linear_interpolation(
              data[time_column_index], data[value_column_index]);
          }
        else if (prm_interpolation_method ==
                 utils::TimeInterpolation::Mode::CubicSpline)
          {
            time_interpolation.setup_as_cubic_spline(
              data[value_column_index],
              data[time_column_index][0],
              data[time_column_index][data[0].size() - 1]);
          }
        else if (prm_interpolation_method ==
                 utils::TimeInterpolation::Mode::FourierSeries)
          {
            time_interpolation.setup_as_fourier(data[time_column_index],
                                                data[value_column_index]);
          }

        initialized = true;
      }

    TimeFunction::update_value(t);
  }

  double
  CSVFunction::evaluate(const double &t) const
  {
    const double period = fluid_dynamics.get_period();

    if (utils::is_positive(period))
      return prm_scaling_factor *
             time_interpolation.evaluate(std::fmod(t, period));
    else
      return prm_scaling_factor * time_interpolation.evaluate(t);
  }

  FlowIntegralEvaluation::FlowIntegralEvaluation(
    const std::string &  subsection,
    const std::string &  variable_name,
    const std::string &  unit_of_measure,
    const FluidDynamics &fluid_dynamics)
    : TimeFunction(subsection + " / " + label,
                   variable_name,
                   unit_of_measure,
                   fluid_dynamics)
  {}

  void
  FlowIntegralEvaluation::declare_parameters(ParamHandler &params) const
  {
    params.set_verbosity(VerbosityParam::Minimal);
    params.enter_subsection_path(prm_subsection_path);
    {
      params.declare_entry("Outlet tags",
                           "",
                           Patterns::List(Patterns::Integer(0)),
                           "Tags of the portion of boundary where the flow "
                           "should be evaluated.");
    }
    params.leave_subsection_path();
    params.reset_verbosity();
  }

  void
  FlowIntegralEvaluation::parse_parameters(ParamHandler &params)
  {
    params.enter_subsection_path(prm_subsection_path);
    {
      prm_outlet_tags = params.get_set<types::boundary_id>("Outlet tags");
    }
    params.leave_subsection_path();
  }

  double
  FlowIntegralEvaluation::evaluate(const double & /*t*/) const
  {
    std::tie(flow, std::ignore) =
      fluid_dynamics.compute_boundary_flow_and_pressure(prm_outlet_tags, {});
    return flow;
  }

  PressureBC::PressureBC(const std::string &          subsection,
                         const std::set<std::string> &valid_pressure_functions_,
                         const std::string &  default_pressure_function_,
                         const FluidDynamics &fluid_dynamics_)
    : CoreModel(subsection)
    , FunctionNeumann(dim + 1)
    , valid_pressure_functions(valid_pressure_functions_)
    , default_pressure_function(default_pressure_function_)
    , fluid_dynamics(fluid_dynamics_)
  {
    Assert(valid_pressure_functions.find(FlowIntegralEvaluation::label) ==
             valid_pressure_functions.end(),
           ExcMessage(
             "Flow integral evaluation is not a valid function for "
             "the pressure of a constant pressure boundary condition."));
  }

  void
  PressureBC::vector_value(const Point<dim> & /*p*/,
                           Vector<double> &values) const
  {
    // Velocity components.
    for (unsigned int i = 0; i < dim; ++i)
      values[i] = -1.0 * pressure_over_time->get_value(this->get_time()) *
                  this->get_normal_vector()[i];

    // Pressure component.
    values[dim] = 0.0;
  }

  void
  PressureBC::declare_parameters(ParamHandler &params) const
  {
    params.set_verbosity(VerbosityParam::Minimal);
    params.enter_subsection_path(prm_subsection_path);
    {
      params.declare_entry_selection(
        "Time evolution",
        default_pressure_function,
        TimeFunction::TimeFunctionFactory::get_registered_keys_prm(
          valid_pressure_functions),
        "The type of evolution in time the pressure will have.");
    }
    params.leave_subsection_path();
    params.reset_verbosity();

    TimeFunction::TimeFunctionFactory::declare_children_parameters(
      params,
      valid_pressure_functions,
      prm_subsection_path,
      "pressure",
      "[Pa]",
      fluid_dynamics);
  }

  void
  PressureBC::parse_parameters(ParamHandler &params)
  {
    std::string time_evolution;
    params.enter_subsection_path(prm_subsection_path);
    {
      time_evolution = params.get("Time evolution");
    }
    params.leave_subsection_path();

    pressure_over_time =
      TimeFunction::TimeFunctionFactory::parse_child_parameters(
        params,
        time_evolution,
        prm_subsection_path,
        "pressure",
        "[Pa]",
        fluid_dynamics);
  }

  SpaceFunction::SpaceFunction(
    const std::string &                 subsection,
    const std::string &                 variable_name_,
    const std::string &                 unit_of_measure_,
    const FluidDynamics &               fluid_dynamics_,
    const SpaceFunction::Normalization &normalization_)
    : CoreModel(subsection)
    , FunctionDirichlet(dim + 1)
    , variable_name(variable_name_)
    , unit_of_measure(unit_of_measure_)
    , fluid_dynamics(fluid_dynamics_)
    , normalization(normalization_)
  {
    AssertThrow(!variable_name.empty(),
                ExcMessage("The variable name cannot be empty."));

    variable_name_capitalized = variable_name;

    variable_name_capitalized.front() =
      std::toupper(variable_name_capitalized.front());
  }

  Parabolic::Parabolic(const std::string &  subsection,
                       const std::string &  variable_name_,
                       const std::string &  unit_of_measure_,
                       const FluidDynamics &fluid_dynamics_)
    : SpaceFunction(subsection + " / " + label,
                    variable_name_,
                    unit_of_measure_,
                    fluid_dynamics_,
                    SpaceFunction::Normalization::UnitFlowrate)
  {}

  void
  Parabolic::vector_value(const Point<dim> &p, Vector<double> &values) const
  {
    const double      boundary_radius_squared = this->get_surface_area() / M_PI;
    const Point<dim> &barycenter              = this->get_barycenter();
    const double      r_squared               = p.distance_square(barycenter);

    // Velocity components.
    for (unsigned int i = 0; i < dim; ++i)
      if (utils::is_positive(boundary_radius_squared - r_squared))
        values[i] = -2.0 * (boundary_radius_squared - r_squared) /
                    (M_PI * boundary_radius_squared * boundary_radius_squared) *
                    this->get_normal_vector()[i];
      else
        values[i] = 0.0;

    // Pressure component.
    values[dim] = 0.0;
  }

  UniformVelocity::UniformVelocity(const std::string &  subsection,
                                   const std::string &  variable_name_,
                                   const std::string &  unit_of_measure_,
                                   const FluidDynamics &fluid_dynamics_)
    : SpaceFunction(subsection + " / " + label,
                    variable_name_,
                    unit_of_measure_,
                    fluid_dynamics_,
                    SpaceFunction::Normalization::UnitVelocity)
  {}

  void
  UniformVelocity::vector_value(const Point<dim> & /*p*/,
                                Vector<double> &values) const
  {
    for (unsigned int i = 0; i < dim; ++i)
      values[i] = -1.0 * this->get_normal_vector()[i];

    values[dim] = 0.0;
  }

  FlowBC::FlowBC(const std::string &          subsection,
                 const std::set<std::string> &valid_time_functions_,
                 const std::string &          default_time_function_,
                 const std::set<std::string> &valid_space_functions_,
                 const std::string &          default_space_function_,
                 const FluidDynamics &        fluid_dynamics_)
    : CoreModel(subsection)
    , FunctionDirichlet(dim + 1)
    , valid_time_functions(valid_time_functions_)
    , default_time_function(default_time_function_)
    , valid_space_functions(valid_space_functions_)
    , default_space_function(default_space_function_)
    , flow_repartition(1.0)
    , fluid_dynamics(fluid_dynamics_)
  {
    Assert(valid_time_functions.find(FlowIntegralEvaluation::label) ==
             valid_time_functions.end(),
           ExcMessage("Flow integral evaluation is not a valid function for "
                      "Dirichlet boundary conditions."));
  }

  void
  FlowBC::vector_value(const Point<dim> &p, Vector<double> &values) const
  {
    Assert(space_function != nullptr,
           ExcMessage("Empty space function in FlowBC class."));

    space_function->vector_value(p, values);
    values *= flow_repartition * time_function->get_value(this->get_time());
  }

  void
  FlowBC::declare_parameters(ParamHandler &params) const
  {
    params.set_verbosity(VerbosityParam::Minimal);
    params.enter_subsection_path(prm_subsection_path);
    {
      params.declare_entry_selection(
        "Time evolution",
        default_time_function,
        TimeFunction::TimeFunctionFactory::get_registered_keys_prm(
          valid_time_functions),
        "The type of evolution in time the flow will have.");

      params.declare_entry_selection(
        "Space distribution",
        default_space_function,
        SpaceFunction::SpaceFunctionFactory::get_registered_keys_prm(
          valid_space_functions),
        "The type of space distribution the flow velocity will have.");
    }
    params.leave_subsection_path();
    params.reset_verbosity();

    TimeFunction::TimeFunctionFactory::declare_children_parameters(
      params,
      valid_time_functions,
      prm_subsection_path,
      "value",
      "[m3/s OR m/s]",
      fluid_dynamics);

    SpaceFunction::SpaceFunctionFactory::declare_children_parameters(
      params,
      valid_space_functions,
      prm_subsection_path,
      "space distribution",
      "[-]",
      fluid_dynamics);
  }

  void
  FlowBC::parse_parameters(ParamHandler &params)
  {
    std::string time_evolution;
    params.enter_subsection_path(prm_subsection_path);
    {
      time_evolution         = params.get("Time evolution");
      prm_space_distribution = params.get("Space distribution");
    }
    params.leave_subsection_path();

    time_function = TimeFunction::TimeFunctionFactory::parse_child_parameters(
      params,
      time_evolution,
      prm_subsection_path,
      "value",
      "[m3/s OR m/s]",
      fluid_dynamics);

    space_function =
      SpaceFunction::SpaceFunctionFactory::parse_child_parameters(
        params,
        prm_space_distribution,
        prm_subsection_path,
        "space distribution",
        "[-]",
        fluid_dynamics);
  }


  WomersleyBC::WomersleyBC(const std::string &  subsection,
                           const FluidDynamics &fluid_dynamics_)
    : CoreModel(subsection)
    , FunctionDirichlet(dim + 1)
    , initialized(false)
    , flow_repartition(1.0)
    , fluid_dynamics(fluid_dynamics_)
  {}

  void
  WomersleyBC::vector_value(const Point<dim> &p, Vector<double> &values) const
  {
    using namespace std::literals::complex_literals;

    const Point<dim> &barycenter = this->get_barycenter();
    const double      r          = std::sqrt(p.distance_square(barycenter));
    const double boundary_radius = std::sqrt(this->get_surface_area() / M_PI);

    const std::vector<double> &re_ck =
      flowrate_over_time->get_time_interpolation().get_re_ck();
    const std::vector<double> &im_ck =
      flowrate_over_time->get_time_interpolation().get_im_ck();

    const unsigned int mu = (re_ck.size() + 1) % 2;
    const size_t       M  = (re_ck.size() - mu - 1) / 2;

    std::complex<double> v_n(0.0, 0.0);
    std::complex<double> v_n_conj(0.0, 0.0);
    std::complex<double> velocity(0.0, 0.0);

    // Map t \in [0, T] into t_mod \in [0, 2 * pi].
    const double t_mod =
      this->get_time() / fluid_dynamics.get_period() * 2 * M_PI;

    const double sigma0 =
      (8.0 * fluid_dynamics.get_viscosity() * re_ck[M]) /
      (fluid_dynamics.get_density() * M_PI * boundary_radius * boundary_radius *
       boundary_radius * boundary_radius);
    const double v0 =
      (sigma0 * boundary_radius * boundary_radius) /
      (4.0 * fluid_dynamics.get_viscosity() / fluid_dynamics.get_density()) *
      (1 - (r / boundary_radius) * (r / boundary_radius));

    velocity += v0;

    for (size_t n = 1; n <= M; ++n)
      {
        v_n      = modes[n - 1].value(p); // Compute v_n.
        v_n_conj = std::conj(v_n);        // Compute v_{-n}.
        velocity +=
          v_n * std::exp<double>(1.0i * static_cast<double>(n) * t_mod) +
          v_n_conj * std::exp<double>(-1.0i * static_cast<double>(n) * t_mod);
      }

    if (mu == 1)
      {
        v_n = modes[M].value(p); // Compute v_n.
        v_n *= 2.0 * std::cos((M + 1) * t_mod);
        velocity += v_n;
      }

    for (unsigned int j = 0; j < dim; ++j)
      {
        if (utils::is_positive(boundary_radius - r))
          values[j] = -1.0 * velocity.real() * this->get_normal_vector()[j];
        else
          values[j] = 0.0;
      }

    // Pressure component.
    values[dim] = 0.0;
  }

  void
  WomersleyBC::declare_parameters(ParamHandler &params) const
  {
    CSVFunction dummy(prm_subsection_path, "value", "[m3/s]", fluid_dynamics);
    dummy.declare_parameters(params);
  }

  void
  WomersleyBC::parse_parameters(ParamHandler &params)
  {
    flowrate_over_time = std::make_shared<CSVFunction>(prm_subsection_path,
                                                       "value",
                                                       "[m3/s]",
                                                       fluid_dynamics);
    flowrate_over_time->parse_parameters(params);
  }


  std::complex<double>
  WomersleyBC::WomersleyMode::value(const Point<dim> &p,
                                    const unsigned int /*component*/) const
  {
    using namespace std::complex_literals;

    const double r       = std::sqrt(p.distance_square(barycenter));
    const double omega_n = 2 * M_PI * index / fluid_dynamics.get_period();

    const double wo_r =
      std::sqrt(r * r * omega_n * fluid_dynamics.get_density() /
                fluid_dynamics.get_viscosity());

    const std::complex<double> j_0r =
      utils::bessel_j(0,
                      0.5 * (std::complex<double>(-M_SQRT2, M_SQRT2)) * wo_r);

    return sigma_n / (1.0i * omega_n) * (1 - j_0r / j_0R);
  }


  ResistanceBC::ResistanceBC(
    const std::string &          subsection,
    const std::set<std::string> &valid_pressure_functions_,
    const std::string &          default_pressure_function_,
    const std::set<std::string> &valid_flow_functions_,
    const std::string &          default_flow_function_,
    const FluidDynamics &        fluid_dynamics_)
    : CoreModel(subsection)
    , FunctionNeumann(dim + 1)
    , valid_pressure_functions(valid_pressure_functions_)
    , default_pressure_function(default_pressure_function_)
    , valid_flow_functions(valid_flow_functions_)
    , default_flow_function(default_flow_function_)
    , fluid_dynamics(fluid_dynamics_)
  {
    Assert(valid_pressure_functions.find(FlowIntegralEvaluation::label) ==
             valid_pressure_functions.end(),
           ExcMessage("Flow integral evaluation is not a valid function for "
                      "the pressure of a resistance boundary condition."));
  }

  void
  ResistanceBC::vector_value(const Point<dim> & /*p*/,
                             Vector<double> &values) const
  {
    double pressure = get_pressure();

    // Velocity components.
    for (unsigned int i = 0; i < dim; ++i)
      values[i] = -1.0 * pressure * this->get_normal_vector()[i];

    // Pressure component.
    values[dim] = 0.0;
  }

  void
  ResistanceBC::declare_parameters(ParamHandler &params) const
  {
    params.set_verbosity(VerbosityParam::Minimal);
    params.enter_subsection_path(prm_subsection_path);
    {
      params.declare_entry("Resistance constant",
                           "0.0",
                           Patterns::Double(0),
                           "The resistance constant [kg*s^{-1}*m^{-4}].");

      params.enter_subsection("Base pressure");
      {
        params.declare_entry_selection(
          "Time evolution",
          default_pressure_function,
          TimeFunction::TimeFunctionFactory::get_registered_keys_prm(
            valid_pressure_functions),
          "The type of evolution in time the pressure will have.");
      }
      params.leave_subsection();

      params.enter_subsection("Flow");
      {
        params.declare_entry_selection(
          "Time evolution",
          default_flow_function,
          TimeFunction::TimeFunctionFactory::get_registered_keys_prm(
            valid_flow_functions),
          "The type of evolution in time the flow will have.");
      }
      params.leave_subsection();
    }
    params.leave_subsection_path();
    params.reset_verbosity();

    TimeFunction::TimeFunctionFactory::declare_children_parameters(
      params,
      valid_pressure_functions,
      prm_subsection_path + " / Base pressure",
      "pressure",
      "[Pa]",
      fluid_dynamics);
    TimeFunction::TimeFunctionFactory::declare_children_parameters(
      params,
      valid_flow_functions,
      prm_subsection_path + " / Flow",
      "flow",
      "[m3/s]",
      fluid_dynamics);
  }

  void
  ResistanceBC::parse_parameters(ParamHandler &params)
  {
    std::string pressure_time_evolution = "";
    std::string flow_time_evolution     = "";

    params.enter_subsection_path(prm_subsection_path);
    {
      resistance_constant = params.get_double("Resistance constant");

      params.enter_subsection("Base pressure");
      {
        pressure_time_evolution = params.get("Time evolution");
      }
      params.leave_subsection();

      params.enter_subsection("Flow");
      {
        flow_time_evolution = params.get("Time evolution");
      }
      params.leave_subsection();
    }
    params.leave_subsection_path();

    pressure_over_time =
      TimeFunction::TimeFunctionFactory::parse_child_parameters(
        params,
        pressure_time_evolution,
        prm_subsection_path + " / Base pressure",
        "pressure",
        "[Pa]",
        fluid_dynamics);

    flow_over_time = TimeFunction::TimeFunctionFactory::parse_child_parameters(
      params,
      flow_time_evolution,
      prm_subsection_path + " / Flow",
      "flow",
      "[m3/s]",
      fluid_dynamics);
  }


} // namespace lifex::FluidDynamicsBCs
