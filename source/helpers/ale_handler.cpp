/********************************************************************************
  Copyright (C) 2019 - 2023 by the lifex authors.

  This file is part of lifex.

  lifex is free software; you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  lifex is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with lifex.  If not, see <http://www.gnu.org/licenses/>.
********************************************************************************/

/**
 * @file
 *
 * @author Ivan Fumagalli <ivan.fumagalli@polimi.it>.
 * @author Michele Bucelli <michele.bucelli@polimi.it>.
 * @author Alberto Zingaro <alberto.zingaro@polimi.it>.
 */

#include "core/source/geometry/move_mesh.hpp"

#include "core/source/io/serialization.hpp"

#include "source/helpers/ale_handler.hpp"
#include "source/helpers/lifting/lifting_harmonic.hpp"

#include <vtkXMLPolyDataWriter.h>

#include <fstream>
#include <map>

namespace lifex
{
  ALEHandler::ALEHandler(const std::string &subsection, const bool &standalone_)
    : CoreModel(subsection)
    , standalone(standalone_)
    , tmp_u_bc_initialized(false)
  {}

  void
  ALEHandler::declare_parameters(ParamHandler &params) const
  {
    // Declare parameters.
    params.enter_subsection_path(prm_subsection_path);
    params.set_verbosity(VerbosityParam::Minimal);
    {
      params.declare_entry_selection("Displacement mode",
                                     "Total",
                                     "Total|Incremental");

      params.enter_subsection("Input file");
      {
        params.declare_entry("Boundary displacements filename",
                             "",
                             Patterns::FileName(
                               Patterns::FileName::FileType::input),
                             "Polygonal surface filename with the definition "
                             "of the displacements on the boundary.\n"
                             "If empty, displacement must be implemented as "
                             "a Function in the code.");

        params.declare_entry(
          "Boundary displacement field basename",
          "",
          Patterns::Anything(),
          "Basename of the displacement fields on the boundary.\n"
          "If Boundary displacements filename is empty, this param is "
          "neglected.");

        params.declare_entry(
          "Geometry scaling factor",
          "1",
          Patterns::Double(0),
          "The geometry of the imported file is scaled by this factor.");

        params.declare_entry("Displacement scaling factor",
                             "1",
                             Patterns::Double(0),
                             "Imported displacement is scaled by this factor.");

        params.declare_entry(
          "Number of figures for the index",
          "0",
          Patterns::Integer(0),
          "Number of figures to represent the time index used in the field "
          "name (e.g. 2 in the case displacement00, displacement01, ...).");

        params.declare_entry(
          "Initial index",
          "0",
          Patterns::Integer(0),
          "First index of the input time-dependent boundary displacement. "
          "(e.g. 00 for \"displacement00\".");

        params.declare_entry("Time subintervals",
                             "0",
                             Patterns::Integer(0),
                             "Number of time subintervals of the input "
                             "time-dependent boundary displacement.");

        params.declare_entry(
          "Time indices stride",
          "1",
          Patterns::Integer(1),
          "Stride between two consecutive time indices of a subinterval (e.g. "
          "3 for displacement00, displacement03, displacement06, ...).");

        params.declare_entry(
          "Time subinterval duration",
          "0",
          Patterns::Double(0),
          "[s]. Time interval between two consecutive time instances of the "
          "input time-dependent displacement, including stride "
          "(if, e.g., stride=2, this is the total time between "
          "displacement00 and displacement02).");

        params.declare_entry(
          "Use time interpolation",
          "false",
          Patterns::Bool(),
          "Toggle time interpolation of all imported displacements (all "
          "imported at the beginning of the simulation). If false, a piecewise "
          "linear interpolation is used.");

        params.declare_entry_selection("Interpolation mode",
                                       "Splines",
                                       "Splines | Fourier | Derivative linear "
                                       "interpolation | Derivative spline "
                                       "interpolation | Smoothing splines");

        params.declare_entry(
          "Zero spline ends derivative",
          "true",
          Patterns::Bool(),
          "Only for splines mode. If true, impose zero derivative at both time "
          "ends; if false, derivative is computed from data.");

        params.declare_entry(
          "Smoothing splines regularization weight",
          "1.0",
          Patterns::Double(0),
          "For smoothing splines interpolation, weight of "
          "the regularization on second derivative. A value of 0 results in "
          "the interpolating cubic spline, while as the value tends to "
          "infinity this tends to a regression line.");

        params.declare_entry(
          "Periodic interpolation",
          "true",
          Patterns::Bool(),
          "If true, the time interpolation is periodic and the "
          "period can be set in the time solver. If false, the time "
          "interpolation is performed in the whole time domain, even if a "
          "periodic simulation is run (for instance in terms of BCs).");
      }
      params.leave_subsection();

      params.enter_subsection("Lifting");
      {
        params.declare_entry_selection(
          "Lifting operator",
          LiftingHarmonic::label,
          Lifting::LiftingFactory::get_registered_keys_prm(),
          "Differential model for the lifting.");
      }
      params.leave_subsection();

      params.enter_subsection("Output");
      {
        params.declare_entry("Enable output",
                             "true",
                             Patterns::Bool(),
                             "Enable/disable output.");

        if (standalone)
          {
            params.declare_entry("Filename",
                                 "lifting",
                                 Patterns::FileName(
                                   Patterns::FileName::FileType::output),
                                 "Output file.");

            params.declare_entry("Save every n timesteps",
                                 "1",
                                 Patterns::Integer(1),
                                 "Save every n timesteps.");

            params.declare_entry(
              "Export mesh at every output",
              "false",
              Patterns::Bool(),
              "Toggle mesh exporting in any ALE output file "
              "(if false, only the initial mesh is exported).");
          }

        params.declare_entry("Total displacement name",
                             "",
                             Patterns::Anything(),
                             "Name of the exported total displacement variable "
                             "(not exported if empty).");

        params.declare_entry("Incremental displacement name",
                             "",
                             Patterns::Anything(),
                             "Name of the exported incremental displacement "
                             "variable (not exported if empty).");

        params.declare_entry("Velocity",
                             "",
                             Patterns::Anything(),
                             "Name of the exported ALE velocity variable (not "
                             "exported if empty).");

        params.declare_entry("Enable surface output",
                             "false",
                             Patterns::Bool(),
                             "Enable/disable output of input vtp files, "
                             "if provided.");
      }
      params.leave_subsection();
    }
    params.reset_verbosity();
    params.leave_subsection_path();

    Lifting::LiftingFactory::declare_children_parameters(params,
                                                         prm_subsection_path +
                                                           " / Lifting");
  }

  void
  ALEHandler::parse_parameters(ParamHandler &params)
  {
    // Parse input file.
    params.parse();

    params.enter_subsection_path(prm_subsection_path);
    {
      prm_disp_mode = params.get("Displacement mode");
      params.enter_subsection("Input file");
      {
        prm_boundary_disp_filename =
          params.get("Boundary displacements filename");
        prm_boundary_disp_field_basename =
          params.get("Boundary displacement field basename");
        prm_geometry_scaling_factor =
          params.get_double("Geometry scaling factor");
        prm_disp_scaling_factor =
          params.get_double("Displacement scaling factor");
        prm_boundary_disp_num_figures_idx =
          params.get_integer("Number of figures for the index");
        prm_boundary_disp_initial_idx = params.get_integer("Initial index");
        prm_boundary_disp_num_subintervals =
          params.get_integer("Time subintervals");
        prm_boundary_disp_subintervals_stride =
          params.get_integer("Time indices stride");
        prm_boundary_disp_subinterval_duration =
          params.get_double("Time subinterval duration");
        prm_use_time_interpolation = params.get_bool("Use time interpolation");

        if (prm_use_time_interpolation)
          {
            AssertThrow(prm_disp_mode == "Total", ExcLifexInternalError());

            const std::string &mode = params.get("Interpolation mode");
            if (mode == "Splines")
              {
                prm_time_interpolation_mode =
                  utils::TimeInterpolation::Mode::CubicSpline;
              }
            else if (mode == "Fourier")
              {
                prm_time_interpolation_mode =
                  utils::TimeInterpolation::Mode::FourierSeries;
              }
            else if (mode == "Derivative linear interpolation")
              {
                prm_time_interpolation_mode =
                  utils::TimeInterpolation::Mode::DerivativeLinearInterpolation;
              }
            else if (mode == "Smoothing splines")
              {
                prm_time_interpolation_mode =
                  utils::TimeInterpolation::Mode::SmoothingCubicSpline;
              }
            else // if (mode == "Derivative spline interpolation")
              {
                prm_time_interpolation_mode =
                  utils::TimeInterpolation::Mode::DerivativeSplineInterpolation;
              }
          }

        prm_time_spline_der_zero =
          params.get_bool("Zero spline ends derivative");
        prm_time_smoothing_spline_lambda =
          params.get_double("Smoothing splines regularization weight");
        prm_time_interpolation_periodic =
          params.get_bool("Periodic interpolation");
      }
      params.leave_subsection();

      params.enter_subsection("Lifting");
      {
        prm_lifting_op = params.get("Lifting operator");
      }
      params.leave_subsection();

      params.enter_subsection("Output");
      {
        prm_enable_output = params.get_bool("Enable output");

        if (standalone)
          {
            prm_output_every_n_timesteps =
              params.get_integer("Save every n timesteps");
            prm_output_basename = params.get("Filename");
            prm_export_mesh_always =
              params.get_bool("Export mesh at every output");
          }

        prm_total_disp_name       = params.get("Total displacement name");
        prm_incr_disp_name        = params.get("Incremental displacement name");
        prm_velocity              = params.get("Velocity");
        prm_enable_surface_output = params.get_bool("Enable surface output");
      }
      params.leave_subsection();
    }
    params.leave_subsection_path();

    lifting = Lifting::LiftingFactory::parse_child_parameters(
      params, prm_lifting_op, prm_subsection_path + " / Lifting");
  }

  void
  ALEHandler::setup_from_file(
    const std::shared_ptr<utils::MeshHandler> &triangulation_,
    const FiniteElement<dim> &                 u_fe_scalar,
    const double &                             time_init_,
    const bool &                               compute_initial_displacement,
    const bool &                               apply_initial_displacement,
    const std::optional<std::string> &         filename_deserialize)
  {
    TimerOutput::Scope timer_section(timer_output,
                                     prm_subsection_path +
                                       " / Setup (displacement from file)");

    time_init = time_init_;

    AssertThrow(
      (compute_initial_displacement && apply_initial_displacement) ||
        !filename_deserialize.has_value(),
      ExcMessage(
        "If restart is active, the initial displacement must be computed."));

    if (prm_boundary_disp_num_subintervals == 0)
      {
        str_current_subinterval_with_stride = []() { return ""; };
        str_previous_subinterval_with_stride =
          str_current_subinterval_with_stride;
      }
    else
      {
        current_subinterval                 = prm_boundary_disp_initial_idx;
        str_current_subinterval_with_stride = [this]() {
          std::ostringstream ss;
          ss << std::setw(prm_boundary_disp_num_figures_idx)
             << std::setfill('0')
             << prm_boundary_disp_subintervals_stride * current_subinterval;
          return ss.str();
        };
        previous_subinterval                 = current_subinterval;
        str_previous_subinterval_with_stride = [this]() {
          std::ostringstream ss;
          ss << std::setw(prm_boundary_disp_num_figures_idx)
             << std::setfill('0')
             << prm_boundary_disp_subintervals_stride * previous_subinterval;
          return ss.str();
        };
      }

    if (prm_use_time_interpolation)
      {
        // NB: implemented only for prm_disp_mode == "Total", as asserted in
        // parse_parameters.

        std::shared_ptr<utils::VTKFunction> imported_displacement_function =
          std::make_shared<utils::VTKFunction>(prm_boundary_disp_filename,
                                               utils::VTKDataType::PolyData,
                                               prm_disp_scaling_factor,
                                               prm_geometry_scaling_factor,
                                               true);

        IndexSet owned_dofs;

        // Lift all time instances of the boundary data and setup time
        // interpolation. Interpolation is performed on the whole time interval,
        // starting from 0, independently of the value of time_init_, and also
        // in the restart case. See documentation of setup_from_file().
        std::vector<double> import_times(prm_boundary_disp_num_subintervals +
                                         1);
        imported_displacements_owned.resize(prm_boundary_disp_num_subintervals +
                                            1);

        time_interpolation =
          std::make_unique<utils::TimeInterpolationFEM<LinAlg::MPI::Vector>>();

        // Use current_subinterval as counter to
        // exploit str_current_subinterval_with_stride().
        for (current_subinterval = prm_boundary_disp_initial_idx;
             current_subinterval <= prm_boundary_disp_num_subintervals;
             ++current_subinterval)
          {
            imported_displacement_function->setup_as_linear_projection(
              prm_boundary_disp_field_basename +
              str_current_subinterval_with_stride());

            // Call setup from function only for the first time interval.
            if (current_subinterval == prm_boundary_disp_initial_idx)
              {
                lifting->set_dirichlet_function(imported_displacement_function);

                setup_from_function(
                  triangulation_,
                  u_fe_scalar,
                  time_init_,
                  imported_displacement_function,
                  compute_initial_displacement,
                  false, // Initial displacement temporarily not applied.
                  ComponentMask(), // All components.
                  {});             // Restart temporarily neglected.

                owned_dofs = lifting->get_dof_handler().locally_owned_dofs();

                pcout << "Setting up time interpolation for ALE displacement"
                      << std::endl;
              }

            // NB: A uniform time interpolation grid is considered, starting
            // from 0. Interpolation is performed on the whole time interval,
            // starting from 0, independently of the value of time_init_.
            import_times[current_subinterval] =
              0.0 +
              current_subinterval * prm_boundary_disp_subinterval_duration;

            pcout << "    Solving lifting problem for "
                  << prm_boundary_disp_field_basename
                  << str_current_subinterval_with_stride()
                  << " (time instance at t="
                  << std::to_string(import_times[current_subinterval]) << ")"
                  << std::endl;

            lifting->assemble(mapping_reference);
            lifting->solve(mapping_reference);
            imported_displacements_owned[current_subinterval] =
              *lifting->get_solution_owned();
          }

        pcout << utils::log::separator_section << std::endl;

        switch (prm_time_interpolation_mode)
          {
            // Interpolation is performed on the whole time interval, starting
            // from 0, independently of the value of time_init_.
            case utils::TimeInterpolation::Mode::LinearInterpolation:
              // This case never actually occurs: linear interpolation is
              // performed only if prm_use_time_interpolation == false.
              AssertThrow(false, ExcLifexNotImplemented());
              break;
            case utils::TimeInterpolation::Mode::CubicSpline:
              if (prm_time_spline_der_zero)
                {
                  time_interpolation->setup_as_cubic_spline(
                    imported_displacements_owned,
                    owned_dofs,
                    0.0,
                    0.0 + prm_boundary_disp_num_subintervals *
                            prm_boundary_disp_subinterval_duration,
                    0,
                    0);
                }
              else
                {
                  time_interpolation->setup_as_cubic_spline(
                    imported_displacements_owned,
                    owned_dofs,
                    0.0,
                    0.0 + prm_boundary_disp_num_subintervals *
                            prm_boundary_disp_subinterval_duration);
                }
              break;
            case utils::TimeInterpolation::Mode::SmoothingCubicSpline:
              time_interpolation->setup_as_smoothing_spline(
                imported_displacements_owned,
                owned_dofs,
                0.0,
                0.0 + prm_boundary_disp_num_subintervals *
                        prm_boundary_disp_subinterval_duration,
                prm_time_smoothing_spline_lambda);
              break;
            case utils::TimeInterpolation::Mode::FourierSeries:
              // Force final=initial, to guarantee periodicity.
              imported_displacements_owned.back() =
                imported_displacements_owned.front();
              time_interpolation->setup_as_fourier(import_times,
                                                   imported_displacements_owned,
                                                   owned_dofs);
              break;
            case utils::TimeInterpolation::Mode::DerivativeLinearInterpolation:
              time_interpolation->setup_as_derivative_linear_interpolation(
                import_times, imported_displacements_owned, owned_dofs);
              break;
            case utils::TimeInterpolation::Mode::DerivativeSplineInterpolation:
              time_interpolation->setup_as_derivative_spline_interpolation(
                imported_displacements_owned,
                owned_dofs,
                0.0,
                0.0 + prm_boundary_disp_num_subintervals *
                        prm_boundary_disp_subinterval_duration);
              break;
            case utils::TimeInterpolation::Mode::NotInitialized:
              // Mode not initialized yet.
              AssertThrow(false,
                          ExcMessage(
                            "Error: time interpolation mode not initialized."));
              break;
            default:
              AssertThrow(false, ExcLifexInternalError());
          }

        // Compute the actual initial displacement in the restart case
        // and possibly apply it (move mesh).
        if (compute_initial_displacement && filename_deserialize.has_value())
          {
            this->compute_initial_displacement(apply_initial_displacement,
                                               filename_deserialize);
          }
        // For backward compatibility.
        else if (apply_initial_displacement)
          {
            move_mesh(prm_boundary_disp_subinterval_duration,
                      *initial_displacement_owned); // Ensure coeff == 1.
          }

        // Restore value of current_subinterval.
        current_subinterval = prm_boundary_disp_initial_idx;
      }
    else // If piecewise linear interpolation.
      {
        current_subinterval = static_cast<unsigned int>(
          std::floor(time_init / prm_boundary_disp_subinterval_duration));

        displacement_function_new =
          std::make_unique<utils::VTKFunction>(prm_boundary_disp_filename,
                                               utils::VTKDataType::PolyData,
                                               prm_disp_scaling_factor,
                                               prm_geometry_scaling_factor,
                                               true);

        pcout << "Setting up ALE import functor with "
              << prm_boundary_disp_field_basename
              << str_current_subinterval_with_stride() << std::endl;
        displacement_function_new->setup_as_linear_projection(
          prm_boundary_disp_field_basename +
          str_current_subinterval_with_stride());

        if (prm_boundary_disp_num_subintervals > 0)
          {
            // At setup, displacement_function_new == displacement_function_old.
            displacement_function_old =
              std::make_unique<utils::VTKFunction>(prm_boundary_disp_filename,
                                                   utils::VTKDataType::PolyData,
                                                   prm_disp_scaling_factor,
                                                   prm_geometry_scaling_factor,
                                                   true);
            displacement_function_old->setup_as_linear_projection(
              prm_boundary_disp_field_basename +
              str_current_subinterval_with_stride());
          }

        setup_from_function(triangulation_,
                            u_fe_scalar,
                            time_init,
                            displacement_function_new,
                            compute_initial_displacement,
                            apply_initial_displacement,
                            ComponentMask(), // All components.
                            filename_deserialize);

        // Dummy values. Ensure that Vector structure complies with that of
        // displacement.
        displacement_from_file_new_owned =
          std::make_shared<LinAlg::MPI::Vector>(*displacement_owned);

        if (prm_boundary_disp_num_subintervals > 0)
          {
            displacement_from_file_old_owned =
              std::make_shared<LinAlg::MPI::Vector>(
                *displacement_from_file_new_owned);
          }
      }
  }

  void
  ALEHandler::setup_from_function(
    const std::shared_ptr<utils::MeshHandler> &triangulation_,
    const FiniteElement<dim> &                 u_fe_scalar,
    const double &                             time_init_,
    const std::shared_ptr<Function<dim>> &     w_bd,
    const bool &                               compute_initial_displacement,
    const bool &                               apply_initial_displacement,
    const ComponentMask &                      component_mask_,
    const std::optional<std::string> &         filename_deserialize)
  {
    // Store boundary function.
    boundary_function = w_bd;
    component_mask    = component_mask_;
    time_init         = time_init_;

    AssertThrow(
      (compute_initial_displacement && apply_initial_displacement) ||
        !filename_deserialize.has_value(),
      ExcMessage(
        "If restart is active, the initial displacement must be computed."));

    setup_displacement_vectors(triangulation_,
                               u_fe_scalar,
                               compute_initial_displacement,
                               apply_initial_displacement,
                               filename_deserialize);
  }

  void
  ALEHandler::setup_displacement_vectors(
    const std::shared_ptr<utils::MeshHandler> &triangulation_,
    const FiniteElement<dim> &                 u_fe_scalar,
    const bool &                               compute_initial_displacement,
    const bool &                               apply_initial_displacement,
    const std::optional<std::string> &         filename_deserialize)
  {
    triangulation = triangulation_;

    AssertThrow(
      (compute_initial_displacement && apply_initial_displacement) ||
        !filename_deserialize.has_value(),
      ExcMessage(
        "If restart is active, the initial displacement must be computed."));

    lifting->setup_system(*triangulation);

    // Setup the position vector and the current-to-reference mapping.
    position_reference_owned.reinit(*lifting->get_solution_owned());
    position_reference.reinit(*lifting->get_solution());
    VectorTools::get_position_vector(lifting->get_dof_handler(),
                                     position_reference_owned);
    position_reference_owned.compress(VectorOperation::insert);
    position_reference = position_reference_owned;

    mapping_reference =
      std::make_shared<MappingFEField<dim, dim, LinAlg::MPI::Vector>>(
        lifting->get_dof_handler(), position_reference);

    displacement_is_total = (prm_disp_mode == "Total");

    // Initialize structure of ghosted vectors from Lifting;
    // actual values will be set at the end of the present function.
    displacement = displacement_increment = velocity = *lifting->get_solution();

    if (compute_initial_displacement)
      {
        this->compute_initial_displacement(apply_initial_displacement,
                                           filename_deserialize);
      }
    else
      {
        // Set all ALE variables to zero.
        displacement_owned =
          std::make_shared<LinAlg::MPI::Vector>(*lifting->get_solution_owned());
        *displacement_owned = 0;
        displacement_increment_owned =
          std::make_shared<LinAlg::MPI::Vector>(*displacement_owned);
        velocity_owned =
          std::make_shared<LinAlg::MPI::Vector>(*displacement_increment_owned);

        // Ghosted versions initialized to comply with the non-ghosted ones.
        displacement           = *displacement_owned;
        displacement_increment = *displacement_increment_owned;
        velocity               = *velocity_owned;
      }

    // Initialization of additional utilities for apply_dirichlet().

    // Construct u_fe as dim copies of u_fe_scalar.
    // This is where we make the assumption
    // mentioned in the function documentation.
    FESystem<dim> u_fe(u_fe_scalar, dim);

    // Create dof_handler corresponding to u_fe: in this way,
    // u_fe can be a sub_fe, and u/u_owned blocks of a BlockVector.
    u_dof_handler.reinit(triangulation->get());
    u_dof_handler.distribute_dofs(u_fe);

    // Compute the this_fe-to-u_fe interpolation matrix.
    interpolation_matrix =
      std::make_shared<FullMatrix<double>>(u_fe.dofs_per_cell,
                                           lifting->get_fe().dofs_per_cell);

    FETools::get_interpolation_matrix(lifting->get_fe(),
                                      u_fe,
                                      *interpolation_matrix);

    IndexSet u_owned_dofs = u_dof_handler.locally_owned_dofs();
    IndexSet u_locally_relevant_dofs;
    DoFTools::extract_locally_relevant_dofs(u_dof_handler,
                                            u_locally_relevant_dofs);
  }

  void
  ALEHandler::compute_initial_displacement(
    const bool &                      apply_initial_displacement,
    const std::optional<std::string> &filename_deserialize)
  {
    AssertThrow(
      (apply_initial_displacement || !filename_deserialize.has_value()),
      ExcMessage(
        "If restart is active, the initial displacement must be applied."));

    bool tmp = displacement_is_total;

    if (!filename_deserialize
           .has_value()) // no restart from previous simulation
      {
        pcout << "ALE: compute initial displacement" << endl;

        // Set displacement_is_total to true, so that if solving an FSI problem
        // the initial displacement is applied correctly.
        displacement_is_total = true;
        if (boundary_function)
          lifting->set_dirichlet_function(boundary_function);

        lifting->assemble(mapping_reference);
        lifting->solve(mapping_reference);

        initial_displacement_owned =
          std::make_unique<LinAlg::MPI::Vector>(*lifting->get_solution_owned());

        // Initial velocity = 0 in any non-restart case.
        velocity_owned =
          std::make_shared<LinAlg::MPI::Vector>(*initial_displacement_owned);
        *velocity_owned = 0;
      }
    else // restart from previous simulation
      {
        // If always using input mesh for the lifting problem, we assemble here,
        // before moving the mesh below. This means that even after restart the
        // lifting problem is always solved on the mesh read from file
        // (like in the non-restart case), to prevent side effects of
        // restart (such as loading the already moved mesh).
        if (lifting->always_use_input_mesh())
          lifting->assemble(mapping_reference);

        restart(filename_deserialize.value());

        // Initial displacement is always total: we copy initial displacement
        // from deserialized total displacement.
        initial_displacement_owned =
          std::make_unique<LinAlg::MPI::Vector>(*displacement_owned);
      }

    // Temporarily, incr displ = total displ,
    // for possible initial mesh motion purpose:
    // if needed, incr displ will be properly initialized after move_mesh().
    LinAlg::MPI::Vector displacement_increment_backup;
    if (filename_deserialize.has_value())
      displacement_increment_backup = *displacement_increment_owned;

    displacement_owned =
      std::make_shared<LinAlg::MPI::Vector>(*initial_displacement_owned);
    displacement_increment_owned =
      std::make_shared<LinAlg::MPI::Vector>(*initial_displacement_owned);

    if (apply_initial_displacement)
      {
        // Move the initial mesh by first displ.
        displacement_is_total = false; // Ensure application of first displ.
        move_mesh(prm_boundary_disp_subinterval_duration); // Ensure coeff == 1.
      }

    displacement_is_total = tmp; // Recover actual value.

    // Correct initialization: incr displ = 0 (if needed) or read from file.
    if (!filename_deserialize.has_value()) // no restart
      {
        if (displacement_is_total)
          {
            *displacement_increment_owned = 0;
          }
      }
    else
      {
        *displacement_increment_owned = displacement_increment_backup;
      }

    // Ghosted versions initialized to comply with the non-ghosted ones.
    displacement           = *displacement_owned;
    displacement_increment = *displacement_increment_owned;
    velocity               = *velocity_owned;
  }

  void
  ALEHandler::restart(const std::string &filename_deserialize)
  {
    std::vector<LinAlg::MPI::Vector> input_vectors;

    pcout << "Restarting ALE displacement and velocity from "
          << filename_deserialize << std::endl;

    // ALE inputs: velocity, total displacement, incremental displacement.
    input_vectors.resize(3);
    std::vector<LinAlg::MPI::Vector *> input_vectors_ptr(input_vectors.size());
    for (size_t i = 0; i < input_vectors.size(); ++i)
      {
        // Get structure from lifting, which is already set up.
        input_vectors[i].reinit(*lifting->get_solution_owned());
        input_vectors_ptr[i] = &(input_vectors[i]);
      }
    utils::deserialize(filename_deserialize,
                       input_vectors_ptr,
                       *triangulation,
                       lifting->get_dof_handler());

    velocity_owned = std::make_shared<LinAlg::MPI::Vector>(input_vectors[0]);
    displacement_owned =
      std::make_shared<LinAlg::MPI::Vector>(input_vectors[1]);
    displacement_increment_owned =
      std::make_shared<LinAlg::MPI::Vector>(input_vectors[2]);

    // Initialize the solution of the lifting problem.
    if (prm_disp_mode == "Total")
      *lifting->get_solution_owned() = *displacement_owned;
    else
      *lifting->get_solution_owned() = *displacement_increment_owned;

    // Ghosted versions initialized to comply with the non-ghosted ones.
    displacement           = *displacement_owned;
    displacement_increment = *displacement_increment_owned;
    velocity               = *velocity_owned;
  }

  void
  ALEHandler::setup_interface(
    const DoFHandler<dim> &other_dof_handler,
    const std::vector<
      std::shared_ptr<Lifting::FSIInterfaceHandler::InterfaceDataDirichlet>>
      &interface_data_dirichlet)
  {
    AssertThrow(lifting != nullptr, ExcNotInitialized());

    lifting->setup_interface(other_dof_handler, interface_data_dirichlet);
  }

  void
  ALEHandler::update(const double &time,
                     const double &time_step,
                     const double &period)
  {
    TimerOutput::Scope timer_section(timer_output,
                                     prm_subsection_path + " / Update");
    pcout << "        Updating and lifting" << std::endl;

    if (displacement_is_total)
      {
        // Store previous displacement.
        *displacement_increment_owned = *displacement_owned;
        *displacement_increment_owned *= -1;
      }

    // Subinterval number s: time goes
    // from s * prm_boundary_disp_subinterval_duration (excluded)
    // to (s + 1) * prm_boundary_disp_subinterval_duration (included).

    // Temporary variable to check whether the counter current_subinterval
    // and subinterval-dependent variables like displacement_function_old, ...
    // need updating.
    unsigned int actual_current_subinterval = static_cast<unsigned int>(
      std::ceil(time / prm_boundary_disp_subinterval_duration));

    if (!prm_use_time_interpolation)
      AssertThrow(actual_current_subinterval <=
                    prm_boundary_disp_num_subintervals,
                  ExcIndexRange(actual_current_subinterval,
                                0,
                                prm_boundary_disp_num_subintervals));

    if (prm_use_time_interpolation)
      {
        if (prm_time_interpolation_periodic)
          {
            const double time_relative = std::fmod(time, period);

            time_interpolation->evaluate(
              time_relative,
              lifting->get_dof_handler().locally_owned_dofs(),
              *displacement_owned);
          }
        else
          { // time interpolation is performed on the whole simulation
            // duration. This might be useful to guarantee more regularity
            // among two consecutive heartcycles.

            time_interpolation->evaluate(
              time,
              lifting->get_dof_handler().locally_owned_dofs(),
              *displacement_owned);
          }

        if (current_subinterval != actual_current_subinterval)
          {
            // Simply update subinterval index.
            previous_subinterval = current_subinterval;
            current_subinterval  = actual_current_subinterval;
          }
      }
    else // If piecewise linear interpolation.
      {
        // 1.+2. Update boundary displacement + solve lifting problem.
        if (displacement_function_new)
          {
            if (current_subinterval != actual_current_subinterval &&
                time <= prm_boundary_disp_num_subintervals *
                          prm_boundary_disp_subinterval_duration)
              {
                // Update old boundary function and old lifted displacement.
                displacement_function_old->setup_as_linear_projection(
                  prm_boundary_disp_field_basename +
                  str_current_subinterval_with_stride());
                *displacement_from_file_old_owned =
                  *displacement_from_file_new_owned;

                // Update subinterval index.
                previous_subinterval = current_subinterval;
                current_subinterval  = actual_current_subinterval;

                // Update boundary function.
                displacement_function_new->setup_as_linear_projection(
                  prm_boundary_disp_field_basename +
                  str_current_subinterval_with_stride());

                pcout << "\t\tpre ALE fields updated to "
                      << prm_boundary_disp_field_basename +
                           str_current_subinterval_with_stride()
                      << std::endl;

                // Assemble and solve the lifting problem.
                lifting->set_dirichlet_function(displacement_function_new);

                lifting->assemble(mapping_reference);
                lifting->solve(mapping_reference);

                pcout << "\t\tALE fields updated to "
                      << prm_boundary_disp_field_basename +
                           str_current_subinterval_with_stride()
                      << std::endl;
              }
          }
        else
          {
            // Update boundary function and solve lifting problem.
            if (boundary_function)
              {
                boundary_function->set_time(time);
                lifting->set_dirichlet_function(boundary_function);
              }

            lifting->assemble(mapping_reference);
            lifting->solve(mapping_reference);
          }

        // Update all displacement vectors.
        if (displacement_function_new)
          {
            displacement_from_file_new_owned = lifting->get_solution_owned();
            if (displacement_is_total)
              {
                // Piecewise linear interpolation.
                interpolation_coeff =
                  time / prm_boundary_disp_subinterval_duration -
                  (current_subinterval - 1);

                // D^{n+1} = D_old + alpha * (D_new - D_old).
                *displacement_owned = *displacement_from_file_old_owned;
                displacement_owned->add(interpolation_coeff,
                                        *displacement_from_file_new_owned,
                                        -interpolation_coeff,
                                        *displacement_from_file_old_owned);
              }
            else
              {
                // Piecewise linear interpolation.
                interpolation_coeff =
                  time_step / prm_boundary_disp_subinterval_duration;

                // d^{n+1} = alpha * D_new.
                *displacement_increment_owned =
                  *displacement_from_file_new_owned;
                *displacement_increment_owned *= interpolation_coeff;
              }
          }
        else
          {
            if (displacement_is_total)
              {
                displacement_owned = lifting->get_solution_owned();
              }
            else
              {
                displacement_increment_owned = lifting->get_solution_owned();
              }
          }
      }

    if (displacement_is_total)
      {
        // d^{n+1} = - D^n + D^{n+1}. (-D^n stored at beginning of this method.)
        displacement_increment_owned->add(+1, *displacement_owned);
      }
    else
      {
        // D^{n+1} = sum_k d^k
        displacement_owned->add(+1, *displacement_increment_owned);
      }

    // 3. Compute velocity.
    *velocity_owned = *displacement_increment_owned;
    *velocity_owned /= time_step;

    // Ghosted vectors comply with non-ghosted ones.
    displacement           = *displacement_owned;
    displacement_increment = *displacement_increment_owned;
    velocity               = *velocity_owned;
  }

  void
  ALEHandler::move_mesh(const double &             time_step,
                        const LinAlg::MPI::Vector &incremental_displacement)
  {
    pcout << "        Moving mesh vertices" << std::endl;
    utils::move_mesh(*triangulation,
                     lifting->get_dof_handler(),
                     incremental_displacement);

    if (displacement_function_new)
      {
        // Update input vtp surfaces.
        double lin_interp_coeff =
          time_step / prm_boundary_disp_subinterval_duration;
        if (displacement_is_total)
          {
            // D^{n+1} = D_old + alpha * (D_new - D_old).
            std::map<const std::string, const double> arraynames_and_scalings(
              {{prm_boundary_disp_field_basename +
                  str_current_subinterval_with_stride(),
                prm_disp_scaling_factor * lin_interp_coeff},
               {prm_boundary_disp_field_basename +
                  str_previous_subinterval_with_stride(),
                -prm_disp_scaling_factor * lin_interp_coeff}});

            displacement_function_new->warp_by_array_combination(
              arraynames_and_scalings);
            if (displacement_function_old)
              {
                displacement_function_old->warp_by_array_combination(
                  arraynames_and_scalings);
              }
          }
        else
          {
            // d^{n+1} = alpha * D_new.
            std::map<const std::string, const double> arraynames_and_scalings(
              {{prm_boundary_disp_field_basename +
                  str_current_subinterval_with_stride(),
                prm_disp_scaling_factor * lin_interp_coeff}});

            displacement_function_new->warp_by_array_combination(
              arraynames_and_scalings);
            if (displacement_function_old)
              {
                displacement_function_old->warp_by_array_combination(
                  arraynames_and_scalings);
              }
          }
      }
  }

  void
  ALEHandler::apply_dirichlet(
    LinAlg::MPI::Vector &               u_owned,
    LinAlg::MPI::Vector &               u,
    const std::set<types::boundary_id> &u_dirichlet_tags) const
  {
    interpolate_dirichlet_vectors(tmp_u_bc,
                                  tmp_u_bc_owned,
                                  !tmp_u_bc_initialized);
    tmp_u_bc_initialized = true;

    // Apply Dirichlet BC.
    IndexSet u_dirichlet_dofs;

    if (!u_dirichlet_tags.empty())
      DoFTools::extract_boundary_dofs(u_dof_handler,
                                      component_mask,
                                      u_dirichlet_dofs,
                                      u_dirichlet_tags);

    for (auto idx : u_dirichlet_dofs)
      {
        u_owned[idx] = tmp_u_bc[idx];
      }
    u_owned.compress(VectorOperation::insert);

    u = u_owned;
  }

  void
  ALEHandler::apply_dirichlet(
    AffineConstraints<double> &         constraints,
    const std::set<types::boundary_id> &u_dirichlet_tags,
    const bool &                        homogeneous) const
  {
    if (!homogeneous)
      {
        interpolate_dirichlet_vectors(tmp_u_bc,
                                      tmp_u_bc_owned,
                                      !tmp_u_bc_initialized);
        tmp_u_bc_initialized = true;
      }

    // Apply Dirichlet BC.
    IndexSet u_dirichlet_dofs;

    if (!u_dirichlet_tags.empty())
      DoFTools::extract_boundary_dofs(u_dof_handler,
                                      component_mask,
                                      u_dirichlet_dofs,
                                      u_dirichlet_tags);

    for (auto idx : u_dirichlet_dofs)
      {
        // u_owned[idx] = tmp_u_bc[idx];
        constraints.add_line(idx);
        constraints.set_inhomogeneity(idx, homogeneous ? 0.0 : tmp_u_bc[idx]);
      }
  }

  void
  ALEHandler::output_results(const double &      time,
                             const unsigned int &timestep_number)
  {
    if (prm_enable_output &&
        (timestep_number % prm_output_every_n_timesteps == 0))
      {
        TimerOutput::Scope timer_section(timer_output,
                                         prm_subsection_path +
                                           " / Output results");

        if (standalone)
          {
            DataOut<dim> data_out;
            attach_output(data_out);
            data_out.build_patches();

            const unsigned int filename_mesh_index =
              prm_export_mesh_always ? timestep_number : 0;

            utils::dataout_write_hdf5(data_out,
                                      prm_output_basename,
                                      timestep_number,
                                      filename_mesh_index,
                                      time);

            data_out.clear();
          }

        // Save moved surfaces.
        if (prm_enable_surface_output)
          {
            if (displacement_function_new)
              {
                std::string vtp_output_basename =
                  prm_output_basename + "_displ_new";
                std::string vtp_output_filename =
                  vtp_output_basename + "_" +
                  utils::timestep_to_string(timestep_number) + ".vtp";
                std::string pvd_output_filename = vtp_output_basename + ".pvd";

                // Write vtp.
                vtkSmartPointer<vtkXMLPolyDataWriter> writer =
                  vtkSmartPointer<vtkXMLPolyDataWriter>::New();
                writer->SetFileName(
                  (prm_output_directory + vtp_output_filename).c_str());
                writer->SetInputData(
                  displacement_function_new->get_vtk_surface());
                writer->Write();

                // Write pvd for Paraview visualization.
                // displacement_new_times_and_names accumulates all times.
                displacement_new_times_and_names.push_back(
                  std::make_pair(time, vtp_output_filename));
                std::ofstream pvd_output(prm_output_directory +
                                         pvd_output_filename);
                DataOutBase::write_pvd_record(pvd_output,
                                              displacement_new_times_and_names);
              }

            if (displacement_function_old)
              {
                std::string vtp_output_basename =
                  prm_output_basename + "_displ_old";
                std::string vtp_output_filename =
                  vtp_output_basename + "_" +
                  utils::timestep_to_string(timestep_number) + ".vtp";
                std::string pvd_output_filename = vtp_output_basename + ".pvd";

                // Write vtp.
                vtkSmartPointer<vtkXMLPolyDataWriter> writer =
                  vtkSmartPointer<vtkXMLPolyDataWriter>::New();
                writer->SetFileName(
                  (prm_output_directory + vtp_output_filename).c_str());
                writer->SetInputData(
                  displacement_function_old->get_vtk_surface());
                writer->Write();

                // Write pvd for Paraview visualization.
                // displacement_new_times_and_names accumulates all times.
                displacement_old_times_and_names.push_back(
                  std::make_pair(time, vtp_output_filename));
                std::ofstream pvd_output(prm_output_directory +
                                         pvd_output_filename);
                DataOutBase::write_pvd_record(pvd_output,
                                              displacement_old_times_and_names);
              }
          }
      }
  }

  void
  ALEHandler::output_serialize(const std::string & basename,
                               const unsigned int &timestep_number) const
  {
    const std::string filename =
      basename + "_ALE_" + utils::timestep_to_string(timestep_number);

    // Copy output vectors into ghosted vectors and serialize.
    std::vector<const LinAlg::MPI::Vector *> output_vectors_ghosted_ptr(
      {&velocity, &displacement, &displacement_increment});

    utils::serialize(filename,
                     output_vectors_ghosted_ptr,
                     *triangulation,
                     lifting->get_dof_handler());
  }

  void
  ALEHandler::interpolate_dirichlet_vectors(LinAlg::MPI::Vector &dest,
                                            LinAlg::MPI::Vector &dest_owned,
                                            bool                 reinit) const
  {
    if (reinit)
      {
        IndexSet locally_relevant_dofs;
        DoFTools::extract_locally_relevant_dofs(u_dof_handler,
                                                locally_relevant_dofs);
        dest.reinit(u_dof_handler.locally_owned_dofs(),
                    locally_relevant_dofs,
                    mpi_comm);
        dest_owned.reinit(u_dof_handler.locally_owned_dofs(), mpi_comm);
      }

    // Perform the fe-to-fe interpolation.
    dest = dest_owned = 0;
    VectorTools::interpolate(lifting->get_dof_handler(),
                             u_dof_handler,
                             *interpolation_matrix,
                             velocity,
                             dest_owned);
    dest = dest_owned;
  }

  void
  ALEHandler::attach_output(DataOut<dim> &data_out) const
  {
    if (prm_enable_output)
      {
        std::vector<DataComponentInterpretation::DataComponentInterpretation>
          data_component_interpretation(
            dim, // vel, displ, displ_incr
            DataComponentInterpretation::component_is_part_of_vector);

        std::string var_name(prm_total_disp_name);
        if (!var_name.empty())
          {
            std::vector<std::string> solution_names(dim, var_name);
            data_out.add_data_vector(lifting->get_dof_handler(),
                                     displacement,
                                     solution_names,
                                     data_component_interpretation);
          }

        var_name = prm_incr_disp_name;
        if (!var_name.empty())
          {
            std::vector<std::string> solution_names(dim, var_name);
            data_out.add_data_vector(lifting->get_dof_handler(),
                                     displacement_increment,
                                     solution_names,
                                     data_component_interpretation);
          }

        var_name = prm_velocity;
        if (!var_name.empty())
          {
            std::vector<std::string> solution_names(dim, var_name);
            data_out.add_data_vector(lifting->get_dof_handler(),
                                     velocity,
                                     solution_names,
                                     data_component_interpretation);
          }
      }
  }
} // namespace lifex
