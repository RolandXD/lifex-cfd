/********************************************************************************
  Copyright (C) 2019 - 2023 by the lifex authors.

  This file is part of lifex.

  lifex is free software; you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  lifex is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with lifex.  If not, see <http://www.gnu.org/licenses/>.
********************************************************************************/

/**
 * @file
 *
 * @author Ivan Fumagalli <ivan.fumagalli@polimi.it>.
 */

#ifndef LIFEX_HELPER_LIFTING_HARMONIC_HPP_
#define LIFEX_HELPER_LIFTING_HARMONIC_HPP_

#include "source/helpers/lifting/lifting.hpp"

#include <memory>
#include <string>
#include <vector>

namespace lifex
{
  /**
   * @brief Class implementing the harmonic lifting of a FE function defined on the boundary of a domain.
   *
   * The equation solved is:
   * @f[
   * \left\{
   * \begin{alignedat}{2}
   * -{\rm div}(K \nabla\mathbf w) & = \mathbf 0, & \quad & \text{in } \Omega,\\
   * \mathbf w & = \mathbf w_{bd}, & \quad & \text{on }
   * \Gamma_D\subseteq\partial\Omega,\\ K\nabla \mathbf w \cdot \mathbf n & = 0,
   * & \quad & \text{on } \Gamma_N=\partial\Omega\setminus\Gamma_D,
   * \end{alignedat} \right.
   * @f]
   * where @f$\mathbf w_{bd}@f$ is the function to lift and @f$K@f$ is a
   * stiffness tensor field, defined in compute_stiffness().
   *
   * This class particularizes the class Lifting to the case @f$L\mathbf w=-{\rm
   * div}(K\nabla\mathbf w)@f$.
   */
  class LiftingHarmonic : public Lifting
  {
  public:
    /// Label for harmonic lifting operator.
    static inline constexpr auto label = "Harmonic";

    /// Default constructor.
    LiftingHarmonic(const std::string &subsection)
      : Lifting(subsection, update_gradients | update_JxW_values)
    {}

    /// Declare parameters.
    virtual void
    declare_parameters(ParamHandler &params) const override
    {
      Lifting::declare_parameters(params);
    }

    /// Parse parameters.
    virtual void
    parse_parameters(ParamHandler &params) override
    {
      Lifting::parse_parameters(params);
    }

  protected:
    /// Assemble linear system contribution on a cell.
    virtual void
    assemble_callback_cell(const DoFHandler<dim>::active_cell_iterator &cell,
                           FullMatrix<double> &       cell_matrix,
                           Vector<double> &           cell_rhs,
                           const std::vector<double> &scaling_factors) override;

    /**
     * @brief Compute stiffness tensor for the current cell.
     *
     * @todo Consider different options. For the moment, the identity tensor is
     * implemented.
     *
     * @param[out] stiffness stiffness tensor
     * @param[in] cell current cell
     */
    inline virtual void
    compute_stiffness(Tensor<2, dim, double> &                     stiffness,
                      const DoFHandler<dim>::active_cell_iterator &cell);
  };

  LIFEX_REGISTER_CHILD_INTO_FACTORY(Lifting,
                                    LiftingHarmonic,
                                    LiftingHarmonic::label,
                                    const std::string &);
} // namespace lifex

#endif /* LIFEX_HELPER_LIFTING_HARMONIC_HPP_ */
