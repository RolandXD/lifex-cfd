/********************************************************************************
  Copyright (C) 2019 - 2023 by the lifex authors.

  This file is part of lifex.

  lifex is free software; you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  lifex is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with lifex.  If not, see <http://www.gnu.org/licenses/>.
********************************************************************************/

/**
 * @file
 *
 * @author Michele Bucelli <michele.bucelli@polimi.it>.
 */

#ifndef LIFEX_HELPER_LIFTING_ELASTIC_HPP_
#define LIFEX_HELPER_LIFTING_ELASTIC_HPP_

#include "source/helpers/lifting/lifting.hpp"

#include <string>
#include <vector>

namespace lifex
{
  /**
   * @brief Linear elastic lifting.
   *
   * The equation solved is:
   * @f[
   * \left\{\begin{alignedat}{2}
   * -\nabla\cdot\sigma(\mathbf w) = \mathbf 0, & \quad & \text{in } \Omega, \\
   * \mathbf w = \mathbf w_\mathrm{bd}, & \quad & \text{on } \Gamma_D
   * \subseteq\partial\Omega, \\ \sigma(\mathbf w)\mathbf n = \mathbf 0, & \quad
   * & \text{on } \Gamma_N=\partial\Omega\setminus\Gamma_D,
   * \end{alignedat}\right.
   * @f]
   * where
   * @f[
   * \sigma(\mathbf w) = 2\mu \nabla^S\mathbf w + \lambda(\nabla\cdot\mathbf w)I
   * @f]
   * is a linear elastic stress tensor, and @f$\mu@f$ and @f$\lambda@f$ are the
   * Lamé parameters, related to the Young modulus @f$E@f$ and Poisson modulus
   * @f$\nu@f$ through
   * @f[
   * \mu = \frac{E}{2(1 + \nu)} \qquad
   * \lambda = \frac{E\,\nu}{(1 + \nu)(1 - 2\nu)}\,.
   * @f]
   *
   * This class particularizes the class @ref Lifting to the case @f$L\mathbf w
   * = -\nabla\cdot\sigma(\mathbf w)@f$.
   */
  class LiftingElastic : public Lifting
  {
  public:
    /// Label for elastic lifting operator.
    static inline constexpr auto label = "Linear elasticity";

    /// Default constructor.
    LiftingElastic(const std::string &subsection)
      : Lifting(subsection, update_gradients | update_JxW_values)
    {}

    /// Declare parameters.
    virtual void
    declare_parameters(ParamHandler &params) const override;

    /// Parse parameters.
    virtual void
    parse_parameters(ParamHandler &params) override;

  protected:
    /// Assemble linear system contribution on a cell.
    virtual void
    assemble_callback_cell(const DoFHandler<dim>::active_cell_iterator &cell,
                           FullMatrix<double> &       cell_matrix,
                           Vector<double> &           cell_rhs,
                           const std::vector<double> &scaling_factors) override;

    /// First Lamé parameter @f$\mu@f$ (computed from Young and Poisson moduli
    /// after parameters are read).
    double mu;

    /// Second Lamé parameter @f$\lambda@f$ (computed from Young and Poisson
    /// moduli after parameters are read).
    double lambda;

    /// @name Parameters read from file.
    /// @{

    /// Young modulus [Pa].
    double prm_young_modulus;

    /// Poisson modulus [-].
    double prm_poisson_modulus;

    /// @}
  };

  LIFEX_REGISTER_CHILD_INTO_FACTORY(Lifting,
                                    LiftingElastic,
                                    LiftingElastic::label,
                                    const std::string &);
} // namespace lifex

#endif /* LIFEX_HELPER_LIFTING_ELASTIC_HPP_ */
