/********************************************************************************
  Copyright (C) 2019 - 2023 by the lifex authors.

  This file is part of lifex.

  lifex is free software; you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  lifex is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with lifex.  If not, see <http://www.gnu.org/licenses/>.
********************************************************************************/

/**
 * @file
 *
 * @author Ivan Fumagalli <ivan.fumagalli@polimi.it>.
 */

#include "source/helpers/lifting/lifting_harmonic.hpp"

#include <deal.II/fe/fe_values_extractors.h>

#include <vector>

namespace lifex
{
  void
  LiftingHarmonic::assemble_callback_cell(
    const DoFHandler<dim>::active_cell_iterator &cell,
    FullMatrix<double> &                         cell_matrix,
    Vector<double> & /*cell_rhs*/,
    const std::vector<double> &scaling_factors)
  {
    const FEValuesExtractors::Vector displacement(0);
    const unsigned int               n_q_points    = quadrature_formula->size();
    const unsigned int               dofs_per_cell = fe->dofs_per_cell;

    // Compute stiffness local tensor.
    Tensor<2, dim, double> stiffness;
    compute_stiffness(stiffness, cell);


    for (unsigned int q = 0; q < n_q_points; ++q)
      for (unsigned int i = 0; i < dofs_per_cell; ++i)
        for (unsigned int j = 0; j < dofs_per_cell; ++j)
          cell_matrix(i, j) +=
            scalar_product(stiffness *
                             (*fe_values)[displacement].gradient(i, q),
                           (*fe_values)[displacement].gradient(j, q)) *
            fe_values->JxW(q) * scaling_factors[q];
  }

  void
  LiftingHarmonic::compute_stiffness(
    Tensor<2, dim, double> &stiffness,
    const DoFHandler<dim>::active_cell_iterator & /* cell */)
  {
    for (unsigned int i = 0; i < dim; ++i)
      {
        for (unsigned int j = 0; j < dim; ++j)
          {
            stiffness[i][j] = (i == j ? 1 : 0);
          }
      }
  }

} // namespace lifex
