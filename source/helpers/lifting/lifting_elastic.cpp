/********************************************************************************
  Copyright (C) 2019 - 2023 by the lifex authors.

  This file is part of lifex.

  lifex is free software; you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  lifex is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with lifex.  If not, see <http://www.gnu.org/licenses/>.
********************************************************************************/

/**
 * @file
 *
 * @author Michele Bucelli <michele.bucelli@polimi.it>.
 */

#include "core/source/numerics/numbers.hpp"

#include "source/helpers/lifting/lifting_elastic.hpp"

#include <deal.II/fe/fe_values_extractors.h>

namespace lifex
{
  void
  LiftingElastic::declare_parameters(ParamHandler &params) const
  {
    Lifting::declare_parameters(params);

    params.set_verbosity(VerbosityParam::Full);
    params.enter_subsection_path(prm_subsection_path + " / " + label);
    {
      params.declare_entry("Young modulus",
                           "1",
                           Patterns::Double(0),
                           "Young modulus [Pa].");

      params.declare_entry("Poisson modulus",
                           "0.25",
                           Patterns::Double(0, 0.5),
                           "Poisson modulus [-].");
    }
    params.leave_subsection_path();
    params.reset_verbosity();
  }

  void
  LiftingElastic::parse_parameters(ParamHandler &params)
  {
    Lifting::parse_parameters(params);

    params.enter_subsection_path(prm_subsection_path + " / " + label);
    {
      prm_young_modulus   = params.get_double("Young modulus");
      prm_poisson_modulus = params.get_double("Poisson modulus");
    }
    params.leave_subsection_path();

    // Sanity check on parameters: throw an exception if E = 0, nu = 0 or nu =
    // 0.5. (Parameter patterns only allow intervals including extremes, but
    // extremes here are invalid values).
    AssertThrow(utils::is_positive(prm_young_modulus),
                ExcMessage(
                  "Young modulus must be strictly greater than zero."));
    AssertThrow(utils::is_definitely_less_than(prm_poisson_modulus, 0.5),
                ExcMessage("Poisson modulus must be strictly less than 0.5."));

    // Compute Lamé constants from Young and Poisson moduli. The latter are
    // easier for physical intuition, and are thus exposed as parameters, while
    // the former allow for an easier formulation and are thus used in the
    // assemble method.
    mu     = prm_young_modulus / 2.0 / (1 + prm_poisson_modulus);
    lambda = prm_young_modulus * prm_poisson_modulus /
             (1.0 + prm_poisson_modulus) / (1.0 - 2.0 * prm_poisson_modulus);
  }

  void
  LiftingElastic::assemble_callback_cell(
    const DoFHandler<dim>::active_cell_iterator & /*cell*/,
    FullMatrix<double> &cell_matrix,
    Vector<double> & /*cell_rhs*/,
    const std::vector<double> &scaling_factors)
  {
    const FEValuesExtractors::Vector displacement(0);
    const unsigned int               n_q_points    = quadrature_formula->size();
    const unsigned int               dofs_per_cell = fe->dofs_per_cell;

    for (unsigned int q = 0; q < n_q_points; ++q)
      for (unsigned int i = 0; i < dofs_per_cell; ++i)
        for (unsigned int j = 0; j < dofs_per_cell; ++j)
          cell_matrix(i, j) +=
            (2.0 * mu *
               scalar_product(
                 (*fe_values)[displacement].symmetric_gradient(i, q),
                 (*fe_values)[displacement].symmetric_gradient(j, q)) +
             lambda * (*fe_values)[displacement].divergence(i, q) *
               (*fe_values)[displacement].divergence(j, q)) *
            fe_values->JxW(q) * scaling_factors[q];
  }
} // namespace lifex
