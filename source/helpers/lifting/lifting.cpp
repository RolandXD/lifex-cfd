/********************************************************************************
  Copyright (C) 2019 - 2023 by the lifex authors.

  This file is part of lifex.

  lifex is free software; you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  lifex is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with lifex.  If not, see <http://www.gnu.org/licenses/>.
********************************************************************************/

/**
 * @file
 *
 * @author Ivan Fumagalli <ivan.fumagalli@polimi.it>.
 */

#include "core/source/numerics/numbers.hpp"

#include "source/helpers/lifting/lifting.hpp"

#include <deal.II/fe/mapping_fe_field.h>

#include <deal.II/numerics/matrix_tools.h>

#include <deal.II/physics/elasticity/kinematics.h>

#include <algorithm>

namespace lifex
{
  Lifting::Lifting(const std::string &subsection,
                   const UpdateFlags &update_flags_)
    : CoreModel(subsection)
    , update_flags(update_flags_ | update_JxW_values)
    , linear_solver(
        std::make_shared<utils::LinearSolverHandler<LinAlg::MPI::Vector>>(
          prm_subsection_path + " / Linear solver",
          std::initializer_list<std::string>({"CG", "GMRES", "BiCGStab"}),
          "CG"))
    , preconditioner(
        std::make_shared<utils::PreconditionerHandler>(prm_subsection_path +
                                                         " / Preconditioner",
                                                       true))
  {}

  void
  Lifting::declare_parameters(ParamHandler &params) const
  {
    params.enter_subsection_path(prm_subsection_path);
    {
      params.set_verbosity(VerbosityParam::Minimal);
      params.declare_entry("Tags Dirichlet",
                           "",
                           Patterns::List(Patterns::Integer(), 0),
                           "Boundary tags for the portions of the boundary "
                           "where to lift data from.");
      params.reset_verbosity();

      params.set_verbosity(VerbosityParam::Standard);
      {
        params.declare_entry(
          "Always solve on input mesh",
          "true",
          Patterns::Bool(),
          "If true, the lifting problem will always be solved on the input "
          "mesh; otherwise, the lifting problem will be solved on the moved "
          "mesh.");
        params.enter_subsection("Jacobian-based stiffening");
        {
          params.declare_entry("Enable",
                               "false",
                               Patterns::Bool(),
                               "Toggle jacobian-based stiffening.");

          params.declare_entry(
            "Use jacobian in current configuration",
            "true",
            Patterns::Bool(),
            "If true, the stiffening is based on the jacobian of the deformed "
            "mesh. Otherwise, it is based on that of the input, undeformed "
            "mesh. "
            "This parameter must be false if Always solve on input mesh = "
            "false.");

          params.declare_entry("J0",
                               "1e-9",
                               Patterns::Double(0),
                               "Reference jacobian value [m3].");

          params.declare_entry("chi",
                               "1.0",
                               Patterns::Double(0),
                               "Stiffening power [-].");
        }
        params.leave_subsection();

        params.enter_subsection("Boundary-based stiffening");
        {
          params.declare_entry("Enable",
                               "false",
                               Patterns::Bool(),
                               "Toggle boundary distance-based stiffening.");

          params.declare_entry("Boundary surface filename",
                               "",
                               Patterns::FileName(
                                 Patterns::FileName::FileType::input),
                               "File from which the boundary surface is read.");

          params.declare_entry("Stiffening power",
                               "1.0",
                               Patterns::Double(0),
                               "Stiffening power [-].");

          params.declare_entry("Stiffening threshold",
                               "0.0",
                               Patterns::Double(0),
                               "[m].");
        }
        params.leave_subsection();
      }
      params.reset_verbosity();
    }
    params.leave_subsection_path();

    linear_solver->declare_parameters(params);
    preconditioner->declare_parameters(params);
  }

  void
  Lifting::parse_parameters(ParamHandler &params)
  {
    params.enter_subsection_path(prm_subsection_path);
    {
      prm_tags_dirichlet = params.get_vector<std::string>("Tags Dirichlet");
      prm_always_use_input_mesh = params.get_bool("Always solve on input mesh");

      params.enter_subsection("Jacobian-based stiffening");
      {
        prm_jacobian_stiffening_enable = params.get_bool("Enable");

        if (prm_jacobian_stiffening_enable)
          {
            prm_jacobian_stiffening_current_configuration =
              params.get_bool("Use jacobian in current configuration");
            prm_jacobian_stiffening_j0  = params.get_double("J0");
            prm_jacobian_stiffening_chi = params.get_double("chi");

            AssertThrow(
              prm_always_use_input_mesh ||
                prm_jacobian_stiffening_current_configuration,
              ExcMessage(
                "Jacobian-based stiffening must use jacobian in current "
                "configuration, if \"Always solve on input mesh\" is false."));
          }
      }
      params.leave_subsection();

      params.enter_subsection("Boundary-based stiffening");
      {
        prm_boundary_stiffening_enable = params.get_bool("Enable");

        if (prm_boundary_stiffening_enable)
          {
            prm_boundary_surface_filename =
              params.get("Boundary surface filename");
            prm_boundary_stiffening_power =
              params.get_double("Stiffening power");
            prm_boundary_stiffening_threshold =
              params.get_double("Stiffening threshold");
          }
      }
      params.leave_subsection();
    }
    params.leave_subsection_path();

    linear_solver->parse_parameters(params);
    preconditioner->parse_parameters(params);

    if (prm_jacobian_stiffening_enable && !prm_always_use_input_mesh)
      update_flags = update_flags | update_jacobians;
  }

  void
  Lifting::setup_system(const utils::MeshHandler &triangulation)
  {
    const auto fe_scalar = triangulation.get_fe_lagrange(1);
    fe                   = std::make_unique<FESystem<dim>>(*fe_scalar, dim);

    quadrature_formula = triangulation.get_quadrature_gauss(2);

    set_dirichlet_tags(prm_tags_dirichlet);

    dof_handler.reinit(triangulation.get());
    dof_handler.distribute_dofs(*fe);

    triangulation.get_info().print(prm_subsection_path,
                                   dof_handler.n_dofs(),
                                   true);

    IndexSet owned_dofs = dof_handler.locally_owned_dofs();

    IndexSet relevant_dofs;
    DoFTools::extract_locally_relevant_dofs(dof_handler, relevant_dofs);

    DynamicSparsityPattern dsp(relevant_dofs);

    DoFTools::make_sparsity_pattern(dof_handler,
                                    dsp,
                                    AffineConstraints<double>(),
                                    false);

    SparsityTools::distribute_sparsity_pattern(dsp,
                                               owned_dofs,
                                               mpi_comm,
                                               relevant_dofs);

    utils::initialize_matrix(matrix, owned_dofs, dsp);
    rhs.reinit(owned_dofs, mpi_comm);

    w       = std::make_shared<LinAlg::MPI::Vector>();
    w_owned = std::make_shared<LinAlg::MPI::Vector>();
    w->reinit(owned_dofs, relevant_dofs, mpi_comm);
    w_owned->reinit(owned_dofs, mpi_comm);

    if (prm_always_use_input_mesh)
      {
        tmp_boundary_values_owned.reinit(owned_dofs, mpi_comm);
        tmp_boundary_values.reinit(owned_dofs, relevant_dofs, mpi_comm);
      }

    if (prm_boundary_stiffening_enable)
      {
        // The distance from the surface is computed once and for all in this
        // setup method. In principle, this distance could be updated whenever
        // the mesh is moved, but this would be very costly, due to the many
        // evaluations of a VTKFunction it involves, and there is no significant
        // advantage in recomputing the distance on the deformed mesh.
        pcout << "Boundary-based stiffening enabled" << std::endl;
        pcout << "Caching distance from the boundary" << std::endl;

        boundary_distance =
          std::make_unique<utils::VTKFunction>(prm_boundary_surface_filename,
                                               utils::VTKDataType::PolyData);
        boundary_distance->setup_as_signed_distance();

        const unsigned int n_q_points = quadrature_formula->size();

        unsigned int c = 0;

        FEValues<dim> fe_values(*fe,
                                *quadrature_formula,
                                update_quadrature_points);

        boundary_distance_loc.clear();
        boundary_distance_loc.resize(
          triangulation.get().n_locally_owned_active_cells(),
          std::vector<double>(n_q_points));

        for (const auto &cell : dof_handler.active_cell_iterators())
          {
            if (!cell->is_locally_owned())
              continue;

            fe_values.reinit(cell);

            for (unsigned int q = 0; q < n_q_points; ++q)
              boundary_distance_loc[c][q] = std::abs(
                boundary_distance->value(fe_values.quadrature_point(q)));

            ++c;
          }

        pcout << utils::log::separator_section << std::endl;
      }

    // Extract boundary DoFs for each component.
    boundary_dofs.resize(dim);

    for (unsigned int d = 0; d < dim; ++d)
      {
        ComponentMask mask(dim, false);
        mask.set(d, true);

        boundary_dofs[d] =
          DoFTools::extract_boundary_dofs(dof_handler, mask, dirichlet_tags);
      }
  }

  void
  Lifting::setup_interface(
    const DoFHandler<dim> &other_dof_handler,
    const std::vector<
      std::shared_ptr<FSIInterfaceHandler::InterfaceDataDirichlet>>
      &interface_data_dirichlet)
  {
    interface_handler = std::make_unique<FSIInterfaceHandler>(
      other_dof_handler,
      interface_data_dirichlet,
      std::vector<std::shared_ptr<FSIInterfaceHandler::InterfaceDataNeumann>>(
        {}),
      std::vector<
        std::shared_ptr<FSIInterfaceHandler::InterfaceDataRobinLinear>>({}),
      std::vector<
        std::shared_ptr<FSIInterfaceHandler::InterfaceDataRobinNonLinear>>({}));
  }

  void
  Lifting::apply_BCs(
    const std::shared_ptr<Mapping<dim>> &             mapping_reference,
    const double                                      ramp_value,
    const std::shared_ptr<const LinAlg::MPI::Vector> &w_before_ramp)
  {
    TimerOutput::Scope timer_section(timer_output,
                                     prm_subsection_path +
                                       " / Apply boundary conditions");

    boundary_values.clear();

    if (w_bd != nullptr && !dirichlet_tags.empty())
      {
        if (prm_always_use_input_mesh)
          {
            std::shared_ptr<Mapping<dim>> mapping_ref;
            LinAlg::MPI::Vector           d_ref, d_ref_owned;

            if (!mapping_reference)
              {
                // We construct a mapping that pulls back from the current
                // configuration (corresponding to the triangulation) to the
                // reference configuration, and feed it to fe_values. This
                // way, integrals done using fe_values will be based on the
                // reference configuration.
                d_ref_owned.reinit(*w_owned);
                VectorTools::get_position_vector(dof_handler, d_ref_owned);
                d_ref_owned.compress(VectorOperation::insert);
                d_ref_owned.add(-1.0, *w_owned);
                d_ref_owned.compress(VectorOperation::add);

                d_ref.reinit(*w);
                d_ref = d_ref_owned;

                mapping_ref = std::make_shared<
                  MappingFEField<dim, dim, LinAlg::MPI::Vector>>(dof_handler,
                                                                 d_ref);
              }
            else
              {
                mapping_ref = mapping_reference;
              }

            // We want to interpolate on the reference mesh. Ideally, we would
            // do:
            // for (auto &fun : dirichlet_map)
            //   fun.second = w_bd.get();
            //
            // VectorTools::interpolate_boundary_values(*mapping_ref,
            //                                          dof_handler,
            //                                          dirichlet_map,
            //                                          boundary_values,
            //                                          component_mask);
            // However, this appears not to work with tetrahedral meshes. To
            // work around that, we do the interpolation manually.

            // We retrieve the support points of the DoFs in reference
            // configuration, i.e. through the provided mapping.
            std::map<types::global_dof_index, Point<dim>> support_points;
            DoFTools::map_dofs_to_support_points(*mapping_ref,
                                                 dof_handler,
                                                 support_points);

            // For each component, we evaluate the boundary data function on the
            // support points boundary DoFs.
            for (unsigned int d = 0; d < dim; ++d)
              {
                if (!component_mask[d])
                  continue;

                for (const auto &dof : boundary_dofs[d])
                  {
                    Vector<double> vector_value(dim);
                    w_bd->vector_value(support_points[dof], vector_value);

                    boundary_values[dof] = vector_value[d];
                  }
              }
          }
        else
          {
            for (auto &id_function : dirichlet_map)
              {
                id_function.second = w_bd.get();
              }

            // Interpolation using the default linear mapping.
            VectorTools::interpolate_boundary_values(dof_handler,
                                                     dirichlet_map,
                                                     boundary_values,
                                                     component_mask);
          }
      }

    // Impose boundary data from the fluid-structure interface in case of an
    // FSI problem. Note that this overrides w_bd in case of conflicts.
    if (interface_handler != nullptr)
      {
        interface_handler->extract();
        interface_handler->apply_dirichlet(boundary_values);
      }

    if (utils::is_definitely_less_than(ramp_value, 1.0))
      {
        Assert(w_before_ramp != nullptr,
               ExcMessage(
                 "A starting displacement for the ramp must be provided."));

        for (auto &dof_val : boundary_values)
          {
            dof_val.second =
              ramp_value * dof_val.second +
              (1.0 - ramp_value) * (*w_before_ramp)[dof_val.first];
          }
      }

    MatrixTools::apply_boundary_values(
      boundary_values, matrix, *w_owned, rhs, false);

    *w = *w_owned;
  }


  const std::shared_ptr<LinAlg::MPI::Vector> &
  Lifting::solve(const std::shared_ptr<Mapping<dim>> &mapping_reference)
  {
    apply_BCs(mapping_reference);

    pcout << "        Solving lifting: " << std::flush;
    solve_linear_system();

    *w = *w_owned; // Performs parallel communication.
    return w;
  }

  void
  Lifting::solve_linear_system(LinAlg::MPI::Vector &dst)
  {
    preconditioner->initialize(matrix);
    linear_solver->solve(matrix, dst, rhs, *preconditioner);
  }

  void
  Lifting::assemble(const std::shared_ptr<Mapping<dim>> &mapping_reference)
  {
    // If we already have assembled the system once, the parameters indicate we
    // always want to assemble on the input mesh, and jacobian stiffening is
    // disabled, we do nothing. (If jacobian stiffening is enabled, we need to
    // assemble again to update stiffening factors).
    if (already_assembled && prm_always_use_input_mesh &&
        (!prm_jacobian_stiffening_enable ||
         !prm_jacobian_stiffening_current_configuration))
      return;

    TimerOutput::Scope timer_section(timer_output,
                                     prm_subsection_path + " / Assembling");

    matrix = 0;
    rhs    = 0;

    const unsigned int dofs_per_cell = fe->dofs_per_cell;
    const unsigned int n_q_points    = quadrature_formula->size();

    FullMatrix<double> cell_matrix(dofs_per_cell, dofs_per_cell);
    Vector<double>     cell_rhs(dofs_per_cell);

    std::vector<types::global_dof_index> dof_indices(dofs_per_cell);
    std::vector<double>                  scaling_factors(n_q_points);

    std::shared_ptr<Mapping<dim>> mapping_ref;
    LinAlg::MPI::Vector           d_ref, d_ref_owned;

    if (prm_always_use_input_mesh)
      {
        if (!mapping_reference)
          {
            // We construct a mapping that pulls back from the current
            // configuration (corresponding to the triangulation) to the
            // reference configuration, and feed it to fe_values. This way,
            // integrals done using fe_values will be based on the reference
            // configuration.
            d_ref_owned.reinit(*w_owned);
            VectorTools::get_position_vector(dof_handler, d_ref_owned);
            d_ref_owned.compress(VectorOperation::insert);
            d_ref_owned.add(-1.0, *w_owned);
            d_ref_owned.compress(VectorOperation::add);

            d_ref.reinit(*w);
            d_ref = d_ref_owned;

            mapping_ref =
              std::make_shared<MappingFEField<dim, dim, LinAlg::MPI::Vector>>(
                dof_handler, d_ref);
          }
        else
          {
            mapping_ref = mapping_reference;
          }

        fe_values = std::make_shared<FEValues<dim>>(*mapping_ref,
                                                    *fe,
                                                    *quadrature_formula,
                                                    update_flags);

        // If jacobian stiffening is enabled, we also need a FEValues for
        // current configuration to extract element jacobians.
        if (prm_jacobian_stiffening_enable)
          fe_values_current =
            std::make_shared<FEValues<dim>>(*fe,
                                            *quadrature_formula,
                                            update_jacobians);
      }
    else
      {
        // If we are using the deformed configuration at all times, we only need
        // to construct one FEValues instance, defined on current configuration.
        // We set fe_values_current to point to the same object for interface
        // consistency.
        fe_values = std::make_shared<FEValues<dim>>(*fe,
                                                    *quadrature_formula,
                                                    update_flags);

        if (prm_jacobian_stiffening_enable)
          fe_values_current = fe_values;
      }

    unsigned int c = 0;

    for (const auto &cell : dof_handler.active_cell_iterators())
      {
        if (!cell->is_locally_owned())
          continue;

        fe_values->reinit(cell);

        // We reinit fe_values_current only if we're always using the input mesh
        // (otherwise, it is the same as fe_values).
        if (prm_always_use_input_mesh && prm_jacobian_stiffening_enable)
          fe_values_current->reinit(cell);

        cell->get_dof_indices(dof_indices);

        std::fill(scaling_factors.begin(), scaling_factors.end(), 1.0);

        for (unsigned int q = 0; q < n_q_points; ++q)
          {
            // Compute scaling factors according to whether jacobian-based
            // stiffening is enabled or not.
            if (prm_jacobian_stiffening_enable)
              {
                scaling_factors[q] *= std::pow(
                  prm_jacobian_stiffening_j0 /
                    std::max(fe_values_current->jacobian(q).determinant(),
                             prm_jacobian_stiffening_j0),
                  prm_jacobian_stiffening_chi);
              }

            // Update scaling factors according to whether boundary-based
            // stiffening is enabled or not.
            if (prm_boundary_stiffening_enable)
              {
                scaling_factors[q] /=
                  std::pow(std::max(boundary_distance_loc[c][q],
                                    prm_boundary_stiffening_threshold),
                           prm_boundary_stiffening_power);
              }
          }

        cell_matrix = 0.0;
        cell_rhs    = 0.0;

        assemble_callback_cell(cell, cell_matrix, cell_rhs, scaling_factors);

        matrix.add(dof_indices, cell_matrix);
        rhs.add(dof_indices, cell_rhs);

        ++c;
      }

    matrix.compress(VectorOperation::add);
    rhs.compress(VectorOperation::add);

    already_assembled = true;
  }
} // namespace lifex
