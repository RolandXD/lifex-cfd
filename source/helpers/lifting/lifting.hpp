/********************************************************************************
  Copyright (C) 2019 - 2023 by the lifex authors.

  This file is part of lifex.

  lifex is free software; you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  lifex is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with lifex.  If not, see <http://www.gnu.org/licenses/>.
********************************************************************************/

/**
 * @file
 *
 * @author Ivan Fumagalli <ivan.fumagalli@polimi.it>.
 */

#ifndef LIFEX_HELPER_LIFTING_HPP_
#define LIFEX_HELPER_LIFTING_HPP_

#include "core/source/core_model.hpp"
#include "core/source/generic_factory.hpp"

#include "core/source/geometry/mesh_handler.hpp"

#include "core/source/io/vtk_function.hpp"

#include "core/source/numerics/interface_handler.hpp"
#include "core/source/numerics/linear_solver_handler.hpp"
#include "core/source/numerics/preconditioner_handler.hpp"
#include "core/source/numerics/tools.hpp"

#include <map>
#include <memory>
#include <set>
#include <string>
#include <vector>

namespace lifex
{
  /**
   * @brief Abstract class representing a generic lifting of a FE function
   * defined on the boundary of a domain.
   *
   * The equation solved is:
   * @f[
   * \left\{
   * \begin{alignedat}{2}
   * L w & = 0, & \quad & \text{in } \Omega,\\
   * w & = w_\text{bd}, & \quad & \text{on } \Gamma_D\subseteq\partial\Omega,\\
   * \boldsymbol\sigma(w)\mathbf n & = 0, & \quad & \text{on }
   * \Gamma_N=\partial\Omega\setminus\Gamma_D,
   * \end{alignedat}
   * \right.
   * @f]
   * where @f$L@f$ is a linear differential operator,
   * @f$w_\text{bd}@f$ is the function to lift, and @f$\boldsymbol\sigma(w)
   * \mathbf{n}@f$ is the natural Neumann term associated with the operator
   * @f$L@f$. The data @f$w_\text{bd}@f$ (and thus the lift @f$w@f$) can be
   * scalar or vectorial.
   *
   * For concrete implementations of the lifting operator @f$L@f$, see
   * @ref LiftingHarmonic and @ref LiftingElastic.
   *
   * @section local-stiffening Local stiffening
   * Lifting is often used in the context of the ALE formulation for
   * Navier-Stokes equations, to extend a displacement field from the domain
   * boundary to the domain interior and displace the mesh accordingly (see
   * @ref FluidDynamics and @ref ALEHandler).
   *
   * In that context, it is important that the resulting displacement field
   * preserves the regularity of the mesh. To this end, it is possible to
   * introduce local stiffening, i.e. modify the lifting operator @f$L@f$ in
   * such a way that the corresponding stiffness (or diffusivity) is higher in
   * critical areas of the mesh.
   *
   * Two different approaches to this are offered. Both are based on
   * computing a local, spatially varying stiffening factor @f$c(\mathbf x)@f$
   * and multiplying it to the quantities integrated in the weak formulation of
   * the lifting problem.
   *
   * @subsection jacobian Jacobian-based local stiffening
   * The stiffening factor is given by @f$c(\mathbf x) = (J_0 / J)^\chi@f$,
   * where @f$J_0@f$ are positive parameters and @f$J@f$ is the jacobian of the
   * map from the reference to the physical element. This amounts to stiffening
   * elements that have a small jacobian, that is elements that are small or
   * skewed.
   *
   * This can be enabled and configured through the parameters in section
   * <kbd>Lifting / Jacobian-based stiffening</kbd>.
   *
   * Reference: @refcite{Stein2003, Stein et al. 2003}.
   *
   * @note
   * - The term @f$J_0^\chi@f$ is a constant scaling factor for all elements,
   * so that the choice of @f$J_0@f$ does not affect the result, but only the
   * scaling of the linear system associated to the problem. It is advisable to
   * use relative tolerance, rather than absolute, for solving the system if
   * stiffening is enabled.
   * - If <kbd>Lifting / Always solve on input mesh</kbd> is true, the
   * lifting problem is always solved on the initial mesh configuration; in that
   * case, the jacobian @f$J@f$ can be evaluated either in the deformed
   * configuration (<kbd>Use jacobian in current configuration = true</kbd>) or
   * in the input configuration (<kbd>Use jacobian in current configuration =
   * false</kbd>). In the former case, you are better guarded against elements
   * that become smaller over time; in the latter case, you save computations by
   * avoiding to reassemble the linear system at every timestep.
   *
   * @subsection boundary Boundary-based stiffening
   * The stiffening factor is computed as @f$c(\mathbf x) = \max\{d(\mathbf x),
   * \varepsilon\}^{-p}@f$, where @f$d@f$ is the distance from a user-defined
   * surface and @f$\varepsilon@f$ and @f$p@f$ are positive parameters.
   *
   * The surface is in principle arbitrary, although this stiffening approach is
   * meant to stiffen locally near the moving boundaries, so that the surface
   * should correspond to the boundary of the domain.
   *
   * Reference: @refcite{jasak2006automatic, Jasak and Tukovic\, 2006}.
   *
   * @note The surface is not displaced along with the mesh: the distance is
   * computed once and for all at the beginning of the simulation for all
   * quadrature nodes. If the mesh is moved, the distance of each quadrature
   * node from the surface is not updated.
   */
  class Lifting : public CoreModel
  {
  public:
    /// Interface handler for the ALE subproblem of a
    /// fluid-structure-interaction problem.
    using FSIInterfaceHandler = utils::InterfaceHandler<LinAlg::MPI::Vector>;

    /// Alias for generic lifting operators factory.
    using LiftingFactory = GenericFactory<Lifting, const std::string &>;

    /// Default constructor.
    Lifting(const std::string &subsection, const UpdateFlags &update_flags_);

    /// Destructor.
    virtual ~Lifting() = default;

    /// Override of @ref CoreModel::declare_parameters.
    virtual void
    declare_parameters(ParamHandler &params) const override;

    /// Override of @ref CoreModel::parse_parameters.
    virtual void
    parse_parameters(ParamHandler &params) override;

    /**
     * @brief Setup system, @a i.e. read external data (Dirichlet tags,
     * linear solver parameters) and allocate matrices and vectors
     * for the given triangulation.
     *
     * @warning This method must be called before calling assemble().
     *
     * @param[in] triangulation mesh for the lifting problem (read/created outside of this class)
     */
    virtual void
    setup_system(const utils::MeshHandler &triangulation);

    /// Sets the function defining the Dirichlet boundary conditions for the
    /// problem.
    void
    set_dirichlet_function(
      const std::shared_ptr<Function<dim>> &w_bd_,
      const ComponentMask &                 component_mask_ = ComponentMask())
    {
      w_bd           = w_bd_;
      component_mask = component_mask_;
    }

    /// Setup lifting's interface handler and interface conditions (for FSI).
    ///
    /// @note Only Dirichlet interface data can be specified, since this is meant
    /// to extract lifting's boundary conditions from the mechanics problem.
    void
    setup_interface(
      const DoFHandler<dim> &other_dof_handler,
      const std::vector<
        std::shared_ptr<FSIInterfaceHandler::InterfaceDataDirichlet>>
        &interface_data_dirichlet);

    /**
     * @brief Assemble linear system.
     *
     * The assembly of the actual lifting operator (on each cell) is done by
     * assemble_callback_cell().
     *
     * @param[in] mapping_reference If always solving on input mesh,
     * a mapping can be provided that represents the reference configuration. If
     * the mapping is not provided (@a i.e. this argument is left to
     * <kbd>nullptr</kbd>), then this method sets one up assuming that the
     * vector w_owned stores the displacement from the reference to the current
     * configuration.
     *
     * @warning This method must be called before calling solve().
     */
    void
    assemble(const std::shared_ptr<Mapping<dim>> &mapping_reference = nullptr);

    /**
     * @brief Solve lifting problem.
     *
     * @param[in] mapping_reference If provided and always solving on
     * the input mesh, interpolation of boundary data is done using this mapping
     * (that is meant to represent the reference configuration). If
     * the mapping is not provided (@a i.e. this argument is left to
     * <kbd>nullptr</kbd>), but still solving on input mesh, then this method
     * sets up a mapping assuming that the vector w_owned stores the
     * displacement from the reference to the current configuration.
     *
     * @return lifting problem solution @f$w@f$.
     */
    virtual const std::shared_ptr<LinAlg::MPI::Vector> &
    solve(const std::shared_ptr<Mapping<dim>> &mapping_reference = nullptr);

    /**
     * @brief Allow changing Dirichlet tags after setup().
     *
     * @warning This method cannot be called before setup().
     *
     * If this method is called, all previous tags are neglected.
     * @param[in] dirichlet_tags_strings list of Dirichlet tags as strings
     */
    void
    set_dirichlet_tags(const std::vector<std::string> &dirichlet_tags_strings)
    {
      prm_tags_dirichlet = dirichlet_tags_strings;

      dirichlet_tags.clear();
      dirichlet_map.clear();

      for (auto &tag_string : dirichlet_tags_strings)
        {
          types::boundary_id tag = Utilities::string_to_int(tag_string);
          dirichlet_tags.insert(tag);
          dirichlet_map.emplace(tag, nullptr);
        }
    }

    /// Get solution.
    virtual const std::shared_ptr<LinAlg::MPI::Vector> &
    get_solution() const
    {
      return w;
    }

    /// Get solution (non-ghosted).
    virtual const std::shared_ptr<LinAlg::MPI::Vector> &
    get_solution_owned() const
    {
      return w_owned;
    }

    /// Get finite element.
    const FESystem<dim> &
    get_fe() const
    {
      return *fe;
    }

    /// Get DoF handler.
    const DoFHandler<dim> &
    get_dof_handler() const
    {
      return dof_handler;
    }

    /// Get whether assembly is always done on input mesh.
    bool
    always_use_input_mesh() const
    {
      return prm_always_use_input_mesh;
    }

  protected:
    /// Assemble on an individual cell.
    ///
    /// Pure virtual method: implementation delegated to inheriting class.
    ///
    /// @param[in] cell Active cell iterator.
    /// @param[in,out] cell_matrix Local matrix to which values are added.
    /// @param[in,out] cell_rhs Local rhs vector to which values are added.
    /// @param[in] scaling_factors Vector of factors multiplying the contribution at each quadrature point.
    virtual void
    assemble_callback_cell(const DoFHandler<dim>::active_cell_iterator &cell,
                           FullMatrix<double> &       cell_matrix,
                           Vector<double> &           cell_rhs,
                           const std::vector<double> &scaling_factors) = 0;

    /// Apply boundary conditions.
    ///
    /// Optionally, boundary conditions can be relaxed: given a certain
    /// displacement vector @f$\mathbf w_\text{old}@f$ and a coefficient @f$c
    /// \in (0, 1)@f$, the condition @f$\mathbf w = \mathbf
    /// w_\text{bd}@f$ is replaced by @f$\mathbf w = c\mathbf w_\text{bd} + (1 -
    /// c)\mathbf w_\text{old}@f$. This allows to progressively move from
    /// @f$\mathbf w_\text{old}@f$ to @f$\mathbf w_\text{bd}@f$, and it can be
    /// used by those lifting operators whose solvers have convergence issues.
    ///
    /// @param[in] mapping_reference A mapping describing the reference
    /// configuration for the interpolation of boundary data, to be used if
    /// solving on input mesh. If the mapping is not provided (@a i.e. this
    /// argument is left to <kbd>nullptr</kbd>), a mapping will be constructed
    /// assuming that the displacement vector w contains the displacement from
    /// the input to the current configuration.
    /// @param[in] ramp_value The relaxation coefficient @f$c@f$.
    /// @param[in] w_before_ramp The vector @f$\mathbf w_\text{old}@f$.
    void
    apply_BCs(const std::shared_ptr<Mapping<dim>> &mapping_reference = nullptr,
              const double                         ramp_value        = 1.0,
              const std::shared_ptr<const LinAlg::MPI::Vector> &w_before_ramp =
                nullptr);

    /// Solve algebraic system.
    void
    solve_linear_system()
    {
      solve_linear_system(*w_owned);
    }

    /// Solve algebraic system.
    void
    solve_linear_system(LinAlg::MPI::Vector &dst);

    /// @name Parameters read from file.
    /// @{

    /// Dirichlet boundary tags for the lifting problem.
    std::vector<std::string> prm_tags_dirichlet;

    /// Always solve on input mesh.
    bool prm_always_use_input_mesh;

    /// Toggle jacobian-based stiffening.
    bool prm_jacobian_stiffening_enable;

    /// Reference jacobian.
    double prm_jacobian_stiffening_j0;

    /// Stiffening power.
    double prm_jacobian_stiffening_chi;

    /// Toggle whether to use the jacobian in current or input configuration.
    bool prm_jacobian_stiffening_current_configuration;

    /// Toggle boundary-based stiffening.
    bool prm_boundary_stiffening_enable;

    /// Boundary based stiffening power.
    double prm_boundary_stiffening_power;

    /// Boundary based stiffening threshold.
    double prm_boundary_stiffening_threshold;

    /// Boundary surface filename, for boundary-based stiffening.
    std::string prm_boundary_surface_filename;

    /// @}

    /// @name Space discretization.
    ///\{

    /// Finite element space (hard-coded piecewise linear).
    /// @todo Allow scalar finite elements.
    std::unique_ptr<FESystem<dim>> fe;

    /// Dof handler.
    DoFHandler<dim> dof_handler;

    /// Quadrature formula.
    std::unique_ptr<Quadrature<dim>> quadrature_formula;

    /// Evaluation of @ref fe on a cell. If prm_always_use_input_mesh is set to
    /// true, this evaluation will be performed on the reference configuration,
    /// corresponding to the input mesh. Otherwise, it will be performed on the
    /// deformed mesh.
    std::shared_ptr<FEValues<dim>> fe_values;

    /// Evaluation of @ref fe on a cell in current, deformed configuration. If
    /// prm_always_use_input_mesh is set to false, this is not initialized.
    std::shared_ptr<FEValues<dim>> fe_values_current;

    /// Flags used to update the fe_values.
    UpdateFlags update_flags;

    ///\}

    /// Matrix coming from the discretization of operator @f$L@f$.
    LinAlg::MPI::SparseMatrix matrix;
    /// Right-hand side of the algebraic equation.
    LinAlg::MPI::Vector rhs;

    /// Linear solver handler.
    std::shared_ptr<utils::LinearSolverHandler<LinAlg::MPI::Vector>>
      linear_solver;

    /// Preconditioner handler.
    std::shared_ptr<utils::PreconditionerHandler> preconditioner;

    /// Flag indicating whether the system has been assembled already or not.
    bool already_assembled = false;

    /// Solution vector.
    std::shared_ptr<LinAlg::MPI::Vector> w;
    /// Solution vector, without ghost entries.
    std::shared_ptr<LinAlg::MPI::Vector> w_owned;

    /// Tags where to read data from.
    std::set<types::boundary_id> dirichlet_tags;

    /// Map pairing tag with corresponding Dirichlet function. TODO: allow
    /// scalar
    std::map<types::boundary_id, const Function<dim> *> dirichlet_map;

    /// Temporary vector for interpolation of boundary data, owned elements.
    LinAlg::MPI::Vector tmp_boundary_values_owned;

    /// Temporary vector for interpolation of boundary data.
    LinAlg::MPI::Vector tmp_boundary_values;

    /// Handler for interface conditions.
    std::unique_ptr<FSIInterfaceHandler> interface_handler;

    /// Pointer to the function to be lifted.
    std::shared_ptr<Function<dim>> w_bd;

    /// Component mask for w_bd.
    ComponentMask component_mask;

    /// Signed distance from boundary surface, for boundary-based stiffening.
    std::unique_ptr<utils::VTKFunction> boundary_distance;

    /// Evaluation of the distance from the boundary surface on quadrature
    /// nodes, for boundary-based stiffening.
    std::vector<std::vector<double>> boundary_distance_loc;

    /// Boundary values map.
    std::map<types::global_dof_index, double> boundary_values;

    /// Boundary DoF indices. For each component, an index set of the Dirichlet
    /// boundary DoFs for that component.
    std::vector<IndexSet> boundary_dofs;
  };

} // namespace lifex

#endif /* LIFEX_HELPER_LIFTING_HPP_ */
