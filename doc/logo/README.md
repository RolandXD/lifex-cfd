# <img alt="lifex" width="150" src="https://gitlab.com/lifex/lifex-cfd/-/raw/main/doc/logo/lifex.png" /> logos

High-resolution light- and dark-background **<kbd>life<sup>x</kbd></tt>** logos.

See also [here](https://gitlab.com/lifex/lifex/-/raw/main/doc/logo/).

Copyright (C) 2020 by Silvia Pozzi <silvia.pozzi@polimi.it>.
