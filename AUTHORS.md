# Authors

The following people (in alphabetical order) have mainly contributed to **<kbd>life<sup>x</sup>-cfd</kbd>**,
with many more who have reported bugs or sent in suggestions, fixes or small enhancements:

## Maintainers
- Pasquale Claudio Africa <pasqualeclaudio.africa@polimi.it>
- Lorenzo Bennati         <lorenzo.bennati@mail.polimi.it>
- Michele Bucelli         <michele.bucelli@polimi.it>
- Marco Fedele            <marco.fedele@polimi.it>
- Ivan Fumagalli          <ivan.fumagalli@polimi.it>
- Alberto Zingaro         <alberto.zingaro@polimi.it>

## Other contributors
- Nicolas Barnafi         <nicolas.barnafi@unipv.it>
- Simone Di Gregorio      <simone.digregorio@polimi.it>
- Stefano Pagani          <stefano.pagani@polimi.it>
- Roberto Piersanti       <roberto.piersanti@polimi.it>
- Francesco Regazzoni     <francesco.regazzoni@polimi.it>
- Matteo Salvador         <matteo1.salvador@polimi.it>
- Simone Stella           <simone.stella@polimi.it>
- Elena Zappon            <elena.zappon@polimi.it>
