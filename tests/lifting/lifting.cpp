/********************************************************************************
  Copyright (C) 2019 - 2023 by the lifex authors.

  This file is part of lifex.

  lifex is free software; you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  lifex is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with lifex.  If not, see <http://www.gnu.org/licenses/>.
********************************************************************************/

/**
 * @file
 *
 * @author Ivan Fumagalli <ivan.fumagalli@polimi.it>.
 */

#include "core/source/core_model.hpp"
#include "core/source/init.hpp"

#include "core/source/geometry/move_mesh.hpp"

#include "core/source/io/csv_test.hpp"
#include "core/source/io/csv_writer.hpp"
#include "core/source/io/data_writer.hpp"

#include "source/helpers/lifting/lifting_elastic.hpp"
#include "source/helpers/lifting/lifting_harmonic.hpp"

namespace lifex::tests
{
  /// @brief Test class for Lifting.
  class TestLifting : public CoreModel
  {
  public:
    /// Boundary data to lift: @f$w_{bd}(x, y, z) = (0, 0, z)@f$.
    class DataToLift : public Function<dim>
    {
    public:
      /// Default constructor (vectorial function).
      DataToLift()
        : Function<dim>(dim)
      {}

      /// Evaluate the function at given point.
      virtual void
      vector_value(const Point<dim> &p, Vector<double> &value) const override
      {
        for (unsigned int component = 0; component < dim; ++component)
          {
            // value[component] = component + p[0];
            value[0] = 0;
            value[1] = 0.1;
            value[2] = 0.1 * p[2];
          }
      }
    };


    /// Constructor
    ///
    /// @param[in] subsection Filename and subsection name for this test.
    TestLifting(const std::string &subsection)
      : CoreModel(subsection)
      , csv_writer(mpi_rank == 0)
    {}

    /// Override of @ref CoreModel::declare_parameters.
    virtual void
    declare_parameters(ParamHandler &params) const override
    {
      params.enter_subsection_path(prm_subsection_path);

      // Declare parameters.
      params.enter_subsection("Output");
      {
        params.declare_entry("Enable output",
                             "true",
                             Patterns::Bool(),
                             "Enable/disable output.");

        params.declare_entry("Filename",
                             "lifting",
                             Patterns::FileName(
                               Patterns::FileName::FileType::output),
                             "Output file.");

        params.declare_entry("CSV filename",
                             "lifting.csv",
                             Patterns::FileName(
                               Patterns::FileName::FileType::output),
                             "Output CSV file.");
      }
      params.leave_subsection();

      params.enter_subsection("Lifting");
      {
        params.declare_entry_selection(
          "Lifting operator",
          LiftingHarmonic::label,
          Lifting::LiftingFactory::get_registered_keys_prm(),
          "Differential model for the lifting.");
      }
      params.leave_subsection();

      params.leave_subsection_path();

      Lifting::LiftingFactory::declare_children_parameters(params,
                                                           prm_subsection_path +
                                                             " / Lifting");
    }

    /// Override of @ref CoreModel::parse_parameters.
    virtual void
    parse_parameters(ParamHandler &params) override
    {
      // Parse input file.
      params.parse();

      // Read input parameters.
      params.enter_subsection_path(prm_subsection_path);

      params.enter_subsection("Output");
      {
        prm_enable_output = params.get_bool("Enable output");
        if (prm_enable_output)
          {
            prm_output_basename = params.get("Filename");
          }
        prm_csv_filename = params.get("CSV filename");
      }
      params.leave_subsection();

      params.enter_subsection("Lifting");
      {
        prm_lifting_op = params.get("Lifting operator");
      }
      params.leave_subsection();

      params.leave_subsection_path();

      lifting = Lifting::LiftingFactory::parse_child_parameters(
        params, prm_lifting_op, prm_subsection_path + " / Lifting");
    }

    /// Run test model.
    virtual void
    run() override
    {
      const double actual_edge_length =
        100 * 1e-3; // edge length * scaling factor.

      std::shared_ptr<utils::MeshHandler> triangulation =
        std::make_shared<utils::MeshHandler>(prm_subsection_path, mpi_comm);

      triangulation->initialize_hypercube(0, actual_edge_length, true);
      triangulation->set_refinement_global(3);
      triangulation->create_mesh();

      const auto    fe_scalar = triangulation->get_fe_lagrange(1);
      FESystem<dim> fe(*fe_scalar, dim);

      DoFHandler dof_handler(triangulation->get());
      dof_handler.distribute_dofs(fe);

      triangulation->get_info().print(prm_subsection_path,
                                      dof_handler.n_dofs(),
                                      true);

      lifting->setup_system(*triangulation);

      IndexSet owned_dofs = dof_handler.locally_owned_dofs();
      IndexSet relevant_dofs;
      DoFTools::extract_locally_relevant_dofs(dof_handler, relevant_dofs);
      LinAlg::MPI::Vector w_owned(owned_dofs, mpi_comm);
      LinAlg::MPI::Vector w(owned_dofs, relevant_dofs, mpi_comm);
      w = w_owned = 0;

      if (prm_enable_output)
        {
          VectorTools::interpolate(dof_handler, DataToLift(), w_owned);
          w = w_owned;

          TimerOutput::Scope timer_section(timer_output,
                                           prm_subsection_path +
                                             " / Output initial condition");

          DataOut<dim> data_out;
          data_out.add_data_vector(dof_handler, w, "w");
          data_out.build_patches();

          utils::dataout_write_hdf5(data_out, prm_output_basename + "_orig");

          data_out.clear();
        }

      // CSV output.
      {
        csv_writer.declare_entries({"time",
                                    "volume",
                                    "barycenter_0",
                                    "barycenter_1",
                                    "barycenter_2",
                                    "mom_inertia_0",
                                    "mom_inertia_1",
                                    "mom_inertia_2"});
        csv_writer.open(get_output_csv_filename(), ',');
        double volume     = triangulation->get_info().compute_mesh_volume();
        auto   barycenter = triangulation->get_info().compute_mesh_barycenter();
        auto   mom_inertia =
          triangulation->get_info().compute_mesh_moment_inertia();
        csv_writer.set_entries({{"time", 0},
                                {"volume", volume},
                                {"barycenter_0", barycenter[0]},
                                {"barycenter_1", barycenter[1]},
                                {"barycenter_2", barycenter[2]},
                                {"mom_inertia_0", mom_inertia[0]},
                                {"mom_inertia_1", mom_inertia[1]},
                                {"mom_inertia_2", mom_inertia[2]}});
        csv_writer.write_line();
      }

      lifting->assemble();

      {
        TimerOutput::Scope timer_section(timer_output,
                                         prm_subsection_path +
                                           " / Solve lifting problem");

        lifting->set_dirichlet_function(std::make_shared<DataToLift>());
        w = *lifting->solve();
      }

      {
        TimerOutput::Scope timer_section(timer_output,
                                         prm_subsection_path + " / Move mesh");
        utils::move_mesh(*triangulation, dof_handler, w);
      }

      if (prm_enable_output)
        {
          TimerOutput::Scope timer_section(timer_output,
                                           prm_subsection_path +
                                             " / Output results");

          DataOut<dim> data_out;
          data_out.add_data_vector(dof_handler, w, "w");
          data_out.build_patches();

          utils::dataout_write_hdf5(data_out, prm_output_basename);

          data_out.clear();
        }

      // CSV output.
      {
        double volume     = triangulation->get_info().compute_mesh_volume();
        auto   barycenter = triangulation->get_info().compute_mesh_barycenter();
        auto   mom_inertia =
          triangulation->get_info().compute_mesh_moment_inertia();
        csv_writer.set_entries({{"time", 1},
                                {"volume", volume},
                                {"barycenter_0", barycenter[0]},
                                {"barycenter_1", barycenter[1]},
                                {"barycenter_2", barycenter[2]},
                                {"mom_inertia_0", mom_inertia[0]},
                                {"mom_inertia_1", mom_inertia[1]},
                                {"mom_inertia_2", mom_inertia[2]}});
        csv_writer.write_line();
      }
    }

    /// Return the name of the file the CSV output has been written to.
    std::string
    get_output_csv_filename() const
    {
      return prm_csv_filename;
    }

  private:
    /// Lifting class.
    std::shared_ptr<Lifting> lifting;
    /// CSV writer to check the results.
    utils::CSVWriter csv_writer;

    /// Toggle output on XDMF file.
    bool prm_enable_output;
    /// Output basename.
    std::string prm_output_basename;
    /// Output CSV filename.
    std::string prm_csv_filename;
    /// String identifying the lifting operator.
    std::string prm_lifting_op;
  };
} // namespace lifex::tests

/// Tests lifting classes and mesh motion, comparing the resulting mesh with a
/// reference one.
int
main(int argc, char **argv)
{
  lifex::lifex_init lifex_initializer(argc, argv, 1);

  try
    {
      lifex::utils::CSVTest<lifex::tests::TestLifting> test("Test lifting");

      test.main_run_generate([&test]() {
        test.generate_parameters_from_json({"harmonic"}, "harmonic");
        test.generate_parameters_from_json({"harmonic_jac_stiff"},
                                           "harmonic_jac_stiff");
        test.generate_parameters_from_json({"elastic"}, "elastic");
        test.generate_parameters_from_json({"elastic_jac_stiff"},
                                           "elastic_jac_stiff");
      });
    }
  LIFEX_CATCH_EXC();

  return EXIT_SUCCESS;
}
