/********************************************************************************
  Copyright (C) 2019 - 2023 by the lifex authors.

  This file is part of lifex.

  lifex is free software; you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  lifex is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with lifex.  If not, see <http://www.gnu.org/licenses/>.
********************************************************************************/

/**
 * @file
 *
 * @author Ivan Fumagalli <ivan.fumagalli@polimi.it>.
 */

#include "core/source/core_model.hpp"
#include "core/source/init.hpp"

#include "source/helpers/ale_handler.hpp"

namespace lifex::tests
{
  /// @brief Test class for ALEHandler.
  ///
  /// Solves a lifting problem with @ref DataToLift on the boundary,
  /// displaces the mesh according to the solution, and then possibly
  /// (if @ref prm_disp_from_file <kbd>= true</kbd>) solves another lifting
  /// problem reading the boundary displacement, and updates the mesh.
  class TestALE : public CoreModel
  {
  public:
    /// Boundary data to lift: @f$w_{bd}(x, y, z, t) = (1, 2 + t (y-2), 3)@f$.
    class DataToLift : public Function<dim>
    {
    public:
      /// Constructor (vectorial function).
      DataToLift()
        : Function<dim>(dim)
      {}

      /// Evaluate the function at given point.
      virtual void
      vector_value(const Point<dim> &p, Vector<double> &value) const override
      {
        for (unsigned int component = 0; component < dim; ++component)
          {
            value[0] = 1;
            value[1] = 2 + this->get_time() * (p[1] - 2);
            value[2] = 3;
          }
      }
    };

    /// Constructor
    TestALE(const std::string &subsection,
            const std::string &subsection_ale_handler_from_function,
            const std::string &subsection_ale_handler_from_file)
      : CoreModel(subsection)
      , ale_handler_from_function(subsection_ale_handler_from_function, true)
      , ale_handler_from_file(subsection_ale_handler_from_file, true)
    {}

    /// Override of @ref CoreModel::declare_parameters.
    virtual void
    declare_parameters(ParamHandler &params) const override
    {
      params.enter_subsection_path(prm_subsection_path);
      params.declare_entry(
        "Test displacement from file",
        "false",
        Patterns::Bool(),
        "Toggle additional test where the displacement is read from file.");
      params.leave_subsection_path();

      // Dependencies.
      {
        ale_handler_from_function.declare_parameters(params);
        ale_handler_from_file.declare_parameters(params);
      }
    }

    /// Override of @ref CoreModel::parse_parameters.
    virtual void
    parse_parameters(ParamHandler &params) override
    {
      // Parse input file.
      params.parse();

      // Read input parameters.
      params.enter_subsection_path(prm_subsection_path);
      prm_disp_from_file = params.get_bool("Test displacement from file");
      params.leave_subsection_path();

      // Dependencies.
      {
        ale_handler_from_function.parse_parameters(params);
        ale_handler_from_file.parse_parameters(params);
      }
    }

    /// Run test.
    virtual void
    run() override
    {
      const double actual_edge_length =
        100 * 1e-3; // edge length * scaling factor.

      // Fictitious time, to allow saving multiple fields in the same file.
      double       time_step       = 0.5;
      double       time            = 0.0;
      unsigned int timestep_number = 0;
      double       period          = 5.0;

      // Displacement read from Function.
      {
        pcout << "Displacement read from Function." << std::endl;

        std::shared_ptr<utils::MeshHandler> triangulation =
          std::make_shared<utils::MeshHandler>(prm_subsection_path, mpi_comm);

        triangulation->initialize_hypercube(0, actual_edge_length, true);
        triangulation->set_refinement_global(3);
        triangulation->create_mesh();

        triangulation->get_info().print(prm_subsection_path, "", true);

        const auto fe_scalar = triangulation->get_fe_lagrange(3);

        std::shared_ptr<DataToLift> data_to_lift =
          std::make_shared<DataToLift>();
        data_to_lift->set_time(time);
        ale_handler_from_function.setup_from_function(triangulation,
                                                      FESystem<dim>(*fe_scalar,
                                                                    1),
                                                      0,
                                                      data_to_lift,
                                                      true,
                                                      true);

        ale_handler_from_function.output_results(time, timestep_number);
        ++timestep_number;
        time += time_step;

        {
          TimerOutput::Scope timer_section(
            timer_output,
            prm_subsection_path +
              " / Solve lifting problem / Displacement from Function");
          ale_handler_from_function.update(time, time_step, period);
        }

        {
          TimerOutput::Scope timer_section(
            timer_output,
            prm_subsection_path + " / Move mesh / Displacement from Function");
          ale_handler_from_function.move_mesh(time_step);
        }

        ale_handler_from_function.output_results(time, timestep_number);
        ++timestep_number;
        time += time_step;
      }

      // Time-dependent displacement read from file.
      // Assumptions on ALEHandler params:
      //    - # of time subintervals >= 2
      //    - Time indices stride = 1
      //    - Time subinterval duration = 0.5
      //    - Displacement mode = Incremental
      if (prm_disp_from_file)
        {
          pcout << "Time-dependent displacement read from file." << std::endl;

          double initial_time = time;

          std::shared_ptr<utils::MeshHandler> triangulation =
            std::make_shared<utils::MeshHandler>(prm_subsection_path, mpi_comm);

          triangulation->initialize_hypercube(0, actual_edge_length, true);
          triangulation->set_refinement_global(3);
          triangulation->create_mesh();

          triangulation->get_info().print(prm_subsection_path, "", true);

          const auto fe_scalar = triangulation->get_fe_lagrange(3);

          ale_handler_from_file.setup_from_file(
            triangulation, FESystem<dim>(*fe_scalar, 1), 0, true, false);

          ale_handler_from_file.output_results(time, timestep_number);
          ++timestep_number;
          time += time_step;

          {
            TimerOutput::Scope timer_section(
              timer_output,
              prm_subsection_path +
                " / Solve lifting problem / Displacement from file");
            ale_handler_from_file.update(time - initial_time,
                                         time_step,
                                         period);
          }

          {
            TimerOutput::Scope timer_section(
              timer_output,
              prm_subsection_path + " / Move mesh / Displacement from file");
            ale_handler_from_file.move_mesh(time_step);
          }

          ale_handler_from_file.output_results(time, timestep_number);
          ++timestep_number;
          time += time_step;

          {
            TimerOutput::Scope timer_section(
              timer_output,
              prm_subsection_path +
                " / Solve lifting problem / Displacement from file");
            ale_handler_from_file.update(time - initial_time,
                                         time_step,
                                         period);
          }

          {
            TimerOutput::Scope timer_section(
              timer_output,
              prm_subsection_path + " / Move mesh / Displacement from file");
            ale_handler_from_file.move_mesh(time_step);
          }

          ale_handler_from_file.output_results(time, timestep_number);
        }
    }

  private:
    /// ALE handler class, read displacement from Function.
    ALEHandler ale_handler_from_function;
    /// ALE handler class, read displacement from file.
    ALEHandler ale_handler_from_file;

    /// Toggle reading the displacement from file, instead of Function.
    bool prm_disp_from_file;
  };
} // namespace lifex::tests

/// Tests lifting classes and mesh motion.
int
main(int argc, char **argv)
{
  lifex::lifex_init lifex_initializer(argc, argv, 1);

  try
    {
      lifex::tests::TestALE test("Test ALE",
                                 "Test ALE / Arbitrary Lagrangian Eulerian",
                                 "Test ALE / Arbitrary Lagrangian Eulerian");

      test.main_run_generate_from_json({"nondefault"});
    }
  LIFEX_CATCH_EXC();

  return EXIT_SUCCESS;
}
