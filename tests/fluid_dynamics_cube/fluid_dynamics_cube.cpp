/********************************************************************************
  Copyright (C) 2019 - 2023 by the lifex authors.

  This file is part of lifex.

  lifex is free software; you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  lifex is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with lifex.  If not, see <http://www.gnu.org/licenses/>.
********************************************************************************/

/**
 * @file
 *
 * @author Alberto Zingaro <alberto.zingaro@polimi.it>.
 *
 * This file contains both the automatic test of a Beltrami flow as well as
 * features for the simulation of a Taylor-Green vortex in a cube.
 */

#include "core/source/core_model.hpp"
#include "core/source/init.hpp"

#include "core/source/numerics/bc_handler.hpp"

#include "source/fluid_dynamics.hpp"

#include "source/helpers/fluid_dynamics_bcs.hpp"

#include <deal.II/grid/reference_cell.h>

#include <tuple>

namespace lifex::tests
{
  /**
   * @brief Initial condition of the Taylor-Green Vortex benchmark problem.
   *
   * Impose the analytical non-null initial condition of the Taylor-Green
   * Vortex (TGV) benchmark problem. As detailed in @refcite{mastellonec3,
   * Taylor-Green vortex technical report}, this method imposes the following
   * analytical condition to the velocity components and pressure:
   * @f[
   * \begin{aligned}
   * u_x & = \sin(x) \cos(y) \cos(z), \\
   * u_y & = - \cos(x) \sin(y) \cos(z), \\
   * u_z & = 0, \\
   * p & = \frac{\rho}{16} (\cos(2x) + \cos(2y)) (\cos(2z) + 2).
   * \end{aligned}
   * @f]
   */
  class InitialConditionTGV : public Function<dim>
  {
  public:
    /// Constructor.
    InitialConditionTGV(const double &density_)
      : Function(dim + 1)
      , density(density_)
    {}

    /// Override of vector_value method.
    virtual void
    vector_value(const Point<dim> &p, Vector<double> &values) const override
    {
      for (unsigned int i = 0; i < dim + 1; ++i)
        {
          values[i] = 0.0;
        }

      // Setting TGV initial conditions
      values[0] = sin(p[0]) * cos(p[1]) * cos(p[2]);  // x-velocity
      values[1] = -cos(p[0]) * sin(p[1]) * cos(p[2]); // y-velocity
      values[2] = 0.0;                                // z-velocity
      values[3] = density * (cos(2 * p[0]) + cos(2 * p[1])) *
                  (cos(2 * p[2]) + 2) / 16; // pressure
    };

  private:
    const double density; ///< Density.
  };

  /**
   * @brief Exact solution of Beltrami flow benchmark problem.
   *
   * Analytical solution of Beltrami flow benchmark problem as described in
   * @refcite{ethier1994exact, Ethier and Steinman 1994}.
   */
  class ExactSolutionBeltrami : public utils::FunctionDirichlet
  {
  public:
    /// Constructor.
    ExactSolutionBeltrami(const double &density_, const double &viscosity_)
      : utils::FunctionDirichlet(dim + 1)
      , density(density_)
      , viscosity(viscosity_)
    {}

    /// Override of vector_value method.
    virtual void
    vector_value(const Point<dim> &p, Vector<double> &values) const override
    {
      const double pi = M_PI;
      const double t  = this->get_time();
      const double x  = p[0];
      const double y  = p[1];
      const double z  = p[2];

      double temporal_factor =
        pi * exp(-(pi * pi * t * viscosity) / (4 * density));

      // Exact solution: Beltrami's flow
      values[0] =
        -(temporal_factor * (exp((pi * x) / 4) * sin((pi * (y + 2 * z)) / 4) +
                             exp((pi * z) / 4) * cos((pi * (x + 2 * y)) / 4))) /
        4;
      values[1] =
        -(temporal_factor * (exp((pi * y) / 4) * sin((pi * (2 * x + z)) / 4) +
                             exp((pi * x) / 4) * cos((pi * (y + 2 * z)) / 4))) /
        4;
      values[2] =
        -(temporal_factor * (exp((pi * z) / 4) * sin((pi * (x + 2 * y)) / 4) +
                             exp((pi * y) / 4) * cos((pi * (2 * x + z)) / 4))) /
        4;
      values[3] =
        -(density * pi * pi * exp(-(pi * pi * t * viscosity) / (2 * density)) *
          (exp((pi * x) / 2) + exp((pi * y) / 2) + exp((pi * z) / 2) +
           2 * exp((pi * (x + y)) / 4) * cos((pi * (y + 2 * z)) / 4) *
             sin((pi * (2 * x + z)) / 4) +
           2 * exp((pi * (x + z)) / 4) * cos((pi * (x + 2 * y)) / 4) *
             sin((pi * (y + 2 * z)) / 4) +
           2 * exp((pi * (y + z)) / 4) * cos((pi * (2 * x + z)) / 4) *
             sin((pi * (x + 2 * y)) / 4))) /
        32;
    }

    /// Compute gradients.
    void
    vector_gradient(
      const Point<dim> &                   p,
      std::vector<Tensor<1, dim, double>> &gradients) const override
    {
      const double pi = M_PI;
      const double t  = this->get_time();
      const double x  = p[0];
      const double y  = p[1];
      const double z  = p[2];

      double gradient_temporal_factor =
        pi * pi * exp(-(pi * pi * t * viscosity) / (4 * density));

      gradients[0][0] = -(gradient_temporal_factor *
                          (exp((pi * x) / 4) * sin((pi * (y + 2 * z)) / 4) -
                           exp((pi * z) / 4) * sin((pi * (x + 2 * y)) / 4))) /
                        16; // u_x
      gradients[0][1] = (gradient_temporal_factor *
                         (2 * exp((pi * z) / 4) * sin((pi * (x + 2 * y)) / 4) -
                          exp((pi * x) / 4) * cos((pi * (y + 2 * z)) / 4))) /
                        16; // u_y
      gradients[0][2] = -(gradient_temporal_factor *
                          (2 * exp((pi * x) / 4) * cos((pi * (y + 2 * z)) / 4) +
                           exp((pi * z) / 4) * cos((pi * (x + 2 * y)) / 4))) /
                        16; // u_z

      gradients[1][0] =
        -(gradient_temporal_factor *
          (exp((pi * x) / 4) * cos((pi * (y + 2 * z)) / 4) +
           2 * exp((pi * y) / 4) * cos((pi * (2 * x + z)) / 4))) /
        16; // v_x
      gradients[1][1] = (gradient_temporal_factor *
                         (exp((pi * x) / 4) * sin((pi * (y + 2 * z)) / 4) -
                          exp((pi * y) / 4) * sin((pi * (2 * x + z)) / 4))) /
                        16; // v_y
      gradients[1][2] = (gradient_temporal_factor *
                         (2 * exp((pi * x) / 4) * sin((pi * (y + 2 * z)) / 4) -
                          exp((pi * y) / 4) * cos((pi * (2 * x + z)) / 4))) /
                        16; // v_z

      gradients[2][0] = (gradient_temporal_factor *
                         (2 * exp((pi * y) / 4) * sin((pi * (2 * x + z)) / 4) -
                          exp((pi * z) / 4) * cos((pi * (x + 2 * y)) / 4))) /
                        16; // w_x
      gradients[2][1] =
        -(gradient_temporal_factor *
          (exp((pi * y) / 4) * cos((pi * (2 * x + z)) / 4) +
           2 * exp((pi * z) / 4) * cos((pi * (x + 2 * y)) / 4))) /
        16; // w_y
      gradients[2][2] = (gradient_temporal_factor *
                         (exp((pi * y) / 4) * sin((pi * (2 * x + z)) / 4) -
                          exp((pi * z) / 4) * sin((pi * (x + 2 * y)) / 4))) /
                        16; // w_z

      gradients[3][0] =
        -(density * pi * pi * exp(-(pi * pi * t * viscosity) / (2 * density)) *
          ((pi * exp((pi * x) / 2)) / 2 +
           pi * exp((pi * (x + y)) / 4) * cos((pi * (2 * x + z)) / 4) *
             cos((pi * (y + 2 * z)) / 4) +
           (pi * exp((pi * (y + z)) / 4) * cos((pi * (x + 2 * y)) / 4) *
            cos((pi * (2 * x + z)) / 4)) /
             2 +
           (pi * exp((pi * (x + y)) / 4) * cos((pi * (y + 2 * z)) / 4) *
            sin((pi * (2 * x + z)) / 4)) /
             2 +
           (pi * exp((pi * (x + z)) / 4) * cos((pi * (x + 2 * y)) / 4) *
            sin((pi * (y + 2 * z)) / 4)) /
             2 -
           (pi * exp((pi * (x + z)) / 4) * sin((pi * (x + 2 * y)) / 4) *
            sin((pi * (y + 2 * z)) / 4)) /
             2 -
           pi * exp((pi * (y + z)) / 4) * sin((pi * (x + 2 * y)) / 4) *
             sin((pi * (2 * x + z)) / 4))) /
        32; // p_x
      gradients[3][1] =
        -(density * pi * pi * exp(-(pi * pi * t * viscosity) / (2 * density)) *
          ((pi * exp((pi * y) / 2)) / 2 +
           (pi * exp((pi * (x + z)) / 4) * cos((pi * (x + 2 * y)) / 4) *
            cos((pi * (y + 2 * z)) / 4)) /
             2 +
           pi * exp((pi * (y + z)) / 4) * cos((pi * (x + 2 * y)) / 4) *
             cos((pi * (2 * x + z)) / 4) +
           (pi * exp((pi * (x + y)) / 4) * cos((pi * (y + 2 * z)) / 4) *
            sin((pi * (2 * x + z)) / 4)) /
             2 +
           (pi * exp((pi * (y + z)) / 4) * cos((pi * (2 * x + z)) / 4) *
            sin((pi * (x + 2 * y)) / 4)) /
             2 -
           (pi * exp((pi * (x + y)) / 4) * sin((pi * (2 * x + z)) / 4) *
            sin((pi * (y + 2 * z)) / 4)) /
             2 -
           pi * exp((pi * (x + z)) / 4) * sin((pi * (x + 2 * y)) / 4) *
             sin((pi * (y + 2 * z)) / 4))) /
        32; // p_y
      gradients[3][2] =
        -(density * pi * pi * exp(-(pi * pi * t * viscosity) / (2 * density)) *
          ((pi * exp((pi * z) / 2)) / 2 +
           (pi * exp((pi * (x + y)) / 4) * cos((pi * (2 * x + z)) / 4) *
            cos((pi * (y + 2 * z)) / 4)) /
             2 +
           pi * exp((pi * (x + z)) / 4) * cos((pi * (x + 2 * y)) / 4) *
             cos((pi * (y + 2 * z)) / 4) +
           (pi * exp((pi * (x + z)) / 4) * cos((pi * (x + 2 * y)) / 4) *
            sin((pi * (y + 2 * z)) / 4)) /
             2 +
           (pi * exp((pi * (y + z)) / 4) * cos((pi * (2 * x + z)) / 4) *
            sin((pi * (x + 2 * y)) / 4)) /
             2 -
           pi * exp((pi * (x + y)) / 4) * sin((pi * (2 * x + z)) / 4) *
             sin((pi * (y + 2 * z)) / 4) -
           (pi * exp((pi * (y + z)) / 4) * sin((pi * (x + 2 * y)) / 4) *
            sin((pi * (2 * x + z)) / 4)) /
             2)) /
        32; // p_z
    }

  private:
    const double density;   ///< Density.
    const double viscosity; ///< Viscosity.
  };

  /**
   * @brief Neumann datum for Beltrami flow benchmark problem.
   */
  class ExactNeumannDatumBeltrami : public utils::FunctionNeumann
  {
  public:
    /// Constructor.
    ExactNeumannDatumBeltrami(const double &density_, const double &viscosity_)
      : utils::FunctionNeumann(dim + 1)
      , density(density_)
      , viscosity(viscosity_)
    {}

    /// Override of vector_value method.
    virtual double
    value(const Point<dim> &p, const unsigned int coord = 0) const override
    {
      double pi = M_PI;
      double t  = this->get_time();
      double x  = p[0];
      double y  = p[1];
      double z  = p[2];

      // Exact Neumann solution on left face.
      if (coord == 0) // u_x
        return (pi * pi * viscosity *
                exp(-(pi * pi * t * viscosity) / (4 * density)) *
                (exp((pi * x) / 4) * sin((pi * (y + 2 * z)) / 4) -
                 exp((pi * z) / 4) * sin((pi * (x + 2 * y)) / 4))) /
                 16 -
               (density * pi * pi *
                exp(-(pi * pi * t * viscosity) / (2 * density)) *
                (exp((pi * x) / 2) + exp((pi * y) / 2) + exp((pi * z) / 2) +
                 2 * exp((pi * (x + y)) / 4) * cos((pi * (y + 2 * z)) / 4) *
                   sin((pi * (2 * x + z)) / 4) +
                 2 * exp((pi * (x + z)) / 4) * cos((pi * (x + 2 * y)) / 4) *
                   sin((pi * (y + 2 * z)) / 4) +
                 2 * exp((pi * (y + z)) / 4) * cos((pi * (2 * x + z)) / 4) *
                   sin((pi * (x + 2 * y)) / 4))) /
                 32;
      else if (coord == 1) // u_y
        return (pi * pi * viscosity *
                exp(-(pi * pi * t * viscosity) / (4 * density)) *
                (exp((pi * x) / 4) * cos((pi * (y + 2 * z)) / 4) +
                 2 * exp((pi * y) / 4) * cos((pi * (2 * x + z)) / 4))) /
               16;
      else if (coord == 2) // u_z
        return -(pi * pi * viscosity *
                 exp(-(pi * pi * t * viscosity) / (4 * density)) *
                 (2 * exp((pi * y) / 4) * sin((pi * (2 * x + z)) / 4) -
                  exp((pi * z) / 4) * cos((pi * (x + 2 * y)) / 4))) /
               16;
      else
        return 0.0;
    }

  private:
    const double density;   ///< Density.
    const double viscosity; ///< Viscosity.
  };

  /**
   * @brief Test class to run the fluid dynamics model in a cube.
   *
   * This test can be employed to simulate different benchmark problems on a
   * cubic domain, selected through the parameter `Benchmark problem`. The
   * specifics of each of the implemented problems are detailed below.
   * - <b>Taylor-Green vortex </b>: this benchmark is
   * set in a cubic domain of length @f$ 2 \pi L@f$, with @f$L=1@f$, starting
   * from a non-null analytical condition (as defined
   * in @ref InitialConditionTGV) and periodic type boundary conditions.
   * For additional details on this benchmark, please refer to
   * @refcite{taylor1937mechanism, Taylor and Green (1937)} to
   * @refcite{mastellonec3, Taylor-Green vortex technical report} for the
   * description of the setup and the reference data available in the
   * literature.
   * - <b> Beltrami flow </b>: this benchmark is set in a cubic domain of length
   * @f$L=1@f$, starting from a non-null analytical condition (as defined
   * in @ref ExactSolutionBeltrami), with Dirichlet boundary conditions on five
   * sides (as defined in @ref ExactSolutionBeltrami) and non-homogeneous
   * Neumann boundary condition on the remaining side (as defined in @ref
   * ExactNeumannDatumBeltrami). Please refer to @refcite{ethier1994exact,
   * Ethier and Steinman 1994} for the whole description of the benchmark
   * problem.
   * - <b> Lid-driven cavity </b>: @todo Implement lid-driven cavity benchmark
   * problem.
   */
  class TestFluidDynamicsCube : public CoreModel
  {
  public:
    /// Constructor.
    TestFluidDynamicsCube(const std::string &subsection,
                          const std::string &subsection_fluid)
      : CoreModel(subsection)
      , fluid_dynamics(subsection_fluid)
    {}

    /// Override of @ref CoreModel::declare_parameters.
    virtual void
    declare_parameters(ParamHandler &params) const override
    {
      // Declare parameters.
      params.enter_subsection_path(prm_subsection_path);

      params.declare_entry_selection(
        "Benchmark problem",
        "Beltrami flow",
        "Taylor-Green vortex | Beltrami flow | Lid-driven cavity",
        "Benchmark problem to be run.");

      /// These default values for tolerances are chosen by increasing the
      /// relative errors computed with the petsc linear algebra backend.
      /// Numerical tests showed that the errors with trilinos are slightly
      /// lower.
      params.declare_entry(
        "Tolerances for Beltrami flow test",
        "4.241e-02, 1.603e-01, 6.868e-02",
        Patterns::List(Patterns::Double(0)),
        "Tolerances for relative error norms for Beltrami flow test (L2 "
        "velocity, H1 velocity, L2 "
        "pressure error norms).");

      params.leave_subsection_path();

      // Dependencies.
      {
        fluid_dynamics.declare_parameters(params);
      }
    }

    /// Override of @ref CoreModel::parse_parameters.
    virtual void
    parse_parameters(ParamHandler &params) override
    {
      // Parse input file.
      params.parse();

      // Read input parameters.
      params.enter_subsection_path(prm_subsection_path);

      prm_benchmark_problem = params.get("Benchmark problem");

      prm_relative_error_norms =
        params.get_vector<double>("Tolerances for Beltrami flow test", 3);

      params.leave_subsection_path();

      // Dependencies.
      {
        fluid_dynamics.parse_parameters(params);
      }
    }

    /// Run the test.
    virtual void
    run() override
    {
      pcout << "Fluid dynamics in a cube" << std::endl;

      if (prm_benchmark_problem == "Taylor-Green vortex")
        {
          pcout << "Taylor-Green Vortex benchmark" << std::endl;

          /// In the Taylor-Green vortex benchmark problem, the characteristic
          /// velocity and length are always taken equal to 1. Thus, the
          /// Reynolds number is simply defined as the ratio between density and
          /// viscosity.
          const double reynolds =
            fluid_dynamics.get_density() / fluid_dynamics.get_viscosity();
          pcout << "Taylor-Green vortex benchmark, with Reynolds = " << reynolds
                << std::endl;

          fluid_dynamics.set_initial_condition(
            std::make_shared<InitialConditionTGV>(
              fluid_dynamics.get_density()));

          // Set boundary conditions.
          fluid_dynamics.set_BCs_periodic(
            {FluidDynamics::PeriodicBC({{0, 1}}, 0),
             FluidDynamics::PeriodicBC({{2, 3}}, 1),
             FluidDynamics::PeriodicBC({{4, 5}}, 2)});
        }
      else if (prm_benchmark_problem == "Beltrami flow")
        {
          pcout << "Beltrami flow benchmark" << std::endl;

          /// Set initial conditions.
          fluid_dynamics.set_initial_condition(
            std::make_shared<ExactSolutionBeltrami>(
              fluid_dynamics.get_density(), fluid_dynamics.get_viscosity()));

          // Build BCs.

          // Neumann BC on left face.
          std::vector<utils::BC<utils::FunctionNeumann>> bc_neumann_beltrami;

          bc_neumann_beltrami.emplace_back(
            0,
            std::make_shared<ExactNeumannDatumBeltrami>(
              fluid_dynamics.get_density(), fluid_dynamics.get_viscosity()),
            ComponentMask({true, true, true, false}));

          // Dirichlet BCs on remaining faces.
          std::vector<utils::BC<utils::FunctionDirichlet>>
            bc_dirichlet_beltrami;

          for (unsigned int i = 1; i < ReferenceCells::Hexahedron.n_faces();
               ++i)
            {
              utils::BC<utils::FunctionDirichlet> bc_dirichlet_beltrami_i(
                i,
                std::make_shared<ExactSolutionBeltrami>(
                  fluid_dynamics.get_density(), fluid_dynamics.get_viscosity()),
                ComponentMask({true, true, true, false}));

              bc_dirichlet_beltrami.emplace_back(bc_dirichlet_beltrami_i);
            }

          // Set BCs.
          fluid_dynamics.set_BCs_dirichlet(
            bc_dirichlet_beltrami,
            // No Dirichlet normal flux BCs.
            std::vector<utils::BC<utils::FunctionDirichlet>>{},
            // Dirichlet tangential flux BCs.
            std::vector<utils::BC<utils::FunctionDirichlet>>{});

          fluid_dynamics.set_BCs_neumann(bc_neumann_beltrami);
        }
      else if (prm_benchmark_problem == "Lid-driven cavity")
        {
          AssertThrow(false, ExcLifexNotImplemented());
        }
      else
        {
          AssertThrow(false,
                      ExcMessage(prm_benchmark_problem +
                                 " is not a viable option."));
        }

      fluid_dynamics.run();

      if (prm_benchmark_problem == "Beltrami flow")
        {
          ExactSolutionBeltrami exact_solution_beltrami(
            fluid_dynamics.get_density(), fluid_dynamics.get_viscosity());

          // Compute absolute errors.
          const std::tuple<double, double, double> absolute_errors =
            fluid_dynamics.integrate_difference(exact_solution_beltrami, false);

          pcout << "\n\nAbsolute errors at final time: " << std::endl;
          pcout << std::endl << std::scientific;
          pcout << "  Velocity L2 error = " << std::get<0>(absolute_errors)
                << std::endl;
          pcout << "  Velocity H1 error = " << std::get<1>(absolute_errors)
                << std::endl;
          pcout << "  Pressure L2 error = " << std::get<2>(absolute_errors)
                << std::endl;

          // Compute relative errors
          const std::tuple<double, double, double> relative_errors =
            fluid_dynamics.integrate_difference(exact_solution_beltrami, true);

          pcout << "\n\nRelative errors at final time: " << std::endl;
          pcout << std::endl << std::scientific;
          pcout << "  Velocity L2 error = " << std::get<0>(relative_errors)
                << std::endl;
          pcout << "  Velocity H1 error = " << std::get<1>(relative_errors)
                << std::endl;
          pcout << "  Pressure L2 error = " << std::get<2>(relative_errors)
                << std::endl;

          AssertThrow(
            (std::get<0>(relative_errors) < prm_relative_error_norms[0] &&
             std::get<1>(relative_errors) < prm_relative_error_norms[1] &&
             std::get<2>(relative_errors) < prm_relative_error_norms[2]),
            ExcMessage("Test failed: relative error greater than tolerance."))
        }
    }

  private:
    /// Fluid dynamics model.
    FluidDynamics fluid_dynamics;

    /// Exact solution of Beltrami flow benchmark problem.
    std::unique_ptr<ExactSolutionBeltrami> exact_solution_beltrami;

    /// Exact Neumann datum of Beltrami flow benchmark problem.
    std::unique_ptr<ExactNeumannDatumBeltrami> neumann_datum_beltrami;

    /// @name Parameters read from file.
    /// @{

    /// Benchmark problem.
    std::string prm_benchmark_problem;

    /// Vectors of error norms.
    std::vector<double> prm_relative_error_norms;

    /// @}
  };
} // namespace lifex::tests


/// Parse parameter files and run the test.
int
main(int argc, char **argv)
{
  lifex::lifex_init lifex_initializer(argc, argv, 1);

  try
    {
      lifex::tests::TestFluidDynamicsCube test("Test fluid dynamics cube",
                                               "Fluid dynamics");

#if defined(LIN_ALG_PETSC)
      test.main_run_generate([&test]() {
        test.generate_parameters_from_json({"nondefault", "petsc"});
      });
#else
      test.main_run_generate(
        [&test]() { test.generate_parameters_from_json({"nondefault"}); });
#endif
    }
  LIFEX_CATCH_EXC();

  return EXIT_SUCCESS;
}
