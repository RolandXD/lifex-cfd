/********************************************************************************
  Copyright (C) 2021 - 2023 by the lifex authors.

  This file is part of lifex.

  lifex is free software; you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  lifex is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with lifex.  If not, see <http://www.gnu.org/licenses/>.
********************************************************************************/

/**
 * @file
 *
 * @author Pasquale Claudio Africa <pasqualeclaudio.africa@polimi.it>.
 * @author Michele Bucelli <michele.bucelli@polimi.it>.
 */

#include "core/source/init.hpp"

#include "apps/fluid_dynamics/app_fluid_dynamics.hpp"

/// Parse parameters and run fluid dynamics model.
int
main(int argc, char **argv)
{
  // Declare and parse additional command-line options
  // for handling multiple inlet and outlet surfaces.
  std::vector<std::string> boundary_names;

  const clipp::group cli =
    (("Boundary conditions:") %
     ((clipp::option("-b", "--boundary-labels") &
       clipp::values("boundary labels", boundary_names)) %
        ("List of labels for boundary portions where boundary conditions are "
         "imposed [default: \"\"]."),
      // Explicitly create a group by adding a second (empty) parameter.
      clipp::parameter()));

  lifex::lifex_init lifex_initializer(argc, argv, 1, true, cli);

  try
    {
      lifex::AppFluidDynamics model("Fluid dynamics", boundary_names);

      model.main_run_generate();
    }
  LIFEX_CATCH_EXC();

  return EXIT_SUCCESS;
}
