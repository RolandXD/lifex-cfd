/********************************************************************************
  Copyright (C) 2021 - 2023 by the lifex authors.

  This file is part of lifex.

  lifex is free software; you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  lifex is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with lifex.  If not, see <http://www.gnu.org/licenses/>.
********************************************************************************/

/**
 * @file
 *
 * @author Pasquale Claudio Africa <pasqualeclaudio.africa@polimi.it>.
 * @author Michele Bucelli <michele.bucelli@polimi.it>.
 */

#include "apps/fluid_dynamics/app_fluid_dynamics.hpp"

namespace lifex
{
  AppFluidDynamics::AppFluidDynamics(
    const std::string &             subsection,
    const std::vector<std::string> &boundary_names_)
    : CoreModel(subsection)
    , fluid_dynamics(subsection)
    , boundary_names(boundary_names_)
    , dirichlet_bcs(0)
    , neumann_bcs(0)
    , resistance_bcs(0)
    , prm_boundary_tags(0)
    , prm_boundary_types(0)
  {
    for (const auto &name : boundary_names)
      {
        // Each surface can be of Dirichlet, Womersley, Neumann or Resistance
        // type.
        dirichlet_bcs.push_back(std::make_shared<FluidDynamicsBCs::FlowBC>(
          subsection + " / Boundary conditions / " + name + " / Dirichlet",
          std::initializer_list<std::string>(
            {FluidDynamicsBCs::Constant::label,
             FluidDynamicsBCs::Ramp::label,
             FluidDynamicsBCs::Pulsatile::label,
             FluidDynamicsBCs::CSVFunction::label}),
          FluidDynamicsBCs::Pulsatile::label,
          std::initializer_list<std::string>(
            {FluidDynamicsBCs::Parabolic::label,
             FluidDynamicsBCs::UniformVelocity::label}),
          FluidDynamicsBCs::Parabolic::label,
          fluid_dynamics));

        womersley_bcs.push_back(std::make_shared<FluidDynamicsBCs::WomersleyBC>(
          subsection + " / Boundary conditions / " + name + " / Womersley",
          fluid_dynamics));

        neumann_bcs.push_back(std::make_shared<FluidDynamicsBCs::PressureBC>(
          subsection + " / Boundary conditions / " + name + " / Neumann",
          std::initializer_list<std::string>(
            {FluidDynamicsBCs::Constant::label,
             FluidDynamicsBCs::Ramp::label,
             FluidDynamicsBCs::Pulsatile::label,
             FluidDynamicsBCs::CSVFunction::label}),
          FluidDynamicsBCs::Pulsatile::label,
          fluid_dynamics));

        resistance_bcs.push_back(
          std::make_shared<FluidDynamicsBCs::ResistanceBC>(
            subsection + " / Boundary conditions / " + name + " / Resistance",
            std::initializer_list<std::string>(
              {FluidDynamicsBCs::Constant::label,
               FluidDynamicsBCs::Ramp::label,
               FluidDynamicsBCs::Pulsatile::label,
               FluidDynamicsBCs::CSVFunction::label}),
            FluidDynamicsBCs::Constant::label,
            std::initializer_list<std::string>(
              {FluidDynamicsBCs::FlowIntegralEvaluation::label,
               FluidDynamicsBCs::CSVFunction::label}),
            FluidDynamicsBCs::FlowIntegralEvaluation::label,
            fluid_dynamics));
      }
  }

  void
  AppFluidDynamics::declare_parameters(ParamHandler &params) const
  {
    params.enter_subsection_path(prm_subsection_path);
    {
      params.declare_entry("Forcing term",
                           "0, 0, 0",
                           Patterns::List(Patterns::Double(), dim, dim),
                           "Constant forcing term (e.g. gravity).");

      params.enter_subsection("Boundary conditions");
      {
        const std::string bc_types = "Dirichlet|Womersley|Neumann|Resistance";

        params.declare_entry(
          "No-slip tags",
          "0",
          Patterns::List(Patterns::Integer(0)),
          "Tags of boundaries where a no-slip condition is imposed.");

        params.declare_entry(
          "Free-slip tags",
          "",
          Patterns::List(Patterns::Integer(0)),
          "Tags of fixed boundaries where a free-slip condition is imposed. "
          "Free-slip conditions on moving boundaries are currently not "
          "supported.");

        for (unsigned int i = 0; i < boundary_names.size(); ++i)
          {
            params.enter_subsection(boundary_names[i]);
            {
              params.declare_entry("Tags",
                                   std::to_string(i + 1),
                                   Patterns::List(Patterns::Integer(0)),
                                   "Tags of the boundary.");

              params.declare_entry_selection(
                "Type of boundary condition",
                "Neumann",
                bc_types + "|Switching",
                "Dirichlet conditions impose a parabolic or flat velocity "
                "profile (positive when inflow); Womersley impose a Womersley "
                "profile with a prescribed flow rate (on circular boundaries); "
                "Neumann conditions a uniform normal stress; Resistance "
                "conditions a uniform normal stress that increases linearly "
                "with outgoing flowrate. Switching sets conditions that "
                "change type over time, among the previous ones.");

              params.declare_entry("Zero tangential velocity",
                                   "false",
                                   Patterns::Bool(),
                                   "If true, the tangential component of "
                                   "velocity is constrained to zero (only for"
                                   "Neumann and Resistance conditions).");

              params.enter_subsection("Switching BCs");
              {
                params.declare_entry(
                  "Sequence",
                  "",
                  Patterns::List(Patterns::Selection(bc_types + "|No-slip")),
                  "Sequence of boundary condition types for the switching BC "
                  "case. Different BC types will be applied over time, in the "
                  "order in which that they appear here. Possible options "
                  "are: " +
                    bc_types + "|No-slip");

                params.declare_entry(
                  "Switch times",
                  "",
                  Patterns::List(Patterns::Double()),
                  "Times at which BC switching occurs. They must be as many as "
                  "the number of BCs in the sequence. The last time is the "
                  "time at which the sequence is restarted from its first "
                  "element. Switching times are relative to the period of the "
                  "simulation.");
              }
              params.leave_subsection();
            }
            params.leave_subsection();
          }

        params.enter_subsection("Periodic conditions");
        {
          params.declare_entry(
            "Tags",
            "",
            Patterns::List(Patterns::List(Patterns::Integer(0), 2, 2, " "), 0),
            "A comma-separated list of pairs of tags where periodic boundary "
            "conditions are imposed. Notice that periodic BCs are supported "
            "only for parallel, conforming boundary surfaces in hexahedral "
            "meshes. Example: \"0 1, 2 3\" for the pairs (0, 1) "
            "and (2, 3).");

          params.declare_entry(
            "Normal axes",
            "",
            Patterns::List(Patterns::Integer(0, dim - 1)),
            "Indices (0, 1, 2) of the axes normal to the periodic faces.");
        }
        params.leave_subsection();
      }
      params.leave_subsection();

      params.enter_subsection("Initial conditions");
      {
        params.declare_entry_selection(
          "Type",
          "Zero",
          "Zero|File",
          "Type of initial conditions. Zero starts from zero velocity, while "
          "File reads the initial velocity from a VTU file. No initial "
          "conditions are prescribed for pressure.");

        params.enter_subsection("File");
        {
          params.declare_entry("File name",
                               "",
                               Patterns::FileName(
                                 Patterns::FileName::FileType::input),
                               "File from which the initial velocity is read.");

          params.declare_entry("Array name",
                               "velocity",
                               Patterns::Anything(),
                               "Name of the array in the input file that "
                               "stores the initial velocity.");
        }
        params.leave_subsection();
      }
      params.leave_subsection();
    }
    params.leave_subsection_path();

    // Dependencies.
    {
      fluid_dynamics.declare_parameters(params);

      for (auto bc : dirichlet_bcs)
        bc->declare_parameters(params);
      for (auto bc : womersley_bcs)
        bc->declare_parameters(params);
      for (auto bc : neumann_bcs)
        bc->declare_parameters(params);
      for (auto bc : resistance_bcs)
        bc->declare_parameters(params);
    }
  }

  void
  AppFluidDynamics::parse_parameters(ParamHandler &params)
  {
    // Parse input file.
    params.parse();

    // Read input parameters.
    params.enter_subsection_path(prm_subsection_path);
    {
      std::vector<double> forcing_term_ =
        params.get_vector<double>("Forcing term");
      forcing_term_.push_back(0.0);
      prm_forcing_term =
        Vector<double>(forcing_term_.begin(), forcing_term_.end());

      params.enter_subsection("Boundary conditions");
      {
        prm_tags_no_slip =
          params.get_vector<types::boundary_id>("No-slip tags");
        prm_tags_free_slip =
          params.get_vector<types::boundary_id>("Free-slip tags");

        for (auto name : boundary_names)
          {
            params.enter_subsection(name);

            prm_boundary_tags.push_back(
              params.get_set<types::boundary_id>("Tags"));
            prm_boundary_types.push_back(
              params.get("Type of boundary condition"));
            prm_zero_tangential_flux.push_back(
              params.get_bool("Zero tangential velocity"));

            if (prm_boundary_types.back() == "Switching")
              {
                params.enter_subsection("Switching BCs");

                switching_bcs_enabled.push_back(true);

                auto sequence = params.get_vector<std::string>("Sequence");
                auto times    = params.get_vector<double>("Switch times");

                AssertThrow(!sequence.empty(),
                            ExcMessage("In subsection " + prm_subsection_path +
                                       " / Boundary conditions / " + name +
                                       " / Switching BCs, the lists "
                                       "\"Sequence\" must not be empty."));

                // "Sequence" and "Switch times" must have the same length.
                AssertThrow(
                  sequence.size() == times.size(),
                  ExcMessage(
                    "In subsection " + prm_subsection_path +
                    " / Boundary conditions / " + name +
                    " / Switching BCs, the lists \"Sequence\" and \"Switch "
                    "times\" must have the same number of elements."));

                prm_switching_bcs_sequence.push_back(sequence);
                prm_switching_bcs_times.push_back(times);

                params.leave_subsection();
              }
            else
              {
                switching_bcs_enabled.push_back(false);
                // We add empty vectors to make sure that
                // prm_switching_bcs_sequence and prm_switching_bcs_times have
                // the same size as boundary_names.
                prm_switching_bcs_sequence.push_back({});
                prm_switching_bcs_times.push_back({});
              }

            switching_bcs_current_indices.push_back(0);

            // Initialize counter for switching bc reset.
            counter_switching_bc_reset.push_back(0);

            params.leave_subsection();
          }

        params.enter_subsection("Periodic conditions");
        {
          const std::vector<std::string> tmp_tag_pairs =
            params.get_vector<std::string>("Tags");
          const std::vector<unsigned int> tmp_axes =
            params.get_vector<unsigned int>("Normal axes");

          AssertThrow(
            tmp_tag_pairs.size() == tmp_axes.size(),
            ExcMessage(
              "The lists Periodic conditions / Tags and Periodic conditions "
              "/ Normal axes must have the same number of elements."));

          for (unsigned int i = 0; i < tmp_tag_pairs.size(); ++i)
            {
              const std::vector<types::boundary_id> tmp_tags =
                utils::string_to_vector<types::boundary_id>(tmp_tag_pairs[i],
                                                            2,
                                                            false,
                                                            " ");
              prm_periodic_bcs.emplace_back(tmp_tags[0],
                                            tmp_tags[1],
                                            tmp_axes[i]);
            }
        }
        params.leave_subsection();
      }
      params.leave_subsection();

      params.enter_subsection("Initial conditions");
      {
        prm_initial_condition_type = params.get("Type");

        if (prm_initial_condition_type == "File")
          {
            params.enter_subsection("File");
            prm_initial_condition_file_name  = params.get("File name");
            prm_initial_condition_array_name = params.get("Array name");
            params.leave_subsection();
          }
      }
      params.leave_subsection();
    }
    params.leave_subsection_path();

    // Dependencies.
    {
      fluid_dynamics.parse_parameters(params);

      for (unsigned int i = 0; i < boundary_names.size(); ++i)
        {
          // Dirichlet conditions are parsed if the boundary type is set to
          // "Dirichlet", or if it is set to "Switching" and the switching
          // sequence contains "Dirichlet". Same goes for other BC types.

          if (prm_boundary_types[i] == "Dirichlet" ||
              (prm_boundary_types[i] == "Switching" &&
               utils::contains(prm_switching_bcs_sequence[i], "Dirichlet")))
            dirichlet_bcs[i]->parse_parameters(params);

          if (prm_boundary_types[i] == "Womersley" ||
              (prm_boundary_types[i] == "Switching" &&
               utils::contains(prm_switching_bcs_sequence[i], "Womersley")))
            womersley_bcs[i]->parse_parameters(params);

          if (prm_boundary_types[i] == "Neumann" ||
              (prm_boundary_types[i] == "Switching" &&
               utils::contains(prm_switching_bcs_sequence[i], "Neumann")))
            neumann_bcs[i]->parse_parameters(params);

          if (prm_boundary_types[i] == "Resistance" ||
              (prm_boundary_types[i] == "Switching" &&
               utils::contains(prm_switching_bcs_sequence[i], "Resistance")))
            resistance_bcs[i]->parse_parameters(params);
        }
    }

    const unsigned int n_periods =
      utils::is_positive(fluid_dynamics.get_period()) ?
        static_cast<unsigned int>(
          std::floor(fluid_dynamics.get_time() / fluid_dynamics.get_period())) :
        0;

    // For switching BCs, we find the current index based on initial time.
    for (unsigned int i = 0; i < switching_bcs_enabled.size(); ++i)
      {
        if (!switching_bcs_enabled[i])
          continue;

        const double time_period =
          std::fmod(fluid_dynamics.get_time(), fluid_dynamics.get_period());

        while (prm_switching_bcs_times[i][switching_bcs_current_indices[i]] <
               time_period)
          ++switching_bcs_current_indices[i];

        // Set BC type according to the current element in the sequence.
        prm_boundary_types[i] =
          prm_switching_bcs_sequence[i][switching_bcs_current_indices[i]];

        counter_switching_bc_reset[i] = n_periods;
      }
  }

  void
  AppFluidDynamics::run()
  {
    // Boundary conditions.
    setup_bcs();

    // Initial conditions.
    if (prm_initial_condition_type == "File")
      {
        const auto initial_condition = std::make_shared<utils::VTKFunction>(
          prm_initial_condition_file_name,
          utils::VTKDataType::UnstructuredGrid,
          /* scaling_factor = */ 1.0,
          /* geometry_scaling_factor = */ 1.0,
          /* data_is_vectorial = */ true);

        initial_condition->setup_as_linear_projection(
          prm_initial_condition_array_name);

        fluid_dynamics.set_initial_condition(initial_condition);
      }

    // Forcing term.
    fluid_dynamics.set_forcing_term(
      std::make_shared<Functions::ConstantFunction<dim>>(prm_forcing_term));

    // Callback for the end of each timestep, to deal with switching BCs.
    auto end_timestep_callback = [this]() {
      bool switching_has_happened = false;

      for (unsigned int i = 0; i < switching_bcs_enabled.size(); ++i)
        {
          if (!switching_bcs_enabled[i])
            continue;

          // If the current time is greater or equal than the next switch
          // time, we switch to the next BC.
          if (!utils::is_definitely_less_than(
                fluid_dynamics.get_time(),
                prm_switching_bcs_times[i][switching_bcs_current_indices[i]] +
                  fluid_dynamics.get_period() * counter_switching_bc_reset[i]))
            {
              ++switching_bcs_current_indices[i];

              // If we reached the end of the sequence, we go back to the
              // beginning.
              if (switching_bcs_current_indices[i] >=
                  prm_switching_bcs_sequence[i].size())
                {
                  switching_bcs_current_indices[i] = 0;
                  ++counter_switching_bc_reset[i];
                }

              pcout << "    Boundary " << boundary_names[i]
                    << " switching from " << prm_boundary_types[i] << " to ";

              // We update the BC type for current boundary.
              prm_boundary_types[i] =
                prm_switching_bcs_sequence[i][switching_bcs_current_indices[i]];

              pcout << prm_boundary_types[i] << std::endl;

              switching_has_happened = true;
            }
        }

      if (switching_has_happened)
        setup_bcs();
    };

    fluid_dynamics.set_end_timestep_callback(end_timestep_callback);
    fluid_dynamics.run();
  }

  void
  AppFluidDynamics::setup_bcs()
  {
    TimerOutput::Scope timer_section(timer_output,
                                     prm_subsection_path + " / Setup BCs");

    std::vector<utils::BC<utils::FunctionDirichlet>> bcs_dir{};
    std::vector<utils::BC<utils::FunctionNeumann>>   bcs_neu{};
    std::vector<utils::BC<utils::FunctionDirichlet>> bcs_dir_normal{};
    std::vector<utils::BC<utils::FunctionDirichlet>> bcs_dir_tangent{};

    std::vector<types::boundary_id> no_slip_tags = prm_tags_no_slip;

    ComponentMask mask_velocity(dim + 1, true);
    mask_velocity.set(dim, false);

    for (size_t i = 0; i < boundary_names.size(); ++i)
      {
        for (const auto &id : prm_boundary_tags[i])
          {
            if (prm_boundary_types[i] == "Dirichlet")
              bcs_dir.emplace_back(id, dirichlet_bcs[i], mask_velocity);
            else if (prm_boundary_types[i] == "Womersley")
              bcs_dir.emplace_back(id, womersley_bcs[i], mask_velocity);
            else if (prm_boundary_types[i] == "Neumann")
              bcs_neu.emplace_back(id, neumann_bcs[i], mask_velocity);
            else if (prm_boundary_types[i] == "Resistance")
              bcs_neu.emplace_back(id, resistance_bcs[i], mask_velocity);
            else // if (prm_boundary_types[i] == "No-slip")
              no_slip_tags.emplace_back(id);

            if (prm_zero_tangential_flux[i] &&
                (prm_boundary_types[i] == "Neumann" ||
                 prm_boundary_types[i] == "Resistance"))
              {
                bcs_dir_tangent.emplace_back(
                  id,
                  std::make_shared<utils::ZeroBCFunction>(dim),
                  ComponentMask({true, true, true}));
              }
          }
      }

    // Wall tags are interpreted as homogeneous Dirichlet BCs (u = 0) if ALE
    // is not enabled.
    if (!fluid_dynamics.ale_enabled())
      {
        for (size_t i = 0; i < no_slip_tags.size(); ++i)
          bcs_dir.emplace_back(no_slip_tags[i],
                               std::make_shared<utils::ZeroBCFunction>(dim + 1),
                               mask_velocity);
      }

    // Free-slip tags.
    for (const auto &tag : prm_tags_free_slip)
      bcs_dir_normal.emplace_back(tag,
                                  std::make_shared<utils::ZeroBCFunction>(dim),
                                  ComponentMask({true, true, true}));

    // Check that no tag has multiple BCs imposed on it.
    if (!setup_bcs_called)
      {
        std::set<types::boundary_id> all_tags;
        std::set<types::boundary_id> duplicated_tags;

        auto found_tag = [&all_tags,
                          &duplicated_tags](const types::boundary_id &tag) {
          bool unique                   = false;
          std::tie(std::ignore, unique) = all_tags.insert(tag);
          if (!unique)
            duplicated_tags.insert(tag);
        };

        for (const auto &bc : bcs_dir)
          found_tag(bc.boundary_id);
        for (const auto &bc : bcs_dir_normal)
          found_tag(bc.boundary_id);
        for (const auto &bc : bcs_dir_tangent)
          found_tag(bc.boundary_id);
        for (const auto &bc : bcs_neu)
          found_tag(bc.boundary_id);
        for (const auto &bc : prm_periodic_bcs)
          {
            found_tag(bc.tags[0]);
            found_tag(bc.tags[1]);
          }
        if (fluid_dynamics.ale_enabled())
          for (const auto &tag : no_slip_tags)
            found_tag(tag);

        std::string duplicated_tags_str;
        for (const types::boundary_id &tag : duplicated_tags)
          duplicated_tags_str += std::to_string(tag) + " ";
        AssertThrow(duplicated_tags.empty(),
                    ExcMessage("More than one boundary condition is imposed on "
                               "the following tags: " +
                               duplicated_tags_str));
      }

    fluid_dynamics.set_BCs_dirichlet(bcs_dir, bcs_dir_normal, bcs_dir_tangent);
    fluid_dynamics.set_BCs_neumann(bcs_neu);
    fluid_dynamics.set_BCs_periodic(prm_periodic_bcs);

    if (fluid_dynamics.ale_enabled())
      {
        fluid_dynamics.set_BCs_ale(
          std::set<types::boundary_id>(no_slip_tags.begin(),
                                       no_slip_tags.end()));
      }

    if (setup_bcs_called)
      fluid_dynamics.reload_BCs();

    setup_bcs_called = true;
  }
} // namespace lifex
