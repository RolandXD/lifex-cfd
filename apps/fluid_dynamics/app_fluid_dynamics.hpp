/********************************************************************************
  Copyright (C) 2021 - 2023 by the lifex authors.

  This file is part of lifex.

  lifex is free software; you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  lifex is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with lifex.  If not, see <http://www.gnu.org/licenses/>.
********************************************************************************/

/**
 * @file
 *
 * @author Pasquale Claudio Africa <pasqualeclaudio.africa@polimi.it>.
 * @author Michele Bucelli <michele.bucelli@polimi.it>.
 */

#include "core/source/core_model.hpp"
#include "core/source/init.hpp"

#include "source/fluid_dynamics.hpp"

#include "source/helpers/fluid_dynamics_bcs.hpp"

#include <memory>
#include <set>
#include <string>
#include <vector>

namespace lifex
{
  /// App class for FluidDynamics.
  class AppFluidDynamics : public CoreModel
  {
  public:
    /// Constructor.
    AppFluidDynamics(const std::string &             subsection,
                     const std::vector<std::string> &boundary_names_);

    /// Override of @ref CoreModel::declare_parameters.
    virtual void
    declare_parameters(ParamHandler &params) const override;

    /// Override of @ref CoreModel::parse_parameters.
    virtual void
    parse_parameters(ParamHandler &params) override;

    /// Run the model.
    virtual void
    run() override;

  private:
    /// Setup boundary conditions.
    void
    setup_bcs();

    /// Fluid dynamics model.
    FluidDynamics fluid_dynamics;

    /// @name Boundary conditions.
    /// @{

    /// Names of boundary surfaces.
    std::vector<std::string> boundary_names;

    /// Dirichlet boundary conditions.
    std::vector<std::shared_ptr<FluidDynamicsBCs::FlowBC>> dirichlet_bcs;

    /// Womersley boundary conditions.
    std::vector<std::shared_ptr<FluidDynamicsBCs::WomersleyBC>> womersley_bcs;

    /// Neumann boundary conditions.
    std::vector<std::shared_ptr<FluidDynamicsBCs::PressureBC>> neumann_bcs;

    /// Resistance boundary conditions.
    std::vector<std::shared_ptr<FluidDynamicsBCs::ResistanceBC>> resistance_bcs;

    /// Switching BCs flags.
    std::vector<bool> switching_bcs_enabled;

    /// Current index in the switching BC sequence for each boundary.
    std::vector<unsigned int> switching_bcs_current_indices;

    /// Counter of resetting inlet bc switching (once for each heartbeat).
    std::vector<unsigned int> counter_switching_bc_reset;

    /// Flag indicating whether setup_bcs has been called.
    bool setup_bcs_called = false;

    /// @}

    /// @name Parameters read from file.
    /// @{

    /// Wall tags.
    std::vector<types::boundary_id> prm_tags_no_slip;

    /// Free-slip tags.
    std::vector<types::boundary_id> prm_tags_free_slip;

    /// Tags. For each entry of boundary_names, a set of tags is specified.
    std::vector<std::set<types::boundary_id>> prm_boundary_tags;

    /// Inlet types (Dirichlet/Neumann).
    std::vector<std::string> prm_boundary_types;

    /// Flags toggling tangential flux constraints on boundaries.
    std::vector<bool> prm_zero_tangential_flux;

    /// Pairs of tags for periodic conditions.
    std::vector<FluidDynamics::PeriodicBC> prm_periodic_bcs;

    /// Sequences for switching BCs.
    std::vector<std::vector<std::string>> prm_switching_bcs_sequence;

    /// Switch times for switching BCs.
    std::vector<std::vector<double>> prm_switching_bcs_times;

    /// Forcing term, constant in time and space.
    Vector<double> prm_forcing_term;

    /// Initial conditions type (at rest or read from file).
    std::string prm_initial_condition_type;

    /// Initial conditions file name.
    std::string prm_initial_condition_file_name;

    /// Initial conditions array name.
    std::string prm_initial_condition_array_name;

    /// @}
  };
} // namespace lifex
